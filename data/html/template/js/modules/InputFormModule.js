export default function InputFormModule() {
    const ipts = document.querySelectorAll('.form-cus input')
    const selects = document.querySelectorAll('.form-cus select')

    function check_ipt(ele) {
        if (ele.value != '') {
            ele.closest('.f-c').classList.add('active')
        }  
    }

    function focus(ele) {
        ele.closest('.f-c').classList.add('active')
    }

    function onchange(ele) {
        if (ele.files) {
            ele.closest('.f-c').classList.add('active')
            let fileName = ele.files[0].name.match(/([^\\\/]+)$/)[0];
            let label = ele.closest('.f-c').querySelector('label')
            label.innerHTML = fileName
        }
    }

    function blur(ele) {
        if (ele.value == '') {
            ele.closest('.f-c').classList.remove('active')
        } else {
            ele.closest('.f-c').classList.add('active')
        }
    }

    if (ipts.length > 0) {
        ipts.forEach((ele) => {
            if (ele.closest('.f-c')) {
                check_ipt(ele)
            }

            ele.onclick = () => {
                if (ele.closest('.f-c')) {
                    focus(ele)
                }
            }

            ele.focus = () => {
                if (ele.closest('.f-c')) {
                    focus(ele)
                }
            }

            ele.onchange = () => {
                onchange(ele)
            }

            ele.onblur = () => {
                if (ele.closest('.f-c')) {
                    blur(ele)
                }
            }
        })
    }

    if (selects.length > 0) {
        selects.forEach((ele) => {
            if (ele.closest('.f-c')) {
                check_ipt(ele)
            }

            ele.focus = () => {
                if (ele.closest('.f-c')) {
                    focus(ele)
                }
            }

            ele.onblur = () => {
                if (ele.closest('.f-c')) {
                    blur(ele)
                }
            }
        })
    }

    // Custom input wpcf7
    const wpcf7Items = document.querySelectorAll('.wpcf7-form-control-wrap .wpcf7-list-item input')

    if (wpcf7Items.length > 0) {
        wpcf7Items.forEach((ele) => {
            $("<span class='ipt-radio-cus'></span>").insertAfter(ele)
        })
    }

    // SEARCH FORM

    const openSearchForm = document.querySelectorAll('.hd-search-js')
    const boxSearchForm = document.querySelector('.hd-search-box-js')

    const searchValue = document.querySelector('.searchValue')
    const searchRemove = document.querySelector('.searchRemove')
    const hdSearch = document.querySelector('.hd-search-form-js')

    if (openSearchForm.length > 0) {
        openSearchForm.forEach((ele) => {
            ele.onclick = (e) => {
                e.stopPropagation()
                boxSearchForm.classList.toggle('active')
            }
        })

        boxSearchForm.onclick = (e) => {
            e.stopPropagation()
        }
    }

    if (searchValue) {
        searchValue.onkeyup = () => {
            checkValue()
        }

        searchValue.onclick = () => {
            checkValue()
        }

        function checkValue() {
            if (searchValue.value != '') {
                hdSearch.classList.add('hasValue')
            } else {
                hdSearch.classList.remove('hasValue')
            }
        }

        // searchValue.onclick = (e) => {
        //     e.stopPropagation()

        //     if (searchHistory) {
        //         searchHistory.classList.add('active')
        //     }
        // }

        searchRemove.onclick = () => {
            searchValue.value = '';
            searchValue.focus();
            hdSearch.classList.remove('hasValue')
        }
    }

    const body = document.querySelector('body')
    if (boxSearchForm) {
        body.onclick = () => {
            boxSearchForm.classList.remove('active')
        }
    }

    // show pass
    const iconEyes = document.querySelectorAll(".password-eye-js");
    if (iconEyes) {
        iconEyes.forEach((iconEye) => {
            iconEye.onmousedown = (e) => {
                e.stopPropagation()
                showPass(iconEye)
            };

            iconEye.onmouseup = (e) => {
                e.stopPropagation()
                hidePass(iconEye)
            };

            iconEye.ontouchstart = (e) => {
                e.stopPropagation()
                showPass(iconEye)
            };

            iconEye.ontouchend = (e) => {
                e.stopPropagation()
                hidePass(iconEye)
            };
        });

        function showPass(ele) {
            const inputTypeShow = ele
                .closest(".f-c")
                .querySelector("input[type=password]");
            inputTypeShow.setAttribute("type", "text");

            const iconImg = ele.querySelector('.fal-2')
            const iconFont = ele.querySelector('.fal-1')

            iconImg.style.opacity = 0.7
            iconFont.style.opacity = 0
        }

        function hidePass(ele) {
            const inputTypeHide = ele
                .closest(".f-c")
                .querySelector("input[type=text]");
            inputTypeHide.setAttribute("type", "password");

            const iconImg = ele.querySelector('.fal-2')
            const iconFont = ele.querySelector('.fal-1')

            iconImg.style.opacity = 0
            iconFont.style.opacity = 0.7
        }
    }

    // birthday
    const birthday = document.querySelectorAll('.label-birthday-js')
    let date = new Date()
    if (birthday.length > 0) {
        birthday.forEach((ele) => {
            ele.onclick = () => {
                let parent = ele.closest('.f-c')
                let birthday

                if (parent) {
                    birthday = parent.querySelector('input[type="date"]')
                }

                if (birthday.value == '') {
                    ele.classList.add('hide')

                    birthday.value = date.toISOString().substr(0, 10);

                    birthday.onblur = () => {
                        if (birthday.value == '') {
                            ele.classList.remove('hide')
                        } else {
                            ele.classList.add('hide')
                        }
                    }
                }
            }
        })
    }
}