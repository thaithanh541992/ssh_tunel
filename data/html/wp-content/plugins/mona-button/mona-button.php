<?php

/**
 * Plugin Name:Mona button plugin
 * Plugin URI: #
 * Description: [mona_button text="tên hiển thị" code="code hiển thị" time="thời gian, khổng để biến time thì default là 10s"]
 * Author: Mona Media
 * Author URI: https://mona.media
 * Text Domain: monamedia
 * Domain Path: monamedia
 * Version: 1.3.1
 * License: GPLv2+
 */
if (!defined('ABSPATH')) {
exit;
}
define('MONA_BUTTON_CORE', plugin_dir_path(__FILE__));
define('MONA_BUTTON_CORE_URL', plugins_url('/', __FILE__));
function mona_plugin_button($att){
    $atts = shortcode_atts(array('text' => 'Nhận mật mã',
        'code' => 'chưa có mã',
        'time' => 10
    ), $att, 'mona_button');
    ob_start();
    ?>
    <a href="javascript:;" class="open-pop-js">Điều khoản</a>
    <div class="modal-pop modal-pop-js">
        <div class="modal-pop-wrap">
            <div class="modal-pop-close modal-pop-close-js">
                <i class="fal fa-times"></i>
            </div>
            <div class="modal-pop-tt">Điều khoản dịch vụ – Chính sách bảo mật</div>
            <div class="modal-pop-desc">Thông tin của khách hàng được cam kết bảo mật tuyệt đối theo chính sách bảo vệ thông tin cá nhân . Việc thu thập và sử dụng thông tin của mỗi khách hàng chỉ được thực hiện khi có sự đồng ý của khách hàng đó...</div>
            <div class="modal-pop-btn">
                <svg class="ani-preview-web" width="50" height="72" viewBox="0 0 37 72" fill="none" xmlns="http://www.w3.org/2000/svg">
                    <path d="M17.5676 2C8.65809 17.6891 -9.28585 43.6671 12.0171 57.3937C17.2357 60.7563 22.6379 60.3913 28.5862 60.3913C28.8431 60.3913 34.7931 59.4376 34.1366 58.913C32.3785 57.5084 22.1462 49.072 28.8329 54.4783C29.5403 55.0502 35 58.1238 35 59.0773C35 61.6751 25.4339 68.0566 23.488 70" stroke="#F41E92" stroke-width="3" stroke-linecap="round" class="preview-web"></path>
                </svg>
                <a href="https://mona.media/dieu-khoan-dich-vu-cua-mona-media/" target="_blank" class="modal-pop-btn-item mona-button-open-link-js tooltipcp-relative">
                    Xem thêm
                    <span class="tooltipcp tooltipcp-js" style="display:none">Warning! Quá trình lấy mã bảo mật <br> sẽ dừng lại nếu chuyển hướng!!!</span>
                </a>
                <span class="modal-pop-btn-item get-code mona-count-down-btn-js" data-code="<?php echo base64_encode($atts['code']) ?>" data-time="<?php echo $atts['time'] ?>"><span class="code"> <?php echo $atts['text'] ?></span><span class="hidden-label">Sao chép</span></span>
            </div>
        </div>
    </div>
    <?php
    // $output= '<div class="mona-btn-shortcode"> <a href="javascript:;" class="mona-count-down-btn-js mona-count-down-btn" data-code="'.base64_encode($atts['code']).'" data-time="'.$atts['time']. '">' . $atts['text'] . '</a></div>';
    return ob_get_clean();
}
add_shortcode('mona_button','mona_plugin_button');
function mona_buttonload_script()
{
    wp_enqueue_script('jquery');
    wp_enqueue_style('mona-button-front', MONA_BUTTON_CORE_URL . 'css/style.css', array(), '133018052023' );
    wp_enqueue_script('mona-button-front', MONA_BUTTON_CORE_URL . 'js/front.js', array(), '133018052023', true );

 
}
add_action('wp_enqueue_scripts', 'mona_buttonload_script');