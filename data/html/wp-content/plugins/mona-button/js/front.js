jQuery(document).ready(function ($) {
    $('.mona-count-down-btn-js').on('click', function (e) {
        e.preventDefault();
        var $this = $(this);
        var duration = $this.attr('data-time');
        var $code = $this.attr('data-code');
        if ( $this.hasClass('done') ) {
            // copy
            var temp = $("<input>");
            $("body").append(temp);
            temp.val(atob($code)).select();
            document.execCommand("copy");
            temp.remove();
            
            // change text
            $this.find('.code').html('Đã sao chép');
            $this.find('.hidden-label').html('Đã sao chép');

            //change text to defaut 
            setTimeout( () => {
                $this.find('.code').html(atob($code));
                // $this.find('.hidden-label').html('Nhấn vào để sao chép');

                $this.removeClass('done')
                $this.addClass('copied')
            }, 2000)
        } else if (!$this.hasClass('run')) {
            $this.addClass('run'); 
                    
            var seconds = parseInt(duration);
            setInterval(function () {
                seconds--;
                if (seconds >= 0) {
                    $this.find('.code').html((seconds));
                }
                if (seconds === 0) {
                    $this.find('.code').html(atob($code));
                    $this.addClass('done');
                }
            }, 1000);
        }
    });

    $('.mona-button-open-link-js').click(function(e) {
        e.preventDefault()

        let href = $(e.target).attr('href');

        if ( href && $('.mona-count-down-btn-js').hasClass('copied') ) {
            window.open(href, "_blank").focus();
        } else {
            $(this).find('.tooltipcp-js').fadeIn();
            setTimeout( ()=> {
                $(this).find('.tooltipcp-js').fadeOut();
            }, 2000 )
        }
    })
});