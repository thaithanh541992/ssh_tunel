<?php
/**
 * Plugin Name: Mona thong bao APP
 * Plugin URI: 
 * Description: A brief description of the Plugin.
 * Version: The Plugin's Version Number, e.g.: 1.0
 * Author: Name Of The Plugin Author
 * Author URI: 
 * License: A "Slug" license name e.g. GPL2
 */
define('MONA_BUTTON_URL', plugin_dir_url(__FILE__));
function _mona_insearch_footer(){
    
    ?>
    
    <style>

        .fixed__box {
            position: fixed;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            transition: all 0.5s ease-in-out;
            opacity: 0;
            z-index: -1;
            pointer-events: none;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
        }
        
        .fixed__box.show {
            opacity: 1;
            z-index: 9999;
            pointer-events: initial;
            -webkit-user-select: initial;
            -moz-user-select: initial;
            -ms-user-select: initial;
            user-select: initial;
        }
        
        .fixed__box.show .box__content {
            transform: translate(-50%, -50%) scale(1) skewX(0);
            opacity: 1;
        }
        
        .fixed__box p {
            margin: 0;
            line-height: 1.5;
        }
        
        .fixed__box img {
            max-width: 100%;
            height: auto;
        }
        
        .fixed__box a {
            text-decoration: none;
        }
        
        .fixed__box .box-overlay {
            position: absolute;
            top: 0;
            left: 0;
            bottom: 0;
            right: 0;
            z-index: 1001;
            background: rgba(0, 0, 0, 0.7);
        }
        
        .fixed__box .box__container {
            position: relative;
            width: 100%;
            height: 100%;
        }
        
        .fixed__box .box__content {
            position: absolute;
            top: 50%;
            left: 50%;
            transform: translate(-50%, -50%) scale(0.5) skewX(0);
            transform-origin: 50% 50%;
            opacity: 0.5;
            background: #fff;
            border-radius: 16px;
            box-shadow: 0px 4px 16px 0px rgba(255, 255, 255, 0.15);
            z-index: 1002;
            transition: all 0.8s ease-in-out;
            width: 90%;
            max-width: 500px;
        }
        
        .fixed__box .box__header {
            padding: 25px 25px 0;
        }
        
        .fixed__box .box__header .image__app {
            display: block;
            width: 300px;
            margin: -150px auto 0;
        }
        
        .fixed__box .box__header .close__box {
            position: absolute;
            top: -50px;
            right: -50px;
            background: #08c;
            display: inline-block;
            width: 35px;
            height: 35px;
            line-height: 35px;
            border-radius: 50%;
            text-align: center;
            color: #fff;
            font-size: 16px;
            cursor: pointer;
            background: linear-gradient(99.14164deg, #0088cc -1.01042%, #5bbef0 83.57292%);
            transition: transform .5s ease;
        }
        
        .fixed__box .box__header .close__box:hover {
            transform: rotate(180deg);
        }
        
        .fixed__box .box__header .close__box:after {
            content: 'X';
        }
        
        .fixed__box .box__body {
            padding: 15px 25px 25px;
            text-align: center;
        }
        
        .fixed__box .login-url {
            margin-top: 15px;
            margin-bottom: 15px;
        }
        
        .fixed__box .login-url .fa {
            width: 20px;
        }
        
        .fixed__box .login-url .fa.fa-mobile {
            font-size: 24px;
            vertical-align: top;
        }
        
        .fixed__box .login-url a {
            text-decoration: none;
            color: #08c;
        }
        
        .fixed__box .download-app {
            display: flex;
            align-items: center;
            margin-top: 15px;
            justify-content: center;
        }
        
        .fixed__box .download-app .app-box {
            margin-left: 5px;
        }
        
        .fixed__box .download-app .app-box>a {
            display: inline-block;
            vertical-align: middle;
            position: relative;
            overflow: hidden;
            transition: all 0.2s ease-in-out;
            margin: 5px;
        }
        
        .fixed__box .download-app .app-box>a:after {
            content: '';
            width: 20px;
            height: 150%;
            position: absolute;
            top: 0;
            left: -50px;
            background: rgba(255, 255, 255, 0.25);
            transform: rotate(20deg);
            transition: all 0.5s ease-in-out;
        }
        
        .fixed__box .download-app .app-box>a:hover {
            box-shadow: 0px 4px 16px 0px rgba(2, 18, 24, 0.31);
        }
        
        .fixed__box .download-app .app-box>a:hover:after {
            left: 100%;
        }
        
        .fixed__box .download-app .app-box img {
            vertical-align: middle;
        }
        
        .fixed__box .mona-color {
            color: #08c;
        }
        
        .fixed__box .small-note {
            font-size: initial;
            line-height: 1.5;
            font-size: 14px;
        }
        
        @media screen and (max-width: 600px) {
            .fixed__box .box__header {
                padding: 15px 15px 0;
            }
            .fixed__box .box__header .image__app {
                width: 200px;
                margin-top: -110px;
                margin-bottom: 10px;
            }
            .fixed__box .box__header .close__box {
                right: 15px;
                top: 15px;
            }
            .fixed__box .box__body {
                padding: 0 15px 15px;
            }
            .fixed__box .download-app {
                display: block;
            }
        }
        
        .fixed-toggle {
            position: fixed;
            right: 0;
            top: 55%;
            transform: translateY(-50%);
            cursor: pointer;
            z-index: 100;
            border-top-left-radius: 7px;
            border-bottom-left-radius: 7px;
            /* border: 1px solid #e1e1e1;
            border-right: 0; */
        }
        
        .fixed-toggle p {
            margin: 0;
        }
        
        .fixed-toggle img {
            max-width: 100%;
            height: auto;
        }
        
        .fixed-toggle .logo-mona {
            padding: 0px;
            width: 50px;
            height: 50px;
            display: block;
            margin-right: 100%;
            background: #f7f7f7;
            border-top-left-radius: 5px;
            border-bottom-left-radius: 5px;
            transition: .3s all;
            -webkit-transition: .3s all;
            position: relative;
            z-index: 2;
        }
        .fixed-toggle:hover .logo-mona{
            background-color: #e1e1e1;
            border-top-left-radius: 0px;
            border-bottom-left-radius: 0px;
        } 
        
        .fixed-toggle .logo-mona img{
            display: block;
            width: 100%;
            height: 100%;
            transition: .3s all;
            -webkit-transition: .3s all;
        }
        .fixed-toggle:hover .logo-mona img{
            fill: #0086da
        }
        .fixed-toggle .toggle-icon {
            text-align: center;
            
            color: #08c;
            transition: all 0.2s ease-in-out;
            
            display: block;
            position: relative;
           
        }
        
        .fixed-toggle .viewmore {
            font-size: 12px;
            line-height: 1.5;
        }
        .fixed-toggle .box-text .title-short{
            display: block;
            position: absolute;
            right: 100%;
            line-height: 30px;
            padding: 10px;
            top: 0;
            border-bottom-left-radius: 5px;
            border-top-left-radius: 5px;
            background-color: #118df0;
            color: #fff;
            white-space: nowrap;
            transition: .3s all;
            -webkit-transition: .3s all;
            opacity: 1;
            transform: translate(100%,0);
            -webkit-transform: translate(100%,0);

        }
        .fixed-toggle .box-text .title,
        .fixed-toggle .box-text .viewmore {
            display: none;
        }
        
        .fixed-toggle .box-text .title-short {
            /* text-transform: uppercase;
            font-weight: 600;
            font-size: 12px;
            max-width: 75px;
            line-height: 1.3;
            text-align: center; */
        }
        
        .fixed-toggle:hover .box-text .title,
        .fixed-toggle:hover .box-text .viewmore {
            display: block;
        }
        
        .fixed-toggle:hover .logo-mona {
            margin-right: auto;
        }
        .fixed-toggle:hover .box-text .title-short{
            opacity: 1;
            transform: translate(0%,0);
            -webkit-transform: translate(0%,0);
        }
        
        @media screen and (max-width: 600px) {
            
            .fixed-toggle .logo-mona{
    
                display: block;
            }
            .fixed-toggle .logo-mona img{
                display: block;
            }

            .fixed-toggle .toggle-icon > .box-text{
                display: none;
            }
            .left-side-fix{
                display: none;
            }
        }
        
    </style>


<div>
    <div class="toggle__box fixed-toggle" id="js-fixed-toggle">
        <div class="toggle-icon">
            <span class="logo-mona"><img src="<?php echo MONA_BUTTON_URL; ?>images/icon-1024.png" alt="PMS"></span>
            <div class="box-text">
                <p class="title-short">Theo dõi tiến độ dự án</p>
                <!-- <p class="title">Bằng Mona Project Management System - PMS</p>
                <p class="viewmore">Click để xem</p> -->
            </div>

        </div>

    </div>
    <div class="fixed__box" id="js-showcase-pm">
        <div class="box__container">
            <div class="box-overlay toggle__box"></div>
            <div class="box__content">
                <div class="box__header">
                    <span class="image__app"><img src="<?php echo MONA_BUTTON_URL; ?>images/mona-app.png" alt="app-image"></span>
                    <span class="toggle__box close__box"></span>
                </div>
                <div class="box__body">
                    <p>Quý khách vui lòng đăng nhập vào hệ thống quản lý dự án để theo dõi tiến độ.</p>
                    <div class="login-url">
                        <p><i class="fa fa-globe"></i> Website:<a href="https://quanly.mona.media/" class="login-link"> quanly.mona.media</a></p>

                        <div class="download-app">
                            <i class="fa fa-mobile"></i> Mobile:
                            <div class="app-box">
                                <a href="https://quanly.mona.media/app" class="app-icon ios" target="_blank" rel='noopener'><img src="<?php echo MONA_BUTTON_URL; ?>images/appstore.svg" alt="ios"></a>
                                <a href="https://quanly.mona.media/app" class="app-icon android" target="_blank" rel='noopener'><img src="<?php echo MONA_BUTTON_URL; ?>images/ggplay.svg" alt="android"></a>
                            </div>
                        </div>
                    </div>
                    <p class="small-note">Tài khoản đã được <a href="https://mona.media" class="mona-color"><strong>Mona Media</strong></a> cung cấp cho quý khách qua hệ thống SMS tự động. Nếu cần hỗ trợ thêm xin vui lòng gọi <strong>1900 636 648</strong></p>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    (function() {
        'use strict';
        let fixedBox = document.getElementById('js-showcase-pm');
        let toggleBox = document.querySelectorAll('.toggle__box');
        let fixedToggle = document.getElementById('js-fixed-toggle');
        const toggleFixedBox = () => {
            fixedBox.classList.toggle('show');
        }
        if (toggleBox.length > 0) {
            [...toggleBox].map(x => x.addEventListener('click', toggleFixedBox));
        }
    }());
</script>
    
    <?php
}
add_action('wp_footer','_mona_insearch_footer');
