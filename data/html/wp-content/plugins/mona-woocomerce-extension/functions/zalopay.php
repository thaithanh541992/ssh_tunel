<?php

function mona_zalo_payment()
{
    if (!class_exists('WC_Payment_Gateway'))
        return;

    class mona_zalo_payment_gateway extends WC_Payment_Gateway
    {

        public static $log_enabled = false;
        private $test_url_create = 'https://sb-openapi.zalopay.vn/v2/create';
        private $live_url_create = 'https://openapi.zalopay.vn/v2/create';
        private $url = false;
        private $unit = 1;
        public static $log = false;

        function __construct()
        {
            $this->id = 'mona_zalo_payment_gateway';
            $this->has_fields = false;
            $this->order_button_text = __('Thanh toán qua Zalopay', 'monamedia');
            $this->method_title = __('Thanh toán Zalopay', 'monamedia');
            $this->method_description = __('', 'monamedia');
            $this->supports = array(
                'products',
            );
            // Define user set variables.
            $this->title = $this->get_option('title');
            $this->description = $this->get_option('description');
            //$this->icon = $this->settings['icon'];

            // Load the settings.
            $this->init_form_fields();
            $this->init_settings();

            // Process the admin options
            add_action('woocommerce_update_options_payment_gateways_' . $this->id, array(
                $this,
                'process_admin_options'
            ));

            add_action('woocommerce_thankyou_' . $this->id, array($this, 'handle_return_url'));

            add_action('woocommerce_api_' . strtolower(__CLASS__), array($this, 'handle_return_url'));
        }

        public static function log($message)
        {
            $log = new WC_Logger();
            $log->add('Zalopay', $message);
        }

        public function init_form_fields()
        {
            // Admin fields
            $this->form_fields = array(
                # thông tin chung
                'enabled' => array(
                    'title' => __('Activate', 'woocommerce'),
                    'type' => 'checkbox',
                    'label' => __('', 'woocommerce'),
                    'default' => 'no',
                    'value' => 'yes'
                ),
                'testMode' => array(
                    'title' => __('Live', 'woocommerce'),
                    'type' => 'checkbox',
                    'label' => __('', 'woocommerce'),
                    'default' => 'no'
                ),
                'title' => array(
                    'title' => __('Name', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Tên phương thức thanh toán ( khi khách hàng chọn phương thức thanh toán )', 'woocommerce'),
                    'default' => __('ZaloPay', 'woocommerce')
                ),
                'icon' => array(
                    'title' => __('Icon', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Icon phương thức thanh toán', 'woocommerce'),
                    'default' => __('https://sbmc.zalopay.vn/assets/svg/logo-zalopay-doanhnghiep.svg', 'woocommerce')
                ),
                'description' => array(
                    'title' => __('Mô tả', 'woocommerce'),
                    'type' => 'textarea',
                    'description' => __('Mô tả phương thức thanh toán.', 'woocommerce'),
                    // 'default' => __('Click place order and you will be directed to the Ngan Luong website in order to make payment', 'woocommerce')
                ),
                # thông tin tích hợp
                'app_id' => array(
                    'title' => __('App Id', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('Định danh cho ứng dụng đã được cấp khi đăng ký ứng dụng với ZaloPay.', 'woocommerce'),
                ),
                'mac_key' => array(
                    'title' => __('Key 1', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('', 'woocommerce'),
                ),
                'callback_key' => array(
                    'title' => __('Key 2', 'woocommerce'),
                    'type' => 'text',
                    'description' => __('', 'woocommerce'),
                ),

            );
        }

        /**
         * Process the payment and return the result.
         * @param  int $order_id
         * @return array
         */
        public function process_payment($order_id)
        {

            $order = wc_get_order($order_id);

            $checkoutUrl = $this->generate_zalo_url($order_id);

            if ($checkoutUrl) {
                return array(
                    'result' => 'success',
                    'redirect' => $checkoutUrl
                );
            }
            $this->log($checkoutUrl);
            return array(
                'result' => 'error',
                'messenger' => ''
            );
        }

        function generate_zalo_url($order_id)
        {
            $order = wc_get_order($order_id);
            $total = $order->get_total();
            $appUser =  ($order->get_billing_email() == '' ? $order->get_billing_first_name() : $order->get_billing_email());
            $order_items = $this->get_items($order->get_items()) ;
            $address = $order->get_formatted_billing_address();
            $emailUser = $order->get_billing_email();
            $phoneUser = $order->get_billing_phone();
            $appTime = getTimestamp() ;
            $returnUrl = wc()->api_request_url($this->id);
            // var_dump( $returnUrl);exit;
            $appId = $this->settings['app_id'];
            $macKey = $this->settings['mac_key'];
            $callbackKey = $this->settings['callback_key'];

            $title = "Thanh toán đơn hàng #" . $order_id . " từ Thiết bị ô tô qua Zalopay";
            $orderInfo = "Thanh toán đơn hàng từ Thiết bị ô tô qua Zalopay";
            $amount = $total;
            $extraData = "merchantName=thietbioto";
            $requestId = time() . "";
            $requestType = "captureZaloPay";

            $appTransId = current_time('ymd') . '_' . $order_id . '_' . rand();

            $code = [
                'app_id' =>(int) $appId,
                'app_trans_id' => $appTransId,
                'app_user' => $appUser,
                'amount' => (float) $amount,
                'app_time' => $appTime,
                'embed_data' => json_encode(["bankgroup" => "" , 'callback_url' => $returnUrl ]),
                'item' =>  $order_items,
            ];
            $hmacInput = implode('|', array_values($code) );

            $mac = hash_hmac("sha256" , $hmacInput, $macKey);
            $codeMerge = array_merge( $code ,
                [
                    'order_type' => 'GOODS',
                    'title' => $title,
                    'description' => $orderInfo,
                    'callback_url' => $returnUrl,
                    'currency' => 'VND',
                    'bank_code' => '',
                    'phone' => $phoneUser,
                    'email' => $emailUser,
                    'address' => $address,
                ]
            );
            $codeMerge['mac'] = $mac;
            $this->url = $this->live_url_create;
            if( $this->settings['testMode'] == 'no') {
                $this->url = $this->test_url_create;
            }

            $result =(array) $this->execPostRequest($this->url, $codeMerge);

            return $result['order_url'];
        }

        public function handle_return_url()
        {
            $check = $this->check_handle($_REQUEST);
            wp_redirect($check);
        }
        protected function check_handle($data_response)
        {

            if (isset($data_response['apptransid']) && !empty($data_response['apptransid'])) {
                $exp = explode('_', $data_response['apptransid']);
                $order = wc_get_order($exp[1]);

                $verifyRedirect =  $this->verifyRedirect( $data_response);

                if (!$order or !$verifyRedirect ) {
                    return $this->get_return_url();
                }

                if ($data_response['status'] == 1) {
                    $order->update_status('completed', 'Thanh toán qua ZaloPay thành công');
                } else {
                    $order->update_status('failed', "Thanh toán qua ZaloPay thất bại");
                }
                wc()->cart->empty_cart();
                return $this->get_return_url($order);

            }
            return $this->get_return_url();
        }
        protected function get_error_mess($code)
        {
            $codes = [
                '1' => __('Thiếu thông tin đối tác', 'monamedia'),
                '2' => __('OrderId sai định dạng', 'monamedia'),
                '4' => __('Số tiền thanh toán không hợp lệ.', 'monamedia'),
                '5' => __('Chữ ký không hợp lệ', 'monamedia'),
                '6' => __('Đơn hàng đã tồn tại.', 'monamedia'),
                '7' => __('Giao dịch đang chờ xử lý', 'monamedia'),
                '12' => __('Yêu cầu đã tồn tại', 'monamedia'),
                '14' => __('Đối tác chưa được kích hoạt', 'monamedia'),
                '29' => __('Hệ thống đang bảo trì', 'monamedia'),
                '32' => __('Giao dịch đã được thanh toán', 'monamedia'),
                '33' => __('Giao dịch không thể refund.', 'monamedia'),
                '34' => __('Giao dịch hoàn tiền đã được xử lý', 'monamedia'),
                '36' => __('Giao dịch đã hết hạn', 'monamedia'),
                '37' => __('Tài khoản hết hạn mức giao dịch trong ngày', 'monamedia'),
                '38' => __('Tài khoản khách hàng không đủ tiền', 'monamedia'),
                '42' => __('Yêu cầu không đúng định dạng', 'monamedia'),
                '44' => __('Dịch vụ không hỗ trợ yêu cầu của bạn', 'monamedia'),
                '49' => __('Khách hàng huỷ giao dịch', 'monamedia'),
                '58' => __('Giao dịch không tồn tại', 'monamedia'),
                '59' => __('Yêu cầu không hợp lệ', 'monamedia'),
                '63' => __('Thanh toán bằng nguồn ngân hàng không thành công', 'monamedia'),
                '76' => __('Thiếu field requestType trong HTTP Request Body', 'monamedia'),
                '80' => __('Xác thực khách hàng không thành công', 'monamedia'),
                '99' => __('Lỗi không xác định (Lỗi hệ thống)', 'monamedia'),
                '9043' => __('Khách hàng chưa liên kết tài khoản ngân hàng', 'monamedia'),
            ];
            return @$codes[$code];
        }
        static function execPostRequest($url, $params)
        {

            $context = stream_context_create([
                "http" => [
                    "header" => "Content-type: application/x-www-form-urlencoded\r\n",
                    "method" => "POST",
                    "content" => http_build_query($params)
                ]
            ]);

            return json_decode(file_get_contents($url, false, $context));
        }
        static function get_items( $items ) {
            $data =[];
            if( is_array( $items ) ) {
                foreach( $items as $key => $item ) {
                    $data[$key] = [
                        'itemid' => $item->get_product_id(),
                        'itename' => $item->get_name(),
                        'itemprice' => $item->get_total(),
                        'itemquantity' => $item->get_quantity(),
                    ] ;
                }
            }
            $data = array_values($data);
            return json_encode($data) ;
        }
        function redirect(Array $params , string $key2 )
        {
            $key2 = $this->settings['callback_key'];
            $string = $params['appid']."|".$params['apptransid']."|".$params['pmcid']."|".$params['bankcode']
            ."|".$params['amount']."|".$params['discountamount']."|".$params["status"];

            return hash_hmac("sha256", $string, $key2);
        }
        function verifyRedirect(Array $data)
        {
            $reqChecksum = $data["checksum"];
            $key2 =$this->settings['callback_key'] ;
            $checksum = $this->redirect($data ,$key2);

            return $reqChecksum === $checksum;
        }
    }
    function getTimestamp() {
        return round(microtime(true) * 1000);
      }
    function mona_payment_add_zalo_gateway($methods)
    {
        $methods[] = 'mona_zalo_payment_gateway';
        return $methods;
    }


    add_filter('woocommerce_payment_gateways', 'mona_payment_add_zalo_gateway');
}

// $params = [
//     'app_id' => 1 ,
//     'app_user' => "pmqc",
//     'app_trans_id' => "160405095135-57032837085b5",
//     'app_time' => "1459823610957",
//     'amount' => 500000,
//     'order_type' => 'GOODS' ,
//     'title' => "string",
//     'description' => "Mua kim nguyên bảo cho game VLTK",
//     'redirect_url' => "string",
//     // 'device_info' => "string",
//     'item' => '[{"itemid":"knb","itemname":"kim nguyen bao","itemquantity":10,"itemprice":50000}]',
//     'embed_data' => '{"promotioninfo":"{\"campaigncode\":\"yeah\"}","merchantinfo":"embeddata123"}',
//     'mac' => '28ecee91f4b32aa1306812f5d74c4ed1f7cbce7b4f2848cf06f23933ae8027e0' ,
//     'currency' => 'VND',
//     'bank_code' => 'zalopayapp',
//     'phone' => 'string',
//     'email' => 'email' ,
//     'address' =>'address',
// ];
