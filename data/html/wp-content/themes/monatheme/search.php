<?php
/**
 * The template for displaying search.
 *
 * @package Monamedia
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
?>

<main class="main">
	<!-- Cấp 2 trang 2 -->
	<section class="sec-blogt sec-blogpc2">
		<div class="blogt blogpc2">
			<div class="blogt-wrap">
				<div class="blogt-dot"></div>
				<div class="container">
					<div class="blogpc2-wrap">
						<div class="blogpc2-py">
							<form id="frmSearching" action="<?php echo home_url('/') ?>">
								<div class="blogpc2-form">
									<div class="blogpc2-control">
										<input placeholder="<?php echo __('Tìm kiếm', 'monamedia'); ?>" name="s" value="<?php echo esc_attr( @$_GET['s'] ); ?>" type="text" class="blogpc2-input">
										<span class="icon">
											<img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-sreach.png"
												alt="">
										</span>
									</div>
								</div>
								<?php 
								$mona_taxonomy_outstanding_post_tags = get_terms(
									[
										'taxonomy' => 'post_tag',
										'hide_empty' => true,
									]
								);
								if( !empty( $mona_taxonomy_outstanding_post_tags ) ){
									$getPostTags = !empty( $_GET['post_tags'] ) ? $_GET['post_tags'] : array();
								?>
								<div class="blogpc2-tag">
									<div class="blogpc2-tag-list">
										<?php foreach ($mona_taxonomy_outstanding_post_tags as $key => $tag) { ?>
										<label class="postTagJS">
											<input type="checkbox" name="post_tags[]" value="<?php echo $tag->slug; ?>" <?php checked( in_array( $tag->slug , $getPostTags ), 1, 'checked' ); ?>/>
											<span class="blogpc2-tag-item postTagName"><?php echo $tag->name; ?></span>
										</label>
										<?php } ?>
									</div>
								</div>
								<?php } ?>
							</form>
							<?php if( isset( $_GET['s'] ) || isset( $_GET['post_tags'] ) ){ ?>
							<div class="blogpc2-result">
								<p class="t-lager">
									<?php echo __('Kết quả tìm kiếm: ', 'monamedia'); ?><?php echo get_search_query(); ?>
								</p>
							</div>
							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php
	$post_type = 'post';
	$posts_per_page = 12;
	$paged = max( 1, get_query_var('paged') );
	$offset = ( $paged - 1 ) * $posts_per_page;
	$argsPost = [
		'post_type' => $post_type,
		'post_status' => 'publish',
		'posts_per_page' => $posts_per_page,
		'paged' => $paged,
		'offset' => $offset,
		'meta_query' => [
			'relation' => 'AND',
		],
		'tax_query' => [
			'relation'=>'AND',
		]
	];

	if( isset( $_GET['s'] ) && !empty( $_GET['s'] )  ){
		$argsPost['s'] = get_search_query();
	}
	if( isset( $_GET['post_tags'] ) && !empty( $_GET['post_tags'] ) ){
		$argsPost['tax_query'][] = [
			'taxonomy' => 'post_tag',
			'field' => 'slug',
			'terms' => (array)@$_GET['post_tags']
		];
	}
	
	$postsMONA = new WP_Query( $argsPost );
	if( $postsMONA->have_posts() ){ ?>
	<section class="sec-blogr sec-bloghc2 sec-blogpc2 fix">
		<div class="container">
			<div class="blogr">
				<div class="blogr-wrap">
					<div class="blogr-flex">
						<div class="blogr-left override">
							<div class="blogr-list">
								<?php 
								while( $postsMONA->have_posts() ){
									$postsMONA->the_post();
									?>
								<div class="blogr-item col-3">
									<?php 
									/**
									 * GET TEMPLATE PART
									 * partials
									 * blog box
									 */
									$slug = '/partials/loop/box';
									$name = 'blog';
									echo get_template_part($slug, $name);
									?>
								</div>
								<?php } wp_reset_query(); ?>
							</div>
							<?php mona_pagination_links( $postsMONA ); ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php }else{ ?>
	<section class="sec-blogr sec-bloghc2 sec-blogpc2 fix">
		<div class="container">
			<div class="blogr">
				<div class="blogr-wrap">
					<div class="blogr-flex">
						<div class="blogr-left">

							<div class="mona-mess-empty">
								<p><?php echo __( 'Không tìm thấy bài viết phù hợp với từ khoá tìm kiếm của bạn', 'monamedia' ) ?></p>
							</div>

						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php } ?>

	<?php
    /**
     * GET TEMPLATE PART
     * contact section
     *  */ 
    $slug = '/partials/global/contact';
    $name = '';
    echo get_template_part($slug, $name);
    ?>
</main>

<?php get_footer();