<footer class="ft">
    <div class="container">
        <div class="ft-row d-flex f-ctn">
            <div class="ft-col col col-3">
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer_monamedia')) : ?>
                <?php endif; ?>
            </div>
            <div class="ft-col col col-3">
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer_monamedia_products')) : ?>
                <?php endif; ?>
            </div>
            <div class="ft-col col col-3">
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer_monamedia_customer_services')) : ?>
                <?php endif; ?>
            </div>
            <div class="ft-col col col-3">
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer_monamedia_development')) : ?>
                <?php endif; ?>
            </div>
            <div class="ft-col col col-6 solution-col">
                <div class="ft-row d-flex">
                    <div class="ft-col col col-6">
                        <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer_monamedia_solution_1')) : ?>
                        <?php endif; ?>
                    </div>
                    <div class="ft-col col col-6">
                        <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer_monamedia_solution_2')) : ?>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
            <div class="ft-col col col-3 contact-col">
                <div class="ft-label">Liên hệ</div>
                <div class="ft-menu">
                    <ul class="ft-menu-list d-flex">
                        <div class="ft-menu-list-col">
                            <div class="hd-menu-info">
                                <div class="hd-menu-info-f d-flex f-start f-nowrap">
                                    <div class="icon">
                                        <img src="<?php echo MONA_SITE_URL; ?>/template/assets/images/footer-icon-mail-host.svg" alt="">
                                    </div>
                                    <div class="d-flex a-start">
                                        <span class="label">Email</span>
                                        <span class="fw-600 hl-sw-pri">
                                            <a href="mailto:info@mona-media.com"
                                                class="link-h">info@mona-media.com</a>
                                        </span>
                                    </div>
                                </div>
                                <div class="hd-menu-info-f d-flex f-start f-nowrap">
                                    <div class="icon">
                                        <img src="<?php echo MONA_SITE_URL; ?>/template/assets/images/footer-icon-address-host.svg" alt="">
                                    </div>
                                    <div class="d-flex a-start">
                                        <span class="label">Địa chỉ</span>
                                        <span class="fw-600 hl-host-pri">1073/23 Cách Mạng Tháng 8, <br> P.7, Q.Tân
                                            Bình, TP.HCM</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </ul>
                </div>
            </div>
            <div class="ft-col col col-3 hotline-col">
                <div class="ft-menu-list-col">
                    <div class="ft-hl-block">
                        <div class="ft-hl-img">
                            <div class="inner">
                                <img src="<?php echo MONA_SITE_URL; ?>/template/assets/images/ft-hl-bg-host.png" alt="">
                            </div>
                            <div class="ft-hl-gr">
                                <div class="ft-hl-avt">
                                    <img src="<?php echo MONA_SITE_URL; ?>/template/assets/images/ft-hl-avt.png" alt="">
                                    <div class="ft-hl-decor">
                                        <span class="text">Gọi ngay <br>
                                            cho chúng tôi</span>
                                        <div class="ft-hl-line">
                                            <img src="<?php echo MONA_SITE_URL; ?>/template/assets/images/ft-hl-line.png" alt="">
                                        </div>
                                    </div>
                                </div>
                                <div class="ft-hl-phone">
                                    <a href="tel:1900636648" class="text">1900 636 648</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="ft-if">
        <div class="container">
            <div class="ft-if-top">
                <div class="ft-if-flex">
                    <div class="ft-if-col">
                        <div class="ft-if-item">
                            <div class="ft-if-inner">
                                <div class="ft-if-gr">
                                    <span class="icon">
                                        <img src="<?php echo MONA_SITE_URL; ?>/template/assets/images/footer-icon-tax-host.svg" alt="">
                                    </span>
                                    <span class="text">
                                        Mã số thuế
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ft-if-col">
                        <div class="ft-if-item">
                            <div class="ft-if-inner">
                                <div class="ft-if-box">
                                    <div class="ft-if-logo">
                                        <img src="<?php echo MONA_SITE_URL; ?>/template/assets/images/logo/media-less-logo.svg" alt="">
                                    </div>
                                    <div class="ft-if-text">
                                        <p class="text">Công ty TNHH MONA MEDIA</p>
                                        <span class="tphone">0313728397</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ft-if-col">
                        <div class="ft-if-item">
                            <div class="ft-if-inner">
                                <div class="ft-if-box">
                                    <div class="ft-if-logo">
                                        <img src="<?php echo MONA_SITE_URL; ?>/template/assets/images/logo/software-less-logo.svg" alt="">
                                    </div>
                                    <div class="ft-if-text">
                                        <p class="text">Công Ty TNHH PHẦN MỀM MONA</p>
                                        <span class="tphone">0316694442</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ft-if-col">
                        <div class="ft-if-item">
                            <div class="ft-if-inner">
                                <div class="ft-if-box">
                                    <div class="ft-if-logo">
                                        <img src="<?php echo MONA_SITE_URL; ?>/template/assets/images/logo/host-less-logo.svg" alt="">
                                    </div>
                                    <div class="ft-if-text">
                                        <p class="text">Công Ty TNHH MONA HOST</p>
                                        <span class="tphone">0317401755</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="ft-if-bot">
                <div class="ft-if-flex">
                    <div class="ft-if-col">
                        <div class="ft-if-item">
                            <div class="ft-if-inner">
                                <div class="ft-if-gr">
                                    <span class="icon">
                                        <img src="<?php echo MONA_SITE_URL; ?>/template/assets/images/footer-icon-card-host.svg" alt="">
                                    </span>
                                    <span class="text">
                                        Số tài khoản
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ft-if-col">
                        <div class="ft-if-item">
                            <div class="ft-if-inner">
                                <div class="ft-if-box">
                                    <div class="ft-if-bank">
                                        <span class="text">Ngân hàng</span>
                                        <span class="icon">
                                            <img src="<?php echo MONA_SITE_URL; ?>/template/assets/images/ft-if-bank.svg" alt="">
                                        </span>
                                    </div>
                                    <div class="ft-if-tbank">
                                        <span class="text">216341549
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ft-if-col">
                        <div class="ft-if-item">
                            <div class="ft-if-inner">
                                <div class="ft-if-box">
                                    <div class="ft-if-bank">
                                        <span class="text">Ngân hàng</span>
                                        <span class="icon">
                                            <img src="<?php echo MONA_SITE_URL; ?>/template/assets/images/ft-if-bank.svg" alt="">
                                        </span>
                                    </div>
                                    <div class="ft-if-tbank">
                                        <span class="text">17268177
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ft-if-col">
                        <div class="ft-if-item">
                            <div class="ft-if-inner">
                                <div class="ft-if-box">
                                    <div class="ft-if-bank">
                                        <span class="text">Ngân hàng</span>
                                        <span class="icon">
                                            <img src="<?php echo MONA_SITE_URL; ?>/template/assets/images/ft-if-bank.svg" alt="">
                                        </span>
                                    </div>
                                    <div class="ft-if-tbank">
                                        <span class="text">1900636648
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="ft-corp">
        <div class="container">
            <div class="d-flex f-ctn">
                <div class="ft-corp-col col col-5">
                    <?php
                    if (!is_front_page() && is_home()) {
                        $footer_logo = get_field('mona_global_footer_logo', MONA_PAGE_BLOG);
                    } else {
                        $footer_logo = get_field('mona_global_footer_logo');
                    }

                    if (!empty($footer_logo)) { ?>
                        <a href="<?php echo home_url(); ?>" class="mona-logo">
                            <?php echo wp_get_attachment_image($footer_logo, 'full'); ?>
                        </a>
                    <?php } else { ?>

                        <?php
                        $footer_logo = mona_get_option('section_footer_logo');
                        if (!empty($footer_logo)) {
                        ?>
                            <a href="<?php echo home_url(); ?>" class="mona-logo">
                                <img src="<?php echo $footer_logo; ?>" alt="">
                            </a>
                        <?php } ?>

                    <?php } ?>
                </div>
                <div class="ft-corp-col col col-7">

                    <?php $section_footer_corp_items = mona_get_option('section_footer_corp_items'); ?>
                    <div class="ft-corp-img d-flex">
                        <div class="ft-corp-img-item d-flex">
                            <?php 
                            if (!empty($section_footer_corp_items)) {
                                foreach ($section_footer_corp_items as $key => $item) {
                                    if (!empty($item['icon'])) { ?>
                                        <?php if (!empty($item['link'])) { ?>
                                            <a href="<?php echo $item['link']; ?>" target="<?php echo $item['target']; ?>" class="ft-corp-img-link <?php echo $item['attachment_class']; ?>">
                                                <img src="<?php echo $item['icon']; ?>" alt="">
                                            </a>
                                        <?php } else { ?>
                                            <div class="ft-corp-img-link <?php echo $item['attachment_class']; ?>">
                                                <img src="<?php echo $item['icon']; ?>" alt="">
                                            </div>
                                        <?php } ?>
                                <?php }
                                }
                            } ?>
                            <?php 
                            echo mona_get_option('section_footer_code') ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="ft-license">
        <div class="container">
            <div class="ft-license-row d-flex f-ctn">
                <div class="ft-license-col col col-5">
                    <div class="f-signature">
                        <p class="reserved">
                            © All rights reserved
                        </p>
                        <div class="f-signature-wrap d-flex f-start">
                            <a href="<?php echo home_url(); ?>" class="mona-logo">
                                <img loading="lazy" src="<?php echo MONA_SITE_URL ?>/template/assets/images/logo/host-less-logo.png" alt="">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="ft-license-col col col-7 d-flex f-end">
                    <ul class="policy-list d-flex f-end">
                        <li class="policy-item">
                            <?php echo do_shortcode('[mona_button text="Nhận mã bảo mật" code="93m2j1" time="60"]'); ?>
                        </li>
                    </ul>
                    <?php $section_global_socialnetwork = mona_get_option('section_global_socialnetwork');
                    if (!empty($section_global_socialnetwork)) { ?>
                        <ul class="social-list d-flex">
                            <?php foreach ($section_global_socialnetwork as $key => $item) {
                                if (!empty($item['icon']) && !empty($item['link'])) { ?>
                                    <li class="social-item <?php echo $item['attachment_class']; ?>">
                                        <a href="<?php echo $item['link']; ?>" target="<?php echo $item['target']; ?>">
                                            <img src="<?php echo $item['icon']; ?>" alt="">
                                        </a>
                                    </li>
                            <?php }
                            } ?>
                        </ul>
                    <?php } ?>
                </div>
            </div>
        </div>
    </div>
</footer>

<!-- BACK TO TOP -->
<!-- <div class="back__to__top moveToTop">
    <div class="triangle"></div>
    <div class="triangle"></div>
    <div class="triangle"></div>
</div> -->

<div class="popup popupBox" id="advise">
    <div class="popup-wrap">
        <div class="popup-close closePop">
            <img loading="lazy" src="<?php echo MONA_SITE_URL ?>/template/assets/images/icon-close.png" alt="">
        </div>
        <div class="sec-seoadv">
            <div class="seoadv-bgdecor">
                <img loading="lazy" src="<?php echo MONA_SITE_URL ?>/template/assets/images/seoadv-decor.png" alt="">
            </div>
            <div class="seoadv">
                <div class="seoadv-wrap">
                    <div class="seoadv-flex">
                        <div class="seoadv-left">
                            <div class="seoadv-content">
                                <div class="seoadv-title">
                                    <div class="seoadv-decor">
                                        <img loading="lazy" src="<?php echo MONA_SITE_URL ?>/template/assets/images/seoadv-vector.png" alt="">
                                    </div>
                                    <?php $section_advise_title = mona_get_option('section_advise_title');
                                    if (!empty($section_advise_title) ) { ?>
                                        <div class="title">
                                            <?php echo str_replace(
                                                ['[', ']'],
                                                ['<span class="text">', '</span>'],
                                                $section_advise_title
                                            ); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="seoadv-img">
                                    <img loading="lazy" src="<?php echo MONA_SITE_URL ?>/template/assets/images/seoadv-img.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="seoadv-right">
                            <div class="seoadv-py">
                                <div class="seoadv-text">
                                    <div class="text text-ani bg-blue bg-orange"><?php echo __('Nhận tư vấn', 'monamedia'); ?></div>
                                    <div class="seoadv-svg">
                                        <svg class="ani" width="293" height="152" viewBox="0 0 293 152" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M222.475 50.0815C90.3672 -6.35193 5.32653 24.5943 1.24921 63.5694C-2.82811 102.544 58.8776 140.941 139.073 149.33C219.268 157.72 287.584 132.925 291.661 93.9504C295.618 56.1328 206.705 11.562 129.997 1.71758" stroke="#2686EC" stroke-width="2" class="svg-elem-1"></path>
                                        </svg>
                                    </div>
                                </div>
                                <?php $section_advise_shortcode = mona_get_option('section_advise_shortcode');
                                if (!empty($section_advise_shortcode)) { ?>
                                    <div class="seoadv-form">
                                        <?php echo do_shortcode($section_advise_shortcode); ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="popup popupBox" id="solutions">
    <div class="popup-wrap">
        <div class="popup-close closePop">
            <img loading="lazy" src="<?php echo MONA_SITE_URL ?>/template/assets/images/icon-close.png" alt="">
        </div>
        <div class="sec-seoadv">
            <div class="seoadv-bgdecor">
                <img loading="lazy" src="<?php echo MONA_SITE_URL ?>/template/assets/images/seoadv-decor.png" alt="">
            </div>
            <div class="seoadv">
                <div class="seoadv-wrap">
                    <div class="seoadv-flex">
                        <div class="seoadv-left">
                            <div class="seoadv-content">
                                <div class="seoadv-title">
                                    <div class="seoadv-decor">
                                        <img loading="lazy" src="<?php echo MONA_SITE_URL ?>/template/assets/images/seoadv-vector.png" alt="">
                                    </div>
                                    <?php $section_solutions_title = mona_get_option('section_solutions_title');
                                    if (!empty($section_solutions_title)) { ?>
                                        <div class="title">
                                            <?php echo str_replace(
                                                ['[', ']'],
                                                ['<span class="text">', '</span>'],
                                                $section_solutions_title
                                            ); ?>
                                        </div>
                                    <?php } ?>
                                </div>
                                <div class="mona-contact-desc" data-aos="fade-up">
                                    Hãy liên hệ với Mona bằng bất cứ kênh nào mà bạn <br> cảm thấy thoải mái nhất
                                </div>

                                <?php
                                /**
                                 * GET TEMPLATE PART
                                 * method contact
                                 */
                                $slug = '/partials/global/contact';
                                $name = 'methods';
                                echo get_template_part($slug, $name);
                                ?>
                                <div class="seoadv-img">
                                    <img loading="lazy" src="<?php echo MONA_SITE_URL ?>/template/assets/images/seoadv-img.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="seoadv-right">
                            <div class="seoadv-py">
                                <div class="seoadv-text">
                                    <div class="text text-ani bg-blue bg-orange"><?php echo __('Nhận tư vấn', 'monamedia'); ?></div>
                                    <div class="seoadv-svg">
                                        <svg class="ani" width="293" height="152" viewBox="0 0 293 152" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <path d="M222.475 50.0815C90.3672 -6.35193 5.32653 24.5943 1.24921 63.5694C-2.82811 102.544 58.8776 140.941 139.073 149.33C219.268 157.72 287.584 132.925 291.661 93.9504C295.618 56.1328 206.705 11.562 129.997 1.71758" stroke="#2686EC" stroke-width="2" class="svg-elem-1"></path>
                                        </svg>
                                    </div>
                                </div>
                                <?php $section_solutions_shortcode = mona_get_option('section_solutions_shortcode');
                                if (!empty($section_solutions_shortcode)) { ?>
                                    <div class="seoadv-form">
                                        <?php echo do_shortcode($section_solutions_shortcode); ?>
                                    </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="popup popup-service popupBox popService">
    <div class="popup-wrap">
        <div class="popup-service-content">
            <div class="popup-service-img">
                <div class="popup-close closePop">
                    <img loading="lazy" src="<?php echo MONA_SITE_URL ?>/template/assets/images/icon-close.png" alt="">
                </div>
                <div class="img">
                    <img loading="lazy" src="<?php echo MONA_SITE_URL ?>/template/assets/images/popup-service.png" alt="" class="cir">
                </div>
            </div>
            <div id="monaServiceBody">
            </div>
        </div>
    </div>
</div>

<div class="c-popup c-modal-js">
    <div class="c-popup-wrap">
        <div class="host-service-aside-wrap cart-wrap-js is-loading-btn-2">
            <?php echo monahost_get_cart_html(); ?>
        </div>
    </div>
</div>

<!-- <div id="popperContextMenu" class="popper-contextMenu">
    <ul class="mega-menu-list">
        <li class="mega-menu-item">
            <a href="https://mona.media/lien-he/?utm_source=google&utm_medium=cpc&utm_campaign=lien-he-menu-an" class="d-flex openPopMona" data-popup="solutions">
                <span class="mega-menu-img">
                    <img loading="lazy" src="<?php echo MONA_SITE_URL ?>/template/assets/images/mega-menu/icon-contact.svg" alt="">
                </span>
                <span class="mega-menu-txt">
                    <span class="title">Liên hệ ngay</span>
                </span>
            </a>
        </li>
        <li class="mega-menu-item">
            <a href="https://mona.media/du-an/?utm_source=google&utm_medium=cpc&utm_campaign=du_an_menu_an" class="d-flex">
                <span class="mega-menu-img">
                    <img loading="lazy" src="<?php echo MONA_SITE_URL ?>/template/assets/images/mega-menu/icon-project.svg" alt="">
                </span>
                <span class="mega-menu-txt">
                    <span class="title">Xem dự án</span>
                </span>
            </a>
        </li>
        <li class="mega-menu-item">
            <a href="https://www.facebook.com/thietkewebsitemonamedia?utm_source=google&utm_medium=cpc&utm_campaign=facebook_menu_an" target="_blank" class="d-flex">
                <span class="mega-menu-img">
                    <img loading="lazy" src="<?php echo MONA_SITE_URL ?>/template/assets/images/mega-menu/icon-fanpage.svg" alt="">
                </span>
                <span class="mega-menu-txt">
                    <span class="title">Tham gia fanpage</span>
                </span>
            </a>
        </li>
        <li class="mega-menu-item">
            <a href="https://www.youtube.com/@CongtyTNHHMONAMEDIA?utm_source=google&utm_medium=cpc&utm_campaign=youtube_menu_an" target="_blank" class="d-flex">
                <span class="mega-menu-img">
                    <img loading="lazy" src="<?php echo MONA_SITE_URL ?>/template/assets/images/mega-menu/icon-channel.svg" alt="">
                </span>
                <span class="mega-menu-txt">
                    <span class="title">Xem MONA.Channel</span>
                </span>
            </a>
        </li>
    </ul>
</div> -->

<div class="popup-account popup-account-js popup-js">
    <div class="popup-account-wrap">
        <div class="popup-account-close pop-close-js">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/icon-x-close.svg" alt="">
        </div>
        <div class="popup-account-ctn d-flex">
            <div class="popup-account-col col-6">
                <div class="popup-account-content">
                    <div class="popup-account-mona">
                        <span class="hello">Hello! 👋</span>
                        Mona
                    </div>
                    <div class="popup-account-desc">
                        <p>Mọi vấn đề quý khách vui lòng liên hệ với Mona Media theo địa chỉ sau đây.</p>
                    </div>
                    <div class="popup-account-address">
                        <div class="item">
                            <div class="icon">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/icon-location.svg" alt="">
                            </div>
                            <div class="txt">1073/23 CMT8, P.7, Q. Tân Bình, TP. HCM</div>
                        </div>
                        <div class="item">
                            <div class="icon">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/icon-email.svg" alt="">
                            </div>
                            <div class="txt">info@mona-media.com</div>
                        </div>
                        <div class="item">
                            <div class="icon">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/icon-customer-care.svg" alt="">
                            </div>
                            <div class="txt">1900 636 648</div>
                        </div>
                    </div>
                    <div class="popup-account-img">
                        <div class="img">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/mona_the_panda.png" alt="">
                        </div>
                        <div class="hotline-ab">
                            <div class="hotline-ab-item">
                                <p class="gt">Bấm 109</p>
                                <p>Phòng kinh doanh</p>
                            </div>
                            <div class="hotline-ab-item">
                                <p class="gt">Bấm 108</p>
                                <p>Phòng kinh doanh</p>
                            </div>
                            <div class="hotline-ab-item">
                                <p class="gt">Bấm 103</p>
                                <p>Phòng kỹ thuật</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="popup-account-col col-6">
                <div class="popup-account-form">
                    <div class="popup-account-form-tab">
                        <div class="tab-item active open-login">Đăng nhập</div>
                        <div class="tab-item open-register">Đăng ký</div>
                    </div>

                    <div class="popup-account-form-panel">
                        <div class="tab-panel-item active form-login">
                            <form id="frmLogin" class="form-cus">
                                <div class="f-r">
                                    <div class="f-c">
                                        <input type="text" name="user_login" placeholder="Email" class="txt-ipt">
                                        <div style="display: none;" class="mona-error mona-error-user-login"></div>
                                    </div>
                                </div>
                                <div class="f-r">
                                    <div class="f-c">
                                        <input type="password" name="user_password" placeholder="Mật khẩu" class="txt-ipt">
                                        <span class="password-eye password-eye-js">
                                            <i class="fal fa-eye fal-1"></i>
                                            <i class="fal fa-eye-slash fal-2"></i>
                                        </span>
                                        <div style="display: none;" class="mona-error mona-error-pass"></div>
                                    </div>
                                </div>
                                <div class="f-r f-forgot">
                                    <div class="f-c">
                                        <label class="checkbox-cus-item d-flex f-start">
                                            <input type="checkbox" name="user_remember" class="checkbox-cus-hidden">
                                            <div class="checkbox-cus-check"></div>
                                            <div class="checkbox-cus-name">Ghi nhớ tài khoản</div>
                                        </label>
                                    </div>
                                    <div class="f-c">
                                        <p class="forgot-password open-forgotpass">Quên mật khẩu</p>
                                    </div>
                                </div>
                                <?php wp_nonce_field( 'login_action', 'login_nonce_field' ); ?> 
                                <div class="mona-error-wrap">
                                    <div style="display: none;" class="mona-notice error"></div>
                                    <div style="display: none;" class="mona-notice success"></div>
                                </div>
                                <div class="f-r f-btn">
                                    <input type="submit" value="ĐĂNG NHẬP" class="host-btn mona-login-submit is-loading-btn">

                                    <div class="f-has">
                                        <div class="label">
                                            Bạn chưa có tài khoản.
                                            <span class="hl-sec open-register">Đăng ký ngay</span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-panel-item form-register">
                            <form id="frmRegister" class="form-cus">
                                <div class="f-r">
                                    <div class="f-c">
                                        <input type="text" name="user_first_name" placeholder="Tên của bạn" class="txt-ipt">
                                        <div style="display: none;" class="mona-error mona-error-first-name"></div>
                                    </div>
                                </div>
                                <div class="f-r">
                                    <div class="f-c">
                                        <input type="text" name="user_last_name" placeholder="Họ của bạn" class="txt-ipt">
                                        <div style="display: none;" class="mona-error mona-error-last-name"></div>
                                    </div>
                                </div>
                                <div class="f-r">
                                    <div class="f-c">
                                        <input type="text" name="user_email" placeholder="Email" class="txt-ipt">
                                        <div style="display: none;" class="mona-error mona-error-email"></div>
                                    </div>
                                </div>
                                <div class="f-r">
                                    <div class="f-c">
                                        <input type="password" name="user_pass" placeholder="Mật khẩu" class="txt-ipt">
                                        <span class="password-eye password-eye-js">
                                            <i class="fal fa-eye fal-1"></i>
                                            <i class="fal fa-eye-slash fal-2"></i>
                                        </span>
                                        <div style="display: none;" class="mona-error mona-error-pass"></div>
                                    </div>
                                </div>
                                <div class="f-r">
                                    <div class="f-c">
                                        <input type="text" name="user_phone" placeholder="Số điện thoại" class="txt-ipt">
                                        <div style="display: none;" class="mona-error mona-error-phone"></div>
                                    </div>
                                </div>
                                <div class="f-r">
                                    <div class="f-c">
                                        <input type="text" name="user_address_1" placeholder="Địa chỉ" class="txt-ipt">
                                        <div style="display: none;" class="mona-error mona-error-address-1"></div>
                                    </div>
                                </div>
                                <?php wp_nonce_field( 'register_action', 'register_nonce_field' ); ?> 
                                <div class="mona-error-wrap">
                                    <div style="display: none;" class="mona-notice error"></div>
                                    <div style="display: none;" class="mona-notice success"></div>
                                </div>
                                <div class="f-r f-btn">
                                    <input type="submit" value="ĐĂNG KÝ" class="host-btn mona-register-submit is-loading-btn">

                                    <div class="f-has">
                                        <div class="label">
                                            Bạn đã có tài khoản.
                                            <span class="hl-sec open-login">Đăng nhập ngay</span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="tab-panel-item form-forgotpass">
                            <form id="frmForgot" class="form-cus">
                                <div class="host-tt txt-center">Quên mật khẩu</div>
                                <div class="f-r">
                                    <div class="f-c">
                                        <input type="text" name="user_email" placeholder="Email" class="txt-ipt">
                                    </div>
                                </div>
                                <?php wp_nonce_field( 'forgot_action', 'forgot_nonce_field' ); ?> 
                                <div class="mona-error-wrap">
                                    <div style="display: none;" class="mona-notice error"></div>
                                    <div style="display: none;" class="mona-notice success"></div>
                                </div>
                                <div class="f-r f-btn">
                                    <input type="submit" value="LẤY LẠI MẬT KHẨU" class="host-btn mona-forgot-submit is-loading-btn">

                                    <div class="f-has">
                                        <div class="label">
                                            Trở về trang
                                            <span class="hl-sec open-login">Đăng nhập</span>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="popup-account popup-whois-js popup-js">
    <div class="popup-account-wrap whois">
        <div class="popup-account-close pop-close-js">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/icon-x-close.svg" alt="">
        </div>
        <div class="popup-whois-main whois-main-js"> 
            
        </div>
    </div>
</div>

<div class="popup-bg popupBg popup-bg-js"></div>

<div class="mega-menu-on-mobile mega-menu-on-mobile-js">
    <!-- <div class="hd-contact hd-menu show-on-mobile">
        <div class="menu">
            <ul class="menu-list d-flex f-center">
                <?php //echo mona_get_hotline(); ?>
            </ul>
        </div>
    </div> -->
</div>

<div class="mega-menu-child mega-menu-child-js"></div>

<!-- <div class="mona-hiring-show mona-hiring">
    <a href="https://mona.media/tuyen-dung" target="_blank" class="mona-hiring-link">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/mona-hiring.png" alt="">
    </a>
</div> -->

<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/swiper/swiper-bundle.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/lightgallery/lightgallery-all.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/magnific/jquery.magnific-popup.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/aos/aos.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/wow/wow.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/select2/select2.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/smoothscroll/SmoothScroll.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/particles/particles.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/img-compare/img-compare.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/locomotive/locomotive-scroll.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/gsap/gsap.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/gsap/ScrollTrigger.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/parallax/simpleParallax.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/marquee/jquery.marquee.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/typing/typed.js@2.0.12"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/splitting/splitting.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/splide/splide.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/splide/splide-extension-auto-scroll.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/splide/splide-extension-video.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/isotope/isotope.pkgd.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/Tippy/unpkg.com_@popperjs_core@2.11.8_dist_umd_popper.min.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/Tippy/unpkg.com_tippy.js@6.3.7_dist_tippy-bundle.umd.js"></script>
<script src="<?php echo MONA_SITE_URL ?>/template/js/main.js" type="module"></script>
<?php wp_footer(); ?>
<script>
    tippy(".tooltip-show", {
        content(reference) {
        const id = reference.getAttribute("data-template");

        const template = document.getElementById(id);

        return template.innerHTML;
        },
        allowHTML: true,
        theme: "gift-content",
        interactive: true,
    });
</script>
<script>
    var Tawk_API = Tawk_API || {},
        Tawk_LoadStart = new Date();
    (function() {
        var s1 = document.createElement("script"),
            s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/5937bb8ab3d02e11ecc689d0/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
        // Tawk_API.onLoad = function(){
        //     Tawk_API.toggle();
        // };
    })();
    //    -- js khi submit pre-chat
    Tawk_API.onPrechatSubmit = function(data) {
        var dataRequest = {
            CustomerContactTawkToRequests: data
        };
        $.ajax({
            type: "POST",
            url: "https://quanly.mona.media/api/ContactCustomerApi/SendRequestContactFromTawkTo",
            data: JSON.stringify(dataRequest),
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            success: function(result) {
                if (result.Data) {

                } else {

                }
            },
            error: function(xmlhttprequest, textstatus, errorthrow) {
                console.log("error:");
            }
        });
    };;
    (function() {
        var c = document.createElement('link');
        c.type = 'text/css';
        c.rel = 'stylesheet';
        c.href = 'https://images.dmca.com/badges/dmca.css?ID=d8049b84-d847-40ee-8782-ecde449fdda0';
        var h = document.getElementsByTagName("head")[0];
        h.appendChild(c);
    })();
</script>
<script type="text/javascript">
    ;
    (function() {
        var c = document.createElement('link');
        c.type = 'text/css';
        c.rel = 'stylesheet';
        c.href = 'https://images.dmca.com/badges/dmca.css?ID=d8049b84-d847-40ee-8782-ecde449fdda0';
        var h = document.getElementsByTagName("head")[0];
        h.appendChild(c);
    })();
</script>
<!-- Facebook Pixel Code -->
<script>
    ! function(f, b, e, v, n, t, s) {
        if (f.fbq)
            return;
        n = f.fbq = function() {
            n.callMethod ?
                n.callMethod.apply(n, arguments) : n.queue.push(arguments)
        };
        if (!f._fbq)
            f._fbq = n;
        n.push = n;
        n.loaded = !0;
        n.version = '2.0';
        n.queue = [];
        t = b.createElement(e);
        t.async = !0;
        t.src = v;
        s = b.getElementsByTagName(e)[0];
        s.parentNode.insertBefore(t, s)
    }(window, document, 'script', 'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '171883630005093');
    fbq('track', 'PageView');
</script>

<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=171883630005093&ev=PageView&noscript=1" /></noscript>
<!-- End Facebook Pixel Code -->
<!--
<span class="skype-button rounded" data-contact-id="demonhunterp"></span>
<script src="https://swc.cdn.skype.com/sdk/v1/sdk.min.js"></script>
-->
<div class="zalo-chat-widget" data-oaid="4408585214232537731" data-welcome-message="Quý khách cần em hỗ trợ gì ạ?" data-autopopup="0" data-width="300" data-height="300"></div>

<script src="https://sp.zalo.me/plugins/sdk.js"></script>
</body>

</html>