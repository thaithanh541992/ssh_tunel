<?php get_header(); ?>

    <main class="main">
        <div class="pageer">
            <div class="img-decor-group img-decor-group-js">
                <div class="img-decor img-panda">
                    <div class="img-decor-wrap cir cir-2">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/home-panda.png" alt="">
                        <div class="flame">
                            <div class="flame-wrap">
                                <div id="fireFront" class="fire-front fireFront"></div>
                                <div id="fireMid" class="fire-mid fireMid"></div>
                                <div id="fireBack" class="fire-back fireBack"></div>
                            </div>
                        </div>
                        <div class="flame">
                            <div class="flame-wrap">
                                <div id="fireFront" class="fire-front fireFront"></div>
                                <div id="fireMid" class="fire-mid fireMid"></div>
                                <div id="fireBack" class="fire-back fireBack"></div>
                            </div>
                        </div>
                        <div class="flame">
                            <div class="flame-wrap">
                                <div id="fireFront" class="fire-front fireFront"></div>
                                <div id="fireMid" class="fire-mid fireMid"></div>
                                <div id="fireBack" class="fire-back fireBack"></div>
                            </div>
                        </div>
                    </div>


                </div>

                <div class="img-decor img-rocket">
                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/home-rocket.png" alt="" class="ani-rocket">

                    <div class="ani-fire-render"></div>
                </div>

                <!-- <div class="img-decor img-comet">
                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/home-comet.png" alt="" class="">
                </div> -->
            </div>
            <div class="decor">
                <div class="inner">
                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/nfs/moon.png" alt="">
                </div>
            </div>
            <div class="decor2">
                <div class="inner">
                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/nfs/earth.png" alt="">
                </div>
            </div>
            <div class="pageer-wrap">
                <div class="pageer-py">
                    <div class="pageer-text ani">
                        <svg stroke="#1EC0F2" stroke-width="2" class="text-line"><text x="50%"
                                dominant-baseline="middle" text-anchor="middle" y="50%">4</text></svg>
                        <svg stroke="#1EC0F2" stroke-width="2" class="text-line"><text x="50%"
                                dominant-baseline="middle" text-anchor="middle" y="50%">0</text></svg>
                        <svg stroke="#1EC0F2" stroke-width="2" class="text-line"><text x="50%"
                                dominant-baseline="middle" text-anchor="middle" y="50%">4</text></svg>
                    </div>
                    <div class="pageer-des">
                        <p>Trang bạn kiếm không có trong MONA.Host rồi</p>
                    </div>
                    <div class="pageer-link">
                        <a rel="nofollow" href="<?php echo home_url(); ?>" class="btn-second">
                            <span class="txt">
                                Trở về trang chủ
                            </span>
                            <span class="icon">
                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-arrow-right.png" alt="">
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </main>

<?php get_footer();