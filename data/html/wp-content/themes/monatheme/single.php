<?php
/**
 * The template for displaying single.
 *
 * @package Monamedia
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
while ( have_posts() ):
    the_post();
    global $post;
    $current_post_id = $post->ID;
    mona_set_post_view();
    ?>

<main class="main">
    <div class="sec-blogt fix-border">
        <div class="sec-blogt-bg">
            <img src="<?php echo get_site_url(); ?>/template/assets/images/blog/sec-blogt-bg-big-host.png" alt="">
        </div>
        <div class="blogt">
            <div class="blogt-wrap">
                <div class="blogt-dot"></div>
                <div class="container">
                    <div class="blogdt-block">
                        <div class="blogdt-top">
                            <div class="blogdt-tag">
                                <?php
                                $primary = get_post_primary_taxonomy( $post->ID , 'category' );
                                if( !empty( $primary['primary_category'] ) ){
                                    $parent = get_taxonomy_term_root( $primary['primary_category'] );
                                    if( !empty( $parent ) ){ ?>
                                <a class="text" href="<?php echo get_term_link( $parent ); ?>">
                                    <?php echo $parent->name; ?>
                                </a>
                                <?php } } ?>
                                <p class="time"><?php echo get_the_date( 'd F, Y' , $post ); ?></p>
                            </div>
                            <h1 class="title">
                                <?php echo get_the_title(); ?>
                            </h1>
                        </div>
                        <div class="blogdt-author">
                            <?php 
                            $author_id = get_post_field( 'post_author', $post->ID );
                            $author = get_userdata($author_id);
                            if( !empty( $author ) ){
                                $author_avatar              = get_field('mona_user_avatar', $author);
                                $author_display             = get_field('mona_user_display_name', $author);
                                $author_display_description = get_field('mona_user_display_description', $author);
                                ?>
                            <a href="<?php echo get_author_posts_url($author_id); ?>" class="blogtc2-box-gr">
                                <div class="img-cir">
                                    <?php if( empty( $author_avatar ) ){ ?>
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-user.png" alt="">
                                    <?php }else{ ?>
                                        <?php echo wp_get_attachment_image($author_avatar, 'full'); ?>
                                    <?php } ?>
                                </div>
                                <div class="gr-txt">
                                    <p class="name"><?php echo !empty( $author_display ) ? $author_display : $author->display_name; ?></p>
                                    <p class="level"><?php echo $author_display_description; ?></p>
                                </div>
                            </a>
                            <?php } ?>
                            <?php 
                            $mona_post_viewed       = get_field('mona_post_viewed', $post);
                            $mona_post_favorited    = get_field('mona_post_favorited', $post);
                            $mona_post_shared       = get_field('mona_post_shared', $post); ?>
                            <?php if( !empty( $mona_post_viewed ) || !empty( $mona_post_favorited ) || !empty( $mona_post_shared ) ){ ?>
                            <div class="blogtc2-bot">
                                <?php if( !empty( $mona_post_viewed ) ){ ?>
                                <div class="blogtc2-bot-gr">
                                    <span class="icon">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-eye.png"
                                            alt="">
                                    </span>
                                    <span class="text"><?php echo $mona_post_viewed; ?></span>
                                </div>
                                <?php } ?>

                                <?php if( !empty( $mona_post_favorited ) ){ ?>
                                <div class="blogtc2-bot-gr">
                                    <span class="icon">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-heart.png"
                                            alt="">
                                    </span>
                                    <span class="text"><?php echo $mona_post_favorited; ?></span>
                                </div>
                                <?php } ?>

                                <?php if( !empty( $mona_post_shared ) ){ ?>
                                <div class="blogtc2-bot-gr">
                                    <span class="icon">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-share.png"
                                            alt="">
                                    </span>
                                    <span class="text"><?php echo $mona_post_shared; ?></span>
                                </div>
                                <?php } ?>
                            </div>
                            <?php } ?>
                        </div>
                       
                        <!-- <div class="blogdt-banner">
                            <div class="inner ani">
                                <?php // $mona_post_outstanding_thumbnail = get_field('mona_post_outstanding_thumbnail', $post);
                                //if( !empty( $mona_post_outstanding_thumbnail ) ){ ?>
                                    <?php // echo wp_get_attachment_image($mona_post_outstanding_thumbnail, 'full'); ?>
                                <?php //}else{ ?>
                                    <img src="<?php //echo get_template_directory_uri() ?>/public/helpers/images/global-blogdetail-thumbnail.jpg" alt="">
                                <?php //} ?>
                            </div>
                        </div> -->
                    </div>
                </div>

            </div>
        </div>
    </div>
    <section class="sec-blogdt" data-aos="fade">
        <div class="container">
            <div class="categories-btn categoriesBtn active">
                <i class="fas fa-tasks"></i>
            </div>
            <div class="blogdt">
                <div class="blogdt-wrap">

                    <div class="blogdt-content">
                        <div class="blog-large-ctn d-flex f-ctn aos-init aos-animate" data-aos="fade-up">
                            <div class="blog-large-aside aside-categories asideCate col col-3">
                                <div class="aside-list">
                                    <div class="aside-item aside-cate toggleParent">
                                        <div class="aside-close asideClose">
                                            <i class="fal fa-times"></i>
                                        </div>

                                        <div class="aside-header d-flex monaTitleToggleShow">
                                            <div class="title"><?php echo __('Nội dung', 'monamedia'); ?></div>
                                            <div class="hide-cate toggleOnclick">
                                                <i class="fas fa-chevron-up icon"></i>
                                            </div>
                                        </div>

                                        <div class="aside-body toggleHide monaTitleToggleHide">
                                            <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar_blog_detail')) : ?>
                                            <?php endif; ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-large-content col col-6">
                                <div class="mona-content blogContent table-modify-js">
                                    <?php the_content(); ?>
                                </div>

                                <div class="blog-large-footer">
                                    <div class="blogdt-author">
                                        <?php
                                        if( !empty( $author ) ){
                                            $author_avatar              = get_field('mona_user_avatar', $author);
                                            $author_display             = get_field('mona_user_display_name', $author);
                                            $author_display_description = get_field('mona_user_display_description', $author);
                                            ?>
                                        <div class="blogtc2-box-gr">
                                            <div class="img-cir">
                                                <?php if( empty( $author_avatar ) ){ ?>
                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-user.png" alt="">
                                                <?php }else{ ?>
                                                    <?php echo wp_get_attachment_image($author_avatar, 'full'); ?>
                                                <?php } ?>
                                            </div>
                                            <div class="gr-txt">
                                                <p class="name"><?php echo !empty( $author_display ) ? $author_display : $author->display_name; ?></p>
                                                <p class="level"><?php echo $author_display_description; ?></p>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php 
                                        $author_viewed       = get_field('mona_post_viewed', $author);
                                        $author_favorited    = get_field('mona_post_favorited', $author);
                                        $author_shared       = get_field('mona_post_shared', $author); ?>
                                        <?php if( !empty( $author_viewed ) || !empty( $author_favorited ) || !empty( $author_shared ) ){ ?>
                                        <div class="blogtc2-bot">
                                            <?php if( !empty( $author_viewed ) ){ ?>
                                            <div class="blogtc2-bot-gr">
                                                <span class="icon">
                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-eye.png"
                                                        alt="">
                                                </span>
                                                <span class="text"><?php echo $author_viewed; ?></span>
                                            </div>
                                            <?php } ?>

                                            <?php if( !empty( $author_favorited ) ){ ?>
                                            <div class="blogtc2-bot-gr">
                                                <span class="icon">
                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-heart-fill.png"
                                                        alt="">
                                                </span>
                                                <span class="text"><?php echo $author_favorited; ?></span>
                                            </div>
                                            <?php } ?>

                                            <?php if( !empty( $author_shared ) ){ ?>
                                            <div class="blogtc2-bot-gr">
                                                <span class="icon">
                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-share.png"
                                                        alt="">
                                                </span>
                                                <span class="text"><?php echo $author_shared; ?></span>
                                            </div>
                                            <?php } ?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="blog-large-aside col col-3">
                                <div class="blogr-pos">
                                    <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar_blog_detail_right')) : ?>
                                    <?php endif; ?>
                                </div>
                            </div>

                            <div class="bg-page-out bgPageOut"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php 
	$post_type = 'post';
	$posts_per_page = 12;
	$paged = max( 1, get_query_var('paged') );
	$offset = ( $paged - 1 ) * $posts_per_page;
	$argsPost = [
		'post_type' => $post_type,
		'post_status' => 'publish',
		'posts_per_page' => $posts_per_page,
		'paged' => $paged,
		'offset' => $offset,
		'meta_query' => [
			'relation' => 'AND',
		],
		'tax_query' => [
			'relation'=>'AND',
		]
	];

    if( !empty( $primary['primary_category'] ) ){

        $argsPost['tax_query']['relation'] = 'OR';
        $argsPost['tax_query'][] = [
            'taxonomy' =>  $primary['primary_category']->taxonomy,
            'field' => 'slug',
            'terms' => $primary['primary_category']->slug,
        ];
        
        $parent = get_taxonomy_term_root( $primary['primary_category'] );
        if( !empty( $parent ) ){
            $argsPost['tax_query'][] = [
                'taxonomy' =>  $parent->taxonomy,
                'field' => 'slug',
                'terms' => $parent->slug,
            ];
        }

    }else{

        $categories = wp_get_post_terms($post->ID, 'category', ['fields'=>'ids']);
        if( !empty( $categories ) ){
            $argsPost['tax_query'][] = [
                'taxonomy' =>  'category',
                'field' => 'id',
                'terms' => $categories,
            ];
        }

    }

	$postsMONA = new WP_Query( $argsPost );  
    if( $postsMONA->have_posts() ){
	?>
    <section class="sec-blogsw">
        <div class="container">
            <div class="blogsw">
                <div class="blogsw-title">
                    <h2 class="title">
                        <?php echo __('Bài viết liên quan', 'monamedia'); ?>
                    </h2>
                </div>
                <div class="blogsw-slide">
                    <div class="swiper">
                        <div class="swiper-wrapper">
                            <?php 
                            while( $postsMONA->have_posts() ){
                                $postsMONA->the_post(); ?>
                            <div class="swiper-slide">
                                <div class="blogr-item">
                                    <?php 
                                    /**
                                     * GET TEMPLATE PART
                                     * partials
                                     * blog box
                                     */
                                    $slug = '/partials/loop/box';
                                    $name = 'blog';
                                    echo get_template_part($slug, $name);
                                    ?>
                                </div>
                            </div>
                            <?php } wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>

    <?php
	/**
	 * GET TEMPLATE PART
	 * contact section
	 *  */ 
	$slug = '/partials/global/contact';
	$name = '';
	echo get_template_part($slug, $name);
	?>
    <?php
	/**
	 * GET TEMPLATE PART
	 * outstanding information section
	 *  */ 
	$slug = '/partials/global/outstanding';
	$name = 'information';
	echo get_template_part($slug, $name);
	?>
</main>

<?php
endwhile;
get_footer();