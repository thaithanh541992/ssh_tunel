<?php
/**
 * The template for displaying page template.
 *
 * @package MONA.Media / Website
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

if ( is_page( MONA_WC_CART ) ) {
    if ( WC()->cart->get_cart_contents_count() == 0 ) {
        wp_redirect(get_the_permalink(MONA_PAGE_HOME));
    } else {
        wp_redirect(get_the_permalink(MONA_WC_CHECKOUT));
    }
}

get_header();
while ( have_posts() ):
    the_post();
    if( is_page( MONA_WC_CHECKOUT ) ) {
        ?>
        <main class="main">
            <div class="breadcrumb">
                <div class="container">
                    <ul class="breadcrumb-list d-flex f-start">
                        <li class="breadcrumb-item" data-aos="fade-right"><a href="#" class="breadcrumb-item-link">Home</a></li>
                        <li class="breadcrumb-item" data-aos="fade-right"><a href="#" class="breadcrumb-item-link">Đăng ký tên miền</a></li>
                        <li class="breadcrumb-item" data-aos="fade-right"><a href="#" class="breadcrumb-item-link">Checkout</a></li>
                    </ul>
                </div>
            </div>
            <div class="host-checkout">
                <div class="container">
                    <?php the_content(); ?>
                </div>
            </div>
        </main>
        <?php
    } else {
        ?>
        <main class="main">
            <section class="sec-blogdt" data-aos="fade">
                <div class="container">
                    <div class="blogdt">
                        <div class="blogdt-wrap">

                            <div class="blogdt-content">
                                <div class="blog-large-ctn d-flex f-ctn aos-init aos-animate" data-aos="fade-up">
                                    <div class="blog-large-content col">
                                        <div class="mona-content blogContent table-modify-js">
                                            <?php the_content(); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <?php
            /**
             * GET TEMPLATE PART
             * contact section
             *  */ 
            $slug = '/partials/global/contact';
            $name = '';
            echo get_template_part($slug, $name);
            ?>
            <?php
            /**
             * GET TEMPLATE PART
             * outstanding information section
             *  */ 
            $slug = '/partials/global/outstanding';
            $name = 'information';
            echo get_template_part($slug, $name);
            ?>
        </main>
        <?php
    }
endwhile;
get_footer();
