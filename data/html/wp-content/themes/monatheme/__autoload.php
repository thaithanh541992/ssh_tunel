<?php 
define( 'APP_PATH', '/app' );
define( 'CONTROLLER_PATH', APP_PATH . '/controllers' );
define( 'AJAX_PATH', APP_PATH . '/ajax' );
define( 'MODULE_PATH', APP_PATH . '/modules' );

define( 'CORE_PATH', '/core' );
define( 'FILES_PATH', '/partials' );
define( 'ADMIN_PATH', CORE_PATH . '/admin' );
define( 'ADMIN_INCLUDES_PATH', ADMIN_PATH . '/includes' );

define( 'THEME_VERSION', _get_theme_type() );
define( 'MENU_FILTER_ADMIN', 'mona-filter-admin' );
define( 'FILTER_ADMIN_SETTING', 'MonaFilterSetting' );

require_once( get_template_directory() . ADMIN_PATH . '/class-mona-admin.php' );
require_once( get_template_directory() . ADMIN_PATH . '/class-mona-page-login.php' );
require_once( get_template_directory() . CORE_PATH . '/classes/class-mona-setup.php' );
require_once( get_template_directory() . CORE_PATH . '/classes/class-mona-core.php' );

// admin setting
if ( class_exists ( 'MonaAdmin' ) ) {
    $Admin = new MonaAdmin();
    $Admin->__init();
    $Admin->include_files();
}

// core theme
if ( class_exists ( 'MonaCore' ) ) {
    $Core = new MonaCore();
    $Supports = $Core->supports();
    $Core->load_core();
}

function _get_theme_type( $type = 'Version' ) {
    $theme = wp_get_theme();
    if ( $theme && $type ) {
        return esc_attr( $theme->get( 'Version' ) );
    } else {
        return '4.0.1';
    }
}
