<?php 
/**
 * Contact Form Custom Field
 */
add_action( 'wpcf7_init', 'custom_add_form_tag_service' );
function custom_add_form_tag_service() {
  wpcf7_add_form_tag( array( 'service', 'service*' ), 'custom_service_form_tag_handler', true );
}
function custom_service_form_tag_handler( $tag ) {

    $tag = new WPCF7_FormTag( $tag );

    if ( empty( $tag->name ) ) {
        return '';
    }

    $validation_error = wpcf7_get_validation_error( $tag->name );

    $class = wpcf7_form_controls_class( $tag->type );

    if ( $validation_error ) {
        $class .= ' wpcf7-not-valid';
    }

    $atts = array();

    $atts['class'] = $tag->get_class_option( $class );
    $atts['id'] = $tag->get_id_option();

    if ( $tag->is_required() ) {
        $atts['aria-required'] = 'true';
    }

    $atts['aria-invalid'] = $validation_error ? 'true' : 'false';

    $atts['name'] = $tag->name;
    $atts['data-placeholder'] = __('Vui lòng chọn dịch vụ mà bạn quan tâm', 'monamedia');

    $atts = wpcf7_format_atts( $atts );

    $output = '';

    $output .= '<option selected disabled></option>';

    $argsPost = array(
        'post_type' => 'mona_service',
        'posts_per_page' => -1,
        'tax_query' => array(
            'relation' => 'AND',
        )
    );
    $loop = new WP_Query( $argsPost );

    if( $loop->have_posts() ){
        $first = true;
        while( $loop->have_posts() ){
            $loop->the_post();
            global $post;

            $mona_service_primary = get_field('mona_service_primary', $post);
            if( $mona_service_primary ){
                $output .= sprintf( '<option value="%1$s">%2$s</option>', get_the_title($post) , get_the_title($post) );
            }

            if( $first )
                $first = false;
        } wp_reset_query();
    }
    
    $output = sprintf(
        '<span class="wpcf7-form-control-wrap %1$s"><select class="%2$s" %3$s>%4$s</select>%5$s</span>', 
        sanitize_html_class( $tag->name ) . ' is-loading-btn',
        'select2choose monaServiceJS is-loading-group',
        $atts,
        $output,
        $validation_error
    );

    return $output;

}

/* Validation filter */

add_filter( 'wpcf7_validate_service', 'wpcf7_service_validation_filter', 10, 2 );
add_filter( 'wpcf7_validate_service*', 'wpcf7_service_validation_filter', 10, 2 );

function wpcf7_service_validation_filter( $result, $tag ) {

    $tag = new WPCF7_FormTag( $tag );
    $name = $tag->name;
    if ( isset( $_POST[$name] ) && is_array( $_POST[$name] ) ) {
        foreach ( $_POST[$name] as $key => $value ) {
            if ( '' === $value ) {
                unset( $_POST[$name][$key] );
            }
        }
    }

    $empty = ! isset( $_POST[$name] ) || empty( $_POST[$name] ) && '0' !== $_POST[$name];

    if ( $tag->is_required() && $empty ) {
        $result->invalidate( $tag, __('Vui lòng chọn dịch vụ mà bạn quan tâm!','monamedia') );
    }

    return $result;

}