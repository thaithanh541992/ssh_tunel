<?php 
function MonaAdmin() {
    return (new MonaAdmin());
}

function MonaSettingOverview() {
    return (new MonaFilterSettingOverview());
}

function MonaSettingNotFound() {
    return (new MonaFilterSettingNotFound());
}

function get_admin_menu_tabs(){
    return MonaAdmin()->register_admin_pages();
}

add_filter( 'admin_body_class', 'mona_filter_admin_classes' );
function mona_filter_admin_classes( $classes ) {
	$currentScreen = get_current_screen();
    if ( isset( $currentScreen->base ) && $currentScreen->base === 'appearance_page_mona-filter-admin' ) { 
        $classes .= ' mona-filter-admin';
    }
	return $classes;
}

add_action( 'wp_head', 'mona_filter_front_head' );
function mona_filter_front_head() {
    echo MonaSettingOverview()->__field_value( 'header_script' );
}

add_action( 'wp_footer', 'mona_filter_front_footer' );
function mona_filter_front_footer() {
    echo MonaSettingOverview()->__field_value( 'footer_script' );
}

add_action( 'admin_footer', 'mona_filter_admin_footer' );
function mona_filter_admin_footer() {
    echo '<div id="mona-toast"></div>';
}

function get_admin_message_success( $text = '' ) {
    $message = '';
    $message .= '<div class="toast__icon"><span class="dashicons dashicons-yes"></span></div>';
    $message .= '<div class="toast__body">';
    $message .= '<h3 class="toast__title">'.__( 'Thông báo!', 'mona-admin' ).'</h3>';
    $message .= '<p class="toast__msg">'.__( $text, 'mona-admin' ).'</p>';
    $message .= '</div>';
    $message .= '<div class="toast__close"><span class="dashicons dashicons-no"></span></div>';
    $message .= '<div class="progress"></div>';
    // result html
    return $message;
}

function get_admin_message_error( $text = '' ) {
    $message = '';
    $message .= '<div class="toast__icon"><span class="dashicons dashicons-info"></span></div>';
    $message .= '<div class="toast__body">';
    $message .= '<h3 class="toast__title">'.__( 'Thông báo!', 'mona-admin' ).'</h3>';
    $message .= '<p class="toast__msg">'.__( $text, 'mona-admin' ).'</p>';
    $message .= '</div>';
    $message .= '<div class="toast__close"><span class="dashicons dashicons-no"></span></div>';
    $message .= '<div class="progress"></div>';
    // result html
    return $message;
}