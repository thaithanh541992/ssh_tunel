<?php

if (class_exists('Kirki')) {

    /**
     * Add sections
     */
    function kirki_demo_scripts() {
        wp_enqueue_style('kirki-demo', get_stylesheet_uri(), array(), time());
    }

    add_action('wp_enqueue_scripts', 'kirki_demo_scripts');

    $priority = 1;

    /**
     * Add panel
     */
    Kirki::add_panel( 'panel_website', 
        [
            'title'     => __( '[MONA HOST]', 'mona-admin' ),
            'priority'   => $priority++,
            'capability' => 'edit_theme_options',
        ]
    );
   
    Kirki::add_section( 'section_header', 
        [
            'title'      => __('Đầu trang', 'mona-admin'),
            'priority'   => $priority++,
            'capability' => 'edit_theme_options',
            'panel'      => 'panel_website'
        ]
    );

    /**
     * Footer
     */
    Kirki::add_section( 'section_footer', 
        [
            'title'      => __('Chân trang', 'mona-admin'),
            'priority'   => $priority++,
            'capability' => 'edit_theme_options',
            'panel'      => 'panel_website'
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'image',
            'settings'    => 'section_footer_logo',
            'label'       => __( 'Logo chân trang', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_footer',
            'default'     => '',
            'priority'    => $priority++,
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'image',
            'settings'    => 'section_menu_logo',
            'label'       => __( 'Logo mobile menu', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_footer',
            'default'     => '',
            'priority'    => $priority++,
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'textarea',
            'settings'    => 'section_footer_description',
            'label'       => __( 'Mô tả', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_footer',
            'default'     => '',
            'priority'    => $priority++,
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'repeater',
            'settings'    => 'section_footer_corp_items',
            'label'       => __( 'Footer IMGs', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_footer',
            'default'     => '',
            'priority'    => $priority++,
            'row_label' => [
                'type'  => 'text',
                'value' => __( 'IMG', 'mona-admin' ),
            ],
            'button_label' => __( 'Thêm mới', 'mona-admin' ),
            'fields' => [
                'icon' => [
                    'type'        => 'image',
                    'label'       => __( 'Hình ảnh', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
                'link' => [
                    'type'        => 'text',
                    'label'       => __( 'Link đính kèm', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
                'attachment_class' => [
                    'type'        => 'text',
                    'label'       => __( 'Class mở rộng', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
                'target' => [
                    'type'        => 'checkbox',
                    'label'       => __( 'Mở tab', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
            ]
        ]
    );

    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'code',
            'settings'    => 'section_footer_code',
            'label'       => __( 'Code', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_footer',
            'default'     => '',
            'choices'     => [
                'language' => 'html',
            ],
            'priority'    => $priority++,
        ]
    );

    /**
     * Global
     */
    Kirki::add_section( 'section_global', 
        [
            'title'      => __('[Global]', 'mona-admin'),
            'priority'   => $priority++,
            'capability' => 'edit_theme_options',
            'panel'      => 'panel_website'
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'text',
            'settings'    => 'section_global_hotline',
            'label'       => __( 'Đường dây nóng', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_global',
            'default'     => '',
            'priority'    => $priority++,
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'text',
            'settings'    => 'section_global_zalo',
            'label'       => __( 'Đường dây ZALO OFFICIAL', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_global',
            'default'     => '',
            'priority'    => $priority++,
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'repeater',
            'settings'    => 'section_global_hotline_note',
            'label'       => __( 'Ghi chú', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_global',
            'default'     => '',
            'priority'    => $priority++,
            'row_label' => [
                'type'  => 'text',
                'value' => __( 'Note', 'mona-admin' ),
            ],
            'button_label' => __( 'Thêm mới', 'mona-admin' ),
            'fields' => [
                'title' => [
                    'type'        => 'textarea',
                    'label'       => __( 'Tiêu đề hiện thị', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
                'description' => [
                    'type'        => 'textarea',
                    'label'       => __( 'Mô tả hiện thị', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
                'attachment_class' => [
                    'type'        => 'text',
                    'label'       => __( 'Class mở rộng', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
            ]
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'repeater',
            'settings'    => 'section_global_socialnetwork',
            'label'       => __( 'Mạng xã hội', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_global',
            'default'     => '',
            'priority'    => $priority++,
            'row_label' => [
                'type'  => 'text',
                'value' => __( 'Social Network', 'mona-admin' ),
            ],
            'button_label' => __( 'Thêm mới', 'mona-admin' ),
            'fields' => [
                'icon' => [
                    'type'        => 'image',
                    'label'       => __( 'Icon', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
                'link' => [
                    'type'        => 'text',
                    'label'       => __( 'Link đính kèm', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
                'attachment_class' => [
                    'type'        => 'text',
                    'label'       => __( 'Class mở rộng', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
                'target' => [
                    'type'        => 'checkbox',
                    'label'       => __( 'Mở tab', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
            ]
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'repeater',
            'settings'    => 'section_global_info_items',
            'label'       => __( 'Thông tin', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_global',
            'default'     => '',
            'priority'    => $priority++,
            'row_label' => [
                'type'  => 'text',
                'value' => __( 'Info', 'mona-admin' ),
            ],
            'button_label' => __( 'Thêm mới', 'mona-admin' ),
            'fields' => [
                'icon' => [
                    'type'        => 'image',
                    'label'       => __( 'Hình ảnh', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
                'title' => [
                    'type'        => 'textarea',
                    'label'       => __( 'Tiêu đề', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
                'content' => [
                    'type'        => 'textarea',
                    'label'       => __( 'Chi tiết', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
                'link' => [
                    'type'        => 'text',
                    'label'       => __( 'Link đính kèm [nếu có]', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
                'attachment_class' => [
                    'type'        => 'text',
                    'label'       => __( 'Class mở rộng', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
                'target' => [
                    'type'        => 'checkbox',
                    'label'       => __( 'Mở tab', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
            ]
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'repeater',
            'settings'    => 'section_global_corp_items',
            'label'       => __( 'CORPs', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_global',
            'default'     => '',
            'priority'    => $priority++,
            'row_label' => [
                'type'  => 'text',
                'value' => __( 'Corp', 'mona-admin' ),
            ],
            'button_label' => __( 'Thêm mới', 'mona-admin' ),
            'fields' => [
                'icon' => [
                    'type'        => 'image',
                    'label'       => __( 'Hình ảnh', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
                'link' => [
                    'type'        => 'text',
                    'label'       => __( 'Link đính kèm', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
                'attachment_class' => [
                    'type'        => 'text',
                    'label'       => __( 'Class mở rộng', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
                'target' => [
                    'type'        => 'checkbox',
                    'label'       => __( 'Mở tab', 'mona-admin' ),
                    'description' => '',
                    'default'     => '',
                ],
            ]
        ]
    );

     /**
     * ADVISE
     */
    Kirki::add_section( 'section_advise', 
        [
            'title'      => __('Contact Form', 'mona-admin'),
            'priority'   => $priority++,
            'capability' => 'edit_theme_options',
            'panel'      => 'panel_website'
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'text',
            'settings'    => 'section_templatewebsite_shortcode',
            'label'       => __( 'TemplateWebsite Contactform Shortcode', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_advise',
            'default'     => '',
            'priority'    => $priority++,
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'text',
            'settings'    => 'section_advise_shortcode',
            'label'       => __( 'Contactform Shortcode', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_advise',
            'default'     => '',
            'priority'    => $priority++,
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'textarea',
            'settings'    => 'section_advise_title',
            'label'       => __( 'Tiêu đề [tư vấn]', 'mona-admin' ),
            'description' => '[] ' . __('Nội dung trong ngoặc sẽ được làm nổi bật', 'mona-admin'),
            'help'        => '',
            'section'     => 'section_advise',
            'default'     => '',
            'priority'    => $priority++,
        ]
    );

    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'text',
            'settings'    => 'section_solutions_shortcode',
            'label'       => __( 'Contactform Shortcode', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_advise',
            'default'     => '',
            'priority'    => $priority++,
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'textarea',
            'settings'    => 'section_solutions_title',
            'label'       => __( 'Tiêu đề [giải pháp]', 'mona-admin' ),
            'description' => '[] ' . __('Nội dung trong ngoặc sẽ được làm nổi bật', 'mona-admin'),
            'help'        => '',
            'section'     => 'section_advise',
            'default'     => '',
            'priority'    => $priority++,
        ]
    );
    /**
     * REGISTER
     */
    Kirki::add_section( 'section_register', 
        [
            'title'      => __('Đăng ký nhận thông tin', 'mona-admin'),
            'priority'   => $priority++,
            'capability' => 'edit_theme_options',
            'panel'      => 'panel_website'
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'textarea',
            'settings'    => 'section_register_title',
            'label'       => __( 'Tiêu đề', 'mona-admin' ),
            'description' => '[]' . __('Nội dung trong ngoặc vuông sẽ đổi màu','mona-admin'),
            'help'        => '',
            'section'     => 'section_register',
            'default'     => '',
            'priority'    => $priority++,
        ]
    );

     /**
     * REGISTER [2]
     */
    Kirki::add_section( 'section_register_2', 
        [
            'title'      => __('Đăng ký nhận thông tin [2]', 'mona-admin'),
            'priority'   => $priority++,
            'capability' => 'edit_theme_options',
            'panel'      => 'panel_website'
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'textarea',
            'settings'    => 'section_register_2_subtitle',
            'label'       => __( 'Tiêu đề phụ', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_register_2',
            'default'     => '',
            'priority'    => $priority++,
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'textarea',
            'settings'    => 'section_register_2_title',
            'label'       => __( 'Tiêu đề chính', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_register_2',
            'default'     => '',
            'priority'    => $priority++,
        ]
    );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'text',
            'settings'    => 'section_register_2_shortcode',
            'label'       => __( 'Shortcode', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_register_2',
            'default'     => '',
            'priority'    => $priority++,
        ]
    );
    // Kirki::add_field( 'mona_setting', 
    //     [
    //         'type'        => 'repeater',
    //         'settings'    => 'section_register_information',
    //         'label'       => __( 'Danh sách', 'mona-admin' ),
    //         'description' => '',
    //         'help'        => '',
    //         'section'     => 'section_register',
    //         'default'     => '',
    //         'priority'    => $priority++,
    //         'row_label' => [
    //             'type'  => 'text',
    //             'value' => __( 'Thông tin', 'mona-admin' ),
    //         ],
    //         'button_label' => __( 'Thêm mới', 'mona-admin' ),
    //         'fields' => [
               
    //             'detail' => [
    //                 'type'        => 'text',
    //                 'label'       => __( 'Chi tiết', 'mona-admin' ),
    //                 'description' => '',
    //                 'default'     => '',
    //             ],
               
    //         ]
    //     ]
    // );
    Kirki::add_field( 'mona_setting', 
        [
            'type'        => 'text',
            'settings'    => 'section_register_shortcode',
            'label'       => __( 'Shortcode', 'mona-admin' ),
            'description' => '',
            'help'        => '',
            'section'     => 'section_register',
            'default'     => '',
            'priority'    => $priority++,
        ]
    );

}

if ( ! function_exists ( 'mona_option' ) ) {

    /**
     * Undocumented function
     *
     * @param [type] $setting
     * @param string $default
     * @return void
     */
    function mona_option($setting, $default = '') {
        echo mona_get_option( $setting, $default );
    }

    /**
     * Undocumented function
     *
     * @param [type] $setting
     * @param string $default
     * @return void
     */
    function mona_get_option( $setting, $default = '' ) {

        if ( class_exists ( 'Kirki' ) ) {

            $value = $default;

            $options = get_option( 'option_name', array() );
            $options = get_theme_mod( $setting, $default );

            if ( isset ( $options ) ) {
                $value = $options;
            }

            return $value;
        }

        return $default;
    }

}
