<?php
add_action( 'after_setup_theme', 'add_after_setup_theme' );
function add_after_setup_theme() {
    // regsiter menu
    register_nav_menus(
        [
            'primary-menu'  => __( 'Menu Chính', 'monamedia' ),
            'policy-menu'   => __( 'Menu Chính sách', 'monamedia' ),
        ]
    );
    // add size image
    add_image_size( 'banner-desktop-image', 1920, 790, false );
    add_image_size( 'banner-mobile-image', 400, 675, false );
    add_image_size( 'square-s', 100, 100, false );
    add_image_size( 'square-m', 300, 300, false );
    add_image_size( 'square-l', 600, 600, false );
    add_image_size( 'thumbnail-post', 935, 705, false );
    add_image_size( 'thumbnail-taxonomy' , 935 , 705 , false);
    
    add_image_size( '400x999999' , 400 , 999999 , false);
    add_image_size( '1920x780' , 1920 , 780 , false);
    add_image_size( '960x390' , 960 , 390 , false);
}

add_action( 'wp_enqueue_scripts', 'mona_add_styles_scripts' );
function mona_add_styles_scripts() {
    // loading css
    wp_enqueue_style( 'mona-custom', get_template_directory_uri() . '/public/helpers/css/mona-custom.css', array(), _get_theme_type() );
    // load css / page 404
    MonaSettingNotFound()->__front_styles();
    // loading js
    wp_enqueue_script( 'mona-frontend', get_template_directory_uri() . '/public/helpers/scripts/mona-frontend.js', array(), _get_theme_type(), true );
    wp_localize_script('mona-frontend', 'mona_ajax_url', 
        [
            'ajaxURL' => admin_url('admin-ajax.php'),
            'siteURL' => get_site_url(),
        ]
    );
}

add_filter( 'script_loader_tag', 'mona_add_module_to_my_script', 10, 3 );
function mona_add_module_to_my_script( $tag, $handle, $src ) {
    if ( 'mona-frontend' === $handle ) {
        $tag = '<script type="module" src="' . esc_url( $src ) . '"></script>';
    }
    return $tag;
}

//add_action( 'wp_logout', 'mona_redirect_external_after_logout' );
function mona_redirect_external_after_logout() {
    wp_redirect( get_the_permalink( MONA_PAGE_HOME ) );
    exit();
}

add_filter( 'pre_get_posts', 'mona_parse_request_post_type' );
function mona_parse_request_post_type( $query ) {
    if ( ! is_admin() ) {
        $query->set('ignore_sticky_posts', true);
        $ptype = $query->get('post_type', true);
        $ptype = (array) $ptype;

        // if ( isset( $_GET['s'] ) ) {
        //     $ptype[] = 'post';
        //     $query->set('post_type', $ptype);
        //     $query->set( 'posts_per_page' , 12);
        // }

        // if ( $query->is_main_query() && $query->is_tax( 'category_library' ) ) {
        //     $ptype[] = 'mona_library';
        //     $query->set('post_type', $ptype);
        //     $query->set('posts_per_page', 12);
        // }

    }
    return $query;
}

function mona_register_sidebars() {
    register_sidebar( 
        [
            'id'            => 'sidebar_blog',
            'name'          => __( 'Blog', 'mona-admin' ),
            'description'   => __( 'Nội dung widget.', 'mona-admin' ),
            'before_widget' => '<div id="%1$s" class="widget sidebar-blog %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="mona-head-title mona-widget-title">',
            'after_title'   => '</div>',
        ]
    );
    register_sidebar( 
        [
            'id'            => 'sidebar_blog_detail',
            'name'          => __( 'Blog Detail [left]', 'mona-admin' ),
            'description'   => __( 'Nội dung widget.', 'mona-admin' ),
            'before_widget' => '<div id="%1$s" class="widget sidebar-blog-detail %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="mona-head-title mona-widget-title">',
            'after_title'   => '</div>',
        ]
    );
    register_sidebar( 
        [
            'id'            => 'sidebar_blog_detail_right',
            'name'          => __( 'Blog Detail [right]', 'mona-admin' ),
            'description'   => __( 'Nội dung widget.', 'mona-admin' ),
            'before_widget' => '<div id="%1$s" class="widget sidebar-blog-detail-right %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="mona-head-title mona-widget-title">',
            'after_title'   => '</div>',
        ]
    );
    register_sidebar( 
        [
            'id'            => 'footer_monamedia',
            'name'          => __( 'Footer MonaMedia', 'mona-admin' ),
            'description'   => __( 'Nội dung widget.', 'mona-admin' ),
            'before_widget' => '<div id="%1$s" class="widget footer-menu-item footer-menu-item-media %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="mona-head-title mona-widget-title">',
            'after_title'   => '</div>',
        ]
    );
    register_sidebar( 
        [
            'id'            => 'footer_monamedia_products',
            'name'          => __( 'Footer MonaMedia Products', 'mona-admin' ),
            'description'   => __( 'Nội dung widget.', 'mona-admin' ),
            'before_widget' => '<div id="%1$s" class="widget footer-menu-item footer-menu-item-products %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="mona-head-title mona-widget-title">',
            'after_title'   => '</div>',
        ]
    );
    register_sidebar( 
        [
            'id'            => 'footer_monamedia_customer_services',
            'name'          => __( 'Footer MonaMedia Customer Services', 'mona-admin' ),
            'description'   => __( 'Nội dung widget.', 'mona-admin' ),
            'before_widget' => '<div id="%1$s" class="widget footer-menu-item footer-menu-item-customer-services %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="mona-head-title mona-widget-title">',
            'after_title'   => '</div>',
        ]
    );
    register_sidebar( 
        [
            'id'            => 'footer_monamedia_development',
            'name'          => __( 'Footer MonaMedia Development', 'mona-admin' ),
            'description'   => __( 'Nội dung widget.', 'mona-admin' ),
            'before_widget' => '<div id="%1$s" class="widget footer-menu-item footer-menu-item-development %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="mona-head-title mona-widget-title">',
            'after_title'   => '</div>',
        ]
    );
    register_sidebar( 
        [
            'id'            => 'footer_monamedia_solution_1',
            'name'          => __( 'Footer MonaMedia Solution 1', 'mona-admin' ),
            'description'   => __( 'Nội dung widget.', 'mona-admin' ),
            'before_widget' => '<div id="%1$s" class="widget footer-menu-item footer-menu-item-solution-1 %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="mona-head-title mona-widget-title">',
            'after_title'   => '</div>',
        ]
    );
    register_sidebar( 
        [
            'id'            => 'footer_monamedia_solution_2',
            'name'          => __( 'Footer MonaMedia Solution 2', 'mona-admin' ),
            'description'   => __( 'Nội dung widget.', 'mona-admin' ),
            'before_widget' => '<div id="%1$s" class="widget footer-menu-item footer-menu-item-solution-2 %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="mona-head-title mona-widget-title">',
            'after_title'   => '</div>',
        ]
    );
    register_sidebar( 
        [
            'id'            => 'footer_policy',
            'name'          => __( 'Footer Policy', 'mona-admin' ),
            'description'   => __( 'Nội dung widget.', 'mona-admin' ),
            'before_widget' => '<div id="%1$s" class="widget footer-menu-item footer-menu-item-policy %2$s">',
            'after_widget'  => '</div>',
            'before_title'  => '<div class="mona-head-title mona-widget-title">',
            'after_title'   => '</div>',
        ]
    );
}
add_action( 'widgets_init', 'mona_register_sidebars' );

add_filter( 'display_post_states', 'mona_add_post_state', 10, 2 );
function mona_add_post_state( $post_states, $post ) {
    if ( $post->ID == MONA_PAGE_HOME ) {
        $post_states[] = __( 'PAGE - Trang chủ', 'mona-admin' );
    }
    if ( $post->ID == MONA_PAGE_BLOG ) {
        $post_states[] = __( 'PAGE - Trang Tin tức', 'mona-admin' );
    }
    return $post_states;
}

add_filter( 'get_custom_logo', 'mona_change_logo_class' );
function mona_change_logo_class( $html ) {
    $custom_logo_id = get_theme_mod( 'custom_logo' );
    $site_title = get_bloginfo( 'name' );
    $html           = sprintf(
        '<a href="%1$s" class="mona-logo" rel="home" itemprop="url">%2$s</a>',
        esc_url( home_url() ),
        wp_get_attachment_image( $custom_logo_id, 'full', false, 
            [
                'class'  => 'header-logo-image',
            ]
        ),
        $site_title
    );
    return $html;  
}

// add_filter( 'admin_url', 'mona_filter_admin_url', 999, 3 );
function mona_filter_admin_url( $url, $path, $blog_id ) {
    if ( $path === 'admin-ajax.php' && ! is_admin() ){
        $url.='?mona-ajax';
    }
    return $url;
}

add_filter( 'wp_get_attachment_image_attributes', 'mona_image_remove_attributes' );
function mona_image_remove_attributes( $attr ) {
    unset( $attr['srcset'] );
    unset( $attr['sizes'] );
    return $attr;
}

function enable_cookie_sessions() {
    if (!session_id()) {
        session_start([
            'cookie_lifetime' => 86400*365*10, // Set the cookie lifetime (in seconds) as needed => 10 years
            'cookie_secure' => isset($_SERVER['HTTPS']), // Set to true if using HTTPS
            'cookie_httponly' => true, // Enforce HTTP-only cookie
            'use_cookies' => true // Use cookies to store session ID
        ]);
    }
}
add_action('init', 'enable_cookie_sessions');


// hook woocomerce start here

add_filter( 'woocommerce_billing_fields', 'mona_custom_billing_fields' );
function mona_custom_billing_fields( $fields = [] ) {

    unset($fields['billing_first_name']);
    unset($fields['billing_last_name']);
    unset($fields['billing_phone']);
    unset($fields['billing_email']);
    unset($fields['billing_address_2']);
    // unset($fields['billing_country']);
    // unset($fields['billing_state']);
    // unset($fields['billing_city']);
    unset($fields['billing_postcode']);
    unset($fields['billing_company']);
    
    unset($fields['billing_email']['label']);
    unset($fields['billing_last_name']['label']);
    unset($fields['billing_phone']['label']);
    unset($fields['billing_address_1']['label']);

    /**Placeholder */
    $fields['billing_first_name']['placeholder'] = __( 'Tên của bạn', 'monamedia' );
    $fields['billing_address_1']['placeholder'] = __( 'Địa chỉ', 'monamedia' );
    $fields['billing_phone']['placeholder'] = __( 'Số điện thoại', 'monamedia' );
    $fields['billing_email']['placeholder'] = __( 'Email', 'monamedia' );
    $fields['billing_country']['placeholder'] = __( 'Quốc gia / khu vực', 'monamedia' );
    $fields['billing_state']['placeholder'] = __( 'Tỉnh / thành phố', 'monamedia' );
    $fields['billing_city']['placeholder'] = __( 'Quận / huyện', 'monamedia' );

    // required
    $fields['billing_email']['required'] = true;
    $fields['billing_phone']['required'] = true;
    $fields['billing_first_name']['required'] = true;

    $contain_domain = false;

    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        // $product = $cart_item['data'];
        $ptype = get_field('mona_product_type', $cart_item['product_id']);

        if ( $ptype == 'domain' ) {
            $contain_domain = true;
            break;
        }
    }

    if ( $contain_domain ) {
        $fields['billing_address_1']['required'] = true;
    } else {
        $fields['billing_address_1']['required'] = false;
    }

    return $fields;
}

// Hook in
add_filter( 'woocommerce_checkout_fields' , 'mona_override_checkout_fields' );
function mona_override_checkout_fields( $fields = [] ) {

    // unset($fields['shipping']['shipping_first_name']);
    // unset($fields['shipping']['shipping_last_name']);
    // unset($fields['shipping']['shipping_phone']);
    // unset($fields['shipping']['shipping_email']);
    // unset($fields['shipping']['shipping_address_2']);
    // unset($fields['shipping']['shipping_country']);
    // unset($fields['shipping']['shipping_state']);
    // unset($fields['shipping']['shipping_city']);
    // unset($fields['shipping']['shipping_postcode']);

    // unset($fields['shipping']['shipping_company']['label']);
    // unset($fields['shipping']['shipping_tax_num']['label']);
    // unset($fields['shipping']['shipping_address_1']['label']);

    // $fields['shipping']['shipping_company']['placeholder'] = __( 'Tên tổ chức', 'monamedia' );
    // $fields['shipping']['shipping_tax_num']['placeholder'] = __( 'Mã số thuế', 'monamedia' );
    // $fields['shipping']['shipping_address_1']['placeholder'] = __( 'Địa chỉ', 'monamedia' );

    // $fields['shipping']['shipping_company']['required'] = false;
    // $fields['shipping']['shipping_tax_num']['required']  = false;
    // $fields['shipping']['shipping_address_1']['required'] = false;

    // $priority = 1;
    // $checkout_fields['shipping']['shipping_company']['priority'] = $priority++;
    // $checkout_fields['shipping']['shipping_tax_num']['priority'] = $priority++;
    // $checkout_fields['shipping']['shipping_address_1']['priority'] = $priority++;

    $fields['billing']['billing_gender'] = array(
        'label'     => false,
        'placeholder'   => __('Giới tính', 'monamedia'),
        'required'  => false,
        'class'     => array('form-row-wide'),
        'clear'     => false
    );

    $fields['billing']['billing_birthday'] = array(
        'label'     => false,
        'placeholder'   => __('Ngày sinh', 'monamedia'),
        'required'  => false,
        'class'     => array('form-row-wide'),
        'clear'     => false
    );

    $fields['billing']['billing_id_number'] = array(
        'label'     => false,
        'placeholder'   => __('CMND/CCCD', 'monamedia'),
        'required'  => false,
        'class'     => array('form-row-wide'),
        'clear'     => false
    );
    
    $fields['billing']['billing_ward'] = array(
        'label'     => false,
        'placeholder'   => __('Phường / xã', 'monamedia'),
        'required'  => false,
        'class'     => array('form-row-wide'),
        'clear'     => false
    );

    $contain_domain = false;

    foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
        // $product = $cart_item['data'];
        $ptype = get_field('mona_product_type', $cart_item['product_id']);

        if ( $ptype == 'domain' ) {
            $contain_domain = true;
            break;
        }
    }

    if ( $contain_domain ) {
        $fields['billing']['billing_gender']['required'] = true;
        $fields['billing']['billing_birthday']['required'] = true;
        $fields['billing']['billing_id_number']['required'] = true;
        $fields['billing']['billing_ward']['required'] = true;
    }


    return $fields;
}

add_filter( 'woocommerce_checkout_fields',  'mona_and_order_fields_checkout');
function mona_and_order_fields_checkout( $checkout_fields = [] ) {
    $priority = 1;
    $checkout_fields['billing']['billing_gender']['priority'] = $priority++;
    $checkout_fields['billing']['billing_first_name']['priority'] = $priority++;
    $checkout_fields['billing']['billing_birthday']['priority'] = $priority++;
    $checkout_fields['billing']['billing_id_number']['priority'] = $priority++;
    $checkout_fields['billing']['billing_email']['priority'] = $priority++;
    $checkout_fields['billing']['billing_phone']['priority'] = $priority++;
    $checkout_fields['billing']['billing_country']['priority'] = $priority++;
    $checkout_fields['billing']['billing_state']['priority'] = $priority++;
    $checkout_fields['billing']['billing_city']['priority'] = $priority++;
    $checkout_fields['billing']['billing_ward']['priority'] = $priority++;
    $checkout_fields['billing']['billing_address_1']['priority'] = $priority++;

    return $checkout_fields;
}

add_action( 'woocommerce_admin_order_data_after_billing_address', 'mona_checkout_field_display_admin_order_meta', 10, 1 );

function mona_checkout_field_display_admin_order_meta($order){
    echo '<p><strong>'.__('Phường xã').':</strong> ' . get_post_meta( $order->get_id(), '_billing_ward', true ) . '</p>';
}

// Save custom field data to order
add_action('woocommerce_checkout_create_order', 'save_custom_field_to_order');

function save_custom_field_to_order($order) {

    if ( isset($_POST['billing_birthday']) && !empty( $_POST['billing_birthday'] ) ) {
        $dateString = sanitize_text_field($_POST['billing_birthday']);
        $formattedDate = date('d/m/Y', strtotime($dateString));
        $order->update_meta_data('_billing_birthday', $formattedDate );
    }

    $custom_key_arr = monaghost_get_custom_field_key();
    $is_organization = true;

    if ( !empty( $custom_key_arr ) && is_array( $custom_key_arr ) ) {
        foreach ( $custom_key_arr as $key => $value) {
            if ( isset($_POST[$key]) ) {
                if ( empty( $_POST[$key] ) ) {
                    $is_organization = false;
                } else {
                    $order->update_meta_data('_' . $key , $_POST[$key] );
                }
            }
        }
    }

    $order->update_meta_data('_is_organization', $is_organization );
}

// move cart item custom data to order item custom data
add_action('woocommerce_checkout_create_order_line_item', 'move_custom_data_to_order_item', 10, 4);

function move_custom_data_to_order_item($item, $cart_item_key, $values, $order)
{
    if (isset($values['custom_data'])) {
        $custom_data = $values['custom_data'];
        $item->add_meta_data('custom_data', $custom_data);
    }
}

// Modify order item information in admin
// add_filter('woocommerce_admin_order_item_values', 'modify_order_item_information', 10, 3);

// function modify_order_item_information($order_item_values, $order, $item)
// {
//     // Retrieve custom data from order item meta
//     $custom_data = get_post_meta('custom_data', $item);

//     // Modify the order item values
//     $order_item_values['custom_data'] = array(
//         'label' => __('Custom Data', 'woocommerce'),
//         'value' => $custom_data,
//     );

//     return $order_item_values;
// }

// Add custom column headers here
add_action('woocommerce_admin_order_item_headers', 'my_woocommerce_admin_order_item_headers');
function my_woocommerce_admin_order_item_headers() {
    // set the column name
    $column_name = __('Additional information');

    // display the column name
    echo '<th>' . $column_name . '</th>';
}

// Add custom column values here
add_action('woocommerce_admin_order_item_values', 'my_woocommerce_admin_order_item_values', 10, 3);
function my_woocommerce_admin_order_item_values($_product, $item, $item_id) {
    // get the post meta value from the associated product
    $custom_data = $item['custom_data'];

    if ( !empty( $custom_data ) ) {
        if ( !empty( $custom_data['billingcycle'] ) ) {
            // display the value
            echo '<td><strong>' . __('Thời gian sử dụng: ', 'monamedia') . '</strong>' . $custom_data['billingcycle'] . __(' tháng', 'monamedia') . '</td>';
        }
    }

}

// Modify display of order item meta key (attribute)
add_filter('woocommerce_order_item_display_meta_key', 'modify_order_item_display_attribute', 10, 3);

function modify_order_item_display_attribute($display_key, $meta_key, $item)
{
    if ($meta_key === 'custom_data') {
        $display_key = __('Custom Data', 'woocommerce');
    }

    return $display_key;
}

/**
 * remove form cupon checkout
 */
add_action( 'woocommerce_before_checkout_form', 'remove_checkout_coupon_form', 9 );
function remove_checkout_coupon_form(){
    remove_action( 'woocommerce_before_checkout_form', 'woocommerce_checkout_coupon_form', 10 );
}

// Set the custom cart item calculated price and quantity
add_action( 'woocommerce_before_calculate_totals', 'mona_add_custom_price_and_quantity' );
function mona_add_custom_price_and_quantity( $cart_object ){
    if ( is_admin() && ! defined( 'DOING_AJAX' ) )
        return;

    if ( did_action( 'woocommerce_before_calculate_totals' ) >= 2 )
        return;

    foreach ( $cart_object->get_cart() as $cart_item_key => $cart_item ) {
        // $product = $cart_item['data'];
        $ptype = get_field('mona_product_type', $cart_item['product_id']);
        $calculated_price = 0;

        if ( $ptype == 'domain' && !empty( $cart_item['custom_data']['domain_name'] ) ) {
            $cart_item['data']->set_name( $cart_item['custom_data']['domain_name'] );
        }

        $mona_billing_cycle_list = get_field('mona_billing_cycle_list', $cart_item['product_id']);
        if( !empty( $mona_billing_cycle_list ) && is_array( $mona_billing_cycle_list ) ) {
            foreach ( $mona_billing_cycle_list as $billing_cycle ) {
                if ( $cart_item['custom_data']['billingcycle'] == $billing_cycle['term_quantity'] ) {
                    $calculated_price = $billing_cycle['price'];
                    break;
                }
            }
        }

        $cart_item['data']->set_price( $calculated_price );
        $cart_object->set_quantity( $cart_item_key, 1 );
    }
}

// remove select2 woocommerce
add_action( 'wp_enqueue_scripts', 'mona_dequeue_stylesandscripts', 100 );

function mona_dequeue_stylesandscripts() {
    if ( class_exists( 'woocommerce' ) ) {
        wp_dequeue_style( 'select2' );
        wp_deregister_style( 'select2' );
    } 
} 