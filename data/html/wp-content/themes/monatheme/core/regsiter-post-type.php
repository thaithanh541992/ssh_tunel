<?php 
function mona_regsiter_custom_post_types() {

    $posttype_video = [
        'labels' => [
            'name'          => 'Video',
            'singular_name' => 'Video',
            'all_items'     => __( 'Tất cả bài viết', 'mona-admin' ),
            'add_new'       => __( 'Viết bài mới', 'mona-admin' ),
            'add_new_item'  => __( 'Bài viết mới', 'mona-admin' ),
            'edit_item'     => __( 'Chỉnh sửa bài viết', 'mona-admin' ),
            'new_item'      => __( 'Thêm bài viết', 'mona-admin' ),
            'view_item'     => __( 'Xem bài viết', 'mona-admin' ),
            'view_items'    => __( 'Xem bài viết', 'mona-admin' ),
        ],
        'description' => 'Thêm bài viết',
        'supports'    => [
            'title',
            'editor',
            'author',
            'thumbnail',
            'comments',
            'revisions',
            'custom-fields',
            'excerpt',
        ],
        'taxonomies'   => array( 'category' ),
        'hierarchical' => false,
        'show_in_rest' => true,
        'public'       => true,
        'has_archive'  => true,
        'rewrite'      => [
            'slug' => 'video',
            'with_front' => true
        ],
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-video-alt3',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'capability_type'     => 'post'
    ];
    register_post_type( 'mona_video', $posttype_video );

    $posttype_service = [
        'labels' => [
            'name'          => 'Dịch vụ',
            'singular_name' => 'Dịch vụ',
            'all_items'     => __( 'Tất cả bài viết', 'mona-admin' ),
            'add_new'       => __( 'Viết bài mới', 'mona-admin' ),
            'add_new_item'  => __( 'Bài viết mới', 'mona-admin' ),
            'edit_item'     => __( 'Chỉnh sửa bài viết', 'mona-admin' ),
            'new_item'      => __( 'Thêm bài viết', 'mona-admin' ),
            'view_item'     => __( 'Xem bài viết', 'mona-admin' ),
            'view_items'    => __( 'Xem bài viết', 'mona-admin' ),
        ],
        'description' => 'Thêm bài viết',
        'supports'    => [
            'title',
            'editor',
            'author',
            'thumbnail',
            'comments',
            'revisions',
            'custom-fields',
            'excerpt',
        ],
        'taxonomies'   => array(),
        'hierarchical' => false,
        'show_in_rest' => true,
        'public'       => true,
        'has_archive'  => true,
        'rewrite'      => [
            'slug' => 'video',
            'with_front' => true
        ],
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-video-alt3',
        'can_export'          => true,
        'has_archive'         => true,
        'exclude_from_search' => true,
        'publicly_queryable'  => false,
        'capability_type'     => 'post'
    ];
    register_post_type( 'mona_service', $posttype_service );
  
    $tax_filters = array(
        'labels' => array(
            'name' => __('Danh mục tên miền', 'monamedia'),
            'singular_name' => __('Danh mục tên miền', 'monamedia'),
            'search_items' => __('Tìm kiếm', 'monamedia'),
            'all_items' => __('Tất cả', 'monamedia'),
            'parent_item' => __('Danh mục tên miền', 'monamedia'),
            'parent_item_colon' => __('Danh mục tên miền', 'monamedia'),
            'edit_item' => __('Chỉnh sửa', 'monamedia'),
            'add_new' => __('Thêm mới', 'monamedia'),
            'update_item' => __('Cập nhật', 'monamedia'),
            'add_new_item' => __('Thêm mới', 'monamedia'),
            'new_item_name' => __('Thêm mới', 'monamedia'),
            'menu_name' => __('Danh mục tên miền', 'monamedia'),
        ),
        'hierarchical' => true,
        'show_admin_column' => true,
        'show_in_rest'=>true,
        'has_archive' => true,
        'public' => true,
        'rewrite' => array(
            'slug' => 'danh-muc-ten-mien',
            'with_front' => true
        ),
        'capabilities' => array(
            'manage_terms' => 'publish_posts',
            'edit_terms' => 'publish_posts',
            'delete_terms' => 'publish_posts',
            'assign_terms' => 'publish_posts',
        ),
    );
    register_taxonomy('domain_cat', 'product', $tax_filters);

    flush_rewrite_rules();
}
add_action( 'init', 'mona_regsiter_custom_post_types' );