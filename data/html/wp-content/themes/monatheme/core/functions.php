<?php
/**
 * Undocumented function
 * Phân trang link
 * @param string $wp_query
 * @return void
 */
function mona_pagination_links( $wp_query = '' ) {
	if ( $wp_query == '' ) {
	    global $wp_query;
	}
    $bignum = 999999999;
    if ( $wp_query->max_num_pages <= 1 ) {
        return;
    }  
    echo paginate_links(
        [
            'base'      => str_replace( $bignum, '%#%', esc_url( get_pagenum_link( $bignum ) ) ),
            'format'    => '',
            'current'   => max( 1, get_query_var( 'paged' ) ),
            'total'     => $wp_query->max_num_pages,
            'prev_text' => '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
            'next_text' => '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
            'type'      => 'list',
            'end_size'  => 3,
            'mid_size'  => 3
        ]
    );
}

function mona_pagination_links_target( $wp_query = '' ) {
	if ( $wp_query == '' ) {
	    global $wp_query;
	}
    $bignum = 999999999;
    if ( $wp_query->max_num_pages <= 1 ) {
        return;
    }  
    echo paginate_links(
        [
            'base'      => str_replace( $bignum, '%#%', esc_url( get_pagenum_link( $bignum ) ) ) . '#posts',
            'format'    => '',
            'current'   => max( 1, get_query_var( 'paged' ) ),
            'total'     => $wp_query->max_num_pages,
            'prev_text' => '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
            'next_text' => '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
            'type'      => 'list',
            'end_size'  => 3,
            'mid_size'  => 3
        ]
    );
}

function mona_pagination_links_active( $wp_query = '' , $active , $argsPost = [] ) {
	if ( $wp_query == '' ) {
	    global $wp_query;
	}
    $bignum = 999999999;
    if ( $wp_query->max_num_pages <= 1 ) {
        return;
    }  

    if( !empty( $argsPost ) || !empty( $active ) ){
        $base = array(
            'paged' => '%#%',
            'active' => $active,
        );
    }else{
        $base = array(
            'paged' => '%#%',
        );
    }

    echo paginate_links(
        [
            // 'base' => @add_query_arg(
            //     $base
            // ),
            'base'      => str_replace( $bignum, '%#%', esc_url( get_pagenum_link( $bignum ) ) ) . '#monaPostList',
            'format'    => '',
            'current'   => max( 1, get_query_var( 'paged' ) ),
            'total'     => $wp_query->max_num_pages,
            'prev_text' => '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
            'next_text' => '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
            'type'      => 'list',
            'end_size'  => 3,
            'mid_size'  => 3
        ]
    );
}

function mona_pagination_links_ajax( $wp_query = '' , $paged = 1 ) {
	if ( $wp_query == '' ) {
	    global $wp_query;
	}
    $bignum = 999999999;
    if ( $wp_query->max_num_pages <= 1 ) {
        return;
    }  
    echo paginate_links(
        [
            'base'      => str_replace( $bignum, '%#%', esc_url( get_pagenum_link( $bignum ) ) ),
            'format'    => '',
            'current'   => !empty( $paged ) ? $paged : max( 1, get_query_var( 'paged' ) ),
            'total'     => $wp_query->max_num_pages,
            'prev_text' => '<i class="fa fa-arrow-left" aria-hidden="true"></i>',
            'next_text' => '<i class="fa fa-arrow-right" aria-hidden="true"></i>',
            'type'      => 'list',
            'end_size'  => 3,
            'mid_size'  => 3
        ]
    );
}

/**
 * Undocumented function
 * Trả về giá trị dạng số
 * @param [type] $value_num
 * @return void
 */
function mona_replace( $value_num = '' ) {
    if ( empty ( $value_num ) ) {
        return;
    }
    $string   = preg_replace( '/\s+/','',$value_num );
    $stringaz = preg_replace( '/[^a-zA-Z0-9_ -]/s', '', $string );
    return $stringaz;
}

/**
 * Undocumented function
 * Trả về format phone
 * @param [type] $hotline
 * @return void
 */
function mona_replace_tel( $hotline = '' ) {
    if ( empty ( $hotline ) ) {
        return;
    }
    $string   = preg_replace( '/\s+/','',$hotline );
    $stringaz = preg_replace( '/[^0-9_ -]/s', '', $string );
    $tel = 'tel:'.$stringaz;
    return $tel;

}

/**
 * Undocumented function
 * Lấy danh sách cate ids của post
 * @param string $postId
 * @param string $taxonomy
 * @return void
 */
function get_post_term_ids( $postId = '', $taxonomy = 'category' ) {
    global $array_ids;
    if ( $postId == '') {
        $postId = get_the_ID();
    }
    $term_list = wp_get_post_terms( $postId, $taxonomy );
    if ( ! empty ( $term_list ) ) {

        foreach ( $term_list as $item ) {
            $array_ids[] = $item->term_id;
        }
    } else {
        return;
    }
    return $array_ids;
}

/**
 * Undocumented function
 * tạo meta lượt xem cho post
 * @param [type] $postId
 * @return void
 */
function mona_set_post_view( $postId = '' ) {
    if ( empty ( $postId ) ) {
        $postId = get_the_ID();
    }
    $count_key = '_mona_post_view';
    $count = get_post_meta( $postId, $count_key, true );
    if ( $count == '' ) {
        $count = 0;
        delete_post_meta( $postId, $count_key );
        add_post_meta( $postId, $count_key, '0' );
    } else {
        $count++;
        update_post_meta( $postId, $count_key, $count );
    }
}

/**
 * Undocumented function
 * lấy meta lượt xem cho post
 * @param [type] $postId
 * @return void
 */
function mona_get_post_view( $postId = '' ) {
    if ( empty ( $postId ) ) {
        $postId = get_the_ID();
    }
    $count_key = '_mona_post_view';
    $count = get_post_meta( $postId, $count_key, true );
    if ( $count == '' ) {
        delete_post_meta( $postId, $count_key );
        add_post_meta( $postId, $count_key, '0' );
        return 0;
    }

    return $count;

}

/**
 * Undocumented function
 * lấy tiêu đề trang chủ 
 * @return void
 */
function mona_get_home_title() {
    $home_title = get_the_title( get_option('page_on_front') );
    if ( $home_title && $home_title !='' ) {
        $result_title = $home_title;
    } else {
        $result_title = __( 'Trang chủ', 'mona-admin' );
    }
    return $result_title;
}

/**
 * Undocumented function
 * lấy tiêu đề trang blogs
 * @return void
 */
function mona_get_blogs_title() {
    $blogs_title = get_the_title( get_option('page_for_posts', true) );
    if ( $blogs_title && $blogs_title !='' ) {
        $result_title = $blogs_title;
    } else {
        $result_title = __( 'Tin tức', 'mona-admin' );
    }
    return $result_title;
}

/**
 * Undocumented function
 * lấy url trang blogs
 * @return void
 */
function mona_get_blogs_url() {
    $blogs_url = get_the_permalink( get_option( 'page_for_posts', true ) );
    return esc_url( $blogs_url );
}

/**
 * Undocumented function
 * lấy sách các cate cha của cate hiện tại
 * trả về khối html
 * @param [type] $term_id
 * @param [type] $taxonomy
 * @param array $args
 * @return void
 */
function breadcrumb_terms_list_html( $term_id, $taxonomy, $args = array() ) {
    $list = '';
    $term = get_term( $term_id, $taxonomy );
    if ( is_wp_error( $term ) ) {
        return $term;
    }
    if ( ! $term ) {
        return $list;
    }
    $term_id  = $term->term_id;
    $defaults = [
        'format'    => 'name',
        'separator' => '',
        'link'      => true,
        'inclusive' => true,
    ];
    $args = wp_parse_args( $args, $defaults );
    foreach ( array( 'link', 'inclusive' ) as $bool ) {
        $args[ $bool ] = wp_validate_boolean( $args[ $bool ] );
    }
    $parents = get_ancestors( $term_id, $taxonomy, 'taxonomy' );
    if ( $args['inclusive'] ) {
        array_unshift( $parents, $term_id );
    }
    $obz = get_queried_object();
    foreach ( array_reverse( $parents ) as $term_id ) {
        $parent = get_term( $term_id, $taxonomy );
        $name   = ( 'slug' === $args['format'] ) ? $parent->slug : $parent->name;
        if ( $obz->term_id != $term_id && $parent->parent == 0 ) {
            if ( $args['link'] ) {
                $list .= '<li class="breadcrumb-list"><a class="item" href="' . esc_url( get_term_link( $parent->term_id, $taxonomy ) ) . '">' . $name . '</a></li>' . $args['separator'];
            } else {
                $list .= $name . $args['separator'];
            }
        } else {
            $list .= '<li class="breadcrumb-list active"><a class="item" href="' . esc_url( get_term_link( $parent->term_id, $taxonomy ) ) . '">' . $name . '</a></li>' . $args['separator'];
        }
    }
    return $list;
}

/**
 * Undocumented function
 * lấy image id theo url
 * @param [type] $image_url
 * @return void
 */
function mona_get_image_id_by_url( $image_url = '' ) {
    if ( empty ( $image_url ) ) {
        return;
    }
	global $wpdb;
	$attachment = $wpdb->get_col( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE guid='%s';", $image_url ) );
    if ( ! empty ( $attachment ) ) {
        return $attachment[0];
    }
}

/**
 * Undocumented function
 *
 * @param string $term_id
 * @param string $taxonomy
 * @return boolean
 */
function is_terms_active( $term_id = '', $taxonomy = 'category' ) {
    $termsObj = get_the_terms( get_the_ID(), $taxonomy );
    $count = 0;
    if ( empty( $termsObj ) ) {
        return $count;
    } else {
        foreach ( $termsObj as $key => $item ) {
            if ( $item->term_id === $term_id ) {
                $count++;
            }
        }
    }
    return $count;
}

/**
 * Undocumented function
 *
 * @param string $method
 * @param string $value
 * @return void
 */
function mona_checked( $method = '', $value = '' ) {
    if ( isset( $method ) && is_array( $method ) || is_object( $method ) ) {
        foreach ( $method as $key => $item ) {
            if ( $item === $value ) {
                $checked = "checked='checked'";
                return $checked;
            }
        }
    } elseif ( ! empty ( $method ) && ! is_array( $method ) ) {
        if ( $method === $value ) {
            $checked = "checked='checked'";
            return $checked;
        }
    } else {
        $checked = '';
        return $checked;
    }
}

/**
 * Undocumented function
 * lấy cấp bậc của cate hiện tại
 * @param integer $term_id
 * @param string $taxonomy_type
 * @return void
 */
function _term_get_ancestors_count( $term_id = '', $taxonomy_type = 'category' ) {
    if ( $term_id == '' ) {
        return;
    }
    $ancestors = get_ancestors( $term_id, $taxonomy_type );
    return isset ( $ancestors ) ? (count( $ancestors ) + 1) : 1;
}

/**
 * Undocumented function
 * kiếm trang xem string/array đó có rỗng hay không
 * @param array $content_args
 * @return boolean
 */
function content_exists( $content_args = [] ) {
    if ( ! empty ( $content_args ) ) {
        $done  = 0;
        $total = count( $content_args );
        foreach ( $content_args as $key => $value ) {
            if ( ! is_array( $value ) && $value != '' || is_array( $value ) && content_exists( $value ) ) {
                $done++;
            }
        }
        if ( isset ( $done ) && $done > 0 ) {
            return true;
        } else {
            return false;
        }
    } else {
        return false;
    }
}

function show( $args ) {
    if ( get_current_user_id() == 1 ) {
        echo '<pre>';
        print_r ( $args );
        echo '</pre>';
    }
}

function text_front( $text = '' ) {
    return __( $text, 'monamedia' );
}

function e_text_front( $text = '' ) {
    echo __( $text, 'monamedia' );
}

function text_admin( $text = '' ) {
    return __( $text, 'mona-admin' );
}

function e_text_admin( $text = '' ) {
    echo __( $text, 'mona-admin' );
}

function get_taxonomy_term_root( $taxonomy_object ) {
	if ( empty( $taxonomy_object ) ) {
		return;
	}
    if( $taxonomy_object->parent != 0 ){
        $taxonomy_object_parent = get_term_by('id', $taxonomy_object->parent, $taxonomy_object->taxonomy );
        return get_taxonomy_term_root( $taxonomy_object_parent );
    }else{
        return $taxonomy_object;
    }
}

function get_post_primary_taxonomy($post_id, $term='category', $return_all_categories=false){
    $return = array();

    if (class_exists('WPSEO_Primary_Term')){
        // Show Primary category by Yoast if it is enabled & set
        $wpseo_primary_term = new WPSEO_Primary_Term( $term, $post_id );
        $primary_term = get_term($wpseo_primary_term->get_primary_term());

        if (!is_wp_error($primary_term)){
            $return['primary_category'] = $primary_term;
        }
    }

    if (empty($return['primary_category']) || $return_all_categories){
        $categories_list = get_the_terms($post_id, $term);

        if (empty($return['primary_category']) && !empty($categories_list)){
            $return['primary_category'] = $categories_list[0];  //get the first category
        }
        if ($return_all_categories){
            $return['all_categories'] = array();

            if (!empty($categories_list)){
                foreach($categories_list as &$category){
                    $return['all_categories'][] = $category->term_id;
                }
            }
        }
    }

    return $return;
}

/**
* Retrieve a post given its title.
*
* @uses $wpdb
*
* @param string $post_title Page title
* @param string $post_type post type ('post','page','any custom type')
* @param string $output Optional. Output type. OBJECT, ARRAY_N, or ARRAY_A.
* @return mixed
*/
function get_post_by_title($page_title, $post_type ='post' , $output = OBJECT) {
    global $wpdb;
        $post = $wpdb->get_var( $wpdb->prepare( "SELECT ID FROM $wpdb->posts WHERE post_title = %s AND post_type= %s", $page_title, $post_type));
        if ( $post )
            return get_post($post, $output);
 
    return null;
}

function mona_get_hotline() {
    
    // ob_start();
    ?>
    <div class="hd-contact hd-menu">
        <div class="menu">
            <div class="menu-list d-flex f-center">
                <div class="menu-item">
                    <div class="menu-item mini-cart open-modal-js">
                        <a href="javascript:;" class="menu-item-link">
                            <span class="menu-item-link-w">
                                <span class="menu-item-link-ani">
                                    <span class="menu-item-link-icon">
                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/header/icon-cart-white.svg" alt="" class="icon">
                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/header/icon-cart.svg" alt="" class="icon">
                                    </span>
                                    <!-- Giỏ hàng -->
                                </span>
                            </span>

                            <span class="mini-cart-qty mini-cart-qty-js">
                                <?php echo WC()->cart->get_cart_contents_count(); ?>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="mini-cart show-on-mobile open-modal-js">
        <a href="javascript:;" class="menu-item-link">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/header/icon-cart-white.svg" alt="" class="icon">

            <span class="mini-cart-qty mini-cart-qty-js">
                <?php echo WC()->cart->get_cart_contents_count(); ?>
            </span>
        </a>
    </div>
    <?php
    return '';
}

function process_buy_domain( $order_id ) {
    $order = wc_get_order( $order_id );

    $domain_arr = [];

    $order_data = $order->get_data();

    echo "<pre>";
    var_dump( $order_data );
    echo "</pre>";

    // iterating through each order item in the order
    foreach( $order->get_items() as $item_id => $item ) {
        // product ID
        $item_data = $item->get_data();

        // var_dump( $item_data );

        $product_id = $item_data['product_id'];

        $ptype = get_field('mona_product_type', $product_id);

        if ( $ptype == 'domain' ) {
            $domain_arr[] = $item_data['name'];
        }
    }

    // personal
    $fullname = $order_data['billing']['first_name'];
    $email = $order_data['billing']['email'];
    $country = $order_data['billing']['country'];
    $province = $order_data['billing']['state'];
    $district = $order_data['billing']['city'];
    $ward = $order->get_meta('_billing_ward');
    $phone = $order_data['billing']['phone'];
    $gender = $order->get_meta('_billing_gender');
    $idNumber = $order->get_meta('_billing_id_number');
    $birthday = $order->get_meta('_billing_birthday');

    // organization
    $organization_fullname = $order->get_meta('_organization_first_name');
    $organization_id_number = $order->get_meta('_organization_id_number');
    $organization_email = $order->get_meta('_organization_email');
    $organization_phone = $order->get_meta('_organization_phone');
    $organization_country = $order->get_meta('_organization_country');
    $organization_state = $order->get_meta('_organization_state');
    $organization_city = $order->get_meta('_organization_city');
    $organization_ward = $order->get_meta('_organization_ward');
    $organization_address_1 = $order->get_meta('_organization_address_1');
    $organization_state = $order->get_meta('_organization_state');

    if ( !empty( $domain_arr ) ) {
        foreach( $domain_arr as $domain ) {
            // $order->add_order_note($domain);
        }
    }
}