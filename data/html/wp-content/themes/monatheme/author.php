<?php
/**
 * The template for displaying taxonomy.
 *
 * @package Monamedia
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header(); ?>

<?php 
$author_id = get_the_author_meta('ID');
$author = get_userdata($author_id);
$author_avatar                  =   get_field('mona_user_avatar', $author);
$author_display                 =   get_field('mona_user_display_name', $author);
$author_display_description     =   get_field('mona_user_display_description', $author);
$author_display_content         =   get_field('mona_user_display_content', $author);
$author_display_socialnetworks  =   get_field('mona_user_socialnetworks', $author);
$current_paged = max( 1, get_query_var('paged') );
?>
<main class="main">
    <?php if( $current_paged == 1 ){ ?>

        <!-- SECTION AUTHOR ONE -->
        <div class="author-one over-hidden">
            <div class="container">
                <div class="dsmall author-one-side">
                    <div class="dsmall-item author-one-left">
                        <div class="author-one-content over-hidden">
                            <h3 class="author-one-name" data-aos="fade-up">
                                <span class="text">
                                    <?php echo !empty($author_display) ? $author_display : $author->display_name; ?>
                                </span>
                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/author-one-tick.svg" alt="">
                            </h3>

                            <?php if( !empty( $author_display_description ) ){ ?>
                            <div class="author-one-tag">
                                <div class="author-one-tag-item" data-aos="fade-up">
                                    <?php echo $author_display_description; ?>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if( !empty( $author_display_content ) ){ ?>
                            <div class="author-one-des" data-aos="fade-up">
                            <?php echo $author_display_content; ?>
                            </div>
                            <?php } ?>

                            <?php if( !empty( $author_display_socialnetworks ) ){ ?>
                            <div class="author-social" data-aos="fade-up">

                                <?php foreach ($author_display_socialnetworks as $key => $item) {
                                    if( !empty( $item['link']['url'] ) && $item['icon'] ){ ?>
                                <a href="<?php echo $item['link']['url']; ?>" target="<?php echo $item['link']['target']; ?>" class="author-social-link">
                                    <?php echo wp_get_attachment_image($item['icon'], 'square-s'); ?>
                                </a>
                                <?php } } ?>

                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="dsmall-item author-one-right">
                        <div class="author-one-img">
                            <div class="author-one-img-inner" data-aos="fade-left" data-aos-delay="800" data-aos-duration="600">
                                <div class="img">
                                    <?php $author_avatar_personal = get_field('mona_user_avatar_personal', $author);
                                    if( !empty( $author_avatar_personal ) ){ ?>
                                        <?php echo wp_get_attachment_image($author_avatar_personal, 'full'); ?>
                                    <?php } ?>
                                </div>
                            </div>
                            <svg class="author-one-line ani" viewBox="0 0 341 152" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M283.602 149.745C279.736 106.275 231.539 99.9144 197.306 104.053C167.59 107.645 139.146 117.475 109.178 120.472C80.0407 123.386 33.1973 131.33 9.31459 108.55C-7.07194 92.9198 7.04072 69.8492 20.7926 58.6605C41.0636 42.1677 66.7514 32.2524 91.4087 24.5795C133.055 11.62 176.592 4.9107 220.114 2.92295C248.617 1.62113 278.161 1.80751 306.203 7.94526C315.973 10.0837 339.09 16.2311 339.461 30.4096C339.917 47.8509 307.296 59.5788 296.094 64.3992C277.479 72.41 257.936 78.4064 238.596 84.3936"
                                    stroke="#FFA51F" stroke-width="3" stroke-linecap="round" class="line"></path>
                            </svg>
                            <div class="author-one-cirle">
                                <div class="inner" data-aos="fade-up" data-aos-duration="600" data-aos-delay="400">
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/author-one-circle.png" alt="">
                                </div>
                            </div>
                            <div class="author-one-bot" data-aos="fade-up" data-aos-duration="600">
                                <div class="author-one-cirle-blue">
                                    <div class="inner"></div>
                                </div>
                                <div class="author-one-cirle-pink">
                                    <div class="inner"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- AUTHOR TWO -->
        <?php $author_wall_personal = get_field('mona_user_wall_personal', $author);
        if( !empty( $author_wall_personal ) ){ ?>
        <div class="author-two parallaxSecJS">
            <div class="author-two-img parallaxJS">
                <?php echo wp_get_attachment_image($author_wall_personal, 'full'); ?>
            </div>
        </div>
        <?php } ?>

        <!-- AUTHOR THREE -->
        <?php 
        $post_type = 'mona_video';
        $posts_per_page = -1;
        $argsPost = [
            'post_type' => $post_type,
            'post_status' => 'publish',
            'posts_per_page' => $posts_per_page,
            'meta_query' => [
                'relation' => 'AND',
                [
                    'key' => 'mona_video_attachment_link',
                    'value' => '',
                    'compare' => '!=',
                ]
            ],
            'tax_query' => [
                'relation'=>'AND',
            ]
        ];
        if( !empty($author_id) ){
            $argsPost['author'] = $author_id;
        }
        $postsMONA = new WP_Query( $argsPost );
        if( $postsMONA->have_posts() ){ ?>
        <section class="sec-author-three">
            <div class="author-three  ss-pd">
                <div class="container">
                    <div class="author-three-slider relative gallery" data-aos="fade-up">
                        <div class="swiper">
                            <div class="swiper-wrapper">
                                <?php 
                                while( $postsMONA->have_posts() ){
                                    $postsMONA->the_post(); ?>
                                <div class="swiper-slide">
                                    <?php 
                                    /**
                                     * GET TEMPLATE PART
                                     * partials
                                     * video box
                                     */
                                    $slug = '/partials/loop/box';
                                    $name = 'video';
                                    echo get_template_part($slug, $name);
                                    ?>
                                </div>
                                <?php } wp_reset_query(); ?>
                            </div>
                        </div>
                        <!-- <div class="swiper-pagination c-pagination hidden-desktop"></div> -->
                        <div class="swiper-pagin swiper-pagination hidden-desktop"></div>
                        <div class="swiper-control posi midle hidden-mobile">
                            <div class="swiper-control-btn swiper-prev">
                                <i class="fas fa-chevron-left"></i>
                            </div>
                            <div class="swiper-control-btn swiper-next">
                                <i class="fas fa-chevron-right"></i>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php } ?>

        <!-- AUTHOR FOUR -->
        <?php $author_outstanding_category_personal = get_field('mona_user_outstanding_category_personal', $author);
        if( !empty( $author_outstanding_category_personal ) && $author_outstanding_category_personal->count > 0 ){ ?>
        <div class="author-four">
            <div class="container">
                <div class="c-line mb-40"></div>
                <div class="dsmall author-four-side">
                    <div class="dsmall-item author-four-left">
                        <div class="author-four-content" data-aos="fade-right" data-aos-duration="600">
                            <div class="author-four-note mb-24">
                                <div class="author-four-note-item">
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/author-four-icon-1.svg" alt="">
                                    <span class="text">
                                        <?php echo ( $author_outstanding_category_personal->count < 10 ) ? '0'.$author_outstanding_category_personal->count . __(' bài đọc', 'monamedia') : $author_outstanding_category_personal->count . __(' bài đọc', 'monamedia'); ?>
                                    </span>
                                </div>
                                <div class="author-four-note-item">
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/author-four-icon-2.svg" alt="">
                                    <span class="text"><?php echo $author_outstanding_category_personal->count*5 . __('  phút đọc','monamedia'); ?></span>
                                </div>
                            </div>
                            <h2 class="c-title author-four-title">
                                <?php echo $author_outstanding_category_personal->name; ?>
                            </h2>
                            <?php 
                            $taxonomy_outstanding_post_tags = get_field('mona_taxonomy_outstanding_post_tags', $author_outstanding_category_personal);
                            if( !empty( $taxonomy_outstanding_post_tags ) ){
                            ?>
                            <div class="tag-list">

                                <?php foreach ($taxonomy_outstanding_post_tags as $key => $tag) { ?>
                                <div class="tag-item tag-hosting">
                                    <a href="<?php echo get_term_link( $tag ); ?>" class="tag-item-link">
                                        <?php echo $tag->name; ?>
                                    </a>
                                </div>
                                <?php } ?>

                            </div>
                            <?php } ?>

                            <a href="<?php echo get_term_link($author_outstanding_category_personal); ?>" class="author-four-more">
                                <?php echo __('Đọc bài viết','monamedia'); ?>
                            </a>
                        </div>
                    </div>
                    <div class="dsmall-item author-four-right">
                        <a href="<?php echo get_term_link($author_outstanding_category_personal); ?>" class="author-four-img" data-aos="fade-left">
                            <?php $mona_taxonomy_thumbnail = get_field('mona_taxonomy_thumbnail', $author_outstanding_category_personal);
                            if( empty( $mona_taxonomy_thumbnail ) ){ ?>
                                <img src="<?php echo get_template_directory_uri() ?>/public/helpers/images/global-thumbnail-mona.png" alt="">
                            <?php }else{ ?> 
                                <?php echo wp_get_attachment_image($mona_taxonomy_thumbnail, 'thumbnail-taxonomy'); ?>	
                            <?php } ?>
                        </a>
                    </div>
                </div>
                <div class="c-line mt-40"></div>
            </div>
        </div>
        <?php } ?>

        <!-- AUTHOR SIX -->
        <?php 
        $post_type = 'post';
        $posts_per_page = 16;
        $paged = max( 1, get_query_var('paged') );
        $offset = ( $paged - 1 ) * $posts_per_page;
        $argsPost = [
            'post_type' => $post_type,
            'post_status' => 'publish',
            'posts_per_page' => $posts_per_page,
            'paged' => $paged,
            'offset' => $offset,
            'meta_query' => [
                'relation' => 'AND',
            ],
            'tax_query' => [
                'relation'=>'AND',
            ]
        ];

        if( !empty($author_id) ){
            $argsPost['author'] = $author_id;
        }

        $postsMONA = new WP_Query( $argsPost );
        if( $postsMONA->have_posts() ){  ?>
        <div class="author-six">
            <div class="container">
                <div class="author-six-wrapper mt-40 blogr-second override">
                    <div class="blogr-list">
                        <?php 
                        while( $postsMONA->have_posts() ){
                            $postsMONA->the_post();
                            ?>
                        <div class="blogr-item col-3">
                            <?php 
                            /**
                             * GET TEMPLATE PART
                             * partials
                             * blog box
                             */
                            $slug = '/partials/loop/box';
                            $name = 'blog';
                            echo get_template_part($slug, $name);
                            ?>
                        </div>
                        <?php } wp_reset_query(); ?>
                    </div>
                    <?php mona_pagination_links( $postsMONA ); ?>
                </div>
            </div>
        </div>
        <?php } ?>

    <?php }else{ ?> 

        <!-- AUTHOR SIX -->
        <div class="author-six ss-pd-t">
            <div class="container">

                <div class="author-top over-hidden mb-20">
                    <div class="author-top-item">
                        <div class="author-one-info">
                            <?php $author_avatar_personal = get_field('mona_user_avatar_personal', $author);
                            if( !empty( $author_avatar_personal ) ){ ?>
                            <div class="author-one-info-ava" data-aos="fade-left">
                                <?php echo wp_get_attachment_image($author_avatar_personal, 'full'); ?>
                            </div>
                            <?php } ?>
                            <h3 class="author-one-name" data-aos="fade-left">
                                <span class="text">
                                    <?php echo !empty( $author_display ) ? $author_display : $author->display_name; ?>
                                </span>
                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/author-one-tick.svg" alt="">
                            </h3>
                            <?php if( !empty( $author_display_description ) ){ ?>
                            <div class="author-one-tag" data-aos="fade-left">
                                <div class="author-one-tag-item">
                                    <?php echo $author_display_description; ?>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="author-top-item">
                        <?php if( !empty( $author_display_socialnetworks ) ){ ?>
                        <div class="author-social" data-aos="fade-right">

                            <?php foreach ($author_display_socialnetworks as $key => $item) {
                                if( !empty( $item['link']['url'] ) && $item['icon'] ){ ?>
                            <a href="<?php echo $item['link']['url']; ?>" target="<?php echo $item['link']['target']; ?>" class="author-social-link">
                                <?php echo wp_get_attachment_image($item['icon'], 'square-s'); ?>
                            </a>
                            <?php } } ?>

                        </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="c-line"></div>
                <div class="author-six-wrapper mt-40 blogr-second override">
                    <!-- AUTHOR SIX -->
                    <?php 
                    $post_type = 'post';
                    $posts_per_page = 16;
                    $paged = max( 1, get_query_var('paged') );
                    $offset = ( $paged - 1 ) * $posts_per_page;
                    $argsPost = [
                        'post_type' => $post_type,
                        'post_status' => 'publish',
                        'posts_per_page' => $posts_per_page,
                        'paged' => $paged,
                        'offset' => $offset,
                        'meta_query' => [
                            'relation' => 'AND',
                        ],
                        'tax_query' => [
                            'relation'=>'AND',
                        ]
                    ];

                    if( !empty($author_id) ){
                        $argsPost['author'] = $author_id;
                    }

                    $postsMONA = new WP_Query( $argsPost );
                    if( $postsMONA->have_posts() ){  ?>
                    <div class="blogr-list">
                        <?php 
                        while( $postsMONA->have_posts() ){
                            $postsMONA->the_post();
                            ?>
                        <div class="blogr-item col-3">
                            <?php 
                            /**
                             * GET TEMPLATE PART
                             * partials
                             * blog box
                             */
                            $slug = '/partials/loop/box';
                            $name = 'blog';
                            echo get_template_part($slug, $name);
                            ?>
                        </div>
                        <?php } wp_reset_query(); ?>
                    </div>
                    <?php mona_pagination_links( $postsMONA ); ?>
                    <?php } ?>
                </div>
            </div>
        </div>
    
    <?php } ?>

    <!-- AUTHOR SEVEN -->
    <?php 
    $post_type = 'post';
    $posts_per_page = 16;
    $paged = max( 1, get_query_var('paged') );
    $offset = ( $paged - 1 ) * $posts_per_page;
    $argsUser = [
        'number' => '12',
        'orderby' => 'post_count',
        'order' => 'DESC'
    ];

    if( !empty($author_id) ){
        $argsUser['exclude'] = $author_id;
    }

    $usersMONA  = new WP_User_Query( $argsUser );
    $authors    = $usersMONA->get_results();
    if( !empty( $authors ) ){  ?>
    <section class="sec-author-seven">
        <div class="author-seven ss-pd">
            <div class="container">
                <div class="head mb-64">
                    <h3 class="sec-tt" data-aos="fade-down">
                        <?php echo __('Các tác giả khác', 'monamedia'); ?>
                    </h3>
                </div>
                <div class="author-seven-slider relative" data-aos="fade-up">
                    <div class="swiper">
                        <div class="swiper-wrapper">
                            <?php foreach ($authors as $keyAuthor => $author_item) { ?>
                            <div class="swiper-slide">
                                <div class="author-seven-wrap">
                                    <a href="<?php echo get_author_posts_url( $author_item->ID ); ?>" class="author-seven-img">
                                        <?php $author_avatar_personal_item = get_field('mona_user_avatar_personal', $author_item);
                                        if( !empty( $author_avatar_personal_item ) ){ ?>
                                            <?php echo wp_get_attachment_image($author_avatar_personal_item, 'full'); ?>
                                        <?php }else{ ?> 
                                            <img src="<?php echo get_template_directory_uri() ?>/public/helpers/images/global-thumbnail-mona.png" alt="">
                                        <?php } ?>
                                    </a>
                                    <div class="author-seven-content">
                                        <div class="author-seven-note">
                                            <div class="author-seven-note-item">
                                                <span class="text">
                                                    <?php echo count_user_posts($author_item->ID, 'post') . __(' Bài viết' , 'monamedia'); ?>
                                                </span>
                                            </div>
                                            <div class="author-seven-note-item">
                                                <span class="text">
                                                    <?php echo count_user_posts($author_item->ID, 'mona_video') . __(' Video ngắn' , 'monamedia'); ?>
                                                </span>
                                            </div>
                                        </div>
                                        <p class="author-seven-name">
                                            <?php 
                                            $author_display_item                 =   get_field('mona_user_display_name', $author_item);
                                            echo !empty( $author_display_item ) ? $author_display_item : $author_item->display_name; ?>
                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/author-one-tick.svg" alt="">
                                        </p>
                                        <?php 
                                        $author_display_description_item                 =   get_field('mona_user_display_description', $author_item);
                                        if( !empty( $author_display_description_item ) ){ ?>
                                        <p class="author-seven-level">
                                            <?php echo $author_display_description_item; ?>
                                        </p>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="swiper-pagination c-pagination hidden-desktop"></div>
                </div>
            </div>
        </div>
    </section>
    <?php } ?>
</main>

<?php get_footer();
