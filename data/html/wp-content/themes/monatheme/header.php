<?php 
/**
 * The template for displaying header.
 *
 * @package MONA.Media / Website
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--[if IE 7]>
<html class="ie ie7" <?php language_attributes(); ?>>
<![endif]-->
<!--[if IE 8]>
<html class="ie ie8" <?php language_attributes(); ?>>
<![endif]-->
<!--[if !(IE 7) & !(IE 8)]><!-->
<html <?php language_attributes(); ?>>
<!--<![endif]-->

<head>
    <!-- Meta
                ================================================== -->
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport"
        content="initial-scale=1.0, maximum-scale=12.0, minimum-scale=1.0, user-scalable=yes, width=device-width">
    <meta name="yandex-verification" content="67a0c90696cbc536" />
    <meta name="ahrefs-site-verification" content="7694edeac83dd1b57202640d166b02af72f09d9996457199abfdd1f0e1acc9f6">
    <meta name="facebook-domain-verification" content="o9bucb4iitm48whrij095h207alzzj" />
    <?php wp_site_icon(); ?>
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <?php wp_head(); ?>

    <!-- async load font -->
    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/assets/fonts/BeVNPro/stylesheet.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/assets/fonts/BeVNPro/stylesheet.css"></noscript>

    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/assets/fonts/Inter/stylesheet.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/assets/fonts/Inter/stylesheet.css"></noscript>

    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/assets/fonts/Bebas_Neue/stylesheet.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/assets/fonts/Bebas_Neue/stylesheet.css"></noscript>

    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/assets/fonts/Just-Saying/stylesheet.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/assets/fonts/Just-Saying/stylesheet.css"></noscript>

    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/assets/fonts/Roboto/stylesheet.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/assets/fonts/Roboto/stylesheet.css"></noscript>

    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/assets/fonts/Oswald/stylesheet.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/assets/fonts/Oswald/stylesheet.css"></noscript>

    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/assets/fonts/Press-Start-2P/stylesheet.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/assets/fonts/Press-Start-2P/stylesheet.css"></noscript>


    <!-- async load script -->
    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/js/libs/fontawesome/css/all.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/js/libs/fontawesome/css/all.css"></noscript>

    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/js/libs/swiper/swiper-bundle.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/js/libs/swiper/swiper-bundle.min.css"></noscript>

    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/js/libs/magnific/magnific-popup.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/js/libs/magnific/magnific-popup.min.css"></noscript>

    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/js/libs/aos/aos.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/js/libs/aos/aos.css"></noscript>

    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/js/libs/wow/animate.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/js/libs/wow/animate.min.css"></noscript>

    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/js/libs/lightgallery/lightgallery.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/js/libs/lightgallery/lightgallery.min.css"></noscript>

    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/js/libs/select2/select2.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/js/libs/select2/select2.min.css"></noscript>
    
    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/js/libs/img-compare/img-compare.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/js/libs/img-compare/img-compare.css"></noscript>

    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/js/libs/splitting/splitting.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/js/libs/splitting/splitting.css"></noscript>

    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/js/libs/splitting/splitting-cells.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/js/libs/splitting/splitting-cells.css"></noscript>

    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/js/libs/splide/splide.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/js/libs/splide/splide.min.css"></noscript>
    
    <link rel="preload" href="<?php echo MONA_SITE_URL ?>/template/js/libs/splide/splide-extension-video.min.css" as="style" onload="this.onload=null;this.rel='stylesheet'">
    <noscript><link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/js/libs/splide/splide-extension-video.min.css"></noscript>



    <link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/css/style.css" />
    
    <link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/css/c_style.css" />
    <link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/css/dvs.css" />
    <link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/css/p_style.css" />
    <link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/css/home.css" />
    <link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/css/mona-host.css" />
    <link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/css/cp-style.css" />
    <link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/css/header.css" />
    <link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/css/fix-header.css" />
    <link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/css/cp-search.css" />
    <link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/css/host.css" />
    <link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/css/host-checkout.css" />
    <link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/css/base.css" />
    <link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/css/backdoor.css" />

    <link rel="stylesheet" href="<?php echo MONA_SITE_URL ?>/template/css/animation.css" />
    <script src="<?php echo MONA_SITE_URL ?>/template/js/libs/jquery/jquery.min.js"></script>
</head>
<!-- Google Tag Manager -->
<script>
    (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            'https://www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-WCTP4CQ');
    </script>
    <!-- End Google Tag Manager -->
    <!-- Global site tag (gtag.js) - Google Ads -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=AW-625006842"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'AW-625006842');
    </script>
    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-73564204-1"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'UA-73564204-1');
    </script>
    <!-- Google tag (gtag.js) -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=G-QM44M8EV2W"></script>
    <script>
    window.dataLayer = window.dataLayer || [];

    function gtag() {
        dataLayer.push(arguments);
    }
    gtag('js', new Date());

    gtag('config', 'G-QM44M8EV2W');
</script>
</head>
<?php
$body = 'host-page ';
$header = '';
if (wp_is_mobile()) {
    $body .= 'mobile-detect';
} else {
    $body .= 'desktop-detect';
}

if ( is_page( MONA_PAGE_HOME ) ) {
    $body .= ' home-software';
} elseif (!is_front_page() && is_home()) {
    $body   .=  ' web-monablog monaBlogPage';
    $header .=  ' in';
} elseif( is_front_page() && is_home() ) {
    $body   .=  ' nhtq-page site-software';
} elseif (is_category()) {
    $body   .=  ' web-monablog monaBlogPage';
    $current_taxonomy = get_queried_object();
    $child_of_current_taxonomy = get_terms(
        [
            'taxonomy'         => $current_taxonomy->taxonomy,
            'hide_empty'    =>    true,
            'parent'        =>    $current_taxonomy->term_id,
        ]
    );
    if (!empty($child_of_current_taxonomy))
        $body .= ' in monaCategoryPage';
    else
        $body .= ' in';
} elseif (is_singular('post')) {

    $body   .=  ' web-monablogdt monaBlogDetail';
} elseif (is_author()) {

    $current_page = max(1, get_query_var('paged'));
    if ($current_page != 1) {
        $body .= ' web-author-more';
    }
} elseif (is_page_template('page-template/about-template.php')) {

    $body .= ' page-about';
} elseif (is_page_template('page-template/home-template.php')) {

    $body .= ' home-page';
} elseif (is_page_template('page-template/projects-template.php')) {

    $body .= ' web-project-all';
} elseif (is_page_template('page-template/projects-website-template.php')) {

    $body .= ' web.prjtkw';
} elseif (is_page_template('page-template/projects-lms-template.php')) {

    $body .= ' page-lms';
} elseif (is_page_template('page-template/projects-nhtq-template.php')) {

    $body .= ' page-nhtq';
} elseif (is_page_template('page-template/single-project-website-template.php') && is_singular('mona_project')) {

    $body .= ' page-pj-detail gang-nam';
} elseif (is_page_template('page-template/single-project-lms-template.php') && is_singular('mona_project')) {

    $body .= ' page-lmsdt';
} elseif (is_page_template('page-template/single-project-nhtq-template.php') && is_singular('mona_project')) {

    $body .= ' page-pj-detail';
} elseif (is_page_template('page-template/templates-website-template.php')) {

    $body .= ' page-dweb';
} elseif (is_page_template('page-template/contact-template.php')) {

    $body .= ' page-contacts';
} elseif (is_page_template('page-template/service-seo-template.php') || is_page_template('page-template/service-website-template.php')) {

    $body .= ' page-service-general';
} elseif ( is_tax('demo_category') || is_tax('demo_tag') ) {

    $body .= ' web-sample web-sample-demo-category-mona';
} elseif (is_page_template('page-template/service-vps-template.php')) {

    $body .= ' premium-cloud-vps';
}
?>

<?php
if (!is_front_page() && is_home()) {
    $sitemap = get_the_title(MONA_PAGE_BLOG);
} elseif (is_category() || is_tag() || is_tax()) {
    $sitemap = get_queried_object()->name;
} else {
    $sitemap = get_the_title();
}
?>

<body <?php body_class( $body ); ?>>
<header class="hd <?php echo $header; ?>" data-sitemap="<?php echo $sitemap; ?>">
        <div class="container">
            <div class="hd-ctn d-flex f-ctn">
                <div class="hd-logo col col-4">
                    <?php
                    if (!is_front_page() && is_home()) {
                        $header_logo = get_field('mona_global_header_logo', MONA_PAGE_BLOG);
                    } else {
                        $header_logo = get_field('mona_global_header_logo');
                    }

                    if (!empty($header_logo)) { ?>
                    <a href="<?php echo home_url(); ?>" class="mona-logo">
                        <?php echo wp_get_attachment_image($header_logo, 'full'); ?>
                    </a>
                    <?php } else { ?>
                    <?php echo get_custom_logo(); ?>
                    <?php } ?>
                </div>

                <div class="hd-controls d-flex f-nowrap f-end col col-8">
                    <div class="hd-menu d-flex f-center f-end hdMenu">
                        <div class="hd-menu-logo">
                            <?php
                            if (!is_front_page() && is_home()) {
                                $menu_logo = get_field('mona_global_menu_logo', MONA_PAGE_BLOG);
                            } else {
                                $menu_logo = get_field('mona_global_menu_logo');
                            }

                            if (!empty($menu_logo)) { ?>
                            <a href="<?php echo home_url(); ?>" class="mona-logo">
                                <?php echo wp_get_attachment_image($menu_logo, 'full'); ?>
                            </a>
                            <?php } else { ?>
                            <?php $menu_logo = mona_get_option('section_menu_logo');
                                if (!empty($menu_logo)) { ?>
                            <a href="<?php echo home_url(); ?>" class="mona-logo">
                                <img src="<?php echo $menu_logo; ?>" alt="">
                            </a>
                            <?php } ?>
                            <?php } ?>

                            <div class="menu-close menuClose">
                                <div class="menu-close-wrap">
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                </div>
                            </div>
                        </div>
                        <div class="menu">
                            <div class="menu-list d-flex f-center">
                                <div
                                    class="menu-item menu-item-ani <?php echo return_megamenu_active( 'https://mona.media/gioi-thieu' ); ?>">
                                    <a href="https://mona.media/gioi-thieu/" class="menu-item-link">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/header/icon-about.svg" alt="" class="icon">
                                        Giới thiệu
                                    </a>
                                </div>
                                <div class="menu-item menu-item-ani mega-menu dropdown mega-menu-js">
                                    <a href="#" class="menu-item-link">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/header/icon-service.svg" alt="" class="icon">
                                        Dịch vụ                                        
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/header/icon-arrow.svg" alt="" class="icon-arrow">
                                    </a>

                                    <div class="mega-menu-box">
                                        <div class="container">
                                            <div class="mega-menu-box-wrap">
                                                <div class="mega-menu-box-list d-flex f-ctn">
                                                    <div class="mega-menu-box-wrap-item col col-12">
                                                        <div class="mega-menu-cate-wrap-w-label d-flex f-ctn">
                                                            <div class="label col">Giải pháp</div>
                                                            <div class="label col">Dịch vụ chính</div>
                                                            <div class="label col">Các nội dung liên quan khác</div>
                                                        </div>
                                                        <div class="mega-menu-header d-flex f-between">
                                                            <div class="mega-menu-back mega-menu-back-js">
                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/header/icon-arrow-pri.svg" alt="">
                                                                Trở lại
                                                            </div>
                                                            <div class="mega-menu-close mega-menu-close-js">
                                                                <div class="hd-bar hdBar">
                                                                    <svg class="menu__svg" viewBox="0 0 100 100">
                                                                        <path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path>
                                                                        <path d="m 50,50 h 20" class="path-2"></path>
                                                                        <path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path>
                                                                    </svg>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="mega-menu-list mega-menu-list-js">

                                                            <div class="mega-menu-background mega-menu-background-js" style="--background-top: 64px; --background-height: 0px">
                                                            </div>

                                                            <div class="mega-menu-detail-bg mega-menu-detail-bg-js"></div>

                                                            <svg class="SiteSubMenu__navHoverArrow-js SiteSubMenu__navHoverArrow" xmlns="http://www.w3.org/2000/svg" width="16" height="53" viewBox="0 0 16 53" fill="none">
                                                                <path d="M0 0L0.761566 3.11765C2.49811 7.91118 3.67479 11.1359 8.40029 14.2556L10.0566 15.349C13.8124 17.8285 16 21.5896 16 25.5672C16 29.2316 14.1421 32.7276 10.8792 35.2029L8.44347 37.0507C3.67397 40.669 2.52537 44.195 0.823452 49.3627L0 53V0Z" fill=""/>
                                                                <path d="M0 14L0.416477 14.9519C1.36614 17.1226 3.25972 19.0427 5.84395 20.4554L6.74973 20.9505C8.80365 22.0733 10 23.7764 10 25.5776C10 27.2369 8.98396 28.8201 7.1996 29.941L5.86756 30.7777C3.25927 32.4162 1.38105 34.5277 0.450321 36.8678L0 38V14Z" fill=""/>
                                                            </svg>

                                                            <div class="mega-menu-item megaMenuItem" style="--mega-background:#EDFBFF; --color:#1EC0F2; --box-shadow:inset 0px 4px 24px 2px rgba(45, 180, 219, 0.16)">
                                                                <div class="d-flex">
                                                                    <span class="mega-menu-img">
                                                                        <img src="<?php echo get_site_url();?>/template/assets/images/header/mega-menu-solution-4.svg" alt="">
                                                                    </span>
                                                                    <span class="mega-menu-txt">
                                                                        <span class="title">Premium Cloud VPS/Hosting</span>
                                                                        <span class="desc">Hạ tầng lưu trữ, điện toán</span>
                                                                    </span>
                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/header/icon-arrow.svg" alt="" class="mega-menu-arrow">
                                                                </div>

                                                                <div class="mega-menu-cate megaMenuCate">
                                                                    <div class="mega-menu-header d-flex f-between">
                                                                        <div class="mega-menu-back mega-menu-back-lv-2-js">
                                                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/header/icon-arrow-pri.svg" alt="">
                                                                            Trở lại
                                                                        </div>
                                                                        <div class="mega-menu-close mega-menu-close-js">
                                                                            <div class="hd-bar hdBar">
                                                                                <svg class="menu__svg" viewBox="0 0 100 100">
                                                                                    <path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path>
                                                                                    <path d="m 50,50 h 20" class="path-2"></path>
                                                                                    <path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path>
                                                                                </svg>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="mega-menu-cate-wrap-w">
                                                                        <div class="mega-menu-cate-wrap d-flex f-ctn">
                                                                            <div class="mega-menu-box-wrap-item col mega-service">
                                                                                <div class="label label-mobile">Premium Cloud VPS/Hosting</div>
    
                                                                                <div class="mega-menu-list mega-menu-list-child-js">
                                                                                    <div class="mega-menu-list-col">
                                                                                        <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/huong-dan-mua-ten-mien-domain' , 'active' ); ?>">
                                                                                            <div
                                                                                                class="d-flex mega-menu-item-link mega-menu-item-link-js ">
                                                                                                <div class="mega-menu-img">
                                                                                                    <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-pen.svg" alt="">
                                                                                                </div>
                                                                                                <div class="mega-menu-txt">
                                                                                                    <a href="#" class="title">
                                                                                                        <span
                                                                                                            class="mega-menu-sticky bg-nhtq">Hot 🔥</span>
                                                                                                            MONA EcoSystem Cloud
                                                                                                    </a>
                                                                                                    <div class="desc">
                                                                                                        Giải pháp hạ tầng dành riêng cho khách hàng của MONA.Software và MONA.Media
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/dedicated-server-hosting' , 'active' ); ?>">
                                                                                            <div 
                                                                                                class="d-flex mega-menu-item-link mega-menu-item-link-js ">
                                                                                                <div class="mega-menu-img">
                                                                                                    <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-premium-custom-server.svg" alt="">
                                                                                                </div>
                                                                                                <div class="mega-menu-txt">
                                                                                                    <a href="https://mona.media/dedicated-server-hosting/" class="title">
                                                                                                        Premium Custom Server
                                                                                                    </a>
                                                                                                    <div class="desc">
                                                                                                        Giải pháp server độc quyền được thiết kế theo mục đích sử dụng của khách hàng
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.host/dang-ky-ten-mien' , 'active' ); ?>">
                                                                                            <div 
                                                                                                class="d-flex mega-menu-item-link mega-menu-item-link-js ">
                                                                                                <div class="mega-menu-img">
                                                                                                    <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-domain-server.svg" alt="">
                                                                                                </div>
                                                                                                <div class="mega-menu-txt">
                                                                                                    <a href="https://mona.host/dang-ky-ten-mien/" class="title">
                                                                                                        Domain
                                                                                                    </a>
                                                                                                    <div class="desc">
                                                                                                    Kiểm tra tên miền miễn phí, nhanh chóng chỉ với 3 giây có ngay domain chất lượng
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="mega-menu-list-col">
                                                                                        <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.host/premium-cloud-hosting' , 'active' ); ?>">
                                                                                            <div 
                                                                                                class="d-flex mega-menu-item-link mega-menu-item-link-js submenu-icon-js">
                                                                                                <div class="mega-menu-img">
                                                                                                    <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-premium-cloud-hosting.svg" alt="">
                                                                                                </div>
                                                                                                <div class="mega-menu-txt">
                                                                                                    <div class="submenu-icon ">
                                                                                                        <span class="submenu-icon-i"></span>
                                                                                                        <span class="submenu-icon-i"></span>
                                                                                                    </div>

                                                                                                    <a href="https://mona.host/premium-cloud-hosting/" class="title">
                                                                                                        Premium Cloud Hosting
                                                                                                    </a>

                                                                                                    <div class="desc">
                                                                                                        Tốc độ tải dưới 3s, uptime 99,9% đảm bảo website hoạt động liên tục
                                                                                                    </div>

                                                                                                    <div class="mega-menu-submenu mega-menu-submenu-js">
                                                                                                        <a href="https://mona.media/web-hosting/" class="mega-menu-submenu-item d-flex mega-menu-submenu-item-js <?php echo return_megamenu_active( 'https://mona.media/web-hosting' , 'active' ); ?>">
                                                                                                            <span class="icon">
                                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-web-hosting.svg" alt="">
                                                                                                            </span>
                                                                                                            <span class="txt">
                                                                                                                Web Hosting
                                                                                                            </span>
                                                                                                        </a>
                                                                                                        <a href="https://mona.media/business-hosting/" class="mega-menu-submenu-item d-flex mega-menu-submenu-item-js <?php echo return_megamenu_active( 'https://mona.media/business-hosting' , 'active' ); ?>">
                                                                                                            <span class="icon">
                                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-ecommerve-hosting.svg" alt="">
                                                                                                            </span>
                                                                                                            <span class="txt">
                                                                                                                Ecommerce Hosting
                                                                                                            </span>
                                                                                                        </a>
                                                                                                        <a href="https://mona.media/wordpress-hosting/" class="mega-menu-submenu-item d-flex mega-menu-submenu-item-js <?php echo return_megamenu_active( 'https://mona.media/wordpress-hosting' , 'active' ); ?>">
                                                                                                            <span class="icon">
                                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-wordpress-hosting.svg" alt="">
                                                                                                            </span>
                                                                                                            <span class="txt">
                                                                                                                WordPress Hosting
                                                                                                            </span>
                                                                                                        </a>
                                                                                                        <a href="https://mona.media/lms-hosting/" class="mega-menu-submenu-item d-flex mega-menu-submenu-item-js <?php echo return_megamenu_active( 'https://mona.media/lms-hosting' , 'active' ); ?>">
                                                                                                            <span class="icon">
                                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-elearning-hosting.svg" alt="">
                                                                                                            </span>
                                                                                                            <span class="txt">
                                                                                                                Elearning Hosting
                                                                                                            </span>
                                                                                                        </a>
                                                                                                        <a href="javascript:;" class="mega-menu-submenu-item d-flex mega-menu-submenu-item-js <?php // echo return_megamenu_active( 'https://mona.media/web-hosting' , 'active' ); ?>">
                                                                                                            <span class="icon">
                                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-booking.svg" alt="">
                                                                                                            </span>
                                                                                                            <span class="txt">
                                                                                                                Booking System Hosting
                                                                                                            </span>
                                                                                                        </a>
                                                                                                        <a href="javascript:;" class="mega-menu-submenu-item d-flex mega-menu-submenu-item-js <?php // echo return_megamenu_active( 'https://mona.media/web-hosting' , 'active' ); ?>">
                                                                                                            <span class="icon">
                                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-video-protect.svg" alt="">
                                                                                                            </span>
                                                                                                            <span class="txt">
                                                                                                                Video Protection Hosting
                                                                                                            </span>
                                                                                                        </a>
                                                                                                        <a href="https://mona.media/windows-hosting/" class="mega-menu-submenu-item d-flex mega-menu-submenu-item-js <?php echo return_megamenu_active( 'https://mona.media/windows-hosting' , 'active' ); ?>">
                                                                                                            <span class="icon">
                                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-windows.svg" alt="">
                                                                                                            </span>
                                                                                                            <span class="txt">
                                                                                                                Windows Hosting
                                                                                                            </span>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.host/premium-cloud-vps' , 'active' ); ?>">
                                                                                            <div 
                                                                                                class="d-flex mega-menu-item-link mega-menu-item-link-js submenu-icon-js">
                                                                                                <div class="mega-menu-img">
                                                                                                    <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-premium-cloud-vps.svg" alt="">
                                                                                                </div>
                                                                                                <div class="mega-menu-txt">
                                                                                                    <div class="submenu-icon ">
                                                                                                        <span class="submenu-icon-i"></span>
                                                                                                        <span class="submenu-icon-i"></span>
                                                                                                    </div>

                                                                                                    <a href="https://mona.host/premium-cloud-vps/" class="title">
                                                                                                        Premium Cloud VPS 
                                                                                                    </a>

                                                                                                    <div class="desc">
                                                                                                        Hạ tầng lưu trữ điện toán tự chủ vận hành với công nghệ hiện đại nhất
                                                                                                    </div>

                                                                                                    <div class="mega-menu-submenu mega-menu-submenu-js">
                                                                                                        <a href="https://mona.media/vps-windows/" class="mega-menu-submenu-item d-flex mega-menu-submenu-item-js <?php echo return_megamenu_active( 'https://mona.media/vps-windows' , 'active' ); ?>">
                                                                                                            <span class="icon">
                                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-windows-vps.svg" alt="">
                                                                                                            </span>
                                                                                                            <span class="txt">
                                                                                                                Windows VPS
                                                                                                            </span>
                                                                                                        </a>
                                                                                                        <a href="https://mona.media/vps-linux/" class="mega-menu-submenu-item d-flex mega-menu-submenu-item-js <?php echo return_megamenu_active( 'https://mona.media/vps-linux' , 'active' ); ?>">
                                                                                                            <span class="icon">
                                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-linux-vps.svg" alt="">
                                                                                                            </span>
                                                                                                            <span class="txt">
                                                                                                                Linux VPS
                                                                                                            </span>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/email-ten-mien' , 'active' ); ?>">
                                                                                            <div 
                                                                                                class="d-flex mega-menu-item-link mega-menu-item-link-js submenu-icon-js">
                                                                                                <div class="mega-menu-img">
                                                                                                    <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-premium-cloud-vps.svg" alt="">
                                                                                                </div>
                                                                                                <div class="mega-menu-txt">
                                                                                                    <div class="submenu-icon ">
                                                                                                        <span class="submenu-icon-i"></span>
                                                                                                        <span class="submenu-icon-i"></span>
                                                                                                    </div>

                                                                                                    <a href="https://mona.media/email-ten-mien/" class="title">
                                                                                                        Business Email 
                                                                                                    </a>

                                                                                                    <div class="desc">
                                                                                                        Giải pháp email theo tên miền của doanh nghiệp, tăng tính nhận diện cao
                                                                                                    </div>

                                                                                                    <div class="mega-menu-submenu mega-menu-submenu-js">
                                                                                                        <a href="https://mona.media/email-ten-mien/" class="mega-menu-submenu-item d-flex mega-menu-submenu-item-js <?php echo return_megamenu_active( 'https://mona.media/email-ten-mien' , 'active' ); ?>">
                                                                                                            <span class="icon">
                                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-business-email-child.svg" alt="">
                                                                                                            </span>
                                                                                                            <span class="txt">
                                                                                                                Business Email
                                                                                                            </span>
                                                                                                        </a>
                                                                                                        <a href="javascript:;" class="mega-menu-submenu-item d-flex mega-menu-submenu-item-js <?php // echo return_megamenu_active( 'https://mona.media/web-hosting' , 'active' ); ?>">
                                                                                                            <span class="icon">
                                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-gg-workspace.svg" alt="">
                                                                                                            </span>
                                                                                                            <span class="txt">
                                                                                                                Google Workspace
                                                                                                            </span>
                                                                                                        </a>
                                                                                                        <a href="javascript:;" class="mega-menu-submenu-item d-flex mega-menu-submenu-item-js <?php // echo return_megamenu_active( 'https://mona.media/web-hosting' , 'active' ); ?>">
                                                                                                            <span class="icon">
                                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-office-365.svg" alt="">
                                                                                                            </span>
                                                                                                            <span class="txt">
                                                                                                                Office 365
                                                                                                            </span>
                                                                                                        </a>
                                                                                                    </div>
                                                                                                </div>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
    
                                                                            <div class="mega-menu-box-wrap-item col mega-related">
                                                                                
                                                                                <div class="mega-menu-list">
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/cach-chon-ten-mien' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/cach-chon-ten-mien/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Cách chọn mua tên miền</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/cach-tao-trang-web' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/cach-tao-trang-web/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Cách khởi tạo website</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/lam-gi-sau-khi-co-website' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/lam-gi-sau-khi-co-website/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Làm gì sau khi có website</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/thiet-ke-website-responsive' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/thiet-ke-website-responsive/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Công nghệ Responsive website</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/y-tuong-thiet-ke-website' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/y-tuong-thiet-ke-website/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Ý tưởng thiết kế website</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="mega-menu-box-banner menu-banner-js">
                                                                                    <div class="mega-menu-box-banner-slide menu-banner-slide-js" style="--transformX:0px">
                                                                                        <div class="mega-menu-box-banner-slide-item menu-banner-slide-item-js">
                                                                                            <a href="https://mona.media/thue-cho-dat-may-chu/" target="_blank" class="mega-menu-box-banner-wrap">
                                                                                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/mega-menu-banner-6.jpg" alt="" class="img-ab">
                                                                                            </a>
                                                                                        </div>

                                                                                        <div class="mega-menu-box-banner-slide-item menu-banner-slide-item-js">
                                                                                            <a href="https://seo.monamedia.co/dua-doanh-nghiep-thoat-kho" target="_blank" class="mega-menu-box-banner-wrap">
                                                                                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/mega-menu-banner-7.jpg" alt="" class="img-ab">
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="mega-menu-item megaMenuItem" style="--mega-background:#F3F9FF; --color:#2686EC">
                                                                <div class="d-flex">
                                                                    <span class="mega-menu-img">
                                                                        <img src="<?php echo get_site_url();?>/template/assets/images/header/mega-menu-solution-5.svg" alt="">
                                                                    </span>
                                                                    <span class="mega-menu-txt">
                                                                        <span class="title">Software as a Service</span>
                                                                        <span class="desc">Quản lý kinh doanh, doanh nghiệp</span>
                                                                    </span>
                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/header/icon-arrow.svg" alt="" class="mega-menu-arrow">
                                                                </div>
                                                                <div class="mega-menu-cate megaMenuCate">
                                                                    <div class="mega-menu-header d-flex f-between">
                                                                        <div class="mega-menu-back mega-menu-back-lv-2-js">
                                                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/header/icon-arrow-pri.svg" alt="">
                                                                            Trở lại
                                                                        </div>
                                                                        <div class="mega-menu-close mega-menu-close-js">
                                                                            <div class="hd-bar hdBar">
                                                                                <svg class="menu__svg" viewBox="0 0 100 100">
                                                                                    <path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path>
                                                                                    <path d="m 50,50 h 20" class="path-2"></path>
                                                                                    <path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path>
                                                                                </svg>
                                                                            </div>
                                                                        </div>
                                                                    </div>

                                                                    <div class="mega-menu-cate-wrap-w">
                                                                        <div class="mega-menu-cate-wrap d-flex f-ctn">
                                                                            <div
                                                                                class="mega-menu-box-wrap-item col mega-service">

                                                                                <div class="label label-mobile">Software as a Service</div>
    
                                                                                <div class="mega-menu-list mega-menu-list-child-js">
                                                                                    <div class="mega-menu-list-col">
                                                                                        <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.software//edutech' , 'active' ); ?>">
                                                                                            <a href="https://mona.software/edutech/"
                                                                                                class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                                <span class="mega-menu-img">
                                                                                                    <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-briefcase-2-1.svg" alt="">
                                                                                                </span>
                                                                                                <span class="mega-menu-txt">
                                                                                                    <span class="title">
                                                                                                        <span class="mega-menu-sticky bgF41E92">Khuyên dùng 🔥</span>
                                                                                                        Hệ thống phần mềm MONA Edutech
                                                                                                    </span>
                                                                                                    <span class="desc">
                                                                                                        LMS-Learning Management System
                                                                                                    </span>
                                                                                                </span>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.software/nhtq' , 'active' ); ?>">
                                                                                            <a href="https://mona.software/nhtq/"
                                                                                                class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                                <span class="mega-menu-img">
                                                                                                    <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-briefcase-2-3.svg" alt="">
                                                                                                </span>
                                                                                                <span class="mega-menu-txt">
                                                                                                    <span class="title">
                                                                                                        <span class="mega-menu-sticky bgF5851E">Bán chạy 🔥</span>
                                                                                                        Hệ thống phần mềm </br>MONA NHTQ
                                                                                                    </span>
                                                                                                    <span class="desc">
                                                                                                        NHTQ DropShipping Management System
                                                                                                    </span>
                                                                                                </span>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'javascript:;' , 'active' ); ?>">
                                                                                            <a href="javascript:;"
                                                                                                class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                                <span class="mega-menu-img">
                                                                                                    <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-software-ecommerce.svg" alt="">
                                                                                                </span>
                                                                                                <span class="mega-menu-txt">
                                                                                                    <span class="title">
                                                                                                        Hệ thống phần mềm
                                                                                                        <br>MONA Ecommerce
                                                                                                    </span>
                                                                                                    <span class="desc">
                                                                                                        Ecommerce Managerment System
                                                                                                    </span>
                                                                                                </span>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.house' , 'active' ); ?>">
                                                                                            <a href="https://mona.house/"
                                                                                                class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                                <span class="mega-menu-img">
                                                                                                    <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-software-house.svg" alt="">
                                                                                                </span>
                                                                                                <span class="mega-menu-txt">
                                                                                                    <span class="title">
                                                                                                        Hệ thống phần mềm
                                                                                                        <br>MONA House
                                                                                                    </span>
                                                                                                    <span class="desc">
                                                                                                        Phần mềm quản lý nhà trọ, căn hộ dịch vụ, phòng trọ cho thuê 
                                                                                                    </span>
                                                                                                </span>
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                    <div class="mega-menu-list-col">
                                                                                        <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.software/phan-mem-quan-ly-tiem-vang-bac/' , 'active' ); ?>">
                                                                                            <a href="https://mona.software/phan-mem-quan-ly-tiem-vang-bac/"
                                                                                                class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                                <span class="mega-menu-img">
                                                                                                    <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-software-jewelry.svg" alt="">
                                                                                                </span>
                                                                                                <span class="mega-menu-txt">
                                                                                                    <span class="title">
                                                                                                        Hệ thống phần mềm</br>
                                                                                                        MONA Jewelry
                                                                                                    </span>
                                                                                                    <span class="desc">
                                                                                                        Phần mềm quản lý xưởng, chuỗi cửa hàng vàng bạc
                                                                                                    </span>
                                                                                                </span>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/dich-vu-thiet-ke-phan-mem-theo-yeu-cau' , 'active' ); ?>">
                                                                                            <a href="https://mona.media/dich-vu-thiet-ke-phan-mem-theo-yeu-cau/"
                                                                                                class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                                <span class="mega-menu-img">
                                                                                                    <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-briefcase-2-2.svg" alt="">
                                                                                                </span>
                                                                                                <span class="mega-menu-txt">
                                                                                                    <span class="title">
                                                                                                        Website Application Outsourcing
                                                                                                    </span>
                                                                                                    <span class="desc">
                                                                                                        Lập trình phần mềm quản lý, kinh doanh theo yêu cầu doanh nghiệp
                                                                                                    </span>
                                                                                                </span>
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/lap-trinh-ios-android' , 'active' ); ?>">
                                                                                            <a href="https://mona.media/lap-trinh-ios-android/"
                                                                                                class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                                <span class="mega-menu-img">
                                                                                                    <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-software-development.svg" alt="">
                                                                                                </span>
                                                                                                <span class="mega-menu-txt">
                                                                                                    <span class="title">
                                                                                                        Mobile Application <br> IOS/Android Outsourcing
                                                                                                    </span>
                                                                                                    <span class="desc">
                                                                                                        Lập trình mọi loại sản phẩm phần mềm, tính năng cho mobile-app
                                                                                                    </span>
                                                                                                </span>
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="mega-menu-box-wrap-item col mega-related">
                                                                                <div class="mega-menu-list">
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/phan-mem-la-gi' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/phan-mem-la-gi/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Phần mềm là gì</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/agile-la-gi-scrum-la-gi' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/agile-la-gi-scrum-la-gi/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Agile, Scrum là gì</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/top-10-cong-cu-viet-phan-mem-tot-nhat' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/top-10-cong-cu-viet-phan-mem-tot-nhat/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Top 10 công cụ viết phần mềm tốt nhất</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/top-10-ngon-ngu-lap-trinh-phan-mem' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/top-10-ngon-ngu-lap-trinh-phan-mem/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Top 10 ngôn ngữ lập trình phần mềm</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/phan-mem-tinh-tien-mien-phi' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/phan-mem-tinh-tien-mien-phi/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Phần mềm tính tiền miễn phí</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="mega-menu-box-banner menu-banner-js">
                                                                                    <div class="mega-menu-box-banner-slide menu-banner-slide-js" style="--transformX:0px">
                                                                                        <div class="mega-menu-box-banner-slide-item menu-banner-slide-item-js" mega-menu-video="true">
                                                                                            <a href="https://www.youtube.com/watch?v=ToAHW_WTPWM" target="_blank" class="mega-menu-box-banner-wrap">
                                                                                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/mega-menu-banner-8.jpg" alt="" class="img-ab">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="mega-menu-box-banner-slide-item menu-banner-slide-item-js">
                                                                                            <a href="https://mona.media/lien-he/" target="_blank" class="mega-menu-box-banner-wrap">
                                                                                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/mega-menu-banner-6.jpg" alt="" class="img-ab">
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="mega-menu-item megaMenuItem" style="--mega-background:#F8F3FF; --color:#2686EC">
                                                                <div class="d-flex">
                                                                    <span class="mega-menu-img">
                                                                        <img src="<?php echo get_site_url();?>/template/assets/images/header/mega-menu-solution-1.svg"
                                                                            alt="">
                                                                    </span>
                                                                    <span class="mega-menu-txt">
                                                                        <span class="title">Premium Web Design</span>
                                                                        <span class="desc">Kinh Doanh với Website</span>
                                                                    </span>
                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/header/icon-arrow.svg" alt="" class="mega-menu-arrow">
                                                                </div>

                                                                <div class="mega-menu-cate megaMenuCate">
                                                                    <div class="mega-menu-header d-flex f-between">
                                                                        <div class="mega-menu-back mega-menu-back-lv-2-js">
                                                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/header/icon-arrow-pri.svg" alt="">
                                                                            Trở lại
                                                                        </div>
                                                                        <div class="mega-menu-close mega-menu-close-js">
                                                                            <div class="hd-bar hdBar">
                                                                                <svg class="menu__svg" viewBox="0 0 100 100">
                                                                                    <path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path>
                                                                                    <path d="m 50,50 h 20" class="path-2"></path>
                                                                                    <path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path>
                                                                                </svg>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                    <div class="mega-menu-cate-wrap-w">
                                                                        <div class="mega-menu-cate-wrap d-flex f-ctn">
                                                                            <div class="mega-menu-box-wrap-item col mega-service">
                                                                                
                                                                                <div class="label label-mobile">Premium Web Design</div>
                                                                                <div class="mega-menu-list mega-menu-list-child-js">
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/thiet-ke-website-tai-hcm' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/thiet-ke-website-tai-hcm/" class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-pen.svg"
                                                                                                    alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    <span class="mega-menu-sticky bgF41E92">Hot 🔥</span>
                                                                                                    Thiết kế website
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Đã có 9689+ dự án hoàn thành, 85% khách hàng đồng hành không thay đổi 
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/thiet-ke-va-lap-trinh-web-theo-yeu-cau' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/thiet-ke-va-lap-trinh-web-theo-yeu-cau/"
                                                                                            class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-code.svg"
                                                                                                    alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Code tính năng theo yêu cầu
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Tăng trải nghiệm khách hàng, bứt phá chuyển đổi 
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/thiet-ke-website-ban-khoa-hoc-online' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/thiet-ke-website-ban-khoa-hoc-online/"
                                                                                            class="d-flex mega-menu-item-link mega-menu-item-link-js" target="_blank">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-online-education.svg"
                                                                                                    alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Thiết kế website <br> Dạy học Trực Tuyến
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Website của giảng viên, bán khóa học
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/dich-vu-content-marketing/' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/dich-vu-content-marketing//"
                                                                                            class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-change-cus.svg"
                                                                                                    alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Tối ưu website <br> chuyển đổi khách hàng
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Chiến lược bài bản, phương pháp hiện đại
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/thiet-ke-landing-page' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/thiet-ke-landing-page/"
                                                                                            class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-landing-page.svg"
                                                                                                    alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Thiết kế Landing Page
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Sáng tạo, focus đúng mục tiêu, tạo chuyển đổi hiệu quả cao
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/dich-vu-toi-uu-website' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/dich-vu-toi-uu-website/"
                                                                                            class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-code-manager.svg"
                                                                                                    alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Code Review <br> Tối ưu code cho website
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Tăng tốc website, điểm cộng trải nghiệm
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/thiet-ke-website-ban-hang' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/thiet-ke-website-ban-hang/"
                                                                                            class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-shopping-basket.svg" alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Thiết kế website bán hàng
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Bứt phá doanh số không giới hạn
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/dich-vu-quan-tri-website' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/dich-vu-quan-tri-website/"
                                                                                            class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-love.svg"
                                                                                                    alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Các gói đồng hành <br> chăm sóc website toàn diện
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Tối ưu chất lượng, tỷ lệ chuyển đổi cao
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                            <div class="mega-menu-box-wrap-item col mega-related">
    
                                                                                <div class="mega-menu-list">
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/cach-tao-trang-web' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/cach-tao-trang-web/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Khởi tạo website</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/lam-gi-sau-khi-co-website' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/lam-gi-sau-khi-co-website/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Làm gì sau khi có website</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/thiet-ke-website-responsive' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/thiet-ke-website-responsive/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Công nghệ Responsive website</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/y-tuong-thiet-ke-website' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/y-tuong-thiet-ke-website/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Ý tưởng thiết kế website</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/mau-website' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/mau-website/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Mẫu website tham khảo</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="mega-menu-box-banner menu-banner-js">
                                                                                    <div class="mega-menu-box-banner-slide menu-banner-slide-js" style="--transformX:0px">
                                                                                        <div class="mega-menu-box-banner-slide-item menu-banner-slide-item-js">
                                                                                            <a href="https://seo.monamedia.co/mona" target="_blank" class="mega-menu-box-banner-wrap">
                                                                                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/mega-menu-banner-1.jpg" alt="" class="img-ab">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="mega-menu-box-banner-slide-item menu-banner-slide-item-js">
                                                                                            <a href="https://mona.media/thiet-ke-website-tai-hcm/" target="_blank" class="mega-menu-box-banner-wrap">
                                                                                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/mega-menu-banner-3.jpg" alt="" class="img-ab">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="mega-menu-box-banner-slide-item menu-banner-slide-item-js">
                                                                                            <a href="https://mona.media/dich-vu-seo/" target="_blank" class="mega-menu-box-banner-wrap">
                                                                                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/mega-menu-banner-10.jpg" alt="" class="img-ab">
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="mega-menu-item megaMenuItem" style="--mega-background: #FFF5FB; --color:#F41E92">
                                                                <div class="d-flex">
                                                                    <span class="mega-menu-img">
                                                                        <img src="<?php echo get_site_url();?>/template/assets/images/header/mega-menu-solution-2.svg"
                                                                            alt="">
                                                                    </span>
                                                                    <span class="mega-menu-txt">
                                                                        <span class="title">Digital Marketing Solutions</span>
                                                                        <span class="desc">Tăng trưởng khách hàng</span>
                                                                    </span>
                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/header/icon-arrow.svg" alt="" class="mega-menu-arrow">
                                                                </div>
                                                                <div class="mega-menu-cate megaMenuCate">
                                                                    <div class="mega-menu-header d-flex f-between">
                                                                        <div class="mega-menu-back mega-menu-back-lv-2-js">
                                                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/header/icon-arrow-pri.svg" alt="">
                                                                            Trở lại
                                                                        </div>
                                                                        <div class="mega-menu-close mega-menu-close-js">
                                                                            <div class="hd-bar hdBar">
                                                                                <svg class="menu__svg" viewBox="0 0 100 100">
                                                                                    <path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path>
                                                                                    <path d="m 50,50 h 20" class="path-2"></path>
                                                                                    <path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path>
                                                                                </svg>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mega-menu-cate-wrap-w">
                                                                        <div class="mega-menu-cate-wrap d-flex f-ctn">
                                                                            <div class="mega-menu-box-wrap-item col mega-service">
                                                                                <div class="label label-mobile">Digital Marketing Solutions</div>
    
                                                                                <div class="mega-menu-list mega-menu-list-child-js">
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/dich-vu-seo' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/dich-vu-seo/"
                                                                                            class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-seo.svg"
                                                                                                    alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    <span class="mega-menu-sticky bgF5851E">Hot 🔥</span>
                                                                                                    Dịch vụ SEO
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Hình thức MKT tạo ROI cao nhất, chi phí chỉ vài chục triệu, giá trị cao bền vững
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/remarketing-la-gi' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/remarketing-la-gi/"
                                                                                            class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-megaphone.svg"
                                                                                                    alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Remarketing Ads
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Tăng doanh số với chi phí thấp
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/dich-vu-content-marketing' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/dich-vu-content-marketing/"
                                                                                            class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-change-cus-2.svg"
                                                                                                    alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Tối ưu Website <br> chuyển đổi khách hàng
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Chiến lược bài bản, phương pháp hiện đại
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'javascript:;' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/du-an/"
                                                                                            class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-multi-social.svg"
                                                                                                    alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Quản lý điều hành <br> các kênh Mạng Xã Hội
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Đội ngũ riêng, hiểu rõ nền tảng social
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/cham-soc-website' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/cham-soc-website/"
                                                                                            class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-love-2.svg"
                                                                                                    alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Các gói đồng hành <br> chăm sóc website toàn diện
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Tối ưu chất lượng, tỷ lệ chuyển đổi cao
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'https://mona.media/email-marketing-la-gi' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/email-marketing-la-gi/"
                                                                                            class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-brain.svg"
                                                                                                    alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Xây dựng chiến lược <br> Email Marketing
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Tiếp cận khách hàng mục tiêu, tăng nhận diện, gắn kết với khách hàng
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'javascript:;' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/du-an/"
                                                                                            class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-social-media-marketing.svg"
                                                                                                    alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Multi - Platform Ads - Quản lý quảng cáo đa nền tảng
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Google, Facebook, Zalo, Tiktok, Youtube,...
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
    
                                                                            <div class="mega-menu-box-wrap-item col mega-related">
                                                                                <div class="mega-menu-list">
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'javascript:;' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/du-an/" target="_blank"
                                                                                        class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Câu chuyện thành công của 300+ doanh nghiệp</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'javascript:;' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/du-an/" target="_blank"
                                                                                        class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Câu chuyện Mona - 3 năm thống lĩnh 3 ngành hàng</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'javascript:;' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/du-an/" target="_blank"
                                                                                        class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Vì sao cần làm SEO/Digital MKT?</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'javascript:;' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/du-an/" target="_blank"
                                                                                        class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Chọn Marketing inhouse hay Agency</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="mega-menu-box-banner menu-banner-js">
                                                                                    <div class="mega-menu-box-banner-slide menu-banner-slide-js" style="--transformX:0px">
                                                                                        <div class="mega-menu-box-banner-slide-item menu-banner-slide-item-js">
                                                                                            <a href="http://seo.monamedia.co/tai-sao-seo-la-cot-loi" target="_blank" class="mega-menu-box-banner-wrap">
                                                                                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/mega-menu-banner-2.jpg" alt="" class="img-ab">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="mega-menu-box-banner-slide-item menu-banner-slide-item-js">
                                                                                            <a href="https://mona.media/thiet-ke-website-tai-hcm/" target="_blank" class="mega-menu-box-banner-wrap">
                                                                                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/mega-menu-banner-5.jpg" alt="" class="img-ab">
                                                                                            </a>
                                                                                        </div>
                                                                                        <div class="mega-menu-box-banner-slide-item menu-banner-slide-item-js">
                                                                                            <a href="https://mona.media/dich-vu-seo/" target="_blank" class="mega-menu-box-banner-wrap">
                                                                                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/mega-menu-banner-10.jpg" alt="" class="img-ab">
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="mega-menu-item megaMenuItem" style="--mega-background:#FFF6ED; --color:#F5851E">
                                                                <div class="d-flex">
                                                                    <span class="mega-menu-img">
                                                                        <img src="<?php echo get_site_url();?>/template/assets/images/header/mega-menu-solution-3.svg"
                                                                            alt="">
                                                                    </span>
                                                                    <span class="mega-menu-txt">
                                                                        <span class="title">Branding Solutions</span>
                                                                        <span class="desc">Phát triển thương hiệu</span>
                                                                    </span>
                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/header/icon-arrow.svg" alt="" class="mega-menu-arrow">
                                                                </div>
                                                                <div class="mega-menu-cate megaMenuCate">
                                                                    <div class="mega-menu-header d-flex f-between">
                                                                        <div class="mega-menu-back mega-menu-back-lv-2-js">
                                                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/header/icon-arrow-pri.svg" alt="">
                                                                            Trở lại
                                                                        </div>
                                                                        <div class="mega-menu-close mega-menu-close-js">
                                                                            <div class="hd-bar hdBar">
                                                                                <svg class="menu__svg" viewBox="0 0 100 100">
                                                                                    <path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path>
                                                                                    <path d="m 50,50 h 20" class="path-2"></path>
                                                                                    <path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path>
                                                                                </svg>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="mega-menu-cate-wrap-w">
                                                                        <div class="mega-menu-cate-wrap d-flex f-ctn">
                                                                            <div class="mega-menu-box-wrap-item col mega-service">
                                                                                <div class="label label-mobile">Branding Solutions</div>
                                                                                
                                                                                <div class="mega-menu-list mega-menu-list-child-js">
                                                                                    <div class="mega-menu-item mega-menu-item-js">
                                                                                        <a href="https://mona.media/thiet-ke-bo-nhan-dien-thuong-hieu/" class="d-flex mega-menu-item-link mega-menu-item-link-js <?php echo return_megamenu_active( 'https://mona.media/thiet-ke-bo-nhan-dien-thuong-hieu' , 'active' ); ?>">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-stationery.svg" alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    <span class="mega-menu-sticky bgF41E92">Hot 🔥</span>
                                                                                                    Bộ nhận diện thương hiệu
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Độc đáo, sáng tạo, xây dựng hình ảnh thương hiệu đồng bộ
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js">
                                                                                        <a href="https://mona.media/chup-anh-profile-cong-ty/" class="d-flex mega-menu-item-link mega-menu-item-link-js <?php echo return_megamenu_active( 'https://mona.media/chup-anh-profile-cong-ty' , 'active' ); ?>">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-diaphragm.svg" alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Chụp hình doanh nghiệp
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Profile doanh nghiệp, hồ sơ năng lực, hình sản phẩm, sự kiện
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js">
                                                                                        <a href="https://mona.media/thiet-ke-logo-thuong-hieu/" class="d-flex mega-menu-item-link mega-menu-item-link-js <?php echo return_megamenu_active( 'https://mona.media/thiet-ke-logo-thuong-hieu' , 'active' ); ?>">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-typography.svg" alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Thiết kế Logo
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Chuyên nghiệp, ấn tượng, nhận diện thương hiệu dễ dàng
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'javascript:;' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/du-an/" class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-photo-camera.svg" alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Chụp hình lãnh đạo, nhân viên
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Cá nhân, teamwork, tư vấn concept phù hợp
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'javascript:;' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/du-an/" class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-id-card.svg" alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Business Card Design
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Yếu tố quảng bá thương hiệu hữu hiệu
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'javascript:;' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/du-an/" class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-drone.svg" alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Flycam Shot
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Chất lượng 6K, độ phân giải FullHD 1080p
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js">
                                                                                        <a href="https://mona.media/thiet-ke-website-3d-vr-360-thuc-te-ao/" class="d-flex mega-menu-item-link mega-menu-item-link-js <?php echo return_megamenu_active( 'https://mona.media/thiet-ke-website-3d-vr-360-thuc-te-ao' , 'active' ); ?>">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-vr-glasses.svg" alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Quay/dựng VR 360
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Website, quảng cáo, sự kiện, tiệc cưới,...
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item mega-menu-item-js <?php echo return_megamenu_active( 'javascript:;' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/du-an/" class="d-flex mega-menu-item-link mega-menu-item-link-js">
                                                                                            <span class="mega-menu-img">
                                                                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-writing.svg" alt="">
                                                                                            </span>
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">
                                                                                                    Gói đồng hành Thiết kế/Nội dung cho doanh nghiệp
                                                                                                </span>
                                                                                                <span class="desc">
                                                                                                    Đúng định hướng và mục tiêu kinh doanh
                                                                                                </span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
    
                                                                            
                                                                            <div class="mega-menu-box-wrap-item col mega-related"> 
                                                                                <div class="mega-menu-list">
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/branding-la-gi' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/branding-la-gi/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Branding là gì</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/mau-sac-nhan-dien-thuong-hieu' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/mau-sac-nhan-dien-thuong-hieu/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Màu sắc nhận diện thương hiệu</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/thiet-ke-logo-thuong-hieu' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/thiet-ke-logo-thuong-hieu/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Thiết kế Logo thương hiệu</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/xay-dung-thuong-hieu-ca-nhan' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/xay-dung-thuong-hieu-ca-nhan/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Xây dựng thương hiệu cá nhân</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                    <div class="mega-menu-item <?php echo return_megamenu_active( 'https://mona.media/brand-extension-la-gi' , 'active' ); ?>">
                                                                                        <a href="https://mona.media/brand-extension-la-gi/" target="_blank" class="d-flex">
                                                                                            <span class="mega-menu-txt">
                                                                                                <span class="title">Brand Extension</span>
                                                                                            </span>
                                                                                        </a>
                                                                                    </div>
                                                                                </div>

                                                                                <div class="mega-menu-box-banner menu-banner-js">
                                                                                    <div class="mega-menu-box-banner-slide menu-banner-slide-js" style="--transformX:0px">
                                                                                        <div class="mega-menu-box-banner-slide-item menu-banner-slide-item-js">
                                                                                            <a href="https://mona.media/thiet-ke-website-tai-hcm/" target="_blank" class="mega-menu-box-banner-wrap">
                                                                                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/mega-menu-banner-3.jpg" alt="" class="img-ab">
                                                                                            </a>
                                                                                        </div>
                                                                    
                                                                                        <div class="mega-menu-box-banner-slide-item menu-banner-slide-item-js">
                                                                                            <a href="https://seo.monamedia.co/mona" target="_blank" class="mega-menu-box-banner-wrap">
                                                                                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/mega-menu-banner-5.jpg" alt="" class="img-ab">
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <div class="menu-item menu-item-ani <?php echo return_megamenu_active( 'https://mona.media/du-an' ); ?>">
                                    <a href="https://mona.media/du-an/" class="menu-item-link">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/header/icon-project.svg" alt="" class="icon">
                                        Dự án
                                    </a>
                                </div>
                                <div class="menu-item menu-item-ani <?php echo return_megamenu_active(  MONA_SITE_URL .'/khach-hang' ); ?>">
                                    <a href="<?php echo MONA_SITE_URL ?>/khach-hang/" class="menu-item-link">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/header/icon-customer.svg" alt="" class="icon">
                                        Khách hàng
                                    </a>
                                </div>
                                <div class="menu-item menu-item-has-sub dropdown menu-item-ani <?php echo return_megamenu_active( MONA_SITE_URL . '/blog' ); ?>">
                                    <a href="<?php echo MONA_SITE_URL ?>/blog/" class="menu-item-link">
                                        <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-blog.svg" alt="" class="icon">
                                        Blog
                                    </a>
                                    <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-arrow.svg" alt="" class="icon-arrow subBtn">
                                    <ul class="submenu">
                                        <li class="submenu-item <?php echo return_megamenu_active( 'https://mona.media/seo' ); ?>">
                                            <a href="https://mona.media/seo/" target="_blank" class="submenu-item-link">
                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-seo-orange.svg" alt="" class="submenu-item-link-icon">
                                                Cẩm nang SEO
                                            </a>
                                        </li>
                                        <li class="submenu-item <?php echo return_megamenu_active( 'https://mona.media/digital-marketing' ); ?>">
                                            <a href="https://mona.media/digital-marketing/" target="_blank" class="submenu-item-link">
                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-briefcase-orange.svg" alt="" class="submenu-item-link-icon">
                                                Digital Marketing
                                            </a>
                                        </li>
                                        <li class="submenu-item <?php echo return_megamenu_active( 'https://mona.media/kien-thuc-website' ); ?>">
                                            <a href="https://mona.media/kien-thuc-website/" target="_blank" class="submenu-item-link">
                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-pen-tool-orange.svg" alt="" class="submenu-item-link-icon">
                                                Kinh nghiệm thiết kế website
                                            </a>
                                        </li>
                                        <li class="submenu-item <?php echo return_megamenu_active( 'https://mona.media/hosting' ); ?>">
                                            <a href="https://mona.media/hosting/" target="_blank" class="submenu-item-link">
                                                <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-cloud-orange.svg" alt="" class="submenu-item-link-icon">
                                                Kiến thức Hosting
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                                <div class="menu-item menu-item-ani <?php echo return_megamenu_active( 'https://mona.media/ban-tin-mona' ); ?>">
                                    <a href="https://mona.media/ban-tin-mona/" class="menu-item-link">
                                        <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-activity.svg" alt="" class="icon">
                                        Hoạt động
                                    </a>
                                </div>
                                <div class="menu-item menu-item-ani <?php echo return_megamenu_active( 'https://mona.media/tuyen-dung' ); ?>">
                                    <a href="https://mona.media/tuyen-dung/" class="menu-item-link" target="_blank">
                                        <img src="<?php echo get_site_url();?>/template/assets/images/header/icon-recruit.svg" alt="" class="icon">
                                        Tuyển dụng
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="hd-search">
                        <div class="hd-search-wrap">
                            <div class="hd-search-icon hd-search-js">
                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/icon-search-color-sw.svg" alt="">
                            </div>

                            <div class="hd-search-box hd-search-box-js">
                                <div class="container">
                                    <div class="hd-search-box-wrap">
                                        <div class="hd-search-form d-flex hd-search-form-js">
                                            <form action="<?php echo esc_url(home_url('/')); ?>" class="form d-flex" id="header-search-form">
                                                <input type="text" name="s" value="<?php echo get_search_query(); ?>" id="" placeholder="Tìm kiếm dịch vụ, giải pháp, bài viết,..." class="search-key search-key-js searchValue">
                                                <input type="submit" value="" class="search-btn">
                                            </form>
                                            <div class="hd-search-form-remove searchRemove">
                                                Xóa
                                            </div>
                                        </div>
                                        <div class="hd-search-history">
                                            <?php
                                            if ( isset( $_COOKIE["search_history"] ) && !empty( $_COOKIE["search_history"] ) ) {
                                                $cookieValue = preg_replace('/\\\\/', '', $_COOKIE["search_history"]);;
                                                $cookieValue = json_decode($cookieValue, true);
                                                if ( !empty( $cookieValue) && is_array( $cookieValue ) ) {
                                                    ?>
                                                    <div class="hd-search-history-list hd-search-suggest">
                                                        <div class="hd-search-suggest-item">
                                                            <div class="suggest-list">
                                                                <?php 
                                                                foreach ( $cookieValue as $key => $value ) {
                                                                    ?>
                                                                    <a href="<?php echo get_site_url('/') . '?s=' . urlencode( $value ) ?>" class="suggest-item d-flex history-item-js" data-value="<?php echo $value; ?>">
                                                                        <span class="icon">
                                                                            <img src="<?php echo get_site_url() ?>/template/assets/images/header/icon-time-circle.svg" alt="">
                                                                        </span>
                                                                        <span class="name"><?php echo $value; ?></span>
                                                                        <span class="history-remove history-remove-js" data-index="<?php echo $key; ?>">Xóa</span>
                                                                    </a>
                                                                    <?php
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                }
                                            }
                                            ?>

                                            <div class="hd-search-suggest">
                                                <div class="hd-search-suggest-item">
                                                    <div class="label">Dịch vụ</div>
        
                                                    <div class="suggest-list">
                                                        <a href="https://mona.media/thiet-ke-website-tai-hcm/" class="suggest-item d-flex" target="_blank">
                                                            <span class="icon">
                                                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/icon-time-circle.svg" alt="">
                                                            </span>
                                                            <span class="name">Thiết kế website</span>
                                                        </a>
                                                        <a href="https://mona.media/dich-vu-seo/" class="suggest-item d-flex" target="_blank">
                                                            <span class="icon">
                                                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/icon-time-circle.svg" alt="">
                                                            </span>
                                                            <span class="name">Dịch vụ SEO</span>
                                                        </a>
                                                        <a href="https://mona.media/web-hosting/" class="suggest-item d-flex" target="_blank">
                                                            <span class="icon">
                                                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/icon-time-circle.svg" alt="">
                                                            </span>
                                                            <span class="name">Hosting cho website</span>
                                                        </a>
                                                    </div>
                                                </div>
                                                <div class="hd-search-suggest-item">
                                                    <div class="label">Khác</div>
        
                                                    <div class="suggest-list">
                                                        <a href="https://seo.monamedia.co/mona/" class="suggest-item d-flex" target="_blank">
                                                            <span class="icon">
                                                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/icon-time-circle.svg" alt="">
                                                            </span>
                                                            <span class="name">Câu chuyện thành công của Mona</span>
                                                        </a>
                                                        <a href="https://mona.media/tuyen-dung/" class="suggest-item d-flex" target="_blank">
                                                            <span class="icon">
                                                                <img src="<?php echo get_site_url() ?>/template/assets/images/header/icon-time-circle.svg" alt="">
                                                            </span>
                                                            <span class="name">Tuyển dụng</span>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <?php echo mona_get_hotline(); ?>
        
                    <div class="hd-bar hdBar">
                        <svg class="menu__svg" viewBox="0 0 100 100">
                            <path d="m 30,33 h 40 c 3.722839,0 7.5,3.126468 7.5,8.578427 0,5.451959 -2.727029,8.421573 -7.5,8.421573 h -20"></path>
                            <path d="m 50,50 h 20" class="path-2"></path>
                            <path d="m 70,67 h -40 c 0,0 -7.5,-0.802118 -7.5,-8.365747 0,-7.563629 7.5,-8.634253 7.5,-8.634253 h 20"></path>
                        </svg>
                    </div>
                </div>
            </div>
        </div>
        <div class="bg-page bgPage"></div>
    </header>