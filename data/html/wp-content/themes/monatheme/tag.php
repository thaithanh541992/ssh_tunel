<?php
/**
 * The template for displaying search.
 *
 * @package Monamedia
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
?>
<main class="main">

    <?php 
    $current_taxonomy = get_queried_object();
	$post_type = 'post';
	$posts_per_page = 12;
	$paged = max( 1, get_query_var('paged') );
	$offset = ( $paged - 1 ) * $posts_per_page;
	$argsPost = [
		'post_type' => $post_type,
		'post_status' => 'publish',
		'posts_per_page' => $posts_per_page,
		'paged' => $paged,
		'offset' => $offset,
		'meta_query' => [
			'relation' => 'AND',
		],
		'tax_query' => [
			'relation'=>'AND',
            array(
                'taxonomy' => $current_taxonomy->taxonomy,
                'field' => 'slug',
                'terms' => $current_taxonomy->slug,
            )
		]
	];

	$postsMONA = new WP_Query( $argsPost );  
	?>
    <section class="sec-blogr">
        <div class="container">
            <div class="blogr">
                <h2 class="title">
                    <?php echo $current_taxonomy->name; ?>
                </h2>
                <div class="blogr-wrap">
                    <div class="blogr-flex">
                        <div class="blogr-left">
                            <?php if( $postsMONA->have_posts() ){ ?>
                            <form id="frmPostAjax">
								<div class="blogr-list is-loading-btn" id="monaPostList">
									<?php while( $postsMONA->have_posts() ){
										$postsMONA->the_post();
										?>
									<div class="blogr-item col-4">
										<?php 
										/**
										 * GET TEMPLATE PART
										 * partials
										 * blog box
										 */
										$slug = '/partials/loop/box';
										$name = 'blog';
										echo get_template_part($slug, $name);
										?>
									</div>
									<?php } wp_reset_query(); ?>
									<div class="pagination-products-ajax col-12">
										<?php mona_pagination_links_ajax( $postsMONA ); ?>
									</div>
								</div>
								<input type="hidden" name="post_type" value="<?php echo $post_type ?>" />
								<input type="hidden" name="posts_per_page" value="<?php echo $posts_per_page ?>" />
								<input type="hidden" name="paged" value="<?php echo $paged ?>" />
                                <?php if( is_category($current_taxonomy) ){ ?>
                                <input type="hidden" name="taxonomies[<?php echo $current_taxonomy->taxonomy ?>]" value="<?php echo $current_taxonomy->slug; ?>" />
                                <?php } ?>
							</form>
							<?php }else{ ?>
								
							<div class="mona-mess-empty">
								<p><?php echo __( 'Nội dung đang được cập nhật', 'monamedia' ) ?></p>
							</div>

							<?php } ?>
                        </div>
                        <div class="blogr-right">
                            <div class="blogr-pos">
                                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar_blog_detail_right')) : ?>
								<?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <?php 
    $slug = '/partials/global/contact';
    $name = '';
    echo get_template_part($slug, $name);
    ?>
</main>

<?php get_footer();
