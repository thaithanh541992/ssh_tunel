<?php
/**
 * Template name: Service Cloud Hosting
 * @author : MONA.Media / Website
 */
get_header();
while ( have_posts() ):
    the_post();
    ?>
    <main class="main">
        <?php
        Elements::Group( 'service-hosting', [
            '14_success' => [
                'insert' => [
                    'after' => [
                        'global/eco-system',
                        'global/faq',
                        'global/contact-2',
                    ]
                ],
            ]
        ] )->Html();
        ?>
    </main>
    <?php
endwhile;
get_footer();