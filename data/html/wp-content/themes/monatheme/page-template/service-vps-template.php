<?php
/**
 * Template name: Service Cloud VPS
 * @author : MONA.Media / Website
 */
get_header();
while ( have_posts() ):
    the_post();
    ?>
    <main class="main">
        <?php
        Elements::Group( 'service-vps', [
            '12_monahost' => [
                'insert' => [
                    'after' => [
                        'global/eco-system',
                        'global/faq',
                        'global/contact-2',
                    ]
                ],
            ]
        ] )->Html();
        ?>
    </main>
    <?php
endwhile;
get_footer();