<?php
/**
 * Template name: Trang chủ
 * @author : MONA.Media / Website
 */
get_header();
while ( have_posts() ):
    the_post();
    ?>
    <main class="main">
        <?php
        Elements::Group( 'home', [
            '19_six_year' => [
                'insert' => [
                    'after' => [
                        'global/eco-system',
                        'global/faq',
                        'global/contact-2',
                    ]
                ],
            ]
        ] )->Html();
        ?>
    </main>
    <?php
endwhile;
get_footer();