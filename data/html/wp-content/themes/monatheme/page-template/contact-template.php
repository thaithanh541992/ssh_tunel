<?php
/**
 * Template name: Liên hệ Page
 * @author : Hy Hý
 */
get_header();
while ( have_posts() ):
    the_post();
    ?>
   
    <main class="main">
        
        <?php 
        Elements::Group( 'contact', 
            [
                'map' => [
                    'insert' => [
                        'before' => [
                            'global/appointment'
                        ]
                    ],
                ],
            ] 
        )->Html(); ?>

    </main>

<?php
endwhile;
get_footer();