<?php
/**
 * Template name: Khách hàng Page
 * @author : Hy Hý
 */
get_header();
while ( have_posts() ):
    the_post();
    ?>
   
   <main class="main">
        <div class="gr-section">
            <!-- CUSC-BANNER -->
            <div class="sec-cusc-banner">
                <div class="cusc-banner">
                    <div class="cusc-banner-br">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-banner-br.png" alt="">
                    </div>
                    <div class="cusc-banner-decor"></div>
                    <div class="container">
                        <div class="head txt-center mb-40" data-aos="fade-up">
                            <h1 class="c-title mb-32 ani" data-spl>
                                Khách hàng của MONA<span class="pri">.Host</span>
                            </h1>
                            <p class="des ani" data-spl>
                                Trong suốt tiến trình phát triển của Mona, chúng tôi vẫn thực hiện nguyên tắc <span
                                    class="c-orange">hợp tác 2 bên cùng có lợi</span>. <br> Mona chỉ hợp tác và triển khai những dự án mà chúng tôi có thể đảm bảo sự thành công của doanh nghiệp đó.
                            </p>
                        </div>
                        <div class="cusc-banner-img">
                            <div class="cusc-banner-user-list">

                                <div class="cusc-banner-user">
                                    <div class="cusc-banner-user-ava">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-banner-user-4.jpg" alt="">
                                    </div>
                                    <div class="cusc-banner-user-content">
                                        <p class="cusc-banner-user-name">
                                            Anh Công
                                        </p>
                                        <p class="cusc-banner-user-des">
                                            tháng này<br>
                                            total 1,482,000,000<br>
                                            cũng hơi ghê chút ha
                                        </p>
                                        <div class="c-bubble">
                                            <div class="c-bubble__container">
                                                <div class="c-bubble__point"></div>
                                                <div class="c-bubble__point"></div>
                                                <div class="c-bubble__point"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="cusc-banner-user">
                                    <div class="cusc-banner-user-ava">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/chiHoangLinh.png" alt="">
                                    </div>
                                    <div class="cusc-banner-user-content">
                                        <p class="cusc-banner-user-name">
                                            chị Hoàng Linh
                                        <p class="cusc-banner-user-des">
                                            Website ok rồi á em<br>
                                            sếp chị duyệt rồi,<br>
                                            khách vào cũng khen
                                        </p>
                                        <div class="c-bubble">
                                            <div class="c-bubble__container">
                                                <div class="c-bubble__point"></div>
                                                <div class="c-bubble__point"></div>
                                                <div class="c-bubble__point"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="cusc-banner-user">
                                    <div class="cusc-banner-user-ava">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/anhXuan.jpg" alt="">
                                    </div>
                                    <div class="cusc-banner-user-content">
                                        <p class="cusc-banner-user-name">
                                            Anh Xuân
                                        </p>
                                        <p class="cusc-banner-user-des">
                                            Doanh thu tăng được 100 triệu <br> so với tháng trước anh
                                        </p>
                                        <div class="c-bubble">
                                            <div class="c-bubble__container">
                                                <div class="c-bubble__point"></div>
                                                <div class="c-bubble__point"></div>
                                                <div class="c-bubble__point"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                
                                <div class="cusc-banner-user">
                                    <div class="cusc-banner-user-ava">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-banner-user-1.jpg" alt="">
                                    </div>
                                    <div class="cusc-banner-user-content">
                                        <p class="cusc-banner-user-name">
                                            Chị Nguyễn Giang
                                        </p>
                                        <p class="cusc-banner-user-des">
                                            Chị rất thích làm việc<br>
                                            cùng team Mona, luôn<br>
                                            nhiệt tình và sáng tạo
                                        </p>
                                        <div class="c-bubble">
                                            <div class="c-bubble__container">
                                                <div class="c-bubble__point"></div>
                                                <div class="c-bubble__point"></div>
                                                <div class="c-bubble__point"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="cusc-banner-user">
                                    <div class="cusc-banner-user-ava">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-banner-user-3.jpg" alt="">
                                    </div>
                                    <div class="cusc-banner-user-content">
                                        <p class="cusc-banner-user-name">
                                            Anh Phú
                                        </p>
                                        <p class="cusc-banner-user-des">
                                            Tháng này êm lắm Tuấn,<br>
                                            khách sỉ hỏi nhiều
                                        </p>
                                        <div class="c-bubble">
                                            <div class="c-bubble__container">
                                                <div class="c-bubble__point"></div>
                                                <div class="c-bubble__point"></div>
                                                <div class="c-bubble__point"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="cusc-banner-user">
                                    <div class="cusc-banner-user-ava">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-banner-user-2.jpg" alt="">
                                    </div>
                                    <div class="cusc-banner-user-content">
                                        <p class="cusc-banner-user-name">
                                            Võ Minh Thiên
                                        </p>
                                        <p class="cusc-banner-user-des">
                                            Viết content chất như cất, <br> content design đồng nhất
                                        </p>
                                        <div class="c-bubble">
                                            <div class="c-bubble__container">
                                                <div class="c-bubble__point"></div>
                                                <div class="c-bubble__point"></div>
                                                <div class="c-bubble__point"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="cusc-banner-img-inner">
                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-banner-img.png" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- CUSC-ONE -->
            <div class="sec-cusc-one">
                <div class="cusc-one ss-pd">
                    <div class="cusc-one-panda">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-one-panda.png" alt="">
                    </div>
                    <div class="container">
                        <div class="cusc-one-side">
                            <div class="cusc-one-left">
                                <div class="cusc-one-person scr-item custom-fadeInLeftBig">
                                    <div class="cusc-one-person-decor">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-one-person-decor.svg" alt="">
                                    </div>
                                    <div class="cusc-one-person-inner">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-one-person.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="cusc-one-right">
                                <div class="cusc-one-number">
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-one-number-sw.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="cusc-one-content">
                        <div class="cusc-one-text-line scr-item custom-fadeInRightBig">
                            Khách hàng đa ngành, <br> lĩnh vực chúng tôi đã và <br>
                            <span class="sline">đang đồng hành</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- CUSC-THREE -->
        <?php 

        $customer_gal_rs = get_media_customer_gallery();
        
        if ( !empty( $customer_gal_rs ) ) {
            $customer_gal_rs = json_decode( $customer_gal_rs );
            if ( $customer_gal_rs->status == 200 
                && !empty( $customer_gal_rs->responses ) 
                && is_array( $customer_gal_rs->responses ) 
            ) {
                ?>
                <div class="sec-cusc-three">
                    <div class="cusc-three getWidth">
                        <div class="cusc-three-list gallery">

                            <?php foreach ($customer_gal_rs->responses as $key => $attachment_url) { ?>
                            <div class="cusc-three-item">
                                <div class="cusc-three-img gallery__img" data-src="<?php echo $attachment_url; ?>">
                                    <img src="<?php echo $attachment_url; ?>" alt="">
                                </div>
                            </div>
                            <?php } ?>
                    
                        </div>
                    </div>
                </div>
                <?php
            }
        }
        ?>

        <section class="sec-cusc-four-five ss-pd">
            <div class="cusc-four-five-decor">
                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-four-five-decor.svg" alt="">
            </div>
            <div class="cusc-four ss-mg-b">
                <div class="container">
                    <div class="head">
                        <h2 class="c-title ani second hide-br">
                            <span class="hl none-rt">
                                <span class="hl-inner">
                                    Bất kể khách hàng nào
                                </span>
                            </span>
                            chúng tôi cũng từng <br> trao đổi và hợp tác <br> thành công
                        </h2>

                    </div>
                    <div class="cusc-four-main">
                        <div class="cusc-four-mess scr-item custom-bouncenInLeft">
                            <svg viewBox="0 0 345 249" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path
                                    d="M225.564 214.252C221.685 178.347 259.824 153.387 284.536 131.781C305.739 113.243 330.472 89.6586 339.768 63.2469C343.821 51.7327 344.522 37.0187 337.82 26.428C329.752 13.6775 312.64 7.18092 297.973 4.1752C267.964 -1.97461 224.666 7.16995 191.981 22.7637C143.323 45.9787 106.018 77.0184 92.0668 88.3409C47.6072 124.423 43.5827 134.945 20.1376 167.161C11.822 182.267 -5.25942 215.302 6.11965 231.294C14.4812 243.044 29.6819 246.258 43.8481 246.973C104.51 250.036 161.573 219.113 212.087 190.298"
                                    stroke="#F41E92" stroke-width="3" stroke-linecap="round" class="svg-elem-1">
                                </path>
                            </svg>
                            <span class="text">
                                Levents & Mona kí kết hợp đồng thứ 3 trong 2 năm
                            </span>
                        </div>
                        <div class="dsmall cusc-four-list">
                            <div class="dsmall-item cusc-four-item">
                                <div class="cusc-four-wrap">
                                    <div class="cusc-four-img">
                                        <div class="cusc-four-img-inner">
                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-four-img-1.jpg" alt="">
                                        </div>
                                        <div class="cusc-four-logo">
                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-four-logo-1.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="dsmall-item cusc-four-item">
                                <div class="cusc-four-wrap">
                                    <div class="cusc-four-img">
                                        <div class="cusc-four-img-inner">
                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-four-img-2.jpg" alt="">
                                        </div>
                                        <div class="cusc-four-logo">
                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-four-logo-2.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="dsmall-item cusc-four-item">
                                <div class="cusc-four-wrap">
                                    <div class="cusc-four-img">
                                        <div class="cusc-four-img-inner">
                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-four-img-3.jpg" alt="">
                                        </div>
                                        <div class="cusc-four-logo">
                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-four-logo-3.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cusc-five">
                <div class="container">
                    <div class="dsmall cusc-five-top">
                        <div class="dsmall-item cusc-five-top-item">
                            <div class="head">
                                <h2 class="c-title ani second">
                                    <span class="hl none-rt">
                                        <span class="hl-inner">
                                            Từ những khách hàng khó
                                        </span>
                                    </span>
                                    Nhật Bản, Hàn Quốc, <br> Trung Quốc,...
                                </h2>
                            </div>
                        </div>
                        <div class="dsmall-item cusc-five-top-item">
                            <div class="dsmall cusc-five-logo">
                                <div class="dsmall-item cusc-five-logo-item">
                                    <div class="cusc-five-logo-img">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-five-logo-1.jpg" alt="">
                                    </div>
                                </div>
                                <div class="dsmall-item cusc-five-logo-item">
                                    <div class="cusc-five-logo-img">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-five-logo-2.jpg" alt="">
                                    </div>
                                </div>
                                <div class="dsmall-item cusc-five-logo-item">
                                    <div class="cusc-five-logo-img">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-five-logo-3.jpg" alt="">
                                    </div>
                                </div>
                                <div class="dsmall-item cusc-five-logo-item">
                                    <div class="cusc-five-logo-img">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-five-logo-4.jpg" alt="">
                                    </div>
                                </div>
                                <div class="dsmall-item cusc-five-logo-item">
                                    <div class="cusc-five-logo-img">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-five-logo-5.jpg" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="dsmall cusc-five-bot">
                        <div class="dsmall-item cusc-five-bot-item">
                            <div class="cusc-five-image">
                                <div class="dsmall cusc-five-image-list">
                                    <div class="dsmall-item cusc-five-image-item">
                                        <div class="cusc-five-image-img">
                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-five-image-1.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="dsmall-item cusc-five-image-item">
                                        <div class="cusc-five-image-img">
                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/khach-hang-nuoc-ngoai-MONA.jpg" alt="">
                                        </div>
                                    </div>
                                    <div class="dsmall-item cusc-five-image-item">
                                        <div class="cusc-five-image-img">
                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-five-image-3.jpg" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="dsmall-item cusc-five-bot-item">
                            <div class="cusc-five-bot-title">
                                <div class="cusc-five-bot-arr">
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-five-bot-arrr.svg" alt="">
                                </div>
                                <p class="c-title ani second">
                                    Đến những <br>
                                    <span class="hl none-rt">
                                        <span class="hl-inner">
                                            khách hàng cầu toàn
                                        </span>
                                    </span>
                                    <br> tối giản Châu Âu, Úc
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="cusc-five-more">
                        <div class="cusc-five-more-item pink">
                            Hơn thế nữa
                        </div>
                        <div class="cusc-five-more-item">
                            Và nhiều hơn thế nữa!!!
                        </div>
                        <div class="cusc-five-more-arr">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-five-more-arr.svg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php
        $customer_gal_2_rs = get_media_customer_gallery_2();
        
        if ( !empty( $customer_gal_2_rs ) ) {
            $customer_gal_2_rs = json_decode( $customer_gal_2_rs );
            if ( $customer_gal_2_rs->status == 200 
                && !empty( $customer_gal_2_rs->responses ) 
            ) {
                $responses = $customer_gal_2_rs->responses;
                $title = $responses->title;
                $description  =   $responses->description;
                $outstanding_projects  =   $responses->outstanding_projects;
                $title_outstanding_projects   =   $responses->title_outstanding_projects;
                $attachment_urls   =   $responses->attachment_urls;
                ?>
                <section class="sec-cusc-six-seven">
                    <div class="sec-cusc-six-seven-decor-top">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/decor-cloud-customer-sw.png" alt="">
                    </div>
                    <div class="cusc-six ss-pd">
                        <div class="container">
                            <div class="head txt-center mb-40">
                                <?php if( !empty( $title ) ){ ?>
                                <h2 class="c-title hide-br second white ani mb-16">
                                    <?php echo str_replace(
                                        ['[', ']'],
                                        ['<span class="hl hl-orange none-rt"><span class="hl-inner">', '</span></span>'],
                                        $title
                                    ); ?>
                                </h2>
                                <?php } ?>

                                <?php if( !empty( $description ) ){ ?>
                                <p class="des t24 c-white">
                                    <?php echo str_replace(
                                        ['[', ']'],
                                        ['<span class="fw-7 c-blue-light">', '</span>'],
                                        $description
                                    ); ?>
                                </p>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="ab-nine-wrapper ab-nine">
                            <div class="ab-nine-list gallery">

                                <?php foreach ($attachment_urls as $attachment_url) { ?>
                                <div class="ab-nine-item" data-aos="fade">
                                    <div class="ab-nine-img gallery__img" data-src="<?php echo $attachment_url; ?>">
                                        <img src="<?php echo $attachment_url; ?>" alt=""> 
                                    </div>
                                </div>
                                <?php } ?>
                                
                            </div>
                        </div>
                    </div>
                    <?php 
                
                    if( is_array( $outstanding_projects ) && !empty( $outstanding_projects ) ){
                    ?>
                    <div class="cusc-seven ss-pd-b">
                        <?php if( !empty( $title_outstanding_projects ) ){ ?>
                        <div class="container">
                            <div class="head txt-center mb-16">
                                <h2 class="c-title hide-br second white ani mb-48">
                                    <?php echo str_replace(
                                        ['[', ']'],
                                        ['<span class="hl hl-orange none-rt"><span class="hl-inner">', '</span></span>'],
                                        $title_outstanding_projects
                                    ); ?>
                                </h2>
                            </div>
                        </div>
                        <?php } ?>
                        <div class="cusc-seven-splide splide">
                            <div class="splide__track">
                                <div class="splide__list">
                                    <?php 
                                    foreach ($outstanding_projects as $key => $outstanding_project) {
                                        if( !empty( $outstanding_project->thumbnail ) ){ ?>
                                    <div class="splide__slide">
                                        <?php if( !empty( $outstanding_project->excerpt ) ){ ?>
                                        <div class="cusc-seven-content">
                                            <p class="text">
                                                <?php echo $outstanding_project->excerpt; ?>
                                            </p>
                                            <div class="cusc-seven-arr">
                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-customer/cusc-six-arr.svg" alt="">
                                            </div>
                                        </div>
                                        <?php } ?>

                                        <?php if( empty( $outstanding_project->url ) ){ ?>
                                        <div class="cusc-seven-img">
                                            <img src="<?php echo $outstanding_project->thumbnail; ?>" alt="">
                                        </div>
                                        <?php }else{ ?> 
                                        <a href="<?php echo $outstanding_project->url ?>" class="cusc-seven-img" target="<?php echo $outstanding_project->target; ?>">
                                            <img src="<?php echo $outstanding_project->thumbnail; ?>" alt="">
                                        </a>
                                        <?php } ?>
                                    </div>
                                    <?php } } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    
                    <?php $default_projects_title = get_field('mona_customers_default_projects_title');
                    $default_projects_description = get_field('mona_customers_default_projects_description');
                    if( !empty( $default_projects_title ) || !empty( $default_projects_description ) ){ ?>
                    <div class="container">
                        <div class="cusc-seven-bot">
                            <div class="cusc-seven-bot-inner">

                                <?php if( !empty( $default_projects_title ) ){ ?>
                                <div class="head txt-center mb-16">
                                    <h2 class="c-title hide-br second white ani mb-16">
                                        <?php 
                                            echo str_replace(
                                                ['[', ']'],
                                                ['<span class="hl hl-orange none-rt"><span class="hl-inner">' , '</span></span>'],
                                                $default_projects_title
                                            );
                                        ?>
                                    </h2>
                                </div>
                                <?php } ?>

                                <?php if( !empty( $default_projects_description ) ){ ?>
                                <div class="des">
                                    <?php 
                                        echo str_replace(
                                            ['[', ']'],
                                            ['<span class="text">' , '</span>'],
                                            $default_projects_description
                                        );
                                    ?>
                                </div>
                                <?php } ?>
                                <div class="decor">
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cuss-sevent-line.png" alt="">
                                </div>
                                <div class="decor2">
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cuss-sevent-line.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <!-- Dự án cố định -->
                    <div id="wd-slide" class="wd-slide viewHover isViewv">
                        <div id="wd-slide-container" class="wd-slide-container">
                            <div id="wd-slide-item-5" class="item wd-slide-item sg-retro wdIntoView">
                                <div class="wd-slide-item-wrap">
                                    <div class="wd-slide-bg">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/sg-retro-bg.png" alt="" class="swiper-lazy">
                                    </div>
            
                                    <div class="wd-slide-item-wrap-content">
                                        <div class="container">
                                            <div class="wd-slide-item-wrap-content-flex">
                                                <div class="wd-slide-img d-flex">
                                                    <div class="wd-slide-img-col col col-4 left">
                                                        <div class="img">
                                                            <div class="img-relative">
                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/sg-retro-website.png" alt=""
                                                                    class=" swiper-lazy">
                                                            </div>
            
                                                            <div class="img-absolute users">
                                                                <div class="img-border-wrap" data-swiper-parallax="-100%">
                                                                    <div class="txt-item">
                                                                        <div class="desc desc-flex">
                                                                            Traffic tự nhiên
                                                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-up-green.png" alt=""
                                                                                class="icon">
                                                                        </div>
                                                                        <div class="title">
                                                                            1.200%
                                                                        </div>
                                                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/line-map.png" alt=""
                                                                            class="desc-img">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="img-absolute re-emp">
                                                                <div class="img-border-wrap" data-swiper-parallax="-100%">
                                                                    <div class="txt-item">
                                                                        <div class="desc desc-flex">
                                                                            <span class="fw-700">Giảm tải </span> nhân sự kho
                                                                        </div>
                                                                        <div class="re-emp-num d-flex f-center">
                                                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/ar-down.png" alt=""
                                                                                class="icon">
                                                                            89%
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wd-slide-img-col col col-8">
                                                        <div class="img d-flex f-ctn">
                                                            <div class="col col-6 as-end oxva">
                                                                <div class="img-relative">
                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/sg-retro-oxva-xlim.png" alt=""
                                                                        class=" swiper-lazy">
                                                                </div>
                                                                <div class="img-absolute open">
                                                                    <div class="img-border-wrap" data-swiper-parallax="-100%">
                                                                        <div class="txt-item">
                                                                            <div class="title">
                                                                                Quản lý đơn hàng
                                                                            </div>
                                                                            <div class="desc desc-flex">
                                                                                online offline <br> chung hệ thống
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col col-6 sg-vape as-end">
                                                                <div class="img-relative">
                                                                    <div class="sg-vape-de">
                                                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/sg-vape-de.png" alt=""
                                                                            class=" swiper-lazy">
                                                                    </div>
            
                                                                    <div class="img-absolute sg-vape-ox">
                                                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/sg-vape-oxva.png" alt=""
                                                                            class="cir swiper-lazy">
                                                                    </div>
                                                                </div>
            
                                                                <div class="img-absolute open">
                                                                    <div class="img-border-wrap" data-swiper-parallax="-100%">
                                                                        <div class="txt-item">
                                                                            <div class="title">
                                                                                <span class="hl-sg-retro">
                                                                                    Mở thêm
                                                                                </span>
                                                                            </div>
                                                                            <div class="title">
                                                                                chi nhánh mới
                                                                            </div>
                                                                            <div class="desc desc-flex">
                                                                                sau 2 tháng bán Online
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
            
                                                                <div class="img-absolute traffic">
                                                                    <div class="img-border-wrap" data-swiper-parallax="-100%">
                                                                        <div class="txt-item txt-center">
                                                                            <div class="title">
                                                                                +500%
                                                                            </div>
                                                                            <div class="desc desc-flex">
                                                                                <span class="fw-700">Doanh số </span> từ bán
                                                                                online
                                                                            </div>
            
                                                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/column-map2.png" alt=""
                                                                                class="desc-img">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
            
                                                <div class="wd-slide-content wdContentJS">
                                                    <div class="wd-slide-info col col-4" data-swiper-parallax="-100%">
                                                        <div class="stt ">01.</div>
                                                        <div class="name ">Saigon Retro Vape</div>
                                                        <div class="desc ">Chuỗi hệ thống Vape/Pod hàng đầu Hồ Chí
                                                            Minh
                                                            <span class="fw-700">bứt phá 500% Doanh số online từ website</span>
                                                        </div>
            
                                                        <div class="pj-cate  hideHover">
                                                            <div class="cate-sm-box">
                                                                <div class="cate-sm-list d-flex">
                                                                    <div class="cate-sm-item">
                                                                        <a target="_blank" href="https://saigonretrovape.vn/">website</a>
                                                                    </div>
                                                                    <div class="cate-sm-item">
                                                                        <a target="_blank" href="https://saigonretrovape.vn/">webapp</a>
                                                                    </div>
                                                                    <div class="cate-sm-item">
                                                                        <a target="_blank" href="http://seo.monamedia.co/case-seo-saigon-retro-vape">seo</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="wd-slide-contact hideHover">
                                    <div class="container">
                                        <div class="contact-btn d-flex f-end f-nowrap">
                                            <a href="javascript:;" class="btn btn-white openPop">Liên hệ tư vấn</a>
                                            <a href="https://saigonretrovape.vn/" class="view-device d-flex f-center" target="_blank">
                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-device.png" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="wd-slide-item-1" class="item wd-slide-item kim-thanh wdIntoView">
                                <div class="wd-slide-item-wrap">
                                    <div class="wd-slide-item-wrap-content">
                                        <div class="container">
                                            <div class="wd-slide-item-wrap-content-flex">
                                                <div class="wd-slide-img d-flex">
                                                    <div class="wd-slide-img-col col col-5">
                                                        <div class="img">
                                                            <div class="img-relative">
                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/kimthanh-bolocnhot.png" alt=""
                                                                    class="kt-gsap-fade-up swiper-lazy">
                                                            </div>
                                                            <div class="img-absolute loc-giua">
                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/kimthanh-locgiua.png" alt=""
                                                                    class="kt-gsap-fade-down">
                                                            </div>
                                                            <div class="img-absolute qlhh gsap-fade-up"
                                                                data-swiper-parallax="-100%">
                                                                <div class="txt-stars d-flex">
                                                                    <div class="txt-stars-lb">Quản lý hàng hóa</div>
                                                                    <div class="stars">
                                                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-star.png" alt="">
                                                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-star.png" alt="">
                                                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-star.png" alt="">
                                                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-star.png" alt="">
                                                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-star.png" alt="">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wd-slide-img-col col col-7">
                                                        <div class="img d-flex">
                                                            <div class="img-traffic col-6">
                                                                <div class="img-traffic-list d-flex">
                                                                    <div class="img-traffic-item col-6">
                                                                        <div class="img-traffic-item-wrap"
                                                                            data-swiper-parallax="-100%">
                                                                            <div class="txt-item kt">
                                                                                <div class="desc desc-flex">
                                                                                    <span class="">Doanh thu Online</span>
            
                                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-dollar-circle.png"
                                                                                        alt="" class="icon">
                                                                                </div>
                                                                                <div class="title">
                                                                                    <span class="">
                                                                                        +250%
                                                                                    </span>
                                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-up-green-light.png"
                                                                                        alt="" class="icon icon-w">
                                                                                </div>
                                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/bd-kt.png" alt=""
                                                                                    class="icon-bd">
                                                                                <div class="desc desc-flex desc-end">
                                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-bd-line1.png"
                                                                                        alt="" class="icon">
                                                                                    so với <br> kinh doanh Offline
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="img-traffic-item col-6">
                                                                        <div class="img-traffic-item-wrap"
                                                                            data-swiper-parallax="-100%">
                                                                            <div class="txt-item kt">
                                                                                <div class="desc desc-flex">
                                                                                    Nhân sự kho
                                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-profile.png"
                                                                                        alt="" class="icon">
                                                                                </div>
                                                                                <div class="title">
                                                                                    -80%
                                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-dropdown-red.png"
                                                                                        alt="" class="icon icon-w">
                                                                                </div>
                                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/bieudo-line2.png" alt=""
                                                                                    class="icon-bd">
                                                                                <div class="desc desc-flex desc-end text-left">
                                                                                    Tự động hóa <br> XXX đơn hàng/ngày
                                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-bd-line1.png"
                                                                                        alt="" class="icon">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="img-traffic-item col-6">
                                                                        <div class="img-traffic-item-wrap"
                                                                            data-swiper-parallax="-100%">
                                                                            <div class="txt-item kt">
                                                                                <div class="desc desc-flex">
                                                                                    Organic Traffic
                                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-activity.png"
                                                                                        alt="" class="icon">
                                                                                </div>
                                                                                <div class="title">
                                                                                    +200%
                                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-up-green-light.png"
                                                                                        alt="" class="icon icon-w">
                                                                                </div>
                                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/bieudo-line.png" alt=""
                                                                                    class="icon-bd">
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
            
                                                                <div class="link-group kt-gsap-fade-down">
                                                                    <div class="link-group-list d-flex f-ctn">
                                                                        <div class="link-group-item col"
                                                                            data-swiper-parallax="-200%">
                                                                            <a href="https://kimthanh.online/"
                                                                                class="d-flex btn-link hideHover" target="_blank">
                                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/arrow-ani-c.png" alt="">
                                                                                https://kimthanh.online/
                                                                            </a>
                                                                        </div>
                                                                        <div class="link-group-item col col-6">
                                                                            <div class="txt-stars d-flex"
                                                                                data-swiper-parallax="-200%">
                                                                                <div class="txt-stars-lb">Tốc độ vận hành</div>
                                                                                <div class="stars">
                                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-star.png" alt="">
                                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-star.png" alt="">
                                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-star.png" alt="">
                                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-star.png" alt="">
                                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-star.png" alt="">
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
            
                                                            <div class="img-big col-6">
                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/kimthanh-website.png" alt=""
                                                                    class="kt-gsap-fade-up swiper-lazy">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
            
                                                <div class="wd-slide-content wdContentJS kt-gsap-fade-down">
                                                    <div class="wd-slide-info col col-4 kt-gsap-fade-up"
                                                        data-swiper-parallax="-100%">
                                                        <div class="stt">02.</div>
                                                        <div class="name">Kim Thành</div>
                                                        <div class="desc">Nhà phân phối phụ tùng xe gắn máy <span class="fw-700">lớn
                                                                nhất Miền Nam.</span> <span class="fw-700 hl-sec-2">Doanh thu 10
                                                                con số
                                                                từ thị trường online chỉ qua website</span></div>
            
                                                        <div class="pj-cate hideHover">
                                                            <div class="cate-sm-box">
                                                                <div class="cate-sm-list d-flex">
                                                                    <div class="cate-sm-item">
                                                                        <a target="_blank" href="https://kimthanh.online/">website</a>
                                                                    </div>
                                                                    <div class="cate-sm-item">
                                                                        <a target="_blank" href="https://kimthanh.online/">webapp</a>
                                                                    </div>
                                                                    <div class="cate-sm-item">
                                                                        <a target="_blank" href="http://seo.monamedia.co/case-seo-kim-thanh">seo</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
            
                                <div class="wd-slide-contact hideHover">
                                    <div class="container">
                                        <div class="contact-btn d-flex f-end f-nowrap">
                                            <a href="javascript:;" class="btn btn-white openPop">
                                                <?php echo __('Liên hệ tư vấn', 'monamedia'); ?>
                                            </a>
                                            <a href="https://kimthanh.online/" class="view-device d-flex f-center" target="_blank">
                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-device.png" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
            
                            <div id="wd-slide-item-2" class="item wd-slide-item levents wdIntoView">
                                <div class="wd-slide-item-wrap">
                                    <div class="wd-slide-bg">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/levents-bg.png" alt="" class="swiper-lazy">
                                    </div>
            
                                    <div class="wd-slide-item-wrap-content">
                                        <div class="container">
                                            <div class="wd-slide-item-wrap-content-flex">
                                                <div class="wd-slide-img d-flex">
                                                    <div class="wd-slide-img-col col col-6 left">
                                                        <div class="img">
                                                            <div class="link-group-item col" data-swiper-parallax="-100%">
                                                                <a href="https://levents.asia/" class="d-flex btn-link btn-link-white hideHover" target="_blank">
                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/arrow-ani-c.png" alt="">
                                                                    https://levents.asia/
                                                                </a>
                                                            </div>
            
                                                            <div class="img-relative img-border gsap-fade-up">
                                                                <div class="img-border-wrap">
                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/levents-website.png" alt=""
                                                                        class=" swiper-lazy">
                                                                </div>
                                                            </div>
                                                            <div class="img-absolute users img-border gsap-fade-down"
                                                                data-swiper-parallax="-100%">
                                                                <div class="img-border-wrap">
                                                                    <div class="txt-item">
                                                                        <div class="title">
                                                                            <span class="">
                                                                                Hơn 5 triệu
                                                                            </span>
                                                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-dropdown-red.png" alt=""
                                                                                class="icon">
                                                                        </div>
                                                                        <div class="desc">Người dùng năm 2022</div>
                                                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/column-map.png" alt=""
                                                                            class="desc-img">
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="img-absolute order img-border gsap-fade-down"
                                                                data-swiper-parallax="-100%">
                                                                <div class="img-border-wrap">
                                                                    <div class="txt-item">
                                                                        <div class="title">
                                                                            <span class="">
                                                                                10.000+
                                                                            </span>
                                                                        </div>
                                                                        <div class="desc">Xử lý lượt mua hàng <br> cùng một lúc
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wd-slide-img-col col col-6">
                                                        <div class="img d-flex f-ctn">
                                                            <div class="col col-6 as-end">
                                                                <div class="img-relative img-border gsap-fade-up"
                                                                    data-swiper-parallax="-100%">
                                                                    <div class="img-border-wrap">
                                                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/levents-cart.png" alt=""
                                                                            class=" swiper-lazy">
                                                                    </div>
                                                                </div>
                                                                <div class="img-absolute stock">
                                                                    <div class="lv-route lv-route-1">
                                                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/levents-route1.png" alt=""
                                                                            class="">
                                                                    </div>
                                                                    <div class="txt-item gsap-fade-up" data-swiper-parallax="-100%">
                                                                        <div class="title">
                                                                            <span class="">
                                                                                Tồn kho
                                                                            </span>
                                                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-dropdown-red.png" alt=""
                                                                                class="icon">
                                                                        </div>
                                                                        <div class="desc">Giảm thất lạc <br>
                                                                            hàng hóa còn 0.1%</div>
                                                                    </div>
                                                                </div>
                                                                <div class="img-absolute emp">
                                                                    <div class="lv-route lv-route-2">
                                                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/levents-route2.png" alt=""
                                                                            class="   swiper-lazy">
                                                                    </div>
                                                                    <div class="txt-item gsap-fade-up" data-swiper-parallax="-100%">
                                                                        <div class="title">
                                                                            <span class="">
                                                                                50%
                                                                            </span>
                                                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-dropdown-red.png" alt=""
                                                                                class="icon">
                                                                        </div>
                                                                        <div class="desc">Giảm nhân lực <br>
                                                                            vận hành</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col col-6 as-center">
                                                                <div class="img-relative img-border mw-230 gsap-fade-up">
                                                                    <div class="img-border-wrap">
                                                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/levents-model.png" alt=""
                                                                            class=" swiper-lazy">
                                                                    </div>
                                                                </div>
            
                                                                <div class="img-absolute img-border order-event gsap-fade-down"
                                                                    data-swiper-parallax="-100%">
                                                                    <div class="img-border-wrap">
                                                                        <div class="txt-item">
                                                                            <div class="title">
                                                                                <span class="">
                                                                                    10.000+
                                                                                </span>
                                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-up-green.png" alt=""
                                                                                    class="icon">
                                                                            </div>
                                                                            <div class="desc">Đơn hàng <br> trong một event</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="img-absolute img-border customer gsap-fade-down"
                                                                    data-swiper-parallax="-100%">
                                                                    <div class="img-border-wrap">
                                                                        <div class="txt-item">
                                                                            <div class="title">
                                                                                <span class="">
                                                                                    200%
                                                                                </span>
                                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-up-green.png" alt=""
                                                                                    class="icon">
                                                                            </div>
                                                                            <div class="desc">Khách hàng mới từ <br> Website sau 5
                                                                                tháng SEO</div>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
            
                                                    <div class="local-brand">
                                                        <div class="local-brand-top">TOP #1</div>
                                                        <div class="local-brand-wrap">
                                                            <div class="local-brand-img">
                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/local-brand.png" alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
            
                                                <div class="wd-slide-content wdContentJS gsap-fade-down">
                                                    <div class="wd-slide-info col col-4" data-swiper-parallax="-100%">
                                                        <div class="stt ">03.</div>
                                                        <div class="name ">Levents</div>
                                                        <div class="desc ">Thương hiệu thời trang Streetwear sành điệu
                                                            cho giới trẻ với <span class="fw-700">2 năm làm chủ
                                                                thị trường Việt Nam</span></div>
            
                                                        <div class="pj-cate  hideHover">
                                                            <div class="cate-sm-box">
                                                                <div class="cate-sm-list d-flex">
                                                                    <div class="cate-sm-item">
                                                                        <a href="https://levents.asia/" target="_blank">website</a>
                                                                    </div>
                                                                    <div class="cate-sm-item">
                                                                        <a href="https://levents.asia/" target="_blank">webapp</a>
                                                                    </div>
                                                                    <div class="cate-sm-item">
                                                                        <a href="https://mona.media/dich-vu-seo/" target="_blank">seo</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
            
                                <div class="wd-slide-contact hideHover">
                                    <div class="container">
                                        <div class="contact-btn d-flex f-end f-nowrap">
                                            <a href="javascript:;" class="btn btn-white openPop"><?php echo __('Liên hệ tư vấn','monamedia'); ?></a>
                                            <a href="https://levents.asia/" class="view-device d-flex f-center" target="_blank">
                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-device.png" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
            
                            <div id="wd-slide-item-3" class="item wd-slide-item e-talk wdIntoView">
                                <div class="wd-slide-item-wrap">
                                    <div class="wd-slide-bg">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/Etalk-bg.png" alt="" class="swiper-lazy">
                                    </div>
            
                                    <div class="wd-slide-item-wrap-content">
                                        <div class="container">
                                            <div class="wd-slide-item-wrap-content-flex">
                                                <div class="wd-slide-img d-flex">
                                                    <div class="wd-slide-img-col col col-4 left">
                                                        <div class="img">
                                                            <div class="link-group-item col" data-swiper-parallax="-100%">
                                                                <a href="https://e-talk.vn/" target="_blank"
                                                                    class="d-flex btn-link btn-link-trans  hideHover">
                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/arrow-ani-etalk.png" alt="">
                                                                    https://e-talk.vn/
                                                                </a>
                                                            </div>
            
                                                            <div class="img-relative">
                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/etalk-website.png" alt=""
                                                                    class=" swiper-lazy">
                                                            </div>
                                                            <div class="img-absolute lap" data-swiper-parallax="-100%">
                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/etalk-lap.png" alt=""
                                                                    class=" swiper-lazy">
                                                            </div>
            
                                                            <div class="etalk-decor etalk-decor-1" data-swiper-parallax="-100%">
                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/etalk-decor1.png" alt="">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wd-slide-img-col col col-8">
                                                        <div class="img d-flex f-ctn">
                                                            <div class="col col-6 as-end">
                                                                <div class="img-relative">
                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/tinhnang-docthu.png" alt=""
                                                                        class=" swiper-lazy">
                                                                </div>
            
                                                                <div class="img-absolute learning" data-swiper-parallax="-100%">
                                                                    <div class="txt-item">
                                                                        <div class="title">
                                                                            Tính năng <br> học thử
                                                                        </div>
                                                                    </div>
                                                                </div>
            
                                                                <div class="etalk-decor etalk-decor-2" data-swiper-parallax="-100%">
                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/etalk-decor2.png" alt="">
                                                                </div>
                                                            </div>
                                                            <div class="col col-6 e-teacher">
                                                                <div class="img-relative">
                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/etalk-teacher.png" alt=""
                                                                        class=" swiper-lazy">
            
                                                                    <div class="img-absolute absolute-cir"></div>
                                                                </div>
                                                                <div class="img-absolute register">
                                                                    <div class="txt-item gsap-fade-down "
                                                                        data-swiper-parallax="-100%">
                                                                        <div class="title">
                                                                            3.500+
                                                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-dropdown-red.png" alt=""
                                                                                class="icon">
                                                                        </div>
                                                                        <div class="desc">lượt đăng ký/năm <br> qua website
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="img-absolute revenue">
                                                                    <div class="txt-item " data-swiper-parallax="-100%">
                                                                        <div class="desc desc-flex">
                                                                            Doanh thu tăng
                                                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-up-green.png" alt=""
                                                                                class="icon">
                                                                        </div>
                                                                        <div class="title">
                                                                            800%
                                                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-bieudo.png" alt=""
                                                                                class="icon icon-w">
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="img-absolute mkt">
                                                                    <div class="txt-item " data-swiper-parallax="-100%">
                                                                        <div class="title">
                                                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-g15.png" alt=""
                                                                                class="icon">
                                                                        </div>
                                                                        <div class="desc"><span class="fw-700">Công cụ
                                                                                Marketing</span> phủ sóng
                                                                            thương hiệu toàn ngành</div>
                                                                    </div>
                                                                </div>
            
                                                                <div class="etalk-decor etalk-decor-3" data-swiper-parallax="-100%">
                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/etalk-decor3.png" alt="">
                                                                </div>
            
                                                                <div class="etalk-decor etalk-decor-4" data-swiper-parallax="-100%">
                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/etalk-decor4.png" alt="">
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
            
                                                <div class="wd-slide-content wdContentJS">
                                                    <div class="wd-slide-info col col-4" data-swiper-parallax="-100%">
                                                        <div class="stt ">04.</div>
                                                        <div class="name ">E-talk</div>
                                                        <div class="desc ">Trung tâm ngoại ngữ Online 1 kèm 1 <span
                                                                class="fw-700">Vượt hơn 10.000 trung tâm dẫn đầu thị
                                                                trường</span></div>
            
                                                        <div class="pj-cate  hideHover">
                                                            <div class="cate-sm-box">
                                                                <div class="cate-sm-list d-flex">
                                                                    <div class="cate-sm-item">
                                                                        <a href="https://e-talk.vn/" target="_blank">website</a>
                                                                    </div>
                                                                    <div class="cate-sm-item">
                                                                        <a href="https://e-talk.vn/" target="_blank">lms</a>
                                                                    </div>
                                                                    <div class="cate-sm-item">
                                                                        <a href="https://mona.media/dich-vu-seo/" target="_blank">seo</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
            
                                <div class="wd-slide-contact hideHover">
                                    <div class="container">
                                        <div class="contact-btn d-flex f-end f-nowrap">
                                            <a href="javascript:;" class="btn btn-white openPop"><?php echo __('Liên hệ tư vấn', 'monamedia'); ?></a>
                                            <a href="https://e-talk.vn/" class="view-device d-flex f-center" target="_blank">
                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-device.png" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
            
                            <div id="wd-slide-item-4" class="item wd-slide-item jang-in wdIntoView">
                                <div class="wd-slide-item-wrap">
                                    <div class="wd-slide-bg">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/jangin-bg.png" alt="" class="swiper-lazy">
                                    </div>
            
                                    <div class="wd-slide-item-wrap-content">
                                        <div class="container">
                                            <div class="wd-slide-item-wrap-content-flex">
                                                <div class="wd-slide-img d-flex">
                                                    <div class="wd-slide-img-col col col-4 left">
                                                        <div class="img">
                                                            <div class="link-group-item col" data-swiper-parallax="-100%">
                                                                <a href="https://jangin.vn/" target="_blank"
                                                                    class="d-flex btn-link btn-link-trans hideHover">
                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/arrow-ani-jangin.png" alt="">
                                                                    https://jangin.vn/
                                                                </a>
                                                            </div>
            
                                                            <div class="img-relative">
                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/jangin-website.png" alt=""
                                                                    class=" swiper-lazy">
                                                            </div>
                                                            <div class="img-absolute revenue-onl">
                                                                <div class="txt-item " data-swiper-parallax="-100%">
                                                                    <div class="title">
                                                                        600%
                                                                    </div>
                                                                    <div class="desc"><span class="fw-600">Doanh thu
                                                                            online</span> <br>
                                                                        tại thị trường Việt <br> qua website</div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="wd-slide-img-col col col-8">
                                                        <div class="img d-flex f-ctn">
                                                            <div class="col col-6 as-end">
                                                                <div class="img-relative">
                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/jangin-sofa-sm.png" alt=""
                                                                        class="swiper-lazy">
                                                                </div>
                                                                <div class="img-absolute process">
                                                                    <div class="txt-item " data-swiper-parallax="-100%">
                                                                        <div class="desc">
                                                                            Xử lý hơn
                                                                        </div>
                                                                        <div class="title">
                                                                            2.000+
                                                                        </div>
                                                                        <div class="desc">đơn hàng trực tuyến cùng lúc</div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col col-6 j-sofa">
                                                                <div class="img-relative">
                                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/jangin-sofa.png" alt=""
                                                                        class=" swiper-lazy">
            
                                                                    <div class="img-absolute mkt">
                                                                        <div class="txt-item" data-swiper-parallax="-100%">
                                                                            <div class="title">
                                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-g16.png" alt=""
                                                                                    class="icon">
                                                                            </div>
                                                                            <div class="desc"><span class="fw-700">Tăng nhận
                                                                                    diện thương hiệu hơn 80%</span></div>
                                                                        </div>
                                                                    </div>
            
                                                                    <div class="img-absolute mkt reduce">
                                                                        <div class="txt-item" data-swiper-parallax="-100%">
                                                                            <div class="title">
                                                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-g17.png" alt=""
                                                                                    class="icon">
                                                                            </div>
                                                                            <div class="desc"><span class="fw-700">Giảm thiểu
                                                                                    chi phí offline</span> và nhân sự kho hơn
                                                                                <span class="hl-jang fw-700">87%</span>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="img-absolute absolute-cir cir"></div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
            
                                                <div class="wd-slide-content wdContentJS">
                                                    <div class="wd-slide-info col col-4" data-swiper-parallax="-100%">
                                                        <div class="stt ">05.</div>
                                                        <div class="name ">Jang In</div>
                                                        <div class="desc ">Thương hiệu nội thất hàng đầu tại Hàn Quốc.
                                                            Mang hơi thở hiện đại vào ngồi nhà Việt bằng sản phẩm chất lượng
                                                        </div>
            
                                                        <div class="pj-cate  hideHover">
                                                            <div class="cate-sm-box">
                                                                <div class="cate-sm-list d-flex">
                                                                    <div class="cate-sm-item">
                                                                        <a href="https://jangin.vn/" target="_blank">website</a>
                                                                    </div>
                                                                    <div class="cate-sm-item">
                                                                        <a href="https://jangin.vn/" target="_blank">webapp</a>
                                                                    </div>
                                                                    <div class="cate-sm-item">
                                                                        <a href="https://mona.media/dich-vu-seo/" target="_blank">seo</a>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
            
                                <div class="wd-slide-contact hideHover">
                                    <div class="container">
                                        <div class="contact-btn d-flex f-end f-nowrap">
                                            <a href="javascript:;" class="btn btn-white openPop"><?php echo __('Liên hệ tư vấn', 'monamedia'); ?></a>
                                            <a href="https://jangin.vn/" class="view-device d-flex f-center" target="_blank">
                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-device.png" alt="">
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <?php 
            }
        }
        ?>
        
        <?php $mona_customers_problems_title = get_field('mona_customers_problems_title');
        $mona_customers_problems_description = get_field('mona_customers_problems_description');
        if( !empty( $mona_customers_problems_title ) || !empty( $mona_customers_problems_description ) ){ ?>
        <section class="sec-cusp">
            <div class="container">
                <div class="cusp">
                    <div class="cusp-wrap">
                        <?php if( !empty( $mona_customers_problems_title ) ){ ?>
                        <div class="cusp-top">
                            <h2 class="title">
                                <?php 
                                    echo str_replace(
                                        ['[', ']', '{', '}'],
                                        ['<span class="text ani">' , '</span>' , '<span class="text2">' , '</span>'],
                                        $mona_customers_problems_title
                                    );
                                ?>
                            </h2>
                            <div class="decor">
                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusp-line.png" alt="">
                            </div>
                        </div>
                        <?php } ?>

                        <?php if( !empty( $mona_customers_problems_description ) ){ ?>
                        <div class="cusp-des">
                            <?php 
                            echo str_replace(
                                ['[', ']', '{', '}'],
                                ['<span class="text">' , '</span>' , '<span class="text2">' , '</span>'],
                                $mona_customers_problems_description
                            );
                            ?>
                        </div>
                        <?php } ?>
                        <div class="cusp-img">
                            <div class="inner">
                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusp-img.png" alt="">
                                <div class="tdecor">
                                    <span class="text">Dạ chị, Dev đang triển <br> khai cho mình rồi nha chị</span>
                                    <div class="dline">
                                        <svg class="ani" width="412" height="264" viewBox="0 0 412 264" fill="none"
                                            xmlns="http://www.w3.org/2000/svg">
                                            <path
                                                d="M290.113 261.884C293.038 241.324 300.277 219.26 278.649 208.475C258.85 198.602 229.856 195.8 208.081 195.755C139.763 195.611 36.3405 202.994 7.90685 123.775C-2.16103 95.7244 0.685564 58.3407 22.092 36.2148C41.837 15.8062 71.8459 8.75025 99.0445 5.24036C145.357 -0.736204 194.214 3.66619 239.889 12.2C299.641 23.3639 414.126 51.746 409.983 132.87C406.638 198.345 320.065 198.123 272.634 199.295"
                                                stroke="#F5851E" stroke-width="3" stroke-linecap="round"
                                                class="svg-elem-1"></path>
                                        </svg>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php } ?>

        <section class="sec-cusbrand co-customer fix">
            <div class="container">
                <div class="txt-center">
                    <div class="cusbrand-title">
                        <h2 class="title-inner">
                            <span class="text ani">Suốt nhiều năm qua</span>
                            <span class="text2">Chúng tôi vẫn không ngừng nỗ lực</span>
                            <span class="text3">để tạo ra sản phẩm, dịch vụ <span class="text4">ngày một tốt
                                    hơn…</span></span>
                        </h2>
                    </div>
                </div>
            </div>
            <div class="co-customer-ctn">
                <div class="co-customer-list d-flex">
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer1.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer2.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer3.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer4.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer5.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer6.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer7.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer8.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer9.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer10.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer11.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer12.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer13.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer14.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer15.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer16.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer17.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer18.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer19.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer20.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer21.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer22.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer23.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer24.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer25.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer26.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer27.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer28.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer29.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer30.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer31.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer32.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer33.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer39.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer34.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer35.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer36.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer37.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer38.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer40.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer41.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer42.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer43.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer44.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer45.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer46.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer47.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer48.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer49.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer50.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer51.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer52.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer53.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer54.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer55.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer56.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer57.png" alt="">
                        </div>
                    </div>
                    <div class="co-customer-item width-2 isotopeItem">
                        <div class="co-customer-wrap">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/co-customer58.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
            <!-- <div class="cusbrand-link">
                <a href="#" class="btn-second btn-trans">
                    <span class="txt">
                        Xem thêm đối tác của MONA
                    </span>
                </a>
            </div> -->
        </section>

        <div class="sec-cusw ani">
            <div class="cusw-bg">
                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-bg.png" alt="">
            </div>
            <div class="cusw-line">
                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-line2.png" alt="">
                <div class="decor">
                    <svg class="ani" width="100%" height="100%" viewBox="0 0 1739 1055" fill="none"
                        xmlns="http://www.w3.org/2000/svg">
                        <path
                            d="M484.001 962.915C190.298 1216.14 -116.001 856.415 52.0011 704.416C210.5 489.417 42.6873 455.786 250.5 128.77C348.999 -26.229 629.5 63.9118 804 109.412C978.5 154.912 1023 86.4126 1164 36.4127C1305 -13.5873 1527.93 -16.0528 1650.5 109.411C1768.36 230.053 1765.5 358.417 1622 420.416C1344 540.528 1487.06 687.236 1430.5 869.412C1297 1299.41 894.001 609.416 484.001 962.915Z"
                            stroke="#9BE7FF" stroke-width="8" class="svg-elem-1"></path>
                    </svg>

                </div>
            </div>
            <div class="cusw-wrap">
                <div class="cusw-img">
                    <div class="inner" data-aos="zoom-in">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/cusw-img-new.png" alt="">
                    </div>
                </div>
            </div>
            <div class="cusw-list gallery">
                <div class="cusw-item">
                    <div class="cusw-box cusw-0">
                        <div class="inner gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi11.jpg">
                            <img class="cir" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi11.jpg" alt="">
                        </div>
                    </div>
                    <div class="cusw-box cusw-1">
                        <div class="inner gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi.jpg">
                            <img class="cir" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="cusw-item">
                    <div class="cusw-box cusw-2">
                        <div class="inner gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi2.jpg">
                            <img class="cir" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi2.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="cusw-item">
                    <div class="cusw-box cusw-3">
                        <div class="inner gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi3.jpg">
                            <img class="cir" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi3.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="cusw-item">
                    <div class="cusw-box cusw-4">
                        <div class="inner gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi4.jpg">
                            <img class="cir" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi4.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="cusw-item">
                    <div class="cusw-box cusw-5">
                        <div class="inner gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi5.jpg">
                            <img class="cir" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi5.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="cusw-item">
                    <div class="cusw-box cusw-6">
                        <div class="inner gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi6.jpg">
                            <img class="cir" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi6.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="cusw-item">
                    <div class="cusw-box cusw-7">
                        <div class="inner gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi7.jpg">
                            <img class="cir" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi7.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="cusw-item">
                    <div class="cusw-box cusw-8">
                        <div class="inner gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi8.jpg">
                            <img class="cir" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi8.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="cusw-item">
                    <div class="cusw-box cusw-9">
                        <div class="inner gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi9.jpg">
                            <img class="cir" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi9.jpg" alt="">
                        </div>
                    </div>
                </div>
                <div class="cusw-item">
                    <div class="cusw-box cusw-10">
                        <div class="inner gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi10.jpg">
                            <img class="cir" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusw-imgi10.jpg" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php 
        $customer_feedback_rs = get_media_customer_feedback();
        
        if ( !empty( $customer_feedback_rs ) ) {
            $customer_feedback_rs = json_decode( $customer_feedback_rs );
            if ( $customer_feedback_rs->status == 200 
                && !empty( $customer_feedback_rs->responses ) 
            ) {
                $responses = $customer_feedback_rs->responses;
                $title = $responses->title;
                $attachment_urls  =   $responses->attachment_urls;
            }
        }

        if( is_array( $attachment_urls ) && !empty( $attachment_urls ) ){ ?>
        <section class="sec-cusis">
            <div class="container">
                <div class="cusis">
                    <div class="cusis-wrap">
                        <div class="cusis-top">
                            <?php if( !empty( $title ) ){ ?>
                            <h2 class="title">
                                <?php echo str_replace(
                                    ['[', ']'],
                                    ['<span class="text ani">', '</span>'],
                                    $title
                                ); ?>
                            </h2>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cusis-ctn">
                <div class="ab-nine-wrapper ab-nine">
                    <div class="ab-nine-list gallery">

                        <?php foreach ($attachment_urls as $attachment_url) { ?>
                        <div class="ab-nine-item aos-init aos-animate" data-aos="fade">
                            <div class="ab-nine-img gallery__img" data-src="<?php echo $attachment_url; ?>">
                                <img src="<?php echo $attachment_url; ?>" alt="">
                            </div>
                        </div>
                        <?php } ?>
                       
                    </div>
                </div>
            </div>


        </section>
        <?php } ?>

        <?php 
        $customer_project_rs = get_media_customer_project();
        
        if ( !empty( $customer_project_rs ) ) {
            $customer_project_rs = json_decode( $customer_project_rs );
            if ( $customer_project_rs->status == 200 
                && !empty( $customer_project_rs->responses ) 
            ) {
                $responses = $customer_project_rs->responses;
                $title = $responses->title;
                $description  =   $responses->description;
                $project_ids  =   $responses->project_ids;
                $first_project   =   $responses->first_project;
                $project_list   =   $responses->project_list;
            }
        }
        if( is_array( $project_ids ) && !empty( $project_ids ) ){ ?>
        <section class="sec-cust ss-pd">
            <div class="container">
                <div class="cust">
                    <div class="cust-wrap">
                        <div class="cust-top">
                            <?php if( !empty( $title ) ){ ?>
                            <h2 class="title">
                                <?php echo str_replace(
                                    ['[', ']'],
                                    ['<span class="text ani">', '</span>'],
                                    $title
                                ); ?>
                            </h2>
                            <?php } ?>
                            <div class="cust-line">
                                <div class="reveal">
                                    <div class="">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cust-line.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="cust-line2">
                                <div class="reveal">
                                    <div class="">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cust-line2.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="cust-line3">
                                <svg class="ani" width="35" height="47" viewBox="0 0 35 47" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M6.2162 45.1995C5.54749 41.9263 4.99532 38.6059 4.20601 35.36C2.60285 28.7674 -0.762596 28.0693 7.64174 28.4348C15.5018 28.7765 21.4648 30.7212 14.3063 19.8432C13.0987 18.0081 18.7506 23.8 20.4012 22.3503C22.6598 20.3667 18.4412 8.27691 17.8337 5.30309C16.3038 -2.18557 19.5187 4.14346 22.4493 7.28876C24.8634 9.8798 29.7149 14.1373 32.9972 15.018"
                                        stroke="#F41E92" stroke-width="3" stroke-linecap="round" class="svg-elem-1">
                                    </path>
                                </svg>

                            </div>
                        </div>
                        <div class="cust-des">
                            <p>
                                <?php echo str_replace(
                                    ['[', ']'],
                                    ['<span class="text">', '</span>'],
                                    $description
                                ); ?>
                            </p>
                        </div>

                        <?php 
                        if( !empty( $first_project ) ){
                            ?>
                            <div class="cust-pri">
                                <div class="cust-py">
                                    <div class="cust-flex">
                                        <div class="cust-left">
                                            <div class="cust-content">
                                                <a href="<?php echo $first_project->url; ?>" class="tlink">
                                                    <?php echo $first_project->title; ?>
                                                </a>

                                                <?php if( !empty( $first_project->slogan ) ){ ?>
                                                <span class="tlogan">
                                                    <?php echo $first_project->slogan; ?>
                                                </span>
                                                <?php } ?>
                                                
                                                <?php if( !empty( $first_project->description ) ){ ?>
                                                <div class="tdes">
                                                    <?php echo $first_project->description; ?>
                                                </div>
                                                <?php } ?>

                                                <div class="sample-one-control fix">
                                                    <a href="<?php echo $first_project->url; ?>" class="c-btn ln-orange txt-small">
                                                        <span class="text">
                                                            <?php echo __('Xem giải pháp' , 'monamedia'); ?>
                                                        </span>
                                                    </a>
                                                    <?php 
                                                    if( !empty( $first_project->external_url ) ){ ?>
                                                    <a href="<?php echo esc_url( $first_project->external_url ); ?>" class="c-btn white txt-small" target="_blank">
                                                        <span class="text">
                                                            Xem Website
                                                        </span>
                                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/iocn-dv-sw.png" alt="">
                                                    </a>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="cust-right">
                                            <div class="cust-img gallery">
                                                <div class="inner">
                                                    <?php if( !empty( $first_project->thumbnail ) ){ ?>
                                                        <img src="<?php echo $first_project->thumbnail; ?>" alt="">
                                                    <?php } ?>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <?php 
                            if( !empty($project_list) ){
                            ?>
                            <div class="cust-blog">
                                <div class="cust-list">
                                    <?php foreach( $project_list as $project ){ ?>
                                    <div class="cust-item col-4">
                                        <div class="cust-ipy" style="background: #F7F7F7;">
                                            <div class="cust-iimg gallery">
                                                <div class="inner">
                                                    <?php if( !empty( $project->thumbnail ) ){ ?>
                                                        <img src="<?php echo $project->thumbnail; ?>" alt="">
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <div class="cust-icontent">
                                                <div class="cust-content">
                                                    <a href="<?php echo $project->url ?>" class="tlink">
                                                        <?php echo $project->title; ?>
                                                    </a>

                                                    <?php if( !empty( $project->slogan ) ){ ?>
                                                    <span class="tlogan">
                                                        <?php echo $project->slogan; ?>
                                                    </span>
                                                    <?php } ?>
                                                    
                                                    <?php if( !empty( $project->description ) ){ ?>
                                                    <div class="tdes">
                                                        <?php echo $project->description; ?>
                                                    </div>
                                                    <?php } ?>

                                                    <div class="sample-one-control fix">
                                                        <a href="<?php echo $project->url; ?>" class="c-btn ln-orange txt-small">
                                                            <span class="text">
                                                                <?php echo __('Xem giải pháp' , 'monamedia'); ?>
                                                            </span>
                                                        </a>
                                                        <?php
                                                        if( !empty( $project->external_url ) ){ ?>
                                                        <a href="<?php echo esc_url( $project->external_url ); ?>" class="c-btn white txt-small" target="_blank">
                                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/iocn-dv-sw.png" alt="">
                                                        </a>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <?php }  wp_reset_query();  ?>
                                </div>
                                <div class="cust-blog-control">
                                    <div class="des">
                                        <p>Xem thêm những câu chuyện khác của MONA với khách hàng</p>
                                    </div>
                                    <a href="https://mona.media/du-an/" class="btn-second btn-trans">
                                        <span class="txt">
                                            <?php echo __('Xem thêm', 'monamedia'); ?>
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <?php 
                            } 
                        }
                        ?>
                    </div>
                </div>
            </div>
        </section>
        <?php } ?>

        <section class="sec-bblock">
            <div class="container">
                <div class="bblock">
                    <div class="bblock-py">
                        <div class="bblock-flex">
                            <div class="bblock-left">
                                <div class="bblock-content">
                                    <div class="bblock-title">
                                        <h2 data-aos="fade-right">
                                            <span class="text ani">Cột mốc 2022</span> <br>
                                            <span>Một năm đại thắng lợi
                                                của Mona về SEO</span>
                                        </h2>
                                        <div class="decor">
                                            <img src="<?php echo get_site_url() ?>/template/assets/images/phong-about/bblock-m.png" alt="">
                                        </div>
                                    </div>
                                    <div class="des" data-aos="fade-up">
                                        <p>
                                            Chúng tôi đưa hàng <span class="text">trăm doanh nghiệp lên top đầu
                                                ngành</span>, <span class="text2">tăng trưởng hơn 200%
                                                doanh thu và đơn hàng</span>.
                                        </p>
                                    </div>
                                    <div class="bblock-link">
                                        <a href="https://seo.monamedia.co/du-an-seo" class="btn-second" target="_blank">
                                            <span class="txt">
                                                Xem chi tiết Case MONA
                                            </span>
                                            <span class="icon">
                                                <img src="<?php echo get_site_url() ?>/template/assets/images/icon-arrow-right.png"
                                                    alt="">
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="bblock-right">
                                <div class="bblock-img">
                                    <div class="bblock-img-inner">
                                        <div class="inner" data-aos="zoom-in">
                                            <img src="<?php echo get_site_url() ?>/template/assets/images/phong-about/bblock-img.png" alt="">
                                        </div>
                                        <div class="tdecor">
                                            <span>#TOP SEARCH</span>
                                            <span class="vdecor">
                                                <img src="<?php echo get_site_url() ?>/template/assets/images/phong-about/bblock-vector.png" alt="">
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        
        <?php 
        $customer_collab_rs = get_media_customer_collab();
        
        if ( !empty( $customer_collab_rs ) ) {
            $customer_collab_rs = json_decode( $customer_collab_rs );
            if ( $customer_collab_rs->status == 200 
                && !empty( $customer_collab_rs->responses ) 
            ) {
                $responses = $customer_collab_rs->responses;
                $title = $responses->title;
                $description_1  =   $responses->description_1;
                $description_2  =   $responses->description_2;
                $collab_items   =   $responses->collab_items;
            }
        }

        if( is_array( $collab_items ) && !empty( $collab_items ) ){ ?>
        <section class="sec-cuss ss-pd">
            <div class="sec-cuss-decor-top">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/sec-cuss-decor-top.png" alt="">
            </div>
            <div class="sec-cuss-decor-bottom">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/sec-cuss-decor-bottom.png" alt="">
            </div>
            <div class="container">
                <div class="cuss">
                    <?php if( !empty( $title ) ){ ?>
                    <div class="cuss-top">
                        <?php if( !empty( $description_1 ) ){ ?>
                        <div class="tdecor">
                            <span>
                                <?php echo $description_1; ?>
                            </span>
                            <div class="dline">
                                <svg class="ani" width="72" height="40" viewBox="0 0 72 40" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M2.4415 36.1319C23.5772 27.5898 46.0811 19.7335 65.2257 7.06276C67.1541 5.78646 71.8454 0.875617 69.714 1.7727C62.5668 4.78089 55.8298 9.54466 49.1052 13.3558C39.667 18.705 27.3687 23.7607 19.8589 31.849C18.6534 33.1473 17.3732 35.3132 20.1082 34.2896C28.6212 31.1033 36.7823 27.0551 45.3473 23.951C48.306 22.8788 51.7153 21.7159 54.7296 20.7207C56.3347 20.1908 59.854 17.5724 59.5795 19.2402C59.2218 21.4125 52.8692 24.1237 51.1919 24.9976C49.3095 25.9784 43.3489 27.9541 45.4644 27.7806C47.06 27.6498 48.6114 26.5081 50.1476 26.0677C54.7347 24.7525 34.1706 33.3865 38.7496 34.7299C41.4181 35.5127 45.5095 34.1562 47.9829 33.2837C52.5756 31.6635 46.68 34.0746 45.4021 34.8242C40.277 37.8308 44.2421 38.5676 47.6848 36.852"
                                        stroke="#F41E92" stroke-width="3" stroke-linecap="round" class="svg-elem-1">
                                    </path>
                                </svg>
                            </div>
                        </div>
                        <?php } ?>

                        <?php if( !empty( $description_2 ) ){ ?>
                        <div class="tdecor2">
                            <span><?php echo $description_2; ?></span>
                            <div class="dline">
                                <svg class="ani" width="285" height="142" viewBox="0 0 285 142" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M125.426 139.507C142.282 118.799 175.233 125.869 199.439 127.625C220.208 129.131 245.464 129.869 264.181 121.023C272.341 117.166 280.44 109.884 282.522 100.855C285.029 89.9839 279.546 77.6112 273.5 68.3389C261.13 49.3674 233.81 31.3628 208.619 22.2905C171.116 8.78467 135.404 5.31463 122.198 3.86837C80.1152 -0.740486 72.4917 2.61519 43.3676 7.0421C31.1037 10.5253 4.85729 18.7235 2.36815 33.0196C0.53909 43.5245 6.75342 53.1856 13.7432 61.0038C43.6748 94.4824 89.5988 108.398 131.01 119.969"
                                        stroke="#F5851E" stroke-width="3" stroke-linecap="round" class="svg-elem-1">
                                    </path>
                                </svg>
                            </div>
                        </div>
                        <?php } ?>

                        <?php if( !empty( $title ) ){ ?>
                        <h2 class="title">
                            <?php echo str_replace(
                                ['[', ']'],
                                ['<span class="text ani">', '</span>'],
                                $title
                            ); ?>
                        </h2>
                        <?php } ?>
                    </div>
                    <?php } ?>
                    <div class="cuss-mid gallery">
                        <div class="cuss-list">
                            <?php foreach ($collab_items as $key => $item) { ?>
                            <div class="cuss-item col-3">
                                <div class="cuss-py">
                                    <div class="cuss-img">
                                        <div class="inner gallery__img"
                                            data-src="<?php echo $item->thumbnail; ?>">
                                            <img src="<?php echo $item->thumbnail; ?>" alt="">
                                        </div>
                                    </div>
                                    <?php if( !empty( $item->description ) ){ ?>
                                    <div class="cuss-content">
                                        <div class="cuss-head">
                                            <p><?php echo $item->description; ?></p>
                                        </div>
                                        <?php if( !empty($item->date ) ){ ?>
                                        <div class="cuss-gr">
                                            <span class="line"></span>
                                            <span class="time"><?php echo$item->date; ?></span>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php } ?>

        <section class="sec-sblock ss-pd">
            <div class="container">
                <div class="sblock">
                    <div class="sblock-py">
                        <div class="decor">
                            <img src="<?php echo get_site_url() ?>/template/assets/images/phong-about/tduyen.png" alt="">
                        </div>
                        <div class="sblock-flex">
                            <div class="sblock-left">
                                <div class="sblock-content">
                                    <div class="sblock-title" data-aos="fade-right">
                                        <h2>
                                            <span class="text ani">
                                                Thành công tạo nên thương hiệu
                                            </span> <br>
                                            Chúng tôi thống lĩnh tất cả các mảng kinh doanh
                                        </h2>
                                    </div>
                                    <div class="sblock-des" data-aos="fade-up">
                                        <p>
                                            Ngành khó TOP đầu thị trường chúng tôi đều đang <span class="text">giữ TOP
                                                1</span> <br>
                                            và <span class="text2">đạt hơn 2 triệu Traffic, 5.000 contact/tháng</span>
                                            không cần quảng cáo
                                        </p>
                                    </div>
                                    <div class="sblock-link">
                                        <a href="https://seo.monamedia.co/mona" class="btn-second" target="_blank">
                                            <span class="txt">
                                                Câu chuyện thành công của MONA
                                            </span>
                                            <span class="icon">
                                                <img src="<?php echo get_site_url() ?>/template/assets/images/icon-arrow-right.png"
                                                    alt="">
                                            </span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="sblock-right">
                                <div class="sblock-img">
                                    <div class="inner" data-aos="fade-up">
                                        <img src="<?php echo get_site_url() ?>/template/assets/images/phong-about/sblock-img.png" alt="">
                                        <div class="tdecor tdecor-one">
                                            <span class="text ani">#TOP1</span>
                                        </div>
                                        <div class="tdecor tdecor-two">
                                            <span class="text ani">#TOP1</span>
                                        </div>
                                        <div class="tdecor tdecor-three">
                                            <span class="text ani">#TOP1</span>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <?php $mona_customers_contact = get_field('mona_customers_contact');
        $title_1        =   $mona_customers_contact['title_1'];
        $title_2        =   $mona_customers_contact['title_2'];
        $description    =   $mona_customers_contact['description'];
        $cta            =   $mona_customers_contact['cta'];
        $button         =   $mona_customers_contact['button']; ?>
        <div class="sec-cusb">
            <div class="decor">
                <div class="reveal">
                    <div class="">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/cusb-bg.png" alt="">
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="cusb">
                    <div class="cusb-wrap">
                        <div class="decor">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/atuan.png" alt="">
                        </div>
                        <div class="cusb-content">
                            <?php if( !empty( $title_1 ) || !empty( $title_2 ) ){ ?>
                            <div class="cusb-top">
                                <div class="cusb-box">
                                    <p class="title">
                                        <?php echo $title_1; ?>
                                    </p>
                                    <p class="title">
                                        <?php echo $title_2; ?>
                                    </p>
                                    <div class="decor">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/p-customer/quote.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <?php } ?>

                            <?php if( !empty( $description ) ){ ?>
                            <div class="des">
                                <p>
                                <?php echo str_replace(
                                    ['[', ']'],
                                    ['<span class="text">', '</span>'],
                                    $description
                                ); ?>
                                </p>
                            </div>
                            <?php } ?>

                            <?php if( !empty( $cta ) ){ ?>
                            <div class="txt2">
                                <p><?php echo $cta; ?></p>
                            </div>
                            <?php } ?>

                           
                            <div class="cusb-link">
                                <?php if( wp_is_mobile() ){ ?>
                                    <?php $section_global_hotline = mona_get_option('section_global_hotline');
                                    if( !empty( $section_global_hotline ) ){ ?>
                                    <a href="<?php echo mona_replace_tel( $section_global_hotline ); ?>" class="btn-second">
                                        <span class="txt">
                                            <?php echo !empty( $button ) ? $button : __('Liên hệ ngay với chúng tôi', 'monamedia'); ?>
                                        </span>
                                        <span class="icon">
                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-arrow-right.png" alt="">
                                        </span>
                                    </a>
                                    <?php } ?>
                                <?php }else{ ?>
                                    <a href="javascript:;" class="btn-second openPop">
                                        <span class="txt">
                                            <?php echo !empty( $button ) ? $button : __('Liên hệ ngay với chúng tôi', 'monamedia'); ?>
                                        </span>
                                        <span class="icon">
                                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-arrow-right.png" alt="">
                                        </span>
                                    </a>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php 
        /**
         * GET TEMPLATE PART
         * global contact 2
         * project box
         */
        $slug = '/partials/global/contact';
        $name = '2';
        echo get_template_part($slug, $name);
        ?>
    </main>

<?php
endwhile;
get_footer();