<?php
/**
 * Template name: Đăng ký tên miền
 * @author : MONA.Media / Website
 */
get_header();
while ( have_posts() ):
    the_post();
    ?>
    <main class="main">
        <div class="host-search">
            <div class="breadcrumb">
                <div class="container">
                    <ul class="breadcrumb-list d-flex f-start">
                        <li class="breadcrumb-item" data-aos="fade-right"><a href="#" class="breadcrumb-item-link">Home</a>
                        </li>
                        <li class="breadcrumb-item" data-aos="fade-right"><a href="#" class="breadcrumb-item-link">Đăng ký
                                tên miền</a></li>
                    </ul>
                </div>
            </div>

            <div class="host-search-bg">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/host-bg-top.png" alt="">
            </div>

            <div class="host-header">
                <div class="container">
                    <div class="host-header-ctn d-flex f-ctn">
                        <div class="host-header-col col col-7">
                            <div class="host-header-content">
                                <div class="host-header-tt">
                                    <h1 data-aos="fade-up">Bắt đầu kinh doanh ngay hôm nay với <br> <span class="hl-pri">tên
                                            miền riêng của bạn</span></h1>
                                </div>

                                <div class="host-header-form" data-aos="fade-up">
                                    <form id="frmSearchDomain" class="is-loading-btn-2">
                                        <div class="f-r">
                                            <div class="f-c">
                                                <input type="search" name="domain_value" value="<?php echo @$_GET['domain_name'] ?>"
                                                    placeholder="Nhập tên miền của bạn tại đây" class="ipt-txt domain-ip-js">
                                                <input type="submit" value="" class="ipt-sub">
                                            </div>
                                        </div>
                                    </form>
                                    <div class="is-loading-btn-3 center domain-loader-js">
                                    </div>
                                    <!-- ADD CLASS `correct` WHEN THE RESULT IS CORRECT, AND `uncorrect` WHEN IT IS INCORRECT -->
                                    <div class="host-search-result uncorrect domain-search-rs-js">
                                        <div class="host-search-result-noti rs-inner-js">
                                            <!-- ajax content here -->
                                        </div>
                                        <div class="host-search-result-desc fw-600">
                                            <p><span class="hl-sec">Đợi một chút!</span> Mona có những gợi ý khác dành cho bạn</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="host-header-col col col-5">
                            <div class="host-header-img" data-aos="fade-left" data-aos-delay="300">
                                <div class="img">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/host-header-img.png" alt="">
                                </div>

                                <div class="host-header-domain">
                                    <div class="host-header-domain-item">
                                        <div class="item cir cir-2">.info</div>
                                    </div>
                                    <div class="host-header-domain-item">
                                        <div class="item cir cir-2">.com</div>
                                    </div>
                                    <div class="host-header-domain-item">
                                        <div class="item cir cir-2">.net</div>
                                    </div>
                                    <div class="host-header-domain-item">
                                        <div class="item cir cir-2">.biz</div>
                                    </div>
                                    <div class="host-header-domain-item">
                                        <div class="item cir cir-2">.xyz</div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php process_buy_domain( 43218 ); ?>
            <div class="host-service">
                <div class="container">
                    <div class="host-service-ctn d-flex f-ctn">
                        <div class="host-service-col col-8 col">
                            <div class="host-service-content">
                                <div class="host-service-box domain-suggest-wr-js" style="display: none;">
                                    <div class="host-service-tt host-tt">
                                        <h2>Các <span class="host-underline"><span class="host-underline-txt">lựa chọn thay
                                                    thế</span></span> dành cho bạn</h2>
                                    </div>

                                    <div class="host-service-list domain-suggest-ls-js">
                                        <!-- ajax content here -->
                                    </div>
                                    <?php //var_dump( inet_whois_check( 'mona.academy' ) ) ?>
                                    <div class="host-viewmore is-loading-btn-2 more-domain-js">
                                        Xem thêm kết quả
                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/down-arrow.svg" alt="">
                                    </div>
                                </div>
                                <div class="host-service-box web-hosting" data-aos="fade-up">
                                    <div class="host-service-tt host-tt">
                                        <h2>Chúng tôi cung cấp <span class="txt-bg">các dịch vụ hosting</span> <br> <span
                                                class="hl-pri">tốt nhất</span> cho website</h2>
                                    </div>
                                    <?php
                                    $mona_suggest_product = get_field('mona_suggest_product');
                                    if ( !empty( $mona_suggest_product ) && is_array( $mona_suggest_product ) ) {
                                        ?>
                                        <div class="host-service-list">
                                            <?php
                                            foreach ( $mona_suggest_product as $product_obj ) {
                                                ?>
                                                <div class="host-service-item product-item-js">
                                                    <div class="host-service-wrap d-flex f-ctn">
                                                        <div class="host-service-wrap-left host-service-wrap-col col">
                                                            <div class="host-service-name">
                                                                <?php echo $product_obj->post_title ?>
                                                            </div>
                                                            <?php
                                                            $mona_billing_cycle_list = get_field('mona_billing_cycle_list', $product_obj->ID);
                                                            if( !empty( $mona_billing_cycle_list ) && is_array( $mona_billing_cycle_list ) ) {
                                                                ?>
                                                                <div class="d-flex f-start host-package-select">
                                                                    <div class="price-discount product-price-js" 
                                                                        data-suffix=<?php echo get_woocommerce_currency_symbol() . __('/th', 'monamedia'); ?>
                                                                    >
                                                                        <?php echo wc_price( $mona_billing_cycle_list[0]['price']/$mona_billing_cycle_list[0]['term_quantity'] ) . __('/th', 'monamedia'); ?>
                                                                    </div>
                                                                    <select class="select2choose billingcycle-select-js" data-pricing_arr=<?php echo json_encode( $mona_billing_cycle_list ) ?>>
                                                                        <?php
                                                                        foreach ( $mona_billing_cycle_list as $billing_cycle ) {
                                                                            ?>
                                                                            <option value="<?php echo $billing_cycle['term_quantity'] ?>"
                                                                                data-price=<?php echo $billing_cycle['price'] ?>
                                                                                data-term_qty=<?php echo $billing_cycle['term_quantity'] ?>
                                                                            >
                                                                                <?php echo $billing_cycle['label'] ?>
                                                                            </option>
                                                                            <?php
                                                                        }
                                                                        ?>
                                                                    </select>
                                                                </div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                        <div class="host-service-wrap-right host-service-wrap-col col">
                                                            <div class="host-service-btn add-to-cart-js is-loading-btn-2" data-pid=<?php echo $product_obj->ID ?> data-ptype="<?php echo get_field('mona_product_type', $product_obj->ID) ?>">
                                                                <span class="host-btn">
                                                                    <span class="host-btn-wrap">
                                                                        <span class="host-btn-ani">
                                                                            <span class="host-btn-icon">
                                                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/shopping-cart.svg" alt=""
                                                                                    class="icon">
                                                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/shopping-cart-color.svg"
                                                                                    alt="" class="icon">
                                                                            </span>
                                                                            THÊM VÀO GIỎ HÀNG
                                                                        </span>
                                                                    </span>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <?php
                                                    $mona_desc_list = get_field('mona_desc_list', $product_obj->ID);
                                                    if (!empty( $mona_desc_list) && is_array($mona_desc_list) ) {
                                                        ?>
                                                        <div class="host-service-detail wdsContent">
                                                            <div class="host-service-detail-header d-flex f-between wdsMore">
                                                                Chi tiết gói
            
                                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/arrow-host.svg" alt="" class="icon">
                                                            </div>
                                                            <div class="host-service-detail-list wdsMoreHide">
                                                                <?php
                                                                foreach ( $mona_desc_list as $desc_item ) {
                                                                    if ( !empty( $desc_item ) ) {
                                                                        ?>
                                                                        <div class="host-service-detail-item">
                                                                            <div class="name"><?php echo $desc_item['desc_label']; ?></div>
                                                                            <div class="detail"><?php echo $desc_item['desc_info']; ?></div>
                                                                        </div>
                                                                        <?php
                                                                    }
                                                                }
                                                                ?>
                                                            </div>
                                                        </div>
                                                        <?php
                                                    } 
                                                    ?>
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <?php
                                    }
                                    ?>
                                </div>
                            </div>
                        </div>
                        <div class="host-service-col col-4 col">
                            <div class="host-service-aside">
                                <div class="host-service-aside-wrap cart-wrap-js is-loading-btn-2">
                                    <?php echo monahost_get_cart_html(); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <?php
        /**
         * GET TEMPLATE PART
         * contact section
         *  */ 
        $slug = '/partials/global/contact';
        $name = '2';
        echo get_template_part($slug, $name);
        ?>
    </main>
    <?php
endwhile;
get_footer();