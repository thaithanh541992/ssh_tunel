export default function MonaDefaultModule() {
    $(document).ready( function() {
        // open account popup
        $('.account-open-js').on('click', function () {
            $('.popup-bg-js').addClass('active');
            $('.popup-account-js').addClass('active');
            $('body').addClass('no-scroll');
        })

        // close popup multi by btn
        $('.pop-close-js').each((i, item) => {
            $(item).on('click', () => {
                $(item).closest('.popup-js').removeClass('active');
                $('.popup-bg-js').removeClass('active');
                $('body').removeClass('no-scroll')
            })
        })

        // close popup by overlay
        $('.popup-bg-js').each((i, item) => {
            $(item).on('click', () => {
                $('.popup-js').removeClass('active');
                $('.popup-bg-js').removeClass('active');
                $('body').removeClass('no-scroll')
            })
        })

        $(document).on('click', '.close-modal-js', () => {
            $('.c-modal-js').removeClass('active')
            $('.popupBg').removeClass('active')
            $('body').removeClass('no-scroll')
        })
    })


    
    const numberr = 100;

    const speed = 800;
    $(document).ready(function(e) {
        const hash = window.location.hash;
        if ($(hash).length) {
            scrollToID(hash, speed, numberr);
        };
    });

    $(".widget.sidebar-blog-detail.toc_widget ul li a").on("click", function(e) {
        e.preventDefault();
        const href = $(this).attr("href");
        const id = href.slice(href.lastIndexOf("#"));
        console.log(numberr);
        if ($(id).length) {
            scrollToID(id, speed, numberr);
        } else {
            window.location.href = href;
        }
    });

    $(".monaScrollToID").on("click", function(e) {
        e.preventDefault();
        const href = $(this).attr("href");
        const id = href.slice(href.lastIndexOf("#"));
        if ($(id).length) {
            scrollToID(id, speed, numberr);
        } else {
            window.location.href = href;
        }
    });

    // HÀM SCROLL CHO MƯỢT MÀ
    function scrollToID(id, speed, numberr) {
        const offSet = $("header").outerHeight();
        const section = $(id).offset();
        const targetOffset = section.top - offSet - numberr;
        $("html,body").animate({ scrollTop: targetOffset }, speed);
    }

    const mouse = document.querySelector(".mouse .icon");
    const id_sl_pr = document.querySelector("#slide-product");
    if (id_sl_pr) {
        mouse.addEventListener("click", () => {
            scrollToID("#slide-product", 0, 250);
        });
    }

    function runLine() {
        var bullets = document.querySelectorAll('ul.toc_widget_list li a');
        $(".blog-large-content :header:has('span')").each(function(i) {
            if ( $(window).scrollTop() > $(this).offset().top - 500) {
                var _this = this;
                bullets.forEach(function(bullet, index) {
                    bullet.classList.remove('current')
                    if (index == i) {
                        bullet.classList.add('current')
                    }
                })
            } else {}
        })
    }
    runLine();

    $(window).scroll(function() {
        runLine();
    })

    /*POST AJAX*/
    function monaProductList(loading, formdata) {
        if (!loading.hasClass('loading')) {
            $.ajax({
                url: mona_ajax_url.ajaxURL,
                type: 'post',
                data: {
                    action: 'mona_ajax_pagination_products',
                    formdata: formdata,
                },
                error: function(request) {
                    loading.removeClass('loading');
                },
                beforeSend: function(response) {
                    loading.addClass('loading');
                },
                success: function(result) {
                    if (result.success) {

                        if (result.data.posts_html) {
                            $('#monaPostList').html(result.data.posts_html);
                            scrollToID('#monaPostList', 500, 250);
                        }

                        if (result.data.paged) {
                            loading.closest('form').find('input[name="paged"]').val(result.data.paged);
                        }

                    }
                    loading.removeClass('loading');
                }
            });
        }
    }

    $("#frmPostAjax").on('click', '.pagination-products-ajax a.page-numbers', function(e) {
        e.preventDefault();
        var $this = $(this);
        var hrefThis = $this.attr('href');
        var paged = hrefThis.match(/\d+/);
        if (!paged) {
            paged = 1;
        } else {
            paged = paged[0];
        }
        if ($this.closest('form').find('input[name="paged"]').length) {
            $this.closest('form').find('input[name="paged"]').val(paged);
        }
        var formdata = $this.closest('form').serialize();
        var loading = $('#monaPostList');
        monaProductList(loading, formdata);
    });

    function ChuanModuleMona() {
        const nfeedItemImages = document.querySelectorAll(".nfeed-item.nfeed-have-image.mutiple");
        if (nfeedItemImages) {
            nfeedItemImages.forEach(item => {
                const nfeedImages = item.querySelectorAll(".nfeed-image-item");
                let numerHide = 0;
                numerHide = nfeedImages.length - 4;
                if (nfeedImages.length > 4) {
                    nfeedImages[3].classList.add("show-number");
                    nfeedImages[3]
                        .querySelector(".nfeed-image-img")
                        .setAttribute("data-number", `+${numerHide}`);
                    nfeedImages.forEach((nfeedImage, index) => {
                        if (index > 3) {
                            nfeedImage.style = `display: none`;
                        }
                    });
                }
            });
        }

        $(".gallery").lightGallery({
            selector: ".gallery__img",
            exThumbImage: "data-src",
            autoplayControls: false,
            flipHorizontal: false,
            flipVertical: false,
            rotate: false,
            share: false,
            fullScreen: false,
            actualSize: false,
            download: false,
        });

    }

    function monaProductLoadMore(loading, formdata) {
        if (!loading.hasClass('loading')) {
            $.ajax({
                url: mona_ajax_url.ajaxURL,
                type: 'post',
                data: {
                    action: 'mona_ajax_loadmore',
                    formdata: formdata,
                },
                error: function(request) {
                    loading.removeClass('loading');
                },
                beforeSend: function(response) {
                    loading.addClass('loading');
                }
            }).done(function(result) {

                if (result.success) {

                    if (result.data.active == 'active') {
                        loading.closest('form').find('input[name="paged"]').val(result.data.paged);
                    } else {
                        loading.remove();
                    }

                    loading.removeClass('loading');

                    var grid = $(".nfeed-list");


                    var tempDiv = document.createElement('div');
                    tempDiv.innerHTML = result.data.posts_html;
                    
                    // push item to isotop
                    $(tempDiv).find('.nfeed-item').each(function(index, element) {
                        grid.append(element).masonry('appended', element);
                    });

                    grid.masonry('reloadItems');

                    ChuanModuleMona();
                    addEventNFeedVideo()

                }

            });
        }
    }

    $("#frmPostAjax").on('click', '.monaLoadMore', function(e) {
        e.preventDefault();
        var $this = $(this);
        var formdata = $this.closest('form').serialize();
        var loading = $(this);
        monaProductLoadMore(loading, formdata);
    });

    $('.monaCategorySearchingTagJS').on("click", function(e) {
        e.preventDefault();
        if (!$(this).hasClass('checked')) {
            $(this).addClass('checked')
            var value = $(this).data('value');
            if ($('#frmCategorySearching').length) {
                $('#frmCategorySearching').find('input[name="tag"]').val(value);
                $('#frmCategorySearching').submit();
            }
        } else {
            $(this).removeClass('checked');
            $(this).removeClass('current');
            if ($('#frmCategorySearching').length) {
                $('#frmCategorySearching').find('input[name="tag"]').val('');
                $('#frmCategorySearching').submit();
            }
        }
    });

    // $('.monaCategorySearchingSubmitJS').on("click", function (e) {
    //     e.preventDefault();
    //     $('#frmCategorySearching').submit();
    //     // window.location.reload();
    // });

    document.addEventListener('wpcf7mailsent', function(event) {
        var contactFormId = event.detail.contactFormId;
        var inputs = event.detail.inputs;
        if (inputs.length) {
            for (var i = 0; i < inputs.length; i++) {
                if ( 'show-position' == inputs[i].name ) {
                    break;
                }
                if ('your-service' == inputs[i].name) {
                    $('.popup').removeClass('active');
                    $('.popup-service').addClass('active');
                    $('.popup-bg').addClass('active');
                }
            }
        }
    }, false);

    function monaRandomServiceByServiceTitle(loading, title) {
        if (!loading.hasClass('loading')) {
            $.ajax({
                url: mona_ajax_url.ajaxURL,
                type: 'post',
                data: {
                    action: 'mona_ajax_extension_services',
                    title: title,
                },
                error: function(request) {
                    //loading.removeClass('loading');
                },
                beforeSend: function(response) {
                    //loading.addClass('loading');
                },
                success: function(result) {
                    if (result.success) {

                        if (result.data.html) {
                            $('#monaServiceBody').html(result.data.html);
                        }

                    }
                    //loading.removeClass('loading');
                }
            });
        }
    }

    $('.monaServiceJS').on("change", function(e) {
        var $this = $(this);
        var value = $this.val();
        var loading = $this.closest('.wpcf7-form-control-wrap');
        monaRandomServiceByServiceTitle(loading, value);
    });

    $(document).on('click', '.monaTemplateWebsiteOpenGallery', function(e) {
        e.preventDefault();
        $('#template-website-gallery > *').trigger('click');
    });

    $(document).on('click', '.openPopMona', function(e) {
        e.preventDefault();
        e.stopPropagation();
        var popup = $(this).data('popup');
        if ($('#' + popup).length) {
            $('#' + popup).addClass('active');
            $('.popup-bg.popupBg').addClass('active');
            $('body').addClass('no-scroll');
        }
    });

    $(document).on('click', '.popup-bg.popupBg', function(e) {
        $('.popup.popupBox').removeClass('active');
        $(this).removeClass('active');
    });

    $(document).on('click', '.popup-close.closePop', function(e) {
        $('.popup-bg.popupBg').removeClass('active');
        $(this).closest('.popup.popupBox').removeClass('active');
    });

    $(document).on('click', '.monaActivitiesOpenGallery', function(e) {
        e.preventDefault();
        $('#monaActivitiesGallery .item').trigger('click');
    });

    $(document).ready(function(e) {
        var sitemap = $('header').data('sitemap');
        if (sitemap && sitemap.length) {
            $("body form").each(function(index, element) {
                $(element).find('input[name="your-sitemap"]').val(sitemap);
            });
        }
    });

    $(document).ready(function(e) {
        if ($('body').hasClass('single')) {
            var heading = $(".mona-content.blogContent :header");
            if (heading.length == 0) {
                $('.blog-large-aside.aside-categories.asideCate').remove();
                $('.blog-large-content').removeClass('col-6');
                $('.blog-large-content').addClass('col-9');
            }
        }

        // add wrapper for table 
        let table_modify = $('.table-modify-js').find('table');
        $(table_modify).each( (index, tableItem) => {
            $(tableItem).wrap('<div class="table-block"></div>')
        });
    });

    $(document).ready(function(e) {
        let subMenuItem = $('.mega-menu-list > .mega-menu-item .mega-menu-item .mega-menu-submenu-item-js');
        $(subMenuItem).each((i, item) => {
            if ( $(item).hasClass('active') ) {
                $(item).closest('.mega-menu-item').addClass('active')
            }
        })
        var first = true;
        var root = $('.menu-list > .menu-item.mega-menu');
        $('.mega-menu-list > .mega-menu-item').each(function(index, element) {
            var megaMenuCate = $(element).find('.mega-menu-cate');
            $(megaMenuCate).each(function(indexCate, elementCate) {
                if ($(elementCate).find('.mega-menu-item').hasClass('active') && first) {
                    $(element).addClass('active');
                    $(elementCate).addClass('active');
                    root.addClass('current-menu-item');
                    first = false;
                }
            });
        });

        if (first) {
            var firstCate = true;
            $('.mega-menu-list > .mega-menu-item').each(function(index, element) {
                var megaMenuCate = $(element).find('.mega-menu-cate');
                $(megaMenuCate).each(function(indexCate, elementCate) {
                    if (firstCate) {
                        $(element).addClass('active');
                        $(elementCate).addClass('active');
                        firstCate = false;
                    }
                });
            });
        }
    });

    $(document).ready(function(e) {
        var root = $('.menu-list > .menu-item-has-sub');
        $('.submenu .submenu-item').each(function(index, element) {
            if ( $(this).hasClass('current-menu-item') ) {
                root.addClass('current-menu-item');
            }
        });
    });

    $('#header-search-form').on('submit', function(e) {
        e.preventDefault();
        // get cookie
        let history = getCookie('search_history');

        // check empty
        history = history ? JSON.parse(history) : [];

        // remove unneed item
        if ( history.length > 5 ) {
            history.splice(4);
        }

        // add to first
        history.unshift($(this).find('.search-key-js').val());

        // encode
        history = JSON.stringify(history);

        setCookie('search_history', history, 30);

        // prevent deadloop
        $(this).off("submit");

        // submit the form
        this.submit();
    })

    $('.history-item-js').on('click', function(e) {
        e.preventDefault();
        $('.search-key-js').val( $(this).attr('data-value') );
    })

    $('.history-remove-js').on('click', function(e) {
        e.preventDefault();
        // get cookie
        let history = getCookie('search_history');
        
        // check empty
        history = history ? JSON.parse(history) : [];
        
        // remove item cookie
        let index = $(this).attr('data-index');
    
        if ( index < history.length ) {
            history.splice(index, 1);
        }

        // encode
        history = JSON.stringify(history);

        setCookie('search_history', history, 30);

        $(this).closest('.history-item-js').remove();
    })

    function setCookie(cname, cvalue, exdays) {
        const d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        let expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    }

    function getCookie(cname) {
        let name = cname + "=";
        let decodedCookie = decodeURIComponent(document.cookie);
        let ca = decodedCookie.split(';');
        for(let i = 0; i <ca.length; i++) {
          let c = ca[i];
          while (c.charAt(0) == ' ') {
            c = c.substring(1);
          }
          if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
          }
        }
        return "";
    }

    function addEventNFeedVideo() {
        const nFeedVideoItems = document.querySelectorAll(".nfeed-have-video");
        if (nFeedVideoItems) {
          nFeedVideoItems.forEach(item => {
            const btnPlay = item.querySelector(".nfeed-play.video");
            const video = item.querySelector(".nfeed-image-img video");
            if (btnPlay) {
              btnPlay.addEventListener("click", e => {
                e.preventDefault();
                if (video.paused) {
                  video.play();
                  item.classList.add("active");
                } else {
                  item.classList.remove("active");
                }
              });
            }
          });
        }
    }
}