export default function MonaDomainModule() {
    $(document).ready( function() {
        // submit form search domain
        $(document).on('submit', '#frmSearchDomain', function(e) {
            e.preventDefault();
            let $this = $(this);
            
            let domain_value = $this.find('.domain-ip-js').val();
            if ( !domain_value ) {
                return;
            }
            
            let loading = $this;
            let loader = $('.domain-loader-js');
            let rs_box = $('.domain-search-rs-js');
            let $form = $this.serialize();
            if (!loading.hasClass('loading') && !loader.hasClass('loading')) {
                $.ajax({
                    url: mona_ajax_url.ajaxURL,
                    type: 'post',
                    data: {
                        action: 'mona_ajax_check_domain',
                        form: $form,
                    },
                    error: function(request) {
                        loading.removeClass('loading');
                        loader.removeClass('loading');
                        rs_box.addClass('active');
                        $('.domain-suggest-wr-js').hide();
                    },
                    beforeSend: function() {
                        loading.addClass('loading');
                        loader.addClass('loading');
                        rs_box.removeClass('active');
                        $('.domain-suggest-wr-js').hide();
                    },
                    success: function(result) {

                        if (result.success) {

                            rs_box.removeClass('uncorrect');    
                            rs_box.addClass('correct');    
                            $('.rs-inner-js').html(`
                            <div class="host-search-result-noti-ctn d-flex product-item-js">
                                    <div class="host-search-result-noti-col">
                                        <div class="host-search-name">${result.data.domain_name}<span class="hl-pri">${result.data.suffix}</span></div>
                                        <div class="host-search-desc">
                                            ${result.data.message}
                                        </div>
                                    </div>
                                    <div class="host-search-result-noti-col host-service-wrap-right host-service-wrap-col col">
                                        <div class="host-service-price d-flex">
                                            <div class="price-cover">
                                                ${result.data.price_data.price_original ? 
                                                    result.data.price_data.price_original.replace(/\B(?=(\d{3})+(?!\d))/g, ",")  
                                                        + result.data.currency : '' }
                                            </div>
                                            <div class="price-discount">
                                                ${result.data.price_data.price ? 
                                                    result.data.price_data.price.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + result.data.currency 
                                                        + result.data.price_data.price_suffix.charAt(0) : '' 
                                                }<span class="txt-small">${result.data.price_data.price_suffix.substring(1)}</span>
                                            </div>
                                        </div>
                                        <div class="host-service-btn">
                                            <span class="host-btn add-to-cart-js is-loading-btn-2" data-pid=${result.data.pid} data-ptype="domain"
                                            data-domain_name=${result.data.domain_name + result.data.suffix} >
                                                <span class="host-btn-wrap"></span>
                                                    <span class="host-btn-ani">
                                                        <span class="host-btn-icon">
                                                            <img src="/template/assets/images/shopping-cart.svg" alt="" class="icon">
                                                            <img src="/template/assets/images/shopping-cart-color.svg" alt="" class="icon">
                                                        </span>
                                                        THÊM VÀO GIỎ HÀNG
                                                    </span>
                                                </span>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            `)

                        } else {

                            rs_box.removeClass('correct');    
                            rs_box.addClass('uncorrect');    
                            $('.rs-inner-js').html(`
                                <div class="host-search-result-noti-ctn d-flex">
                                    <div class="host-search-result-noti-col">
                                        <div class="host-search-name">${result.data.domain_name}<span class="hl-pri">${result.data.suffix}</span></div>
                                        <div class="host-search-desc">
                                            ${result.data.message}
                                        </div>
                                    </div>
                                    <div class="host-search-result-noti-col host-service-wrap-right host-service-wrap-col col">
                                        <div class="host-service-btn">
                                            <a href="javascript:;" class="host-btn host-btn-blue whois-open-js is-loading-btn-2" data-domain="${result.data.domain_name + result.data.suffix}">
                                                <span class="host-btn-wrap">
                                                    <span class="host-btn-ani">
                                                        <span class="host-btn-icon">
                                                            <img src="/template/assets/images/headphone-white.svg" alt="" class="icon">
                                                            <img src="/template/assets/images/headphone-color.svg" alt="" class="icon">
                                                        </span>
                                                        THÔNG TIN SỞ HỮU
                                                    </span>
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            `)
                        }

                        loading.removeClass('loading');
                        loader.removeClass('loading');
                        rs_box.addClass('active');

                        $('.suggest-loader-js').addClass('loading');
                        $('.domain-suggest-wr-js').show();
                        $('.domain-suggest-ls-js').html('');
                        
                        let suggest_domains = result.data.suggest_domains;
                        call_load_more_suggest( suggest_domains );
                    }
                });
            }
        });

        // if has query param => submit form
        const urlParams = new URLSearchParams(window.location.search);
        const domain_name = urlParams.get('domain_name');

        if ( domain_name ) {
            $('#frmSearchDomain').submit();
        }

        $(document).on('click', '.more-domain-js', function() {
            $(this).addClass('loading');
            let suggest_domains = $(this).attr('data-suggest');
            if ( !suggest_domains ) {
                $(this).hide();
            }

            suggest_domains = JSON.parse(suggest_domains);
            call_load_more_suggest( suggest_domains );
            $(this).removeClass('loading');
        })
    })

    function call_load_more_suggest( suggest_domains ) {
        if ( suggest_domains ) {

            let number_of_request = 6;

            let remain_domains = suggest_domains.splice(number_of_request);

            if ( remain_domains.length > 0 ) {
                $('.more-domain-js').show();
                $('.more-domain-js').attr('data-suggest', JSON.stringify(remain_domains));
            } else {
                $('.more-domain-js').hide();
            }

            for( let domain of suggest_domains ) {
                check_suggest_domain( domain.domain_name, domain.suffix )
            }
        }
    }

    async function check_suggest_domain( domain_name, suffix ) {

        $('.domain-suggest-ls-js').append(`
            <div class="host-service-item host-service-item-js" data-suffix="${suffix}">
                <div class="host-service-wrap d-flex f-ctn">
                    <div class="host-service-wrap-left host-service-wrap-col col">
                        <div class="host-service-name">
                            ${domain_name}<span class="hl-sec">${suffix}</span>
                        </div>
                        <ul class="host-service-tag d-flex f-start host-service-cat-js">
                        </ul>
                    </div>
                    <div class="host-service-wrap-right host-service-wrap-col col host-price-wrap-js">
                        <div class="is-loading-btn-3 loading">
                        </div>
                    </div>
                </div>
            </div>
        `)

        let current_host_item = $(`.host-service-item-js[data-suffix='${suffix}']`);
        
        let formData = new FormData();
        formData.append('domain_value', domain_name + suffix);
        formData.append('is_suggest', true);

        let body_obj = {
            action: 'mona_ajax_check_domain',
            form: new URLSearchParams(formData).toString(),
        }

        let fetch_option = {
            method: 'post',
            mode: "cors", // no-cors, *cors, same-origin
            cache: "no-cache", // *default, no-cache, reload, force-cache, only-if-cached
            credentials: "same-origin", // include, *same-origin, omit
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
            },
            redirect: "follow", // manual, *follow, error
            referrerPolicy: "no-referrer",
            body: new URLSearchParams(body_obj).toString() 
        }

        fetch(mona_ajax_url.ajaxURL, fetch_option)
        .then((response) => response.json())
        .then((response) => {
            let cat_list_ele = $(current_host_item).find('.host-service-cat-js')
            let price_wrap_ele = $(current_host_item).find('.host-price-wrap-js')
            let categories = JSON.parse(response.data.categories)

            $(cat_list_ele).html(`
                ${(() => {
                    try {
                        let html = ''
                        for( let cat of categories ) {
                            html += `<div class="host-service-tag-item">${cat.name}</div>`
                        }
                        return html;
                    } catch (error) {
                        console.error(error);
                        return '';
                    }
                })()}
            `)
            // Do something with the response
            if (response.success) {

                $(price_wrap_ele).html(`
                    <div class="host-service-price d-flex">
                        <div class="price-cover">
                            ${response.data.price_data.price_original ? 
                                response.data.price_data.price_original.replace(/\B(?=(\d{3})+(?!\d))/g, ",")  
                                    + response.data.currency : '' }
                        </div>
                        <div class="price-discount">
                            ${response.data.price_data.price ? 
                                response.data.price_data.price.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + response.data.currency 
                                    + response.data.price_data.price_suffix.charAt(0) : '' 
                            }<span class="txt-small">${response.data.price_data.price_suffix.substring(1)}</span>
                        </div>
                    </div>
                    <div class="host-service-btn">
                        <span class="host-btn add-to-cart-js is-loading-btn-2" data-pid=${response.data.pid} data-ptype="domain"
                        data-domain_name=${response.data.domain_name + response.data.suffix} >
                            <span class="host-btn-wrap"></span>
                                <span class="host-btn-ani">
                                    <span class="host-btn-icon">
                                        <img src="/template/assets/images/shopping-cart.svg" alt="" class="icon">
                                        <img src="/template/assets/images/shopping-cart-color.svg" alt="" class="icon">
                                    </span>
                                    THÊM VÀO GIỎ HÀNG
                                </span>
                            </span>
                        </span>
                    </div>
                `)

                // $('.domain-suggest-ls-js').append(`
                // <div class="host-service-item">
                //     <div class="host-service-wrap d-flex f-ctn">
                //         <div class="host-service-wrap-left host-service-wrap-col col">
                //             <div class="host-service-name">
                //                 ${domain_name}<span class="hl-sec">${data.suffix}</span>
                //             </div>
                //             <ul class="host-service-tag d-flex f-start">
                //                 ${(() => {
                //                     try {
                //                         let html = ''
                //                         for( let cat of categories ) {
                //                             html += `<div class="host-service-tag-item">${cat.name}</div>`
                //                         }
                //                         return html;
                //                     } catch (error) {
                //                         console.error(error);
                //                         return '';
                //                     }
                //                 })()}
                //             </ul>
                //         </div>
                //         <div class="host-service-wrap-right host-service-wrap-col col">
                //             <div class="host-service-price d-flex">
                //                 <div class="price-cover">
                //                     ${response.data.price_data.price_original ? 
                //                         response.data.price_data.price_original.replace(/\B(?=(\d{3})+(?!\d))/g, ",")  
                //                             + response.data.currency : '' }
                //                 </div>
                //                 <div class="price-discount">
                //                     ${response.data.price_data.price ? 
                //                         response.data.price_data.price.replace(/\B(?=(\d{3})+(?!\d))/g, ",") + response.data.currency 
                //                             + response.data.price_data.price_suffix.charAt(0) : '' 
                //                     }<span class="txt-small">${response.data.price_data.price_suffix.substring(1)}</span>
                //                 </div>
                //             </div>
                //             <div class="host-service-btn">
                //                 <span class="host-btn add-to-cart-js is-loading-btn-2" data-pid=${response.data.pid} data-ptype="domain"
                //                 data-domain_name=${response.data.domain_name + response.data.suffix} >
                //                     <span class="host-btn-wrap"></span>
                //                         <span class="host-btn-ani">
                //                             <span class="host-btn-icon">
                //                                 <img src="/template/assets/images/shopping-cart.svg" alt="" class="icon">
                //                                 <img src="/template/assets/images/shopping-cart-color.svg" alt="" class="icon">
                //                             </span>
                //                             THÊM VÀO GIỎ HÀNG
                //                         </span>
                //                     </span>
                //                 </span>
                //             </div>
                //         </div>
                //     </div>
                // </div>
                // `)
            } else {
                $(price_wrap_ele).html(`
                    <div class="host-service-btn">
                        <a href="javascript:;" class="host-btn host-btn-blue whois-open-js is-loading-btn-2" data-domain="${domain_name + suffix}">
                            <span class="host-btn-wrap">
                                <span class="host-btn-ani">
                                    <span class="host-btn-icon">
                                        <img src="/template/assets/images/headphone-white.svg" alt="" class="icon">
                                        <img src="/template/assets/images/headphone-color.svg" alt="" class="icon">
                                    </span>
                                    THÔNG TIN SỞ HỮU
                                </span>
                            </span>
                        </a>
                    </div>
                `)
            }
        })
        .catch((error) => {
            console.log(error)
        });
    }

    // open account popup
    $(document).on('click', '.whois-open-js', function () {
        let domain_value = $(this).attr('data-domain');

        let loading = $(this);
        if (!loading.hasClass('loading')) {
            $.ajax({
                url: mona_ajax_url.ajaxURL,
                type: 'post',
                data: {
                    action: 'mona_ajax_whois_check',
                    domain_value: domain_value ? domain_value : '',
                },
                error: function (request) {
                    loading.removeClass('loading');
                },
                beforeSend: function () {
                    loading.addClass('loading');
                },
                success: function (result) {
                    if (result.success) {

                        $('.whois-main-js').html(`
                            <p class="title">Thông tin tên miền</p>
                            <div class="domain-name-gr">
                                <p class="domain">${result.data.whois_data.domainName}</p>
                                <p class="desc">Dữ liệu được cập nhật ngày ${result.data.whois_data.updatedDate}</p>
                            </div>
                            <div class="whois-info-list">
                                <div class="info-item">
                                    <span class="label">Tên miền:</span>
                                    <span class="text cl-blue">${result.data.whois_data.domainName}</span>
                                </div>
                                <div class="info-item">
                                    <span class="label">Ngày đăng ký:</span>
                                    <span class="text">${result.data.whois_data.creationDate}</span>
                                </div>
                                <div class="info-item">
                                    <span class="label">Ngày hết hạn:</span>
                                    <span class="text bold">${result.data.whois_data.expirationDate}</span>
                                </div>
                                <div class="info-item">
                                    <span class="label">Chủ sở hữu tên miền:</span>
                                    <span class="text">${result.data.whois_data.registrantName}</span>
                                </div>
                                <div class="info-item">
                                    <span class="label">Danh sách trạng thái:</span>
                                    <span class="text">${result.data.whois_data.status.join(',</br>')}</span>
                                </div>
                                <div class="info-item">
                                    <span class="label">Quản lý tại Nhà đăng ký:</span>
                                    <span class="text">${result.data.whois_data.registrar}</span>
                                </div>
                                <div class="info-item">
                                    <span class="label">Name Servers:</span>
                                    <span class="text">${result.data.whois_data.nameServer.join(',</br>')}</span>
                                </div>
                                <div class="info-item">
                                    <span class="label">DNSSC:</span>
                                    <span class="text">${result.data.whois_data.DNSSEC}</span>
                                </div>
                            </div>
                            <div class="confirm-btn btn-clh pop-close-js">Xác nhận</div>
                        `)

                    } else {
                        $('.whois-main-js').html(`
                            <div class="empty-mess cart-mess">
                                <img class="empty-img" src="/template/assets/images/host-header-img.png" alt="">
                                <p class="empty-content">${result.data.message}!</p>
                                <div class="confirm-btn btn-clh openPopMona" data-popup="solutions">Liên hệ</div>
                            </div>
                        `)
                    }
                    $('.popup-bg-js').addClass('active');
                    $('.popup-whois-js').addClass('active');
                    $('body').addClass('no-scroll');
                    loading.removeClass('loading');
                }
            });
        }
    })
}