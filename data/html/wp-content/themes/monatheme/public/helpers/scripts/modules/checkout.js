jQuery(document).ready(function ($) {
    $(".select2choose").each(function (i, v) {
        var placeholder = $(this).attr("data-placeholder");
        $(this).select2({
            width: '100%',
            placeholder: placeholder,
            dropdownCssClass: 'no-search'
        });

        $(this).on('change', function() {
            if ( $(this).hasClass('billingcycle-cart-js') ) {
                let billingcycle = $(this).val();
                let pid = $(this).attr('data-pid');
                let cart_item_key = $(this).attr('data-cart_item_key');
    
                let loading = $(this).closest('.checkout-wrap-js');
                if (!loading.hasClass('loading')) {
                    $.ajax({
                        url: mona_ajax_url.ajaxURL,
                        type: 'post',
                        data: {
                            action: 'mona_ajax_update_cart_item',
                            billingcycle: billingcycle,
                            pid: pid,
                            cart_item_key: cart_item_key,
                        },
                        error: function (request) {
                            loading.removeClass('loading');
                        },
                        beforeSend: function () {
                            loading.addClass('loading');
                        },
                        success: function (result) {
                            if (result.success) {
    
                                // $('.cart-wrap-js').html(result.data.cart_html);
                                $('body').trigger("update_checkout");
                            }
                            loading.removeClass('loading');
                        }
                    });
                }
            }

            // change address when change country 
            if ( $(this).hasClass('country-js') ) {

                let js_form_wrap = $(this).closest('.form-checkout-wrap-js');

                let state_name = $(js_form_wrap).find('.state-js').attr('data-post_name');
                let country_label = $(js_form_wrap).find('.country-js option:checked').html();
                $(js_form_wrap).find('.country-label-js').val(country_label);

                if ( $(this).val() == 'VN' ) {

                    // show tỉnh, huyện, phường
                    $(js_form_wrap).find('.state-js').show();
                    $(js_form_wrap).find('.state-wrap-js .select2-container').show();
                    $(js_form_wrap).find('.state-js').attr('name', state_name);
                    $(js_form_wrap).find('.state-default-js').attr('hidden', true);
                    $(js_form_wrap).find('.state-default-js').removeAttr('name');
                    $(js_form_wrap).find('.city-wrap-js').show();
                    $(js_form_wrap).find('.ward-wrap-js').show();

                    
                    $(js_form_wrap).find('.city-input-js').attr('name', 'billing_city');
                    $(js_form_wrap).find('.city-default-js').removeAttr('name');
                    $(js_form_wrap).find('.city-default-js').val('');
                    $(js_form_wrap).find('.ward-input-js').attr('name', 'billing_ward');
                    $(js_form_wrap).find('.ward-default-js').removeAttr('name');
                    $(js_form_wrap).find('.ward-default-js').val('');

                    let loading = $(this);
                    if (!loading.hasClass('loading')) {
                        $.ajax({
                            url: mona_ajax_url.ajaxURL,
                            type: 'post',
                            data: {
                                action: 'mona_ajax_get_provinces',
                            },
                            error: function (request) {
                                loading.removeClass('loading');
                            },
                            beforeSend: function () {
                                loading.addClass('loading');
                            },
                            success: function (result) {
                                if (result.success) {
                                    if ( result.data.provinces ) {
                                        // create new state selection
                                        let provinces = JSON.parse(result.data.provinces)
                                        if ( provinces.length > 0 ) {
                                            $(js_form_wrap).find('.state-js').select2('destroy');
                                            $(js_form_wrap).find('.state-js').html('');

                                            let first_val = '';
                                            $(provinces).each((i, item) => {
                                                if ( first_val == '' ) {
                                                    first_val = item.name;
                                                }
                                                $(js_form_wrap).find('.state-js').append(`<option value=${item.name}>${item.value}</option>`)
                                            })
                                            $(js_form_wrap).find('.state-js').select2({
                                                width: '100%',
                                                placeholder: placeholder,
                                                dropdownCssClass: 'no-search'
                                            });
                                            
                                            //Select value and trigger change event
                                            $(js_form_wrap).find('.state-js').val(first_val).trigger("change")

                                            // clear all option
                                            $(js_form_wrap).find(".city-js").empty();
                                            $(js_form_wrap).find(".ward-js").empty();
                                        } 
                                    }
                                }
                                loading.removeClass('loading');
                            }
                        });
                    }

                } else {

                    // hide tỉnh, huyện, phường 
                    $(js_form_wrap).find('.state-js').hide();
                    $(js_form_wrap).find('.state-wrap-js .select2-container').hide();
                    $(js_form_wrap).find('.state-js').removeAttr('name');
                    $(js_form_wrap).find('.state-default-js').removeAttr('hidden');
                    $(js_form_wrap).find('.state-default-js').attr('name', state_name);
                    $(js_form_wrap).find('.city-wrap-js').hide();
                    $(js_form_wrap).find('.city-input-js').removeAttr('name');
                    $(js_form_wrap).find('.city-default-js').attr('name', 'billing_city');
                    $(js_form_wrap).find('.city-default-js').val('default');
                    $(js_form_wrap).find('.ward-wrap-js').hide();
                    $(js_form_wrap).find('.ward-input-js').removeAttr('name');
                    $(js_form_wrap).find('.ward-default-js').attr('name', 'billing_ward');
                    $(js_form_wrap).find('.ward-default-js').val('default');
                }
            }

            // change address when change provinces 
            if ( $(this).hasClass('state-js') ) {
                let matinh = $(this).val();
                let loading = $(this);
                let js_form_wrap = $(this).closest('.form-checkout-wrap-js');

                let state_label = $(js_form_wrap).find('.state-js option:checked').html();
                $(js_form_wrap).find('.state-label-js').val(state_label);

                if (!loading.hasClass('loading')) {
                    $.ajax({
                        url: mona_ajax_url.ajaxURL,
                        type: 'post',
                        data: {
                            action: 'mona_ajax_get_districts',
                            matinh: matinh
                        },
                        error: function (request) {
                            loading.removeClass('loading');
                        },
                        beforeSend: function () {
                            loading.addClass('loading');
                        },
                        success: function (result) {
                            if (result.success) {
                                if ( result.data.districts ) {

                                    // create new city selection
                                    let districts = JSON.parse(result.data.districts)
                                    if ( districts.length > 0 ) {
                                        $(js_form_wrap).find('.city-js').select2('destroy');
                                        $(js_form_wrap).find('.city-js').html('');

                                        let first_val = '';
                                        $(districts).each((i, item) => {
                                            if ( first_val == '' ) {
                                                first_val = item;
                                            }
                                            $(js_form_wrap).find('.city-js').append(`<option value="${item}" data-matinh="${matinh}">${item}</option>`)
                                        })
                                        $(js_form_wrap).find('.city-js').select2({
                                            width: '100%',
                                            placeholder: placeholder,
                                            dropdownCssClass: 'no-search'
                                        });

                                        //Select value and trigger change event
                                        $(js_form_wrap).find('.city-js').val(first_val).trigger("change")

                                        // clear all option
                                        $(js_form_wrap).find(".ward-js").empty();
                                    } 
                                }
                            }
                            loading.removeClass('loading');
                        }
                    });
                }
            }

            // change address when change provinces 
            if ( $(this).hasClass('city-js') ) {
                
                let quanhuyen = $(this).val();
                let js_form_wrap = $(this).closest('.form-checkout-wrap-js');
                let matinh = $(js_form_wrap).find('.state-js').val();
                let loading = $(this);

                if (!loading.hasClass('loading')) {
                    $.ajax({
                        url: mona_ajax_url.ajaxURL,
                        type: 'post',
                        data: {
                            action: 'mona_ajax_get_wards',
                            matinh: matinh,
                            quanhuyen: quanhuyen,
                        },
                        error: function (request) {
                            loading.removeClass('loading');
                        },
                        beforeSend: function () {
                            loading.addClass('loading');
                        },
                        success: function (result) {
                            if (result.success) {
                                if ( result.data.wards ) {

                                    // create new city selection
                                    let wards = JSON.parse(result.data.wards)
                                    if ( wards.length > 0 ) {
                                        $(js_form_wrap).find('.ward-js').select2('destroy');
                                        $(js_form_wrap).find('.ward-js').html('');
                                        $(wards).each((i, item) => {
                                            $(js_form_wrap).find('.ward-js').append(`<option value="${item}">${item}</option>`)
                                        })
                                        $(js_form_wrap).find('.ward-js').select2({
                                            width: '100%',
                                            placeholder: placeholder,
                                            dropdownCssClass: 'no-search'
                                        });
                                    } 
                                }
                            }
                            loading.removeClass('loading');
                        }
                    });
                }
            }

        })
        if ( $(this).hasClass('country-js') ) {
            $(this).val('VN')
            $(this).trigger('change')
        }
    });

})