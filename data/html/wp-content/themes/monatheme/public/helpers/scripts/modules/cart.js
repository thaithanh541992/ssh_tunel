jQuery(document).ready(function ($) {
    $(".select2choose").each(function (i, v) {
        var placeholder = $(this).attr("data-placeholder");
        $(this).select2({
            width: '100%',
            placeholder: placeholder,
            dropdownCssClass: 'no-search'
        });

        $(this).on('change', function() {
            if ( $(this).hasClass('billingcycle-cart-js') ) {
                let billingcycle = $(this).val();
                let pid = $(this).attr('data-pid');
                let cart_item_key = $(this).attr('data-cart_item_key');
    
                let loading = $(this).closest('.cart-wrap-js');
                if (!loading.hasClass('loading')) {
                    $.ajax({
                        url: mona_ajax_url.ajaxURL,
                        type: 'post',
                        data: {
                            action: 'mona_ajax_update_cart_item',
                            billingcycle: billingcycle,
                            pid: pid,
                            cart_item_key: cart_item_key,
                        },
                        error: function (request) {
                            loading.removeClass('loading');
                        },
                        beforeSend: function () {
                            loading.addClass('loading');
                        },
                        success: function (result) {
                            if (result.success) {
    
                                $('.cart-wrap-js').html(result.data.cart_html);

                            }
                            loading.removeClass('loading');
                        }
                    });
                }
            } else if ( $(this).hasClass('billingcycle-select-js') ) { // change billingcycle => change price UI
                let price_ele = $($(this).siblings('.product-price-js'));
                let suffix = price_ele.attr('data-suffix');
                let price = $(this).find('option:selected').attr('data-price');
                let term_qty = $(this).find('option:selected').attr('data-term_qty');

                price_per_term = parseInt(price/term_qty).toString();

                let formatted_price = price_per_term.replace(/\B(?=(\d{3})+(?!\d))/g, ".");

                formatted_price = formatted_price + suffix;

                price_ele.text(formatted_price)
            }
        })
    });
})