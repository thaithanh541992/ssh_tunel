
import Select2Module from "/template/js/modules/Select2Module.js";

export default function MonaProductModule() {
    $(document).ready(function () {

        // add to cart
        $(document).on('click', '.add-to-cart-js', function () {
            let product_item = $(this).closest('.product-item-js');
            let pid = $(this).attr('data-pid');
            let domain_name = $(this).attr('data-domain_name');
            let ptype = $(this).attr('data-ptype');
            let billingcycle = product_item.find('.billingcycle-select-js').val();
            if ( billingcycle == null ) {
                billingcycle = $(this).attr('data-billing_cycle');
            }

            let loading = $(this);
            if (!loading.hasClass('loading')) {
                $.ajax({
                    url: mona_ajax_url.ajaxURL,
                    type: 'post',
                    data: {
                        action: 'mona_ajax_add_to_cart',
                        pid: pid ? pid : '',
                        ptype: ptype ? ptype : '',
                        domain_name: domain_name ? domain_name : '',
                        billingcycle: billingcycle ? billingcycle : '',
                    },
                    error: function (request) {
                        loading.removeClass('loading');
                    },
                    beforeSend: function () {
                        loading.addClass('loading');
                    },
                    success: function (result) {
                        if (result.success) {
                            Noti({
                                text: result.data.message,
                                title: result.data.title,
                                icon: 'success',
                                timer: 3000
                            })

                            $('.mini-cart-qty-js').html(result.data.cart_quantity)
                            $('.cart-wrap-js').html(result.data.cart_html);
                            Select2Module()
                        }
                        loading.removeClass('loading');
                    }
                });
            }
        })

        // delete cart item 
        $(document).on('click', '.delete-cart-item-js', function() {

            let cart_item_key = $(this).attr('data-cart_item_key');

            let loading = $(this).closest('.cart-wrap-js');
            if (!loading.hasClass('loading')) {
                $.ajax({
                    url: mona_ajax_url.ajaxURL,
                    type: 'post',
                    data: {
                        action: 'mona_ajax_delete_cart_item',
                        cart_item_key: cart_item_key,
                    },
                    error: function (request) {
                        loading.removeClass('loading');
                    },
                    beforeSend: function () {
                        loading.addClass('loading');
                    },
                    success: function (result) {
                        if (result.success) {

                            console.log(window.location.href);

                            if ( window.location.href.includes('/thanh-toan/') ) {
                                window.location.href = window.location.href
                            }

                            $('.cart-wrap-js').html(result.data.cart_html);
                            $('.mini-cart-qty-js').html(result.data.cart_quantity)
                            Select2Module()
                        }
                        loading.removeClass('loading');
                    }
                });
            }
        })

        $('.hosting-tab-js').on('click', function() {

            // change ui tab btn
            let wrapper = $(this).closest('.hosting-wrap-js');
            $(wrapper).find('.hosting-tab-js').removeClass('active');
            $(this).addClass('active');

            // calculate price'
            let term_qty = parseInt($(this).attr('data-qty'));
            $(wrapper).find('.hosting-price-js').each( (index, ele) => {

                let billing_list = $(ele).attr('data-billing_list');
                let original_price = $(ele).attr('data-original_price');
               
                let price = 0;
                let sale_percent = 0;
                if ( billing_list != null ) {
                    billing_list = JSON.parse(billing_list);
                    
                    for ( let billing_item of billing_list ) {
                        if ( billing_item.term_quantity == term_qty ) {
                            price = Math.floor(billing_item.price / billing_item.term_quantity);
                            if ( original_price > 0 ) {
                                sale_percent = 100 - Math.ceil(price / original_price * 100);
                            }
                        }
                    }
                }

                price = String(price)

                // change data and html
                let html = 
                `<div class="txt-price">
                ${price.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} <span class="dash-month fw-400"> đ/tháng </span>
                </div>
                ${(() => {
                    if ( sale_percent > 0 ) {
                        return `<div><span class="txt-sale">${sale_percent}%</span></div>`
                    } else {
                        return '';
                    }
                })()}
                `
                $(ele).html(html);
            })
            
            $(wrapper).find('.add-to-cart-js').attr('data-billing_cycle', term_qty);
        })

        $('.vps-billing-cycle-js').on('change', (e) => {
            
            let wrapper = $(e.target).closest('.vps-item-js');
            let billing_list = $(e.target).attr('data-billing_list');
            let original_price = $(e.target).attr('data-original_price');

            let term_qty = $(e.target).val();

            let price = 0;
            let sale_percent = 0;
            if ( billing_list != null ) {
                billing_list = JSON.parse(billing_list);
                
                for ( let billing_item of billing_list ) {
                    if ( billing_item.term_quantity == term_qty ) {
                        price = Math.floor(billing_item.price / billing_item.term_quantity);
                        if ( original_price > 0 ) {
                            sale_percent = 100 - Math.ceil(price / original_price * 100);
                        }
                    }
                }
            }

            price = String(price)

            // change data and html
            let html = 
            `<div>
            ${price.replace(/\B(?=(\d{3})+(?!\d))/g, ",")} đ<span class="dash-month">/tháng</span>
            </div>
            ${(() => {
                if ( sale_percent > 0 ) {
                    return `<div class="sale">${sale_percent}%</div>`
                } else {
                    return '';
                }
            })()}
            `
            $(wrapper).find('.vps-price-js').html(html);
            $(wrapper).find('.add-to-cart-js').attr('data-billing_cycle', term_qty);
        })

        const mainElement = document.querySelector('main');
        function Noti({ icon = 'success', text, title, timer = 4000, redirect = '' }) {
            var noti_con = document.querySelector('.noti_con');
            if (!noti_con) {
                var noti_con = document.createElement('div');
                noti_con.setAttribute('class', 'noti_con');
                mainElement.appendChild(noti_con);
            }
            var noti_alert = document.createElement('div');
            var noti_icon = document.createElement('div');
            var noti_process = document.createElement('div');
            noti_icon.setAttribute('class', 'noti_icon ' + icon);
            noti_alert.setAttribute('class', 'noti_alert');
            noti_process.setAttribute('class', 'progress active ' + icon);
            noti_process.setAttribute('style', '--delay: ' + timer/1000 + 's');
            noti_alert.innerHTML = '<div class="message"><p class="text1">' + title + '</p><p class="text2">' + text + '</p></div>';
            noti_alert.prepend(noti_icon);
            noti_alert.prepend(noti_process);
            noti_con.prepend(noti_alert);

            if (icon == 'success') {
                // noti_icon.style.background = '#00b972';
                noti_icon.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"><path stroke-dasharray="60" stroke-dashoffset="60" d="M3 12C3 7.02944 7.02944 3 12 3C16.9706 3 21 7.02944 21 12C21 16.9706 16.9706 21 12 21C7.02944 21 3 16.9706 3 12Z"><animate fill="freeze" attributeName="stroke-dashoffset" dur="0.5s" values="60;0" /></path><path stroke-dasharray="14" stroke-dashoffset="14" d="M8 12L11 15L16 10"><animate fill="freeze" attributeName="stroke-dashoffset" begin="0.6s" dur="0.2s" values="14;0" /></path></g></svg>';

            } else if (icon == 'info') {
                // noti_icon.style.background = '#0395FF';
                noti_icon.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none" stroke="currentColor" stroke-linecap="round" stroke-width="2"><path stroke-dasharray="60" stroke-dashoffset="60" d="M12 3C16.9706 3 21 7.02944 21 12C21 16.9706 16.9706 21 12 21C7.02944 21 3 16.9706 3 12C3 7.02944 7.02944 3 12 3Z"><animate fill="freeze" attributeName="stroke-dashoffset" dur="0.5s" values="60;0" /></path><path stroke-dasharray="20" stroke-dashoffset="20" d="M8.99999 10C8.99999 8.34315 10.3431 7 12 7C13.6569 7 15 8.34315 15 10C15 10.9814 14.5288 11.8527 13.8003 12.4C13.0718 12.9473 12.5 13 12 14"><animate fill="freeze" attributeName="stroke-dashoffset" begin="0.6s" dur="0.4s" values="20;0" /></path></g><circle cx="12" cy="17" r="1" fill="currentColor" fill-opacity="0"><animate fill="freeze" attributeName="fill-opacity" begin="1s" dur="0.2s" values="0;1" /></circle></svg>';

            } else if (icon == 'danger' || icon == 'error') {
                // noti_icon.style.background = '#FF032C';
                noti_icon.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none" stroke="currentColor" stroke-linecap="round" stroke-width="2"><path stroke-dasharray="60" stroke-dashoffset="60" d="M12 3C16.9706 3 21 7.02944 21 12C21 16.9706 16.9706 21 12 21C7.02944 21 3 16.9706 3 12C3 7.02944 7.02944 3 12 3Z"><animate fill="freeze" attributeName="stroke-dashoffset" dur="0.5s" values="60;0" /></path><path stroke-dasharray="8" stroke-dashoffset="8" d="M12 12L16 16M12 12L8 8M12 12L8 16M12 12L16 8"><animate fill="freeze" attributeName="stroke-dashoffset" begin="0.6s" dur="0.2s" values="8;0" /></path></g></svg>';

            } else {
                // noti_icon.style.background = '#00b972';
                noti_icon.innerHTML = '<svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" preserveAspectRatio="xMidYMid meet" viewBox="0 0 24 24"><g fill="none" stroke="currentColor" stroke-linecap="round" stroke-linejoin="round" stroke-width="2"><path stroke-dasharray="60" stroke-dashoffset="60" d="M3 12C3 7.02944 7.02944 3 12 3C16.9706 3 21 7.02944 21 12C21 16.9706 16.9706 21 12 21C7.02944 21 3 16.9706 3 12Z"><animate fill="freeze" attributeName="stroke-dashoffset" dur="0.5s" values="60;0" /></path><path stroke-dasharray="14" stroke-dashoffset="14" d="M8 12L11 15L16 10"><animate fill="freeze" attributeName="stroke-dashoffset" begin="0.6s" dur="0.2s" values="14;0" /></path></g></svg>';

            }

            setTimeout(() => {
                noti_alert.classList.add('active');
            }, 100);

            setTimeout(() => {
                noti_alert.classList.remove('active');
            }, timer);

            setTimeout(() => {
                noti_alert.remove();
            }, timer + 2000);
        }
        function success(text) {
            Noti({
                text: text,
                icon: 'success',
                timer: 5000
            })
        }
        function info(text) {
            Noti({
                text: text,
                icon: 'info',
                timer: 5000
            })
        }
        function danger(text) {
            Noti({
                text: text,
                icon: 'danger',
                timer: 5000
            })
        }
    })

}