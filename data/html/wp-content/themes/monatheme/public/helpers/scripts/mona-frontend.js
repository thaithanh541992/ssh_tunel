import MonaProductModule from './modules/product.js';
import MonaDefaultModule from './modules/default.js';
import MonaAccountModule from './modules/account.js';
import MonaDomainModule from './modules/domain.js';

/****************************/

MonaProductModule();
MonaDefaultModule();
MonaAccountModule();
MonaDomainModule();