export default function MonaAccountModule() {
    $(document).ready( function() {

        // submit form login
        $(document).on('submit', '#frmLogin', function(e) {
            e.preventDefault();
            var $this = $(this);
            let loading = $this.find('.mona-login-submit');
            var $form = $this.serialize();
            if (!loading.hasClass('loading')) {
                $.ajax({
                    url: mona_ajax_url.ajaxURL,
                    type: 'post',
                    data: {
                        action: 'mona_ajax_custommer_login',
                        form: $form,
                    },
                    error: function(request) {
                        loading.removeClass('loading');
                    },
                    beforeSend: function() {
                        $('#frmLogin .mona-error').fadeOut();
                        $('#frmLogin .mona-notice').fadeOut();
                        loading.addClass('loading');
                    },
                    success: function(result) {
                        if (result.success) {

                            $('#frmLogin .mona-notice.success').text(result.data.message);
                            $('#frmLogin .mona-notice.success').fadeIn();

                            setTimeout(function () {
                                if (result.data.redirect) {
                                    window.location.href = result.data.redirect;
                                } else {
                                    window.location.reload();
                                }
                            }, 500 )

                        } else {

                            if (result.data.title) {

                                $('#frmLogin .mona-notice.error').text(result.data.message);
                                $('#frmLogin .mona-notice.error').fadeIn();

                            } else {
    
                                if (result.data.error) {
    
                                    $.each(result.data.error, function(key, val) {
                                        $('#frmLogin .' + key).text(val);
                                        $('#frmLogin .' + key).fadeIn();
                                    });
                                }
                            }
                        }
                        loading.removeClass('loading');
                    }
                });
            }

        });

         // submit form register
        $(document).on('submit', '#frmRegister', function(e) {
            e.preventDefault();
            var $this = $(this);
            let loading = $this.find('.mona-register-submit');
            var $form = $this.serialize();
            if (!loading.hasClass('loading')) {
                $.ajax({
                    url: mona_ajax_url.ajaxURL,
                    type: 'post',
                    data: {
                        action: 'mona_ajax_custommer_register',
                        form: $form,
                    },
                    error: function(request) {
                        loading.removeClass('loading');
                    },
                    beforeSend: function() {
                        $('#frmRegister .mona-error').fadeOut();
                        $('#frmRegister .mona-notice').fadeOut();
                        loading.addClass('loading');
                    },
                    success: function(result) {
                        if (result.success) {
                            
                            // change tap to login tab
                            $('.popup-account-js .open-register').removeClass('active');
                            $('.popup-account-js .open-login').addClass('active');
                            $('.tab-panel-item.form-register').removeClass('active');
                            $('.tab-panel-item.form-login').addClass('active');
    
                            // show message
                            $('#frmLogin .mona-notice.success').text(result.data.message);
                            $('#frmLogin .mona-notice.success').fadeIn();
    
                        } else {
    
                            if (result.data.title) {
    
                                $('#frmRegister .mona-notice.error').text(result.data.message);
                                $('#frmRegister .mona-notice.error').fadeIn();
    
                            } 
                            
                            if (result.data.error) {

                                $.each(result.data.error, function(key, val) {
                                    $('#frmRegister .' + key).html(val);
                                    $('#frmRegister .' + key).fadeIn();
                                });

                            }
                        }
                        loading.removeClass('loading');
                    }
                });
            }
    
        });

        // submit form forgot password
        $(document).on('submit', '#frmForgot', function(e) {
            e.preventDefault();
            var $this = $(this);
            let $loading = $this.find('.mona-forgot-submit');
            var $form = $this.serialize();
            if (!$loading.hasClass('loading')) {
                $.ajax({
                    url: mona_ajax_url.ajaxURL,
                    type: 'post',
                    data: {
                        action: 'mona_ajax_custommer_forget_password',
                        form: $form,
                    },
                    error: function(request) {
                        $loading.removeClass('loading');
                    },
                    beforeSend: function() {
                        $('.mona-notice').hide();
                        $loading.addClass('loading');
                    },
                    success: function(result) {
                        if (result.success) {
    
                            $('#frmForgot .mona-notice.success').text(result.data.message);
                            $('#frmForgot .mona-notice.success').fadeIn();
    
                        } else {
                            $('#frmForgot .mona-notice.error').text(result.data.error);
                            $('#frmForgot .mona-notice.error').fadeIn();
                        }
                        $loading.removeClass('loading');
                    }
                });
            }
        });

        // get management link
        $(document).on('click', '.monahost-open-manage-js', function() {
            $.ajax({
                url: mona_ajax_url.ajaxURL,
                type: 'post',
                data: {
                    action: 'mona_ajax_open_manage',
                },
                success: function(result) {
                    if (result.success && result.data.url) {

                        window.open(result.data.url, "_blank");
                    }
                }
            });
        })

        // logout
        $(document).on('click', '.monahost-logout-js', monahost_logout)

        function monahost_logout() {
            $.ajax({
                url: mona_ajax_url.ajaxURL,
                type: 'post',
                data: {
                    action: 'mona_ajax_logout',
                },
                success: function(result) {
                    location.reload();
                }
            });
        }
    })
}