<?php
class MonahostCart
{
    private $cart_items;
    private $total = 0.0;

    public function __construct() {

        // Initialize the cart
        $this->cart_items = new stdClass();
        $this->total = 0.0;
    }

    public static function construct_with_data() {
        
        $cart = new MonahostCart();
        if ( !empty( monahost_get_current_user_id() ) ) {

        } else {
            if ( session_id() && !empty( $_SESSION['monahost_cart'] )) {
                $cart_data = json_decode($_SESSION['monahost_cart']);
                $cart->cart_items = $cart_data->cart_items;
                $cart->total = $cart_data->total;
            }
        }
        
        return $cart;
    }

    // Add an item to the cart
    public function add_item($item_id, $billingcylcle = '') {
        if ( !emptY( $item_id ) ) {
            
            // Check if the item already exists in the cart
            if (isset($this->cart_items->$item_id)) {
                // Update billing cycle
                $this->cart_items->$item_id->billingcylcle = $billingcylcle;
            } else {
                // Add a new item to the cart
                $item_data = new stdClass();
                $item_data->billingcylcle = $billingcylcle;
                $this->cart_items->$item_id = $item_data;
            }
        }
    }

    public function get_item_billingcycle( $item_id ) {
        if ( !empty( $this->cart_items->$item_id ) ) {
            return $this->cart_items->$item_id->billingcylcle;
        }
        return 0;
    }

    // Remove an item from the cart
    public function remove_item($item_id) {

        // Check if the item exists in the cart
        if (isset($this->cart_items->$item_id)) {
            // Remove the item from the cart
            unset($this->cart_items->$item_id);
        }
    }

    // Get the total number of items in the cart
    public function get_total_items() {
        
        return count((array)$this->cart_items);
    }

    // Get the cart items
    public function get_cart_items() {

        return $this->cart_items;
    }

    // get product ids at string
    public function get_product_ids() {
        return implode(',', array_keys((array)$this->cart_items));
    }

    // Clear the cart
    public function clear_cart() {

        $this->cart_items = new stdClass();
    }

    //check cart empty
    public function is_emtpy() {
        
        return empty( $this->cart_items );
    }

    // Save the cart to persistent storage
    public function save_cart($user_id) {

        $cart_data = array(
            'cart_items' => $this->cart_items,
            'total' => $this->total,
        );

        if ( !empty( $user_id ) ) {

        } else {
            if ( session_id() ) {
                $_SESSION['monahost_cart'] = json_encode( $cart_data, JSON_UNESCAPED_UNICODE );
            }
        }
    }
}