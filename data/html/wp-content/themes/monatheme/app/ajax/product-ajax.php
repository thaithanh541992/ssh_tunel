<?php

add_action( 'wp_ajax_mona_ajax_add_to_cart',  'mona_ajax_add_to_cart' ); // login
add_action( 'wp_ajax_nopriv_mona_ajax_add_to_cart',  'mona_ajax_add_to_cart' ); // no login
function mona_ajax_add_to_cart() {
    
    $pid = esc_attr( $_POST['pid'] );
    $domain_name = esc_attr( $_POST['domain_name'] );
    $ptype = esc_attr( $_POST['ptype'] );
    $billingcycle = esc_attr( $_POST['billingcycle'] );

    $_product = wc_get_product( $pid );
    $mona_attr = [];

    switch( $ptype ) {
        case 'domain':
            // Your product attribute settings
            $taxonomy   = 'pa_domain-name'; // The taxonomy
            $term_name  = $domain_name; // The term
        
            $attributes = (array) $_product->get_attributes();
            $term_obj = get_term_by( 'name', $term_name, $taxonomy );
            $term_id    = $term_obj->term_id;
        
            // 1) If The product attribute is set for the product
            if( array_key_exists( $taxonomy, $attributes ) ) {
                foreach( $attributes as $key => $attribute ){
                    if( $key == $taxonomy ){
                        $attribute->set_options( array( $term_id ) );
                        $attributes[$key] = $attribute;
                        break;
                    }
                }
                $_product->set_attributes( $attributes );
            }
            // 2. The product attribute is not set for the product
            else {
                $attribute = new WC_Product_Attribute();
        
                $attribute->set_id( sizeof( $attributes) + 1 );
                $attribute->set_name( $taxonomy );
                $attribute->set_options( array( $term_id ) );
                $attribute->set_position( sizeof( $attributes) + 1 );
                $attribute->set_visible( true );
                $attribute->set_variation( false );
                $attributes[] = $attribute;
        
                $_product->set_attributes( $attributes );
            }
        
            $_product->save();
        
            // Append the new term in the product
            if( ! has_term( $term_name, $taxonomy, $_product->get_id() ) )
                wp_set_object_terms($_product->get_id(), $term_name, $taxonomy, true );
        
            $term_obj = get_term_by( 'name', $term_name, $taxonomy );
            $mona_attr['attribute_' . $taxonomy] = $term_obj->slug;
        
            $custom_data = [
                'domain_name' => $domain_name,
                'billingcycle' => 12
            ];
            // var_dump($custom_data);
            break;
        default:
            $custom_data = [
                'domain_name' => $domain_name,
                'billingcycle' => $billingcycle
            ];
            break;
    }

    
    // Add the product to the cart
    WC()->cart->add_to_cart($pid, 1, 0, $mona_attr, ['custom_data' => $custom_data]);

    echo wp_send_json_success(
        [
            'title'=> __('Thông báo', 'monamedia'),
            'message' => __('Thêm sản phẩm vào giỏ hàng thành công.', 'monamedia'),
            'cart_html' => monahost_get_cart_html(),
            'cart_quantity' => WC()->cart->get_cart_contents_count(),
        ]
    );
    wp_die();
}

add_action( 'wp_ajax_mona_ajax_update_cart_item',  'mona_ajax_update_cart_item' ); // login
add_action( 'wp_ajax_nopriv_mona_ajax_update_cart_item',  'mona_ajax_update_cart_item' ); // no login
function mona_ajax_update_cart_item() {
    
    // clean input data
    $cart_item_key = esc_attr( $_POST['cart_item_key'] );
    $billingcycle = esc_attr( $_POST['billingcycle'] );


    // get field data
    $cart = WC()->cart;
    $cart_list = $cart->cart_contents;
    $selected_item = $cart_list[$cart_item_key];

    // update data
    $selected_item['custom_data']['billingcycle'] = $billingcycle;

    
    // update cart
    $cart_list[$cart_item_key] = $selected_item;
    $cart->cart_contents = $cart_list;
    WC()->cart->calculate_totals();
    WC()->cart->set_session();
    WC()->cart->maybe_set_cart_cookies();

    echo wp_send_json_success(
        [
            'title'=> __('Thông báo', 'monamedia'),
            'message' => __('Cập nhật giỏ hàng thành công.', 'monamedia'),
            'cart_html' => monahost_get_cart_html(),
        ]
    );
    wp_die();
}


add_action( 'wp_ajax_mona_ajax_delete_cart_item',  'mona_ajax_delete_cart_item' ); // login
add_action( 'wp_ajax_nopriv_mona_ajax_delete_cart_item',  'mona_ajax_delete_cart_item' ); // no login
function mona_ajax_delete_cart_item() {
    
    $cart_item_key = esc_attr( $_POST['cart_item_key'] );
    
    WC()->cart->remove_cart_item($cart_item_key);

    echo wp_send_json_success(
        [
            'title'=> __('Thông báo', 'monamedia'),
            'message' => __('Cập nhật giỏ hàng thành công.', 'monamedia'),
            'cart_html' => monahost_get_cart_html(),
            'cart_quantity' => WC()->cart->get_cart_contents_count(),
        ]
    );
    wp_die();
}
