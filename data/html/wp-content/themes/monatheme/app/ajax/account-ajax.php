<?php
add_action( 'wp_ajax_mona_ajax_custommer_login',  'mona_ajax_custommer_login' ); // login
add_action( 'wp_ajax_nopriv_mona_ajax_custommer_login',  'mona_ajax_custommer_login' ); // no login
function mona_ajax_custommer_login() {

    $form = array();
    parse_str($_POST['form'], $form);
    $error              = [];
    $user_login = esc_attr($form['user_login']);
    $user_password = esc_attr($form['user_password']);

    if ( ! isset( $form['login_nonce_field'] ) || ! wp_verify_nonce( $form['login_nonce_field'], 'login_action' )  ) {

        echo wp_send_json_error(
            [
                'title'=> __('Thông báo', 'monamedia'),
                'message' => __('Hành động của bạn không được xác thực, vui lòng tải lại trang.', 'monamedia'),
            ]
        );
        wp_die();

    }

    if ( $user_login == '' ) {
        $error['mona-error-user-login'] = __( 'Vui lòng điền vào trường này', 'monamedia' );
    } elseif ( !filter_var($user_login, FILTER_VALIDATE_EMAIL) ) {
        $error['mona-error-user-login'] = __( 'Email không hợp lệ', 'monamedia' );
    }

    if ( $user_password == '' ) {
        $error['mona-error-pass'] = __( 'Vui lòng điền vào trường này', 'monamedia' );
    }

    
    
    if ( !empty( $error )) {
        echo wp_send_json_error(
            [
                'error'=> $error,
            ]
        );
        wp_die();
    }

    $validate_rs = monahost_validate_login( $user_login, $user_password );
    if ( !empty( $validate_rs ) ) {
        $validate_rs = json_decode( $validate_rs );

        if ( $validate_rs->result == 'success' ) {
            
            $_SESSION['user_id'] = $validate_rs->userid;
            echo wp_send_json_success(
                [
                    'title' => __('Thông báo', 'monamedia'),
                    'message' => __('Đăng nhập thành công.', 'monamedia'),
                ]
            );
            wp_die();
        } else {
            echo wp_send_json_error(
                [
                    'title' => __('Thông báo', 'monamedia'),
                    'message' => monahost_error_translate( $validate_rs->message ),
                ]
            );
            wp_die();
        }
    }
}


add_action( 'wp_ajax_mona_ajax_custommer_register',  'mona_ajax_custommer_register' ); // login
add_action( 'wp_ajax_nopriv_mona_ajax_custommer_register',  'mona_ajax_custommer_register' ); // no login
function mona_ajax_custommer_register() {
    $form = array();
    parse_str( $_POST['form'], $form );
    $user_first_name         = esc_attr( $form['user_first_name'] );
    $user_last_name         = esc_attr( $form['user_last_name'] );
    $user_email         = esc_attr( $form['user_email'] );
    $user_phone         = esc_attr( $form['user_phone'] );
    $user_address_1         = esc_attr( $form['user_address_1'] );
    $user_pass         = esc_attr( $form['user_pass'] );
    $error              = [];
    // verify
    if ( ! wp_verify_nonce( $form['register_nonce_field'], 'register_action' )  ) {
        echo wp_send_json_error(
            [
                
                'title'=> __('Thông báo', 'monamedia'),
                'message' => __('Hành động của bạn không được xác thực. Vui lòng tải lại trang.', 'monamedia'),
            ]
        );
        wp_die();
    }

    if ( $user_first_name == '' ) {
        $error['mona-error-first-name'] = __( 'Vui lòng điền vào trường này.', 'monamedia' );
    }

    if ( $user_last_name == '' ) {
        $error['mona-error-last-name'] = __( 'Vui lòng điền vào trường này.', 'monamedia' );
    }

    if ( $user_email == '' ) {
        $error['mona-error-email'] = __( 'Vui lòng điền vào trường này.', 'monamedia' );
    }
    
    if ( !empty( $user_email) && !filter_var($user_email, FILTER_VALIDATE_EMAIL)) {
        $error['mona-error-email'] = __( 'Email không hợp lệ.', 'monamedia' );
    }
    
    if ( $user_phone == '' ) {
        $error['mona-error-phone'] = __( 'Vui lòng điền vào trường này.', 'monamedia' );
    }

    if ( $user_address_1 == '' ) {
        $error['mona-error-address-1'] = __( 'Vui lòng điền vào trường này.', 'monamedia' );
    }

    if ( $user_pass == '' ) {
        $error['mona-error-pass'] = __( 'Vui lòng điền vào trường này.', 'monamedia' );
    }

    
    if ( !empty( $error ) ) {
        echo wp_send_json_error(
            [
                'error'=> $error,
            ]
        );
        wp_die();
    } 

    $register_rs = monahost_create_client( 
        $user_first_name, 
        $user_last_name,
        $user_pass,
        $user_email,
        $user_address_1,
        $user_phone
    );
    if ( !empty( $register_rs ) ) {
        $register_rs = json_decode( $register_rs );

        if ( $register_rs->result == 'success' ) {
            
            echo wp_send_json_success(
                [
                    'title' => __('Thông báo', 'monamedia'),
                    'message' => __('Đăng ký thành công. Hãy đăng nhập bằng tài khoản bạn vừa tạo.', 'monamedia'),
                ]
            );
            wp_die();
        } else {
            echo wp_send_json_error(
                [
                    'title' => __('Thông báo', 'monamedia'),
                    'message' => monahost_error_translate( $register_rs->message ),
                ]
            );
            wp_die();
        }
    }
}

add_action( 'wp_ajax_nopriv_mona_ajax_custommer_forget_password',  'mona_ajax_custommer_forget_password' ); // no login
function mona_ajax_custommer_forget_password() {

    $form = array();
    parse_str( $_POST['form'], $form );
    $user_email = esc_attr($form['user_email']);
    $error              = [];

    if ( ! isset( $form['forgot_nonce_field'] ) || ! wp_verify_nonce( $form['forgot_nonce_field'], 'forgot_action' )  ) {

        echo wp_send_json_error(
            [
                
                'title'=> __('Thông báo', 'monamedia'),
                'message' => __('Hành động của bạn không được xác thực. Vui lòng tải lại trang.', 'monamedia'),
                
            ]
        );
        wp_die();

    }

    if ( empty ( $user_email ) ) {
        $error = __( 'Vui lòng điền đầy đủ thông tin', 'monamedia' );
        echo wp_send_json_error(
            [
                'error' => $error
            ]
        );
        wp_die();
    } 

    $reset_pass_rs = monahost_reset_password( $user_email );
    if ( !empty( $reset_pass_rs ) ) {
        $reset_pass_rs = json_decode( $reset_pass_rs );

        if ( $reset_pass_rs->result == 'success' ) {
            echo wp_send_json_success(
                [
                    'message' => __('Chúng tôi đã gửi đến ' . $reset_pass_rs->email . ' đường dẫn để đặt lại mật khẩu. Vui lòng kiểm tra email của bạn.', 'monamedia')
                ]
            );
            wp_die();
        } else {
            echo wp_send_json_error(
                [
                    'error' => monahost_error_translate( $reset_pass_rs->message )
                ]
            );
            wp_die();
        }
    }
}


add_action( 'wp_ajax_mona_ajax_open_manage',  'mona_ajax_open_manage' ); // login
add_action( 'wp_ajax_nopriv_mona_ajax_open_manage',  'mona_ajax_open_manage' ); // no login
function mona_ajax_open_manage() {
    if ( session_id() && !empty( $_SESSION['user_id'] ) ) {

        $token_rs = monahost_create_sso_token( $_SESSION['user_id'] );
        if ( !empty( $token_rs ) ) {
            $token_rs = json_decode( $token_rs );
            
            if ( $token_rs->result == 'success' ) {

                echo wp_send_json_success(
                    [
                        'title' => __('Thông báo', 'monamedia'),
                        'message' => __('Đăng nhập thành công.', 'monamedia'),
                        'url' => $token_rs->redirect_url
                    ]
                );
                wp_die();

            } else {
                echo wp_send_json_error(
                    [
                        'title' => __('Thông báo', 'monamedia'),
                        'message' => monahost_error_translate( $token_rs->message ),
                    ]
                );
                wp_die();
            }
        }

    } else {
        echo wp_send_json_error(
            [
                'title'=> __('Thông báo', 'monamedia'),
                'message' => __('Hành động của bạn không được xác thực. Vui lòng tải lại trang.', 'monamedia'),
            ]
        );
        wp_die();
    }
}

add_action( 'wp_ajax_mona_ajax_logout',  'mona_ajax_logout' ); // login
add_action( 'wp_ajax_nopriv_mona_ajax_logout',  'mona_ajax_logout' ); // no login
function mona_ajax_logout() {
    wp_clear_auth_cookie();
    session_unset();
    session_destroy();
}

add_action( 'wp_ajax_mona_ajax_get_provinces',  'mona_ajax_get_provinces' ); // login
add_action( 'wp_ajax_nopriv_mona_ajax_get_provinces',  'mona_ajax_get_provinces' ); // no login
function mona_ajax_get_provinces() {

    echo wp_send_json_success(
        [
            'title' => __('Thông báo', 'monamedia'),
            'message' => __('Tải thêm thành công', 'monamedia'),
            'provinces' => inet_get_list_province(),
        ]
    );
    wp_die();

}

add_action( 'wp_ajax_mona_ajax_get_wards',  'mona_ajax_get_wards' ); // login
add_action( 'wp_ajax_nopriv_mona_ajax_get_wards',  'mona_ajax_get_wards' ); // no login
function mona_ajax_get_wards() {

    $matinh = esc_attr($_POST['matinh']);
    $quanhuyen = esc_attr($_POST['quanhuyen']);

    echo wp_send_json_success(
        [
            'title' => __('Thông báo', 'monamedia'),
            'message' => __('Tải thêm thành công', 'monamedia'),
            'wards' => inet_get_list_ward( $matinh, $quanhuyen ),
        ]
    );
    wp_die();

}


add_action( 'wp_ajax_mona_ajax_get_districts',  'mona_ajax_get_districts' ); // login
add_action( 'wp_ajax_nopriv_mona_ajax_get_districts',  'mona_ajax_get_districts' ); // no login
function mona_ajax_get_districts() {

    $matinh = esc_attr($_POST['matinh']);

    echo wp_send_json_success(
        [
            'title' => __('Thông báo', 'monamedia'),
            'message' => __('Tải thêm thành công', 'monamedia'),
            'districts' => inet_get_list_district( $matinh,  ),
        ]
    );
    wp_die();

}
