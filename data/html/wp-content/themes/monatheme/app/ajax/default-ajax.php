<?php 
add_action( 'wp_ajax_mona_ajax_pagination_products',  'mona_ajax_pagination_products' ); // login
add_action( 'wp_ajax_nopriv_mona_ajax_pagination_products',  'mona_ajax_pagination_products' ); // no login
function mona_ajax_pagination_products(){
    $form = array();
    parse_str($_POST['formdata'], $form);
   
    $post_type          =   $form['post_type'];
    $posts_per_page     =   $form['posts_per_page'];
    $paged              =   $form['paged'];
    $offset             =   ( $paged - 1 ) * $posts_per_page;

    $argsPost = array(
        'post_type' => $post_type,
        'post_status' => 'publish',
        'posts_per_page' => $posts_per_page,
        'paged' => $paged,
        'offset' => $offset,
        'meta_query' => [
            'relation' => 'AND',
        ],
        'tax_query' => [
            'relation'=>'AND',
            // array(
            //     'taxonomy' => $current_taxonomy->taxonomy,
            //     'field' => 'slug',
            //     'terms' => $current_taxonomy->slug
            // )
        ]
    );

    if( !empty( $form['taxonomies'] ) ){
        if( is_array( $form['taxonomies'] ) ){
            foreach ($form['taxonomies'] as $taxonomy => $slug) {
                $argsPost['tax_query'][] =  array(
                    'taxonomy' => $taxonomy,
                    'field' => 'slug',
                    'terms' => $slug
                );
            }
        }
    }
    
    ob_start();
    $posts = new WP_Query( $argsPost );

    if( $posts->have_posts() ){
        while( $posts->have_posts() ){
            $posts->the_post();
            global $post; ?>

        <?php if( $post_type == 'post' ){ ?>
        <div class="blogr-item col-4">
            <?php 
            $slug = '/partials/loop/box';
            $name = 'blog';
            echo get_template_part($slug, $name);
            ?>
        </div>
        <?php } ?>

        <?php 
        } wp_reset_query(); ?>

        <div class="pagination-products-ajax col-12 override">
            <?php mona_pagination_links_ajax( $posts , $paged ); ?>
        </div>
    <?php
    }
    echo wp_send_json_success( 
        [
            'title'             => __( 'Thông báo!', 'monamedia' ),
            'message'           =>  __( 'Load thêm thành công!', 'monamedia' ),
            'title_close'       =>  __('Đóng', 'monamedia'),
            'posts_html'     => ob_get_clean(),
            'paged'             =>  $paged
        ]
    );
    wp_die();

}

add_action( 'wp_ajax_mona_ajax_extension_services',  'mona_ajax_extension_services' ); // login
add_action( 'wp_ajax_nopriv_mona_ajax_extension_services',  'mona_ajax_extension_services' ); // no login
function mona_ajax_extension_services(){

    $title  =   $_POST['title'];
    $service_obj = get_post_by_title( $title , 'mona_service' );
    ob_start();
    if( !empty( $service_obj ) ){
        $mona_service_primary = get_field('mona_service_primary', $service_obj);
        $mona_service_extension_ids = get_field('mona_service_extension_ids', $service_obj);
        if( $mona_service_primary && !empty( $mona_service_extension_ids ) ){
            $extension_key = array_rand($mona_service_extension_ids, 1);
        ?>

    <div class="popup-service-body">
        <div class="title">
            <p class="ani-cus">
                <?php echo get_the_title( $mona_service_extension_ids[$extension_key] ); ?>
            </p>
        </div>
        <div class="desc">
            <p class="ani-cus">
                <?php echo get_the_excerpt( $mona_service_extension_ids[$extension_key] ); ?>
            </p>
        </div>
    </div>

    <div class="popup-service-btn d-flex f-ctn">
        <?php
        $mona_service_extenal_link = get_field('mona_service_extenal_link', $mona_service_extension_ids[$extension_key]);
        if( !empty( $mona_service_extenal_link ) ){
        ?>
        <div class="popup-service-btn-tem col col-6 ani-cus">
            <a href="<?php echo $mona_service_extenal_link['url']; ?>" class="btn-second" target="<?php echo $mona_service_extenal_link['url']; ?>">
                <span class="txt">
                    <?php echo $mona_service_extenal_link['title']; ?>
                </span>
                <span class="icon">
                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-arrow-right2.png" alt="">
                </span>
            </a>
        </div>
        <?php } ?>
        <div class="popup-service-btn-tem col col-6 ani-cus" style="display:none;">
            <a href="<?php echo get_the_permalink(MONA_PAGE_BLOG); ?>" class="btn-default">
                <span class="txt">
                    <?php echo __('Dịch vụ khác', 'monamedia'); ?>
                </span>
            </a>
        </div>
    </div>

    <?php
    } }
    echo wp_send_json_success( 
        [
            'title'             =>  __( 'Thông báo!', 'monamedia' ),
            'message'           =>  __( 'Load thêm thành công!', 'monamedia' ),
            'title_close'       =>  __('Đóng', 'monamedia'),
            'html'              =>  ob_get_clean(),
            'service_title'     => get_the_title( $mona_service_extension_ids[$extension_key] )
        ]
    );
    wp_die();
}

add_action( 'wp_ajax_mona_ajax_loadmore',  'mona_ajax_loadmore' ); // login
add_action( 'wp_ajax_nopriv_mona_ajax_loadmore',  'mona_ajax_loadmore' ); // no login
function mona_ajax_loadmore(){
    $form = array();
    parse_str($_POST['formdata'], $form);
   
    $post_type          =   $form['post_type'];
    $posts_per_page     =   $form['posts_per_page'];
    $paged              =   $form['paged'];
    $offset             =   ( $paged - 1 ) * $posts_per_page;

    $argsPost = array(
        'post_type' => $post_type,
        'post_status' => 'publish',
        'posts_per_page' => $posts_per_page,
        'paged' => $paged,
        'offset' => $offset,
        'order' => 'DESC',
        'meta_query' => [
            'relation' => 'AND',
            [
                'key'       => 'mona_reel_version',
                'value'     => '1',
                'compare'   => '!='
            ]
        ],
        'tax_query' => [
            'relation'=>'AND',
        ]
    );

    if( !empty( $form['taxonomies'] ) ){
        if( is_array( $form['taxonomies'] ) ){
            foreach ($form['taxonomies'] as $taxonomy => $slug) {
                $argsPost['tax_query'][] =  array(
                    'taxonomy' => $taxonomy,
                    'field' => 'slug',
                    'terms' => $slug
                );
            }
        }
    }
    
    ob_start();
    $posts = new WP_Query( $argsPost );

    if( $posts->have_posts() ){
        while( $posts->have_posts() ){
            $posts->the_post();
            global $post; ?>

            <?php $mona_reel_default_option = get_field( 'mona_reel_default_option' , $post); 
            switch ($mona_reel_default_option) {
                case 'thumbnail':
                    $mona_reel_thumbnail = get_field('mona_reel_thumbnail', $post); ?>
                    <div id="<?php echo get_the_ID(); ?>" class="nfeed-item nfeed-have-image single">
                        <div class="nfeed-wrap">
                            <div class="nfeed-top">
                                <div class="nfeed-admin">
                                    <p class="name">
                                        <?php echo get_the_author_meta('mona_user_display_name') ?>
                                    </p>
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-newfeed/nfeed-icon-tick.svg" alt="" class="icon">
                                </div>
                                <div class="nfeed-time">
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-newfeed/nfeed-icon-clock.svg" alt="" class="icon">
                                    <p class="time">
                                        <?php
                                        $date_string = sprintf( __('Vừa đăng %s trước', 'monamedia'), human_time_diff(
                                            mysql2date('U', get_the_date('Y-m-d H:i:s e', $post) ),  
                                            current_time('timestamp') 
                                        ) );

                                        echo $date_string;
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="nfeed-content">
                                <?php $mona_reel_default_title = get_field('mona_reel_default_title', $post );
                                if( $mona_reel_default_title ){ ?>
                                <h5 class="nfeed-title">
                                    <?php echo get_the_title( $post ); ?>
                                </h5>
                                <?php } ?>
                                <?php $content = apply_filters('the_content', get_post_field('post_content', $post->ID) );
                                if( !empty( $content ) ){ ?>
                                <div class="nfeed-des">
                                    <?php echo $content; ?>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="nfeed-image">
                                <div class="nfeed-image-list gallery">
                                    <div class="nfeed-image-item">
                                        <div class="nfeed-image-img gallery__img"
                                            data-src="<?php echo wp_get_attachment_image_url($mona_reel_thumbnail, 'full'); ?>">
                                            <?php echo wp_get_attachment_image($mona_reel_thumbnail, 'full'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    break;
                
                case 'default': ?>
                    <?php $mona_reel_default_w = get_field('mona_reel_default_w', $post);
                    $mona_reel_default_color = get_field('mona_reel_default_color', $post); ?>
                    <div id="<?php echo get_the_ID(); ?>" class="nfeed-item <?php echo $mona_reel_default_w ? 'nfeed-only-big w-2' : 'nfeed-only-small'; ?> <?php echo $mona_reel_default_color; ?>">
                        <div class="nfeed-wrap">
                            <div class="nfeed-top">
                                <div class="nfeed-admin">
                                    <p class="name">
                                        <?php echo get_the_author_meta('mona_user_display_name') ?>
                                    </p>
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-newfeed/nfeed-icon-tick.svg" alt="" class="icon">
                                </div>
                                <div class="nfeed-time">
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-newfeed/nfeed-icon-clock.svg" alt="" class="icon">
                                    <p class="time">
                                        <?php
                                        $date_string = sprintf( __('Vừa đăng %s trước', 'monamedia'), human_time_diff(
                                            mysql2date('U', get_the_date('Y-m-d H:i:s e', $post) ),  
                                            current_time('timestamp') 
                                        ) );

                                        echo $date_string;
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="nfeed-content">
                                <?php $mona_reel_default_title = get_field('mona_reel_default_title', $post );
                                if( $mona_reel_default_title ){ ?>
                                <h5 class="nfeed-title">
                                    <?php echo get_the_title( $post ); ?>
                                </h5>
                                <?php } ?>
                                <?php $content = apply_filters('the_content', get_post_field('post_content', $post->ID) );
                                if( !empty( $content ) ){ ?>
                                <div class="nfeed-des">
                                    <?php echo $content; ?>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                    <?php 
                    break;

                case 'gallery': ?>

                    <?php $mona_reel_gallery = get_field('mona_reel_gallery', $post);
                    if( !empty( $mona_reel_gallery ) ){ ?>
                    <div id="<?php echo get_the_ID(); ?>" class="nfeed-item nfeed-have-image <?php echo count( $mona_reel_gallery ) > 3 ? 'mutiple' : 'three'; ?> w-2">
                        <div class="nfeed-wrap">
                            <div class="nfeed-top">
                                <div class="nfeed-admin">
                                    <p class="name">
                                        <?php echo get_the_author_meta('mona_user_display_name') ?>
                                    </p>
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-newfeed/nfeed-icon-tick.svg" alt="" class="icon">
                                </div>
                                <div class="nfeed-time">
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-newfeed/nfeed-icon-clock.svg" alt="" class="icon">
                                    <p class="time">
                                        <?php
                                        $date_string = sprintf( __('Vừa đăng %s trước', 'monamedia'), human_time_diff(
                                            mysql2date('U', get_the_date('Y-m-d H:i:s e', $post) ),  
                                            current_time('timestamp') 
                                        ) );

                                        echo $date_string;
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="nfeed-content">
                                <?php $mona_reel_default_title = get_field('mona_reel_default_title', $post );
                                if( $mona_reel_default_title ){ ?>
                                <h5 class="nfeed-title">
                                    <?php echo get_the_title( $post ); ?>
                                </h5>
                                <?php } ?>
                                <?php $content = apply_filters('the_content', get_post_field('post_content', $post->ID) );
                                if( !empty( $content ) ){ ?>
                                <div class="nfeed-des">
                                    <?php echo $content; ?>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="nfeed-image ">
                                <div class="nfeed-image-list gallery">
                                    <?php foreach ($mona_reel_gallery as $key => $attachment_id) { ?>
                                    <div class="nfeed-image-item">
                                        <div class="nfeed-image-img gallery__img"
                                            data-src="<?php echo wp_get_attachment_image_url($attachment_id, 'full'); ?>">
                                            <?php echo wp_get_attachment_image($attachment_id, 'full'); ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php 
                    break;
                
                case 'video': ?>

                    <div id="<?php echo get_the_ID(); ?>" class="nfeed-item nfeed-have-video w-2">
                        <div class="nfeed-wrap">
                            <div class="nfeed-top">
                                <div class="nfeed-admin">
                                    <p class="name">
                                        <?php echo get_the_author_meta('mona_user_display_name') ?>
                                    </p>
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-newfeed/nfeed-icon-tick.svg" alt="" class="icon">
                                </div>
                                <div class="nfeed-time">
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-newfeed/nfeed-icon-clock.svg" alt="" class="icon">
                                    <p class="time">
                                        <?php
                                        $date_string = sprintf( __('Vừa đăng %s trước', 'monamedia'), human_time_diff(
                                            mysql2date('U', get_the_date('Y-m-d H:i:s e', $post) ),  
                                            current_time('timestamp') 
                                        ) );

                                        echo $date_string;
                                        ?>
                                    </p>
                                </div>
                            </div>
                            <div class="nfeed-content">
                                <?php $mona_reel_default_title = get_field('mona_reel_default_title', $post );
                                if( $mona_reel_default_title ){ ?>
                                <h5 class="nfeed-title">
                                    <?php echo get_the_title( $post ); ?>
                                </h5>
                                <?php } ?>
                                <?php $content = apply_filters('the_content', get_post_field('post_content', $post->ID) );
                                if( !empty( $content ) ){ ?>
                                <div class="nfeed-des">
                                    <?php echo $content; ?>
                                </div>
                                <?php } ?>
                            </div>
                            <div class="nfeed-image single" data-video="<?php echo wp_get_attachment_url( get_field('mona_reel_short', $post) ); ?>">
                                <div class="nfeed-image-list">
                                    <div class="nfeed-image-item">
                                        <div class="nfeed-image-img">
                                            <?php
                                            echo get_the_post_thumbnail($post, 'full');

                                            $mona_reel_short = get_field('mona_reel_short');
                                            $mona_reel_short_ytb = get_field('mona_reel_short_ytb');
                                            if ( empty( $mona_reel_short_ytb ) && !empty( $mona_reel_short ) ) {
                                                ?>
                                                <video playsinline width="320" height="240" controls="">
                                                    <source src="<?php echo $mona_reel_short; ?>" type="video/mp4">
                                                </video>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <?php
                                        if ( !empty( $mona_reel_short_ytb ) ) {
                                            ?>
                                            <a href="<?php echo $mona_reel_short_ytb ?>" class="nfeed-play popup-youtube">
                                                <span class="nfeed-play-icon">
                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-newfeed/nfeed-icon-play.svg" alt="">
                                                </span>
                                                <span class="nfeed-play-text">
                                                    Click The Button
                                                </span>
                                            </a>
                                            <?php
                                        } elseif ( !empty( $mona_reel_short ) ) {
                                            ?>
                                            <a href="javascript:;" class="nfeed-play video">
                                                <span class="nfeed-play-icon">
                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-newfeed/nfeed-icon-play.svg" alt="">
                                                </span>
                                                <span class="nfeed-play-text">
                                                    Click The Button
                                                </span>
                                            </a>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                <?php 
                break;
            }
            ?>

        <?php 
        } wp_reset_query(); ?>
    <?php
    }
    echo wp_send_json_success( 
        [
            'title'             => __( 'Thông báo!', 'monamedia' ),
            'message'           =>  __( 'Load thêm thành công!', 'monamedia' ),
            'title_close'       =>  __('Đóng', 'monamedia'),
            'posts_html'        => ob_get_clean(),
            'active'            =>  $posts->found_posts > $paged*$posts_per_page ? 'active' : 'non-active',
            'paged'             =>  ++$paged,
        ]
    );
    wp_die();

}

add_action( 'wp_ajax_mona_ajax_load_reel',  'mona_ajax_load_reel' ); // login
add_action( 'wp_ajax_nopriv_mona_ajax_load_reel',  'mona_ajax_load_reel' ); // no login

function mona_ajax_load_reel() {
    $paged       = intval(esc_attr($_POST['paged']));
    $post_per_page = intval(esc_attr($_POST['postperpage']));
    $offset = ( $paged - 1 ) * $post_per_page;

    $post_type = 'mona_reels';
    $argsPost = [
        'post_type'         => $post_type,
        'post_status'       => 'publish',
        'posts_per_page'    => $post_per_page,
        'paged'             => $paged,
        'offset'            => $offset,
        'order'             => 'DESC',
        'meta_query'        => [
            'relation' => 'AND',
            [
                'key'       => 'mona_reel_version',
                'value'     => '1',
                'compare'   => '=='
            ],
            [
                'relation' => 'OR',
                [
                    'key'       => 'mona_reel_short_ytb',
                    'value'     => '',
                    'compare'   => '!='
                ],
                [
                    'key'       => 'mona_reel_short',
                    'value'     => '',
                    'compare'   => '!='
                ],
            ]
        ],
        'tax_query' => [
            'relation'=>'AND',
        ]
    ];

    $reel_arr = [];

    $postsMONA = new WP_Query( $argsPost );  
        if( $postsMONA->have_posts() ){
            while( $postsMONA->have_posts() ){
                $postsMONA->the_post();
                global $post;
                $mona_reel_short = get_field('mona_reel_short', $post);
                $mona_reel_short_ytb = get_field('mona_reel_short_ytb', $post);
                $mona_reel_short_view = get_field('mona_reel_short_view', $post);
                $user_link = get_author_posts_url(get_the_author_meta('ID'));
                $user_avatar = get_user_meta(get_the_author_meta('ID'),'mona_user_avatar', true);
                $user_avatar = wp_get_attachment_url( $user_avatar );
                $user_name = get_the_author_meta('mona_user_display_name');
                $title = get_the_title( $post );
                $content = get_the_content( $post );


                $reel_item = [
                    'id' => $post->ID,
                    'image' => get_the_post_thumbnail_url( $post, 'large'),
                    'video' => $mona_reel_short ? $mona_reel_short : '',
                    'youtube' => $mona_reel_short_ytb ? $mona_reel_short_ytb : '',
                    'view' => $mona_reel_short_view ? $mona_reel_short_view : '',
                    'user_link' => $user_link ? $user_link : '',
                    'user_avatar' => $user_avatar ? $user_avatar : '',
                    'user_name' => $user_name ? $user_name : '',
                    'title' => $title ? $title : '',
                    'content' => $content ? $content : '',

                ];
                $reel_arr[] = $reel_item;
            } wp_reset_query();
        }

    if( $post_per_page*$paged < $postsMONA->found_posts ){

        $active_loadmore = 1;

    }else{

        $active_loadmore = 0;

    }

    wp_send_json_success( 
        [
            'message'   => __( 'Loading more products successfully', 'monamedia' ),
            'active_loadmore' => $active_loadmore,
            'reel_arr' => json_encode($reel_arr),
        ]
    );
    wp_die();
    
}
