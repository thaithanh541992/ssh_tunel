<?php

add_action( 'wp_ajax_mona_ajax_check_domain',  'mona_ajax_check_domain' ); // login
add_action( 'wp_ajax_nopriv_mona_ajax_check_domain',  'mona_ajax_check_domain' ); // no login
function mona_ajax_check_domain() {
    
    $form = array();
    parse_str($_POST['form'], $form);
    $domain_value = esc_attr( $form['domain_value'] );
    $is_suggest = esc_attr( $form['is_suggest'] );
    
    $protocol = ['http://', 'https://'];

    $domain_value = str_replace($protocol, '', $domain_value);

    // clean and split to get array of parts in domain 
    $domain_clean = preg_replace('/[^a-zA-Z0-9.-]/', '', $domain_value);
    $domain_arr = explode(".", $domain_clean);
    $domain_arr_len = count( $domain_arr );    

    $suffix = '';
    // get suffix
    if ( $domain_arr_len > 2 ) {
        for( $i = 1; $i < $domain_arr_len; $i++ ) {
            $suffix .= '.' . $domain_arr[$i];
        }
    } elseif (  $domain_arr_len > 1 ) {
        $suffix = '.' . $domain_arr[1];
    } else {
        $suffix = '.vn';
        $domain_value .= '.vn';
    }

    // get domain name 
    $domain_name = $domain_arr[0];
    
    // get supported suffix
    $supported_suffix_field = get_field('mona_suffix_domain_list', MONA_PAGE_HOME);
    $supported_suffixs = array_column($supported_suffix_field, 'post_title');

    if ( !$is_suggest ) {
    
        $suggest_domains = [];
        // get suggest domains
        if ( !empty( $supported_suffixs ) && is_array( $supported_suffixs ) ) {
            foreach( $supported_suffixs as $item ) {
                if ( $item != $suffix ) {
                    $suggest_domains[] = 
                    [
                        'domain_name' => $domain_name,
                        'suffix' => $item,
                    ];
                }
            }
        }
    
        // check supported suffix
        if ( !in_array( $suffix, $supported_suffixs ) ) {
            echo wp_send_json_error(
                [
                    'title'=> __('Thông báo', 'monamedia'),
                    'message' => __('Đuôi tên miền bạn tìm kiếm không được hỗ trợ', 'monamedia'),
                    'domain_name' => $domain_name,
                    'suffix' => $suffix,
                    'suggest_domains' => $suggest_domains,
                ]
            );
            wp_die();
        }
    }

    $check_rs = inet_check_domain( $domain_value );

    if ( empty( $check_rs ) ) {
        echo wp_send_json_error(
            [
                'title'=> __('Thông báo', 'monamedia'),
                'message' => __('Có lỗi không xác định', 'monamedia'),
            ]
        );
        wp_die();
    }

    
    $check_rs = json_decode( $check_rs );

    $post_key = array_search( $suffix, $supported_suffixs );
    $chosen_id = $supported_suffix_field[$post_key]->ID;
    
    if ( $check_rs->status != 'available' ) {
        echo wp_send_json_error(
            [
                'title'=> __('Thông báo', 'monamedia'),
                'message' => __('Oops, tên miền này đã có người mua', 'monamedia'),
                'domain_name' => $domain_name,
                'suffix' => $suffix,
                'suggest_domains' => !$is_suggest ? $suggest_domains : "",
                'categories' => $is_suggest ? json_encode(wp_get_post_terms( $chosen_id, 'domain_cat')) : '',
            ]
        );
        wp_die();
    }
    

    echo wp_send_json_success(
        [
            'title'=> __('Thông báo', 'monamedia'),
            'message' => __('Chúc mừng, bạn có thể sử dụng tên miền này', 'monamedia'),
            'pid' => $chosen_id,
            'domain_name' => $domain_name,
            'suffix' => $suffix,
            'currency' => get_woocommerce_currency_symbol(),
            'price_data' => get_domain_price_data( $chosen_id ),
            'suggest_domains' => !$is_suggest ? $suggest_domains : '',
            'categories' => $is_suggest ? json_encode(wp_get_post_terms( $chosen_id, 'domain_cat')) : '',
        ]
    );
    wp_die();
}

add_action( 'wp_ajax_mona_ajax_whois_check',  'mona_ajax_whois_check' ); // login
add_action( 'wp_ajax_nopriv_mona_ajax_whois_check',  'mona_ajax_whois_check' ); // no login
function mona_ajax_whois_check() {
    
    $domain_value = esc_attr( $_POST['domain_value'] );

    $protocol = ['http://', 'https://'];

    $domain_value = str_replace($protocol, '', $domain_value);

    // clean and split to get array of parts in domain 
    $domain_value = preg_replace('/[^a-zA-Z0-9.-]/', '', $domain_value);
    $check_rs = inet_whois_check( $domain_value );

    if ( empty( $check_rs ) ) {
        echo wp_send_json_error(
            [
                'title'=> __('Thông báo', 'monamedia'),
                'message' => __('Có lỗi không xác định', 'monamedia'),
            ]
        );
        wp_die();
    }

    
    $check_rs = json_decode( $check_rs );
    
    if ( $check_rs->code != '0' ) {
        echo wp_send_json_error(
            [
                'title'=> __('Thông báo', 'monamedia'),
                'message' => __('Vui lòng liên hệ để xem thông tin tên miền này', 'monamedia'),
            ]
        );
        wp_die();
    }
    

    echo wp_send_json_success(
        [
            'title'=> __('Thông báo', 'monamedia'),
            'message' => __('Kiểm tra thông tin thành công', 'monamedia'),
            'whois_data' => $check_rs
        ]
    );
    wp_die();
}

