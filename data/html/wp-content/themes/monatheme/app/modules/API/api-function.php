<?php
define( 'MONA_HOST_IDENTIFIER', '7eQIVQwgjd1N48EBZuNfSs9fdOhcGwOw' );
define( 'MONA_HOST_SECRET', '3RbUfHBIndICE13C03LzAruLlIq6mCaF' );
define( 'MY_MONA_HOST_BASE', 'https://my.mona.host/includes/api.php');

define( 'INET_BASE', 'dms.inet.vn' );
define( 'INET_SELLER_TOKEN', '60D5C816BA7E110FBCC55C6A513C5BBD2E0E4FD4' );

define( 'DEFAULT_CITY', 'Ho Chi Minh');
define( 'DEFAULT_STATE', 'HCM' );
define( 'DEFAULT_COUNTRY', 'VN' );
define( 'DEFAULT_POSTCODE', '7000' );

// normal function below
function monahost_error_translate( $error_en ) {
    $error_arr = [
        'Email or Password Invalid' => __('Email hoặc mật khẩu chưa chính xác.', 'monamedia'),
        'You did not enter your first name' => __('Vui lòng điền tên của bạn', 'monamedia'),
        'You did not enter your last name' => __('Vui lòng nhập họ của bạn', 'monamedia'),
        'You did not enter your email address' => __('Vui lòng nhập địa chỉ email', 'monamedia'),
        'The email address you entered was not valid' => __('Email không hợp lệ', 'monamedia'),
        'You did not enter your address line 1' => __('Vui lòng nhập địa chỉ', 'monamedia'),
        'You did not enter your phone number' => __('Vui lòng nhập số điện thoại', 'monamedia'),
        'Invalid telephone phone number' => __('Số điện thoại không hợp lệ', 'monamedia'),
        'You did not provide required custom field value for' => __('Vui lòng nhập đầy đủ thông tin'),
        'The email address entered is not available for use' => __('Email không hợp lệ để sử dụng', 'monamedia'),
        'A user already exists with that email address' => __('Đã có user sử dụng email này', 'monamedia')
    ];

    if ( !empty( $error_arr[$error_en] ) ) {
        return $error_arr[$error_en];
    } else {
        if ( !empty( $error_en ) ) {
            return $error_en;
        } else {
            return __('Có lỗi xảy ra vui lòng thử lại sau.', 'monamedia');
        }
    }
}

function monahost_get_cart_html() {

    ob_start();
    ?>
    <div class="host-service-aside-tt host-tt">
        <span class="txt">
            <?php echo __('Giỏ hàng của bạn', 'monamedia'); ?> 
        </span>
        <span class="close-modal close-modal-js">
            <i class="fas fa-times"></i>
        </span>   
    </div>

    <?php
    $cart = WC()->cart; // The WC_Cart Object
    if ( ! $cart->is_empty() ) {
        ?><div class="host-service-cart"><?php
        foreach ( $cart->get_cart() as $cart_item_key => $cart_item ) {
            $custom_data = $cart_item['custom_data'];
            $product = $cart_item['data'];
            $product_name = $product->get_name();
            $ptype = get_field('mona_product_type', $product->get_id());

            if ( $ptype == 'domain' && !empty( $cart_item['custom_data']['domain_name'] ) ) {
                $product_name = $cart_item['custom_data']['domain_name'];
            }

            ?>
            <div class="host-service-cart-item">
                <div class="host-service-cart-left">
                    <div class="name"><?php echo $product_name; ?></div>
                    <?php
                    $mona_billing_cycle_list = get_field('mona_billing_cycle_list', $product->get_id());
                    if( !empty( $mona_billing_cycle_list ) && is_array( $mona_billing_cycle_list ) ) {
                        ?>
                        <div class="desc">
                            <div class="label"><?php echo __('Thời gian sử dụng', 'monamedia') ?></div>
                            <select class="select2choose billingcycle-cart-js" 
                                data-pid=<?php echo $product->get_id() ?>
                                data-cart_item_key="<?php echo $cart_item_key; ?>"
                            >
                                <?php
                                foreach ( $mona_billing_cycle_list as $billing_cycle ) {
                                    ?>
                                    <option value="<?php echo $billing_cycle['term_quantity'] ?>"
                                        data-price=<?php echo $billing_cycle['price'] ?>
                                        <?php selected($custom_data['billingcycle'], $billing_cycle['term_quantity']) ?>
                                    >
                                        <?php echo $billing_cycle['label'] ?>
                                    </option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                        <?php
                    }
                    ?>
                </div>
                <div class="host-service-cart-right">
                    <div class="price">
                        <?php echo wc_price($cart_item['line_subtotal']) ?>
                    </div>
                    <div class="remove delete-cart-item-js" data-cart_item_key=<?php echo $cart_item_key; ?>>
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/trash-can.svg" alt="">
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
        </div>
        
        <!-- <div class="host-service-aside-item">
            <div class="host-service-aside-item-header">
                <div class="label-small">
                    Dịch vụ đi kèm
                </div>
            </div>
        
            <div class="host-service-aside-item-body">
                <div class="checkbox-cus d-flex">
                    <label class="checkbox-cus-item d-flex f-start">
                        <input type="checkbox" name="" id="" class="checkbox-cus-hidden">
                        <div class="checkbox-cus-check"></div>
                        <div class="checkbox-cus-name">Email</div>
                    </label>
                    <label class="checkbox-cus-item d-flex f-start">
                        <input type="checkbox" name="" id="" class="checkbox-cus-hidden">
                        <div class="checkbox-cus-check"></div>
                        <div class="checkbox-cus-name">SSL</div>
                    </label>
                    <label class="checkbox-cus-item d-flex f-start">
                        <input type="checkbox" name="" id="" class="checkbox-cus-hidden">
                        <div class="checkbox-cus-check"></div>
                        <div class="checkbox-cus-name">Bảo mật DNS</div>
                    </label>
                    <label class="checkbox-cus-item d-flex f-start">
                        <input type="checkbox" name="" id="" class="checkbox-cus-hidden">
                        <div class="checkbox-cus-check"></div>
                        <div class="checkbox-cus-name">SSL</div>
                    </label>
                    <label class="checkbox-cus-item d-flex f-start">
                        <input type="checkbox" name="" id="" class="checkbox-cus-hidden">
                        <div class="checkbox-cus-check"></div>
                        <div class="checkbox-cus-name">Bảo mật DNS</div>
                    </label>
                    <label class="checkbox-cus-item d-flex f-start">
                        <input type="checkbox" name="" id="" class="checkbox-cus-hidden">
                        <div class="checkbox-cus-check"></div>
                        <div class="checkbox-cus-name">SSL</div>
                    </label>
                </div>
            </div>
        </div> -->
        
        <div class="host-service-aside-item">
            <div class="host-service-aside-item-header">
                <div class="label-small">
                    Tổng tiền dịch vụ
                </div>
                <div class="price price-big"><?php wc_cart_totals_subtotal_html(); ?></div>
            </div>
            <div class="host-service-aside-item-body">
                <?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
                    <div class="item cart-discount cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
                        <p><?php wc_cart_totals_coupon_label( $coupon ); ?></p>
                        <p><?php wc_cart_totals_coupon_html( $coupon ); ?></p>
                    </div>
                <?php endforeach; ?>
                
                <?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
                    <div class="item">
                        <p><?php echo esc_html( $fee->name ); ?></p>
                        <p><?php wc_cart_totals_fee_html( $fee ); ?></p>
                    </div>
                <?php endforeach; ?>
        
                <?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) : ?>
                    <?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
                        <?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited ?>
                            <div class="item tax-rate tax-rate-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
                                <p><?php echo esc_html( $tax->label ); ?></p>
                                <p><?php echo wp_kses_post( $tax->formatted_amount ); ?></p>
                            </div>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <div class="item tax-total tax-rate tax-rate-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
                            <p><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></p>
                            <p><?php wc_cart_totals_taxes_total_html(); ?></p>
                        </div>
                    <?php endif; ?>
                <?php endif; ?>
            </div>
        </div>
        
        <div class="host-service-aside-item">
        
            <div class="host-service-aside-item-header">
                <div class="label-small">
                    Tổng tiền thanh toán
                </div>
                <div class="price price-biggest"><?php echo wc_price($cart->total) ?></div>
            </div>
        </div>
        <a href="<?php echo get_the_permalink( MONA_WC_CHECKOUT ) ?>" class="host-service-aside-btn mona-dblock">Thanh toán</a>
        <?php
    } else {
        ?>
        <div class="empty-mess cart-mess">
            <img class="empty-img" src="<?php echo get_template_directory_uri() . '/public/helpers/images/icon_empty_cart.png' ?>" alt="">
            <p class="empty-content"><?php echo __('Giỏ hàng trống!', 'monamedia') ?></p>
        </div>
        <?php
    }
    ?>
    <script src="<?php echo MONA_SITE_URL ?>/template/js/libs/select2/select2.min.js"></script> 
    <script src="<?php echo get_template_directory_uri() . '/public/helpers/scripts/modules/cart.js' ?>"></script> 
    <?php

    return ob_get_clean();
}

function monaghost_get_custom_field_key () {
    return [
        'organization_first_name' => __('Tên tổ chức', 'monamedia'),
        'organization_id_number' => __('Mã số thuế', 'monamedia'),
        'organization_phone' => __('Số điện thoại', 'monamedia'),
        'organization_email' => __('Email', 'monamedia'),
        'organization_country' => __('Mã quốc gia / khu vực', 'monamedia'),
        'organization_country_label' => __('Quốc gia / khu vực', 'monamedia'),
        'billing_country_label' => __('Quốc gia / khu vực', 'monamedia'),
        'organization_state' => __('Mã tỉnh / thành phố', 'monamedia'),
        'organization_state_label' => __('Tỉnh / thành phố', 'monamedia'),
        'billing_state_label' => __('Tỉnh / thành phố', 'monamedia'),
        'organization_city' => __('Quận / huyện', 'monamedia'),
        'organization_ward' => __('Phường / xã', 'monamedia'),
        'organization_address_1' => __('Địa chỉ', 'monamedia'),
    ];
}
 
// Call API function below
function monahost_get_products( $pid = '' ) {
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => MY_MONA_HOST_BASE,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array(
            'action' => 'GetProductsActive',
            'identifier' => MONA_HOST_IDENTIFIER,
            'secret' => MONA_HOST_SECRET,
            'responsetype' => 'json',
            'hidden' => '0',
            'pid' => $pid,
            'gid' => ''
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

function monahost_validate_login( $email, $password ) {
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => MY_MONA_HOST_BASE,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array(
            'action' => 'ValidateLogin',
            'identifier' => MONA_HOST_IDENTIFIER,
            'secret' => MONA_HOST_SECRET,
            'responsetype' => 'json',
            'email' => $email,
            'password2' => $password
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

function monahost_create_sso_token( $user_id ) {
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => MY_MONA_HOST_BASE,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array(
            'action' => 'CreateSsoToken',
            'identifier' => MONA_HOST_IDENTIFIER,
            'secret' => MONA_HOST_SECRET,
            'responsetype' => 'json',
            'user_id' => $user_id,
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

function monahost_create_client( $firstname, $lastname, $password2, $email, $address1, $phonenumber ) {
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => MY_MONA_HOST_BASE,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array(
            'action' => 'AddClient',
            'identifier' => MONA_HOST_IDENTIFIER,
            'secret' => MONA_HOST_SECRET,
            'responsetype' => 'json',
            'firstname' => $firstname,
            'lastname' => $lastname,
            'password2' => $password2,
            'email' => $email,
            'address1' => $address1,
            'phonenumber' => $phonenumber,
            'city' => DEFAULT_CITY,
            'state' => DEFAULT_STATE,
            'postcode' => DEFAULT_POSTCODE,
            'country' => DEFAULT_COUNTRY
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

function monahost_reset_password( $email ) {
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => MY_MONA_HOST_BASE,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => array(
            'action' => 'ResetPassword',
            'identifier' => MONA_HOST_IDENTIFIER,
            'secret' => MONA_HOST_SECRET,
            'responsetype' => 'json',
            'email' => $email,
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

// inet api start here
function inet_check_domain( $domain ) {

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => INET_BASE . '/api/rms/v1/domain/checkavailable',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode([
            'name' => $domain,
        ]),
        CURLOPT_HTTPHEADER => array(
            'token: ' . INET_SELLER_TOKEN,
            'Content-Type: application/json',
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

function inet_validate_idn( $idnName ) {    

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => INET_BASE . '/api/rms/v1/domain/validateidnname',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode([
            'idnName' => $idnName,
        ]),
        CURLOPT_HTTPHEADER => array(
            'token: ' . INET_SELLER_TOKEN,
            'Content-Type: application/json',
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

function inet_get_list_country() {    

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => INET_BASE . '/api/rms/v1/category/countrylist',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode([]),
        CURLOPT_HTTPHEADER => array(
            'token: ' . INET_SELLER_TOKEN,
            'Content-Type: application/json',
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

function inet_get_list_province() {    

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => INET_BASE . '/api/rms/v1/category/provincelist',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode([]),
        CURLOPT_HTTPHEADER => array(
            'token: ' . INET_SELLER_TOKEN,
            'Content-Type: application/json',
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

function inet_get_list_district( $matinh ) {    

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => INET_BASE . '/api/rms/v1/dvhc/getquanhuyen',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode([
            "matinh" => $matinh,
            "tinh" => $matinh
        ]),
        CURLOPT_HTTPHEADER => array(
            'token: ' . INET_SELLER_TOKEN,
            'Content-Type: application/json',
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

function inet_get_list_ward( $matinh, $quanhuyen ) {    

    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => INET_BASE . '/api/rms/v1/dvhc/getphuongxa',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'POST',
        CURLOPT_POSTFIELDS => json_encode([
            "matinh" => $matinh,
            "quanhuyen" => $quanhuyen,
            "tinh" => $matinh
        ]),
        CURLOPT_HTTPHEADER => array(
            'token: ' . INET_SELLER_TOKEN,
            'Content-Type: application/json',
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

function inet_whois_check( $domain ) {
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => 'https://whois.inet.vn/api/whois/domainspecify/' . $domain,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
        CURLOPT_HTTPHEADER => array(
            'Content-Type: application/json'
        ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

function inet_create_domain( $data ) {

    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => INET_BASE . '/api/rms/v1/domain/create',
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'POST',
    CURLOPT_POSTFIELDS =>'{
        "name": "test.vn",
        "period": 1,
        "customerId": 0,
        "nsList": [
            {
                "hostname": "ns1.inet.vn"
            },
            {
                "hostname": "ns2.inet.vn"
            }
        ],
        "contacts": [
            {
                "fullname": "Công ty A",
                "organization": true,
                "email": "company@example.vn",
                "country": "VN",
                "province": "HNI",
                "address": "247 Cầu Giấy",
                "ward": "Phường Dịch Vọng",
                "district": "Quận Cầu Giấy",
                "phone": "0438385588",
                "fax": "0438385588",
                "type": "registrant",
                "taxCode": "0123456789"
            },
            {
                "fullname": "Nguyễn Văn A",
                "organization": false,
                "email": "a@example.vn",
                "country": "VN",
                "province": "HNI",
                "address": "247 cau giay",
                "ward": "Phường Dịch Vọng",
                "district": "Quận Cầu Giấy",
                "phone": "0974019049",
                "fax": "0974019049",
                "type": "admin",
                "gender": "male",
                "idNumber": "030810700",
                "birthday": "01/01/1971"
            },
            {
                "fullname": "Nguyễn Văn A",
                "organization": false,
                "email": "a@example.vn",
                "country": "VN",
                "province": "HNI",
                "address": "247 cau giay",
                "ward": "Phường Dịch Vọng",
                "district": "Quận Cầu Giấy",
                "phone": "0974019049",
                "fax": "0974019049",
                "type": "technique",
                "gender": "male",
                "idNumber": "030810700",
                "birthday": "01/01/1971"
            },
            {
                "fullname": "Nguyễn Văn A",
                "organization": false,
                "email": "a@example.vn",
                "country": "VN",
                "province": "HNI",
                "address": "247 cau giay",
                "ward": "Phường Dịch Vọng",
                "district": "Quận Cầu Giấy",
                "phone": "0974019049",
                "fax": "0974019049",
                "type": "billing",
                "gender": "male",
                "idNumber": "030810700",
                "birthday": "01/01/1971"
            }
        ]
    }',
    CURLOPT_HTTPHEADER => array(
        'token: 60D5C816BA7E110FBCC55C6A513C5BBD2E0E4FD4',
        'Content-Type: application/json',
        'Cookie: sessionId=s%3AyJxUGdLUGWrv_ZLRTSVhTf9sGg4AdZiF.PzO2aIIPEbFhOG4fBIga0bfPeO25mRt%2BbkFHIWeBSdw'
    ),
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    echo $response;

}

// normal function below
function inet_error_translate( $msg ) {
    $error_arr = [
        "error,Don't support suffix not vn" => __('Đuôi tên miền bạn chọn không được hỗ trợ', 'monamedia'),
    ];

    if ( !empty( $error_arr[$msg] ) ) {
        return $error_arr[$msg];
    } else {
        if ( !empty( $msg ) ) {
            return $msg;
        } else {
            return __('Tên miền bạn chọn không thể đăng ký.', 'monamedia');
        }
    }
}

function get_available_suffix() {
    $data = get_field('mona_suffix_domain_list', MONA_PAGE_HOME);
    return !empty($data) && is_array( $data ) ? array_column($data, "domain_item_suffix") : 0;
}

function get_domain_price_data( $pid ) {
    $mona_billing_cycle_list = get_field('mona_billing_cycle_list', $pid);
    $mona_domain_detail = get_field('mona_domain_detail', $pid);

    if ( !empty( $mona_billing_cycle_list ) && is_array( $mona_billing_cycle_list ) ) {
        foreach( $mona_billing_cycle_list as $billing_item ) {
            if ( !empty( $billing_item ) && $billing_item['price'] ) {
                return [
                    'price' => $billing_item['price'],
                    'original_price' => $billing_item['original_price'],
                    'price_suffix' => $mona_domain_detail ? $mona_domain_detail['price_suffix'] : ''
                ];
            }
        }
    }

    return [
        'price' => 0,
        'original_price' => 0,
        'price_suffix' => $mona_domain_detail ? $mona_domain_detail['price_suffix'] : ''
    ];
}