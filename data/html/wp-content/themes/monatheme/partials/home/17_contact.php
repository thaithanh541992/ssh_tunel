<?php

/**
 * Section name: Home Contact
 * Description: 
 * Author: Monamedia
 * Order: 17
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> home-sesion-not-contact">
    <div class="container">
        <div class="home-sesion-not-contact-wrap">
            <div class="content">
                <div class="header">
                    <div class="host-tt header-tt fw-600 txt-white">
                        <h2>
                            Đội ngũ trực tuyến của Mona
                            <span class="header-sub txt-bg bg-lms">
                                Luôn hỗ trợ bất kể ngày đêm
                            </span>
                        </h2>
                    </div>
                </div>
                <div class="main-member">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/home-session-contact-member-1.png" alt="" />
                </div>
                <a href="#" class="btn-clh accion-main fw-600">
                    Bạn không chắc nên bắt đầu từ đâu?<br />
                    Liên hệ với MONA vì chúng tôi luôn sẵn sàng<br />
                    hỗ trợ bạn.
                </a>
                <div class="decor decor-left-1">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/home-sesion-not-contact-decor-left-1.png" alt="" />
                </div>
                <div class="decor decor-left-2">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/home-sesion-not-contact-decor-left-2.jpg" alt="" />
                </div>
                <div class="decor decor-left-3">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/home-sesion-not-contact-decor-left-3.png" alt="" />
                </div>
                <div class="decor decor-right-1">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/home-sesion-not-contact-decor-right-1.png" alt="" />
                </div>
                <div class="decor decor-right-2">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/home-sesion-not-contact-decor-right-2.png" alt="" />
                </div>
                <div class="decor decor-right-3">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/home-sesion-not-contact-decor-right-3.png" alt="" />
                </div>
                <div class="decor decor-bottom-left">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/home-sesion-not-contact-decor-bottom-left.png" alt="" />
                </div>
                <div class="decor decor-bottom-right">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/home-sesion-not-contact-decor-bottom-right.png" alt="" />
                </div>
            </div>
        </div>
    </div>
    <div class="content-521">
        <div class="header">
            <div class="container">
                <div class="tt">
                    <h2 class="fw-600">
                        <span class="block-center txt-bg bg-host">
                            Bỏ ngay hosting giá rẻ
                        </span>
                        nếu không muốn tiền mất tật mang
                    </h2>
                </div>
            </div>
        </div>
        <div class="line-decor-jun">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/line-decor-jun.png" alt="" />
        </div>
        <div class="line-decor">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/home-session-521-line-decor.png" alt="" />
        </div>
        <div class="main-content">
            <div class="container content">
                <div class="content">
                    <div class="content-left">
                        <div class="txt-quote-wrap">
                            <div class="txt-quote-img">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/quoter-521.png" alt="" />
                            </div>
                            <div class="txt fw-400">
                                <p class="">
                                    Đang chạy sales thì sập, đang <br />add hàng thì sập,
                                    đang xử lý<br />
                                    đơn hàng thì sập...
                                </p>
                            </div>
                        </div>
                        <div class="harmfull-programs">
                            <div>
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/harmfull-programs.png" alt="" />
                            </div>
                            <div class="txt-hacker">
                                <span> Hacker tấn công </span>
                            </div>
                        </div>
                    </div>
                    <div class="content-right">
                        <div class="error-521">
                            <div>
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/error-521.png" alt="" />
                            </div>
                            <div class="txt-web-ramdom">
                                <span>Web “random sập</span>
                            </div>
                        </div>
                    </div>
                    <div class="main-member">
                        <div class="main-member-img">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/home-session-521-main-member-1.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content-session-panda">
        <div class="panda">
            <div class="panda-wrap">
                <div class="line-decor-panda">
                    <div class="line-decor-img">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/line-decor-panda.png" alt="" />
                    </div>
                    <div class="txt-decor">
                        Muốn biết tại sao không?<br />Cuộn xuống đi sẽ biết
                    </div>
                </div>
                <div class="panda-img">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/panda_ta-da.png" alt="" />
                </div>
            </div>
        </div>
    </div>

    <div class="content-bottom">
        <div class="container">
            <div class="content">
                <div class="content-left">
                    <div class="main-img">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/home-session-not-contact-content-bottom.png" alt="" />
                    </div>
                    <div class="decor decor-customer">
                        <div class="txt-bg bg-digital">
                            <p class="statistic-name fw-400">Khách hàng mới</p>
                            <p class="statistic-number fw-600">
                                -20%
                                <span class="statistic-icon">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistic-down.png" alt="" class="statistic-cus-up" />
                                </span>
                            </p>
                        </div>
                    </div>
                    <div class="decor decor-server-uptime">
                        <div class="txt-bg bg-nhtq">
                            <p class="statistic-name fw-400">Server uptime</p>
                            <p class="statistic-number fw-600">
                                50%
                                <span class="statistic-icon">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistic-down.png" alt="" class="statistic-cus-up" />
                                </span>
                            </p>
                        </div>
                    </div>
                    <div class="decor decor-quote">
                        <div class="txt-quote-wrap">
                            <div class="txt-quote-img">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/quote-quoter-521-2.png" alt="" />
                            </div>
                            <div class="txt fw-400">
                                <p class="">
                                    Tức quá. không fix<br />
                                    được lỗi rồi.
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="content-right">
                    <div class="host-tt header">
                        <p class="fw-600">
                            Ham rẻ một chút
                            <span class="txt-bg bg-lms">Lợi bất cập hại sẽ đến với bạn</span>
                        </p>
                    </div>
                    <div class="about">
                        <p>
                            Không chỉ là sập web vài phút, vài ngày,
                            <span class="txt-digital fw-600">
                                khách hàng sẽ rời bỏ,
                            </span>
                            thậm chí không quay lại vì web tệ, mỗi việc truy cập thôi
                            cũng không được,
                            <span class="txt-lms fw-600">
                                <u>chết lên chết xuống,...</u>
                            </span>
                        </p>
                        <br />
                        <p>
                            <span class="txt-nhtq fw-600">
                                Lỗi gì cũng khiến web bạn bị đánh giá là không an toàn </span>: Khách hàng bỏ chạy vì sợ virus,
                            hacker, mất thông tin,
                            web đen
                        </p>
                        <br />

                        <p>
                            <span class="txt-software fw-600">
                                <u>Website load chậm, giật lag</u>
                            </span>
                            khiến việc SEO lên top gần như không thể, muốn khách hàng
                            tìm thấy bạn khó như mò kim đáy bể
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>