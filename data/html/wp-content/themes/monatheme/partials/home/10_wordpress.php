<?php

/**
 * Section name: Home Wordpress
 * Description:  
 * Author: Monamedia
 * Order: 10
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> session-wordpress">
    <div class="wordpress-header">
        <div class="file-hacker">
            <div class="img">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-file-hacker.png" alt="" />
            </div>
        </div>
        <div class="main-title">
            <h2 class="host-tt txt-tt">
                Website Wordpress bị hack

                <p class="txt-sub txt-bg">
                    <span class="bg-sub">
                        Bạn sẽ đối mặt với hậu quả nghiêm trọng
                    </span>
                </p>
            </h2>
            <div class="txt-about">
                Có thể bạn đã làm tất cả những gì cần thiết để bảo vệ website của
                mình và tự tin rằng “Không ai có thể tấn công website của tôi
                hết!”
            </div>
        </div>
        <div class="hacker">
            <div class="img">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-hacker.png" alt="" />
            </div>
        </div>
    </div>

    <div class="wordpress-content">
        <div class="container">
            <div class="session-line">
                <div class="txt fw-600">Nhưng có thể</div>
                <div class="txt fw-600 txt-host">
                    Bạn chưa hề hay biết là...
                </div>
                <div class="line-sesion">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-line-1.png" alt="" />
                </div>
            </div>
            <div class="session-line">
                <div class="d-flex f-between">
                    <div class="img-decor col-6">
                        <div class="img-decor-wrap">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-group-session-1.png" alt="" />
                        </div>
                    </div>
                    <div class="text-decor col-6">
                        <div class="content line-digital">
                            <h3 class="text">
                                Có hơn <span class="active txt-lms">90.000</span> lần tấn
                                công vào website Wordpress mỗi phút
                            </h3>
                            <span class="about">
                                Đỉnh điểm là những cuộc tấn công hàng loạt từ “máy tính
                                ma” khiến người dùng Wordpress trở thành tâm điểm của tin
                                tặc
                            </span>
                            <div class="tag-decor bg-digital fz-700">Sự thật #1</div>
                        </div>
                    </div>
                </div>
                <div class="line-sesion">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-line-2.png" alt="" />
                </div>
            </div>
            <div class="session-line">
                <div class="d-flex f-between wrap-reverse">
                    <div class="text-decor col-6">
                        <div class="content line-lms">
                            <h3 class="text">
                                Hơn <span class="acitve txt-nhtq"> 90% </span> nền tảng
                                CMS bị hack đều từ wordpress
                            </h3>
                            <span class="about">
                                Vấn đề chủ yếu đến từ việc dùng Hosting không an toàn, bảo
                                mật kém khiến website dễ bị hacker tấn công hoặc spam
                            </span>

                            <div class="tag-decor bg-lms fz-700">Sự thật #2</div>
                        </div>
                    </div>
                    <div class="img-decor col-6">
                        <div class="img-decor-wrap">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-group-session-2.png" alt="" />
                        </div>
                    </div>
                </div>
                <div class="line-sesion">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-line-3.png" alt="" />
                </div>
            </div>
        </div>

        <div class="wordpress-footer">
            <div class="title-footer">
                Đối mặt với hậu quả nghiêm trọng - Trở thành con mồi cho hacker
            </div>
            <div class="content-main">
                <div class="main">
                    <div class="main-center">
                        <div class="img">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-footer-hacker.png" alt="" />
                        </div>
                    </div>
                    <div class="cloud">
                        <div class="cloud-list">
                            <div class="cloud-item">
                                <div class="txt-quote">
                                    <div class="txt-quote-wrap">
                                        <div class="">
                                            <div class="txt-quote-img">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-quote-img-1.png" alt="" />
                                            </div>
                                            <div class="txt">
                                                Bị Google cảnh cáo<br />
                                                và chặn truy cập
                                            </div>
                                            <div class="decor">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-quote-decor-1.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cloud-item cloud-item-2">
                                <div class="txt-quote">
                                    <div class="txt-quote-wrap">
                                        <div class="">
                                            <div class="txt-quote-img">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-quote-img-2.png" alt="" />
                                            </div>
                                            <div class="txt">
                                                Mất thứ hạng SEO<br />
                                                và lượng truy cập
                                            </div>
                                            <div class="decor">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-quote-decor-2.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cloud-item cloud-item-3">
                                <div class="txt-quote">
                                    <div class="txt-quote-wrap">
                                        <div class="">
                                            <div class="txt-quote-img">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-quote-img-3.png" alt="" />
                                            </div>
                                            <div class="txt">
                                                Mất uy tín và niềm tin<br />
                                                của khách hàng và đối tác
                                            </div>
                                            <div class="decor">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-quote-decor-3.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cloud-item cloud-item-4">
                                <div class="txt-quote">
                                    <div class="txt-quote-wrap">
                                        <div class="">
                                            <div class="txt-quote-img">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-quote-img-4.png" alt="" />
                                            </div>
                                            <div class="txt">
                                                Mất dữ liệu quan trọng<br />
                                                khó có thể khôi phục
                                            </div>
                                            <div class="decor">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-quote-decor-4.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="cloud-item cloud-item-5">
                                <div class="txt-quote">
                                    <div class="txt-quote-wrap">
                                        <div class="">
                                            <div class="txt-quote-img">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-quote-img-5.png" alt="" />
                                            </div>
                                            <div class="txt">
                                                Bị phạt tiền hoặc<br />
                                                kiện tụng bản quyền
                                            </div>
                                            <div class="decor">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/wordpress-quote-decor-5.png" alt="" />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>