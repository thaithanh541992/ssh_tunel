<?php

/**
 * Section name: Home Six Year
 * Description: 
 * Author: Monamedia
 * Order: 19
 */
?>
<section  id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> home-session-six-year">
    <div class="home-session-six-year-wrap">
        <div class="bg-start">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/bg-six-year.png" alt="" />
        </div>
        <div class="cloud-img">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/six-year-cloud-1.png" alt="" />
        </div>
        <div class="home-session-six-year-header">
            <div class="six-year-intergration">
                <div class="six-year-integration-wrap">
                    <div class="container">
                        <div class="content-six-year">
                            <div class="content-six-year-wrap">
                                <h2 class="txt-content">
                                    <p class="txt-year fw-400">6</p>
                                    <p>
                                        <span class="txt-intergration fw-600">
                                            Năm phát triển
                                        </span>
                                        <span class="txt-hosting txt-bg bg-nhtq fw-600">
                                            đến chi phối mảng Hosting
                                        </span>
                                    </p>
                                </h2>
                                <div class="security">
                                    <div class="security-content">
                                        <div class="security-content-wrap">
                                            <div class="content">
                                                <div class="txt-title">Security (Bảo mật)</div>
                                                <ul class="txt-option">
                                                    <li>Firewall</li>
                                                    <li>Bot management</li>
                                                    <li>SSL / TLS</li>
                                                    <li>DDos protection</li>
                                                    <li>Secure Origin Connection</li>
                                                    <li>Rate limiting</li>
                                                </ul>
                                            </div>
                                            <div class="icon-security">
                                                <svg width="22" height="22" viewBox="0 0 22 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M4.31934 8.3405C4.41421 9.47549 4.63209 11.0302 5.12507 12.6626C6.17207 16.1293 8.02167 18.5943 10.6253 19.9951C13.229 18.5943 15.0786 16.1293 16.1256 12.6626C16.6186 11.0303 16.8364 9.47558 16.9314 8.3405L10.6253 5.1875L4.31934 8.3405Z" fill="white" />
                                                    <path d="M21.2119 6.16363C21.1988 5.91079 21.0507 5.68452 20.8242 5.57132L10.9394 0.628902C10.7415 0.529949 10.5085 0.529949 10.3106 0.628902L0.425788 5.57132C0.199335 5.68452 0.0512097 5.91079 0.0380847 6.16363C0.0305846 6.30754 -0.131978 9.73401 1.03526 13.7122C1.72592 16.0659 2.74179 18.1217 4.05467 19.8225C5.70912 21.9657 7.83557 23.5424 10.3749 24.5088C10.4555 24.5394 10.5402 24.5547 10.625 24.5547C10.7098 24.5547 10.7945 24.5394 10.8751 24.5088C13.4144 23.5424 15.5409 21.9657 17.1953 19.8225C18.5082 18.1217 19.5241 16.0659 20.2148 13.7122C21.382 9.73401 21.2194 6.30754 21.2119 6.16363ZM10.9366 21.4174C10.8384 21.466 10.7317 21.4903 10.625 21.4903C10.5183 21.4903 10.4115 21.466 10.3134 21.4174C7.1825 19.8694 4.98387 17.061 3.77862 13.0701C3.17581 11.074 2.95817 9.19312 2.88176 7.96677C2.86418 7.68515 3.01671 7.42035 3.26909 7.29416L10.3106 3.77342C10.5086 3.67452 10.7415 3.67447 10.9395 3.77342L17.9811 7.29416C18.2334 7.4203 18.386 7.68519 18.3684 7.96682C18.2919 9.19326 18.0743 11.0742 17.4715 13.0701C16.2661 17.0609 14.0675 19.8694 10.9366 21.4174Z" fill="white" />
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="security-content">
                                        <div class="security-content-wrap">
                                            <div class="content">
                                                <div class="txt-title">Security (Bảo mật)</div>
                                                <ul class="txt-option">
                                                    <li>Firewall</li>
                                                    <li>Bot management</li>
                                                    <li>SSL / TLS</li>
                                                    <li>DDos protection</li>
                                                    <li>Secure Origin Connection</li>
                                                    <li>Rate limiting</li>
                                                </ul>
                                            </div>
                                            <div class="icon-security">
                                                <svg width="22" height="22" viewBox="0 0 22 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M4.31934 8.3405C4.41421 9.47549 4.63209 11.0302 5.12507 12.6626C6.17207 16.1293 8.02167 18.5943 10.6253 19.9951C13.229 18.5943 15.0786 16.1293 16.1256 12.6626C16.6186 11.0303 16.8364 9.47558 16.9314 8.3405L10.6253 5.1875L4.31934 8.3405Z" fill="white" />
                                                    <path d="M21.2119 6.16363C21.1988 5.91079 21.0507 5.68452 20.8242 5.57132L10.9394 0.628902C10.7415 0.529949 10.5085 0.529949 10.3106 0.628902L0.425788 5.57132C0.199335 5.68452 0.0512097 5.91079 0.0380847 6.16363C0.0305846 6.30754 -0.131978 9.73401 1.03526 13.7122C1.72592 16.0659 2.74179 18.1217 4.05467 19.8225C5.70912 21.9657 7.83557 23.5424 10.3749 24.5088C10.4555 24.5394 10.5402 24.5547 10.625 24.5547C10.7098 24.5547 10.7945 24.5394 10.8751 24.5088C13.4144 23.5424 15.5409 21.9657 17.1953 19.8225C18.5082 18.1217 19.5241 16.0659 20.2148 13.7122C21.382 9.73401 21.2194 6.30754 21.2119 6.16363ZM10.9366 21.4174C10.8384 21.466 10.7317 21.4903 10.625 21.4903C10.5183 21.4903 10.4115 21.466 10.3134 21.4174C7.1825 19.8694 4.98387 17.061 3.77862 13.0701C3.17581 11.074 2.95817 9.19312 2.88176 7.96677C2.86418 7.68515 3.01671 7.42035 3.26909 7.29416L10.3106 3.77342C10.5086 3.67452 10.7415 3.67447 10.9395 3.77342L17.9811 7.29416C18.2334 7.4203 18.386 7.68519 18.3684 7.96682C18.2919 9.19326 18.0743 11.0742 17.4715 13.0701C16.2661 17.0609 14.0675 19.8694 10.9366 21.4174Z" fill="white" />
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="security-content">
                                        <div class="security-content-wrap">
                                            <div class="content">
                                                <div class="txt-title">Security (Bảo mật)</div>
                                                <ul class="txt-option">
                                                    <li>Firewall</li>
                                                    <li>Bot management</li>
                                                    <li>SSL / TLS</li>
                                                    <li>DDos protection</li>
                                                    <li>Secure Origin Connection</li>
                                                    <li>Rate limiting</li>
                                                </ul>
                                            </div>
                                            <div class="icon-security">
                                                <svg width="22" height="22" viewBox="0 0 22 25" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path d="M4.31934 8.3405C4.41421 9.47549 4.63209 11.0302 5.12507 12.6626C6.17207 16.1293 8.02167 18.5943 10.6253 19.9951C13.229 18.5943 15.0786 16.1293 16.1256 12.6626C16.6186 11.0303 16.8364 9.47558 16.9314 8.3405L10.6253 5.1875L4.31934 8.3405Z" fill="white" />
                                                    <path d="M21.2119 6.16363C21.1988 5.91079 21.0507 5.68452 20.8242 5.57132L10.9394 0.628902C10.7415 0.529949 10.5085 0.529949 10.3106 0.628902L0.425788 5.57132C0.199335 5.68452 0.0512097 5.91079 0.0380847 6.16363C0.0305846 6.30754 -0.131978 9.73401 1.03526 13.7122C1.72592 16.0659 2.74179 18.1217 4.05467 19.8225C5.70912 21.9657 7.83557 23.5424 10.3749 24.5088C10.4555 24.5394 10.5402 24.5547 10.625 24.5547C10.7098 24.5547 10.7945 24.5394 10.8751 24.5088C13.4144 23.5424 15.5409 21.9657 17.1953 19.8225C18.5082 18.1217 19.5241 16.0659 20.2148 13.7122C21.382 9.73401 21.2194 6.30754 21.2119 6.16363ZM10.9366 21.4174C10.8384 21.466 10.7317 21.4903 10.625 21.4903C10.5183 21.4903 10.4115 21.466 10.3134 21.4174C7.1825 19.8694 4.98387 17.061 3.77862 13.0701C3.17581 11.074 2.95817 9.19312 2.88176 7.96677C2.86418 7.68515 3.01671 7.42035 3.26909 7.29416L10.3106 3.77342C10.5086 3.67452 10.7415 3.67447 10.9395 3.77342L17.9811 7.29416C18.2334 7.4203 18.386 7.68519 18.3684 7.96682C18.2919 9.19326 18.0743 11.0742 17.4715 13.0701C16.2661 17.0609 14.0675 19.8694 10.9366 21.4174Z" fill="white" />
                                                </svg>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="home-session-six-year-about">
            <div class="about-bg">
                <img class="img-rocket" src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/bg-rocket-cloud.png" alt="" />
            </div>

            <div class="container about-content">
                <div class="content-tt fw-600">
                    <p class="txt-tt">
                        <span> Không có vấn đề gì là quá lớn </span>
                        <span class="txt-tt-sub">
                            Mona đảm bảo trong tầm kiểm soát
                        </span>
                    </p>
                    <p class="sub-about fw-400">
                        Chúng tôi cung cấp
                        <span class="fw-600 txt-digital">
                            <u>hỗ trợ đầy đủ</u>
                        </span>
                        cho tất cả các sự cố từ thiết lập đến bảo trì.
                    </p>
                    <div class="content-247">
                        <p class="txt-247 fw-600">24/7</p>
                        <p class="sub-247 fw-400">
                            <span> Đội ngũ chuyên gia </span>
                            <span>Luôn sẵn sàng </span>
                        </p>
                    </div>
                    <p class="sub-about fw-400">
                        <span class="txt-nhtq fw-600">
                            <u>Giúp giải quyết mọi vấn đề có thể phát sinh</u>
                        </span>
                        để bạn có thể tập trung vào<br />
                        điều hành và phát triển doanh nghiệp của mình mà
                        <span class="fw-600"> không phải lo lắng. </span>
                    </p>
                </div>
            </div>
        </div>

        <div class="home-session-six-year-business">
            <div class="container">
                <div class="d-flex f-between">
                    <div class="business-content col-6">
                        <p class="txt-header">
                            <span> Bạn kinh doanh mảng nào </span>
                            <span class="txt-bg-hd">
                                Chúng tôi đều có giải pháp mảng đó
                            </span>
                        </p>
                        <p class="txt-about">
                            Trải qua nhiều năm kinh nghiệm, Mona chúng tôi đã có
                            <span class="fw-600">rất nhiều giải pháp cho tất cả các mảng mà bạn đang quan
                                tâm.</span>
                            <br />
                            <br />
                            Mỗi dự án chúng tôi đều được chăm chút kỹ lưỡng nhằm
                            <span class="fw-600">đảm bảo đầu ra sản phẩm </span>cho
                            doanh nghiệp của bạn
                        </p>

                        <a class="btn-clh">
                            <span class="txt">
                                Xem tất cả giải pháp của chúng tôi
                            </span>
                            <span class="icon">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/right-02.png" alt="">
                            </span>
                        </a>
                        <div class="about-action-project">
                            <div class="arrow">
                                <div class="icon">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/arrow-29.png" alt="" />
                                </div>
                            </div>
                            <p class="txt-view-project">
                                Cùng xem những dự án của<br />
                                chúng tôi ngay!!
                            </p>
                        </div>
                    </div>
                    <div class="bussiness-project col-6">
                        <div class="list-project">
                            <div class="project-item">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/bussiness-project-1.png" alt="" />
                            </div>
                            <div class="project-item">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/bussiness-project-2.png" alt="" />
                            </div>
                            <div class="project-item">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/bussiness-project-4.png" alt="" />
                            </div>
                            <div class="project-item">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/bussiness-project-3.png" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="home-session-six-year-footer">
            <div class="container">
                <div class="wrap">
                    <div class="header">
                        <h2 class="host-tt txt-header fw-600">
                            <span>Gải quyết vấn đề kinh doanh</span>
                            <span class="txt-header-sub txt-bg">
                                Bằng những dịch vụ cao cấp của Mona
                            </span>
                        </h2>
                        <div class="decor-line">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/decor-line-digital.png" alt="" />
                        </div>
                        <div class="quote-save">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/quote-tiet-kiem.png" alt="" />
                        </div>
                    </div>
                    <div class="dot-digital">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/dot-digital.png" alt="" />
                    </div>
                    <div class="list-cloud">
                        <div class="list-clout item">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/cloud-item.png" alt="" />
                        </div>
                        <div class="list-clout item">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/cloud-item.png" alt="" />
                        </div>
                        <div class="list-clout item">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/cloud-item.png" alt="" />
                        </div>
                        <div class="list-clout item">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/cloud-item.png" alt="" />
                        </div>
                        <div class="list-clout item">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/cloud-item.png" alt="" />
                        </div>
                    </div>
                    <div class="bottom-content">
                        <div class="bg-panda-rocket">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/panda-rocket-sky.png" alt="" />
                        </div>
                        <div class="header-about">
                            <div class="txt-about">
                                Mục tiêu của chúng tôi không phải là giành giải thưởng.
                                Mục tiêu của chúng tôi là
                            </div>

                            <div class="txt-content-solution">
                                <div>
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/txt-solution.png" alt="" />
                                </div>
                            </div>
                        </div>
                        <div class="footer-content">
                            <div class="footer-content-wrap">
                                <h2 class="host-tt txt-header">
                                    <p class="fw-600">Dịch vụ Premium Web Hosting tại MONA</p>
                                    <p class="txt-bg-header-digital fw-600 txt-bg">
                                        Đáp ứng mọi nhu cầu lưu trữ và phát triển
                                        </a>
                                </h2>

                                <div class="txt-about">
                                    <div class="about">
                                        <p>
                                            Bạn sẽ không bao giờ phải chuyển đổi gói hay máy chủ
                                            với dịch vụ Premium Web Hosting tại MONA
                                        </p>
                                        <ul class="list-about" style="list-style-type: disc">
                                            <li>Lưu trữ đẳng cấp thế giới</li>
                                            <li>Thiết lập nhanh chóng và dễ dàng</li>
                                            <li>Bảo vệ quyền riêng tư</li>
                                            <li>Hiệu suất dẫn đầu thị trường</li>
                                            <li>Dịch vụ khách hàng</li>
                                            <li>Đăng ký tên miền miễn phí</li>
                                        </ul>
                                    </div>
                                    <div class="img-about">
                                        <div class="main-panda">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/panda-dot.png" alt="" />
                                        </div>

                                    </div>
                                </div>
                                <a href="" class="footer-txt btn-clh">
                                    Đăng ký sử dụng gói Premium Web Hosting của Mona ngay
                                    hôm nay để trải nghiệm sự chuyên nghiệp của chúng tôi
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>