<?php

/**
 * Section name: Home About Line
 * Description:  
 * Author: Monamedia
 * Order: 7
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> home-host-about-line">

    <div class="bg-img">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-line-bg.png" alt="" />
    </div>
    <div class="home-host-about-content">
        <div class="decor-sc">
            <div class="image">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/fix-home/sc.png" alt="">
            </div>
        </div>
        <div class="header">
            <h2 class="host-tt title">
                Chúng tôi có trọn gói giải pháp cho bạn

                <p class="sub-title txt-bg bg-lms">Từ thiết kế đến kiếm tiền</p>
            </h2>
            <div class="desc">
                Bắt đầu với việc tạo dựng bộ mặt trên Internet bằng website, chúng
                tôi có…
            </div>
        </div>
        <div class="container">
            <div class="about-session">
                <div class="about-session-line">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-line-1.png" alt="" />
                </div>
                <div class="d-flex f-between">
                    <div class="about-session-content col-6">
                        <div class="session-about-title">
                            <div class="tt">Mua Domain</div>
                            <div class="sub">
                                Bắt đầu hành trình thống trị lĩnh vực kinh doanh của bạn
                                trên thị trường Online với dịch vụ đăng ký domain tại
                                MONA.Host
                            </div>
                            <a class="btn-clh">
                                <span class="txt">
                                    Mua gói hosting ngay
                                </span>
                                <span class="icon">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/right-02.png" alt="">
                                </span>
                            </a>
                        </div>
                        <div class="session-about-sub">
                            <div class="about">
                                Live, lưu trữ và quản lý website trên hệ thống cloud của
                                MONA
                            </div>
                            <div class="about-line">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-vector-sub-domain.png" alt="" />
                            </div>
                        </div>
                    </div>
                    <div class="about-session-main-img col-6">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-domain.png" alt="" />
                    </div>
                </div>
            </div>
            <div class="about-session about-session-vps">
                <div class="about-session-line-hosting">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-line-hosting.png" alt="" />
                </div>
                <div class="d-flex f-between wrap-reverse">
                    <div class="about-session-main-img col-6">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-premium.png" alt="" />
                    </div>
                    <div class="about-session-content col-6">
                        <div class="session-about-title">
                            <div class="tt">Premium Cloud VPS/Hosting</div>
                            <div class="sub">
                                Giải pháp lưu trữ chuyên nghiệp, an toàn và hiệu quả hàng đầu Việt Nam
                                Sẵn sàng cho việc phát triển trên Internet với đội ngũ đứng sau thành công của MONA.Media.
                            </div>
                            <a class="btn-clh">
                                <span class="txt">
                                    Mua gói hosting ngay
                                </span>
                                <span class="icon">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/right-02.png" alt="">
                                </span>
                            </a>
                        </div>
                        <div class="session-about-sub">
                            <div class="about">
                                Live, lưu trữ và quản lý website trên hệ thống cloud của MONA
                            </div>
                            <div class="about-line-hosting">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/line-01.png" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about-session about-session-hosting">
                <div class="about-session-line-design">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-line-design.png" alt="" />
                </div>
                <div class="d-flex f-between">
                    <div class="about-session-content col-6">
                        <div class="session-about-title">
                            <div class="tt">Premium Web Design</div>
                            <div class="sub">
                                Thiết kế các trang web tuyệt đẹp và độc đáo mang đến cho
                                bạn sự hiện diện cao cấp, sáng tạo và vượt trội
                            </div>
                            <a class="btn-clh">
                                <span class="txt">
                                    Đặt thiết kế ngay
                                </span>
                                <span class="icon">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/right-02.png" alt="">
                                </span>
                            </a>
                        </div>
                        <div class="session-about-sub">
                            <div class="about">
                                Kiếm tiền bằng website, tận dụng tối đa tài nguyên để chạy
                                Sales, Marketing campaign...
                            </div>
                            <div class="about-line">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-vector-sub-webdesign.png" alt="" />
                            </div>
                        </div>
                    </div>
                    <div class="about-session-main-img col-6">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-web-design.png" alt="" />
                    </div>
                </div>
            </div>
            <div class="about-session about-session-solution">
                <div class="about-session-line-solution">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-line-solution.png" alt="" />
                </div>
                <div class="d-flex f-between wrap-reverse">
                    <div class="about-session-main-img col-6">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-digital.png" alt="" />
                    </div>
                    <div class="about-session-content col-6">
                        <div class="session-about-title">
                            <div class="tt">Digital Marketing Solutions</div>
                            <div class="sub">
                                Giải pháp Marketing toàn diện phù hợp với từng nhu cầu
                                kinh doanh, từ SEO đến các chiến dịch truyền thông - chúng
                                tôi làm tất cả để giúp bạn tăng trưởng khách hàng!
                            </div>
                            <a class="btn-clh">
                                <span class="txt">
                                    Liên hệ ngay để được tư vấn
                                </span>
                                <span class="icon">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/right-02.png" alt="">
                                </span>
                            </a>
                        </div>
                        <div class="session-about-sub">
                            <div class="about">
                                Làm Marketing hiệu quả, mở rộng quy mô và nhân sự, bạn cần
                                phần mềm quản lý...
                            </div>
                            <div class="about-line">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-vector-sub-digital.png" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about-session about-session-service">
                <div class="about-session-line-service">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-line-service.png" alt="" />
                </div>
                <div class="d-flex f-between">
                    <div class="about-session-content col-6">
                        <div class="session-about-title">
                            <div class="tt">Software as a Service</div>
                            <div class="sub">
                                Chúng tôi cũng cung cấp các giải pháp SaaS sẽ giúp bạn hợp
                                lý hóa các hoạt động và tận dụng tối đa các quy trình kinh
                                doanh của mình
                            </div>
                            <a class="btn-clh">
                                <span class="txt">
                                    Mua gói hosting ngay
                                </span>
                                <span class="icon">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/right-02.png" alt="">
                                </span>
                            </a>
                        </div>
                        <div class="session-about-sub">
                            <div class="about">
                                MONA sẵn sàng đồng hàng cùng khách hàng trên mọi nẻo đường
                                để đi đến thành công!
                            </div>
                            <div class="about-line">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-vector-sub-software.png" alt="" />
                            </div>
                        </div>
                    </div>
                    <div class="about-session-main-img col-6">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-software.png" alt="" />
                    </div>
                </div>
            </div>
            <div class="about-session about-session-banding">
                <div class="about-session-line-branding">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-line-brand.png" alt="" />
                </div>

                <div class="d-flex f-between wrap-reverse">
                    <div class="about-session-main-img col-6">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-brand-solution.png" alt="" />
                    </div>
                    <div class="about-session-content col-6">
                        <div class="session-about-title">
                            <div class="tt">Branding Solutions</div>
                            <div class="sub">
                                MONA có giải pháp giúp phát triển thương hiệu từ Profile
                                công ty đến định hướng chiến lược cho thương hiệu của bạn
                                ngày một lớn mạnh!
                            </div>
                            <a class="btn-clh">
                                <span class="txt">
                                    Xây dựng thương hiệu ngay
                                </span>
                                <span class="icon">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/right-02.png" alt="">
                                </span>
                            </a>
                        </div>
                        <div class="session-about-sub">
                            <div class="about">
                                Bạn đang dẫn đầu thị trường, quy mô lớn mạnh, nhưng như
                                thế là chưa đủ tham vọng...
                            </div>
                            <div class="about-line">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-vector-sub-brand.png" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="about-session about-session-end">
                <div class="about-session-line-end">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-line-end.png" alt="" />
                </div>
                <div class="about-session-content">
                    <div class="tt">
                        Cuối cùng là Kiếm được nhiều tiền, được nhiều người biết tới
                    </div>
                    <div class="sub">
                        Giải pháp lưu trữ chuyên nghiệp, an toàn và hiệu quả hàng đầu
                        Việt Nam Sẵn sàng cho việc phát triển trên Internet với đội
                        ngũ đứng sau thành công của MONA.Media.
                    </div>
                    <div class="action">
                        <a class="btn-clh">
                            <span class="txt">
                                Mua gói hosting ngay
                            </span>
                            <span class="icon">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/right-02.png" alt="">
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div class="about-session">
                <div class="container">
                    <div class="d-flex f-between">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/about-decor-money-1.png" alt="" />
                    </div>
                </div>
            </div>
        </div>

        <div class="host-big-content">
            <div class="container">
                <div class="content">
                    <div class="bg-main">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/about-bg-big-conent.png" alt="" />
                    </div>

                    <div class="content-main">
                        <div class="content-tt">
                            <p class="tt">Tài nguyên của Mona là vô cùng lớn</p>
                            <p class="sub sub-bg">
                                <span class="">Đảm bảo trang web hoạt động trơn tru và tạo lợi
                                    nhuận</span>
                            </p>
                        </div>
                        <div class="main-img">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-big-content-main-1.png" alt="" />
                        </div>

                        <div class="tag tag-profestional">
                            <span class="txt-bg bg-lms"> Chuyên nghiệp </span>
                        </div>
                        <div class="tag tag-dont-know">
                            <span class="txt-bg bg-nhtq">
                                Không ngán ngại bất cứ gì
                            </span>
                        </div>

                        <div class="tag tag-young">
                            <span class="txt-bg bg-software"> Trẻ trung </span>
                        </div>
                        <div class="tag tag-active">
                            <span class="txt-bg bg-digital"> Năng động </span>
                        </div>
                        <div class="decor-jun">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/decor-jun.png" alt="" />
                        </div>
                    </div>
                    <div class="content-footer">
                        <a href="#" class="btn-clh btn-mess content-footer-wrap">
                            <span class="txt">
                                Hãy cho chúng tôi biết những gì bạn đang phải đối mặt</br>
                                chuyên gia Mona Host sẽ giúp bạn tìm giải pháp tốt nhất!
                            </span>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>