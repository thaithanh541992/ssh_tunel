<?php

/**
 * Section name: Home News
 * Description:  
 * Author: Monamedia
 * Order: 9
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> session-news">
    <div class="header-cloud-1">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/session-news-cloud-1.png" alt="" />
    </div>
    <div class="bg-start">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/bg-start-session-news.png" alt="" />
    </div>
    <div class="main-content">
        <div class="traffic">
            <div class="container">
                <div class="d-flex f-between">
                    <div class="content-left col-7">
                        <h2 class="host-tt tt">
                            Ở Mona quy tụ những nhân sự tay to

                            <p class="sub txt-bg bg-nhtq">
                                Hoàn toàn “ăn đứt” mảng hosting
                            </p>
                        </h2>
                        <div class="list-about">
                            <p class="about">
                                &#x2022; Thiết kế và triển khai hệ thống Hosting cho
                                website Thethao247
                            </p>
                            <p class="about">
                                &#x2022; Tối ưu hoá và bảo mật hệ thống hosting cho Báo
                                Thanh Niên
                            </p>
                            <p class="about">
                                &#x2022; Cài đặt và cấu hình hệ thống Hosting cho website
                                Levents, giải quyết bài toán "giờ cao điểm" , giúp tiết
                                kiệm chi phí và tăng khả năng mở rộng
                            </p>
                        </div>
                        <div class="panda-ufo">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/panda-ufo.png" alt="" />
                        </div>

                        <div class="star-saturn">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/news-star-saturn.png" alt="" />
                        </div>
                    </div>
                    <div class="content-right col-5">
                        <div class="main">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/traffic-group.png" alt="" />
                        </div>
                    </div>
                    <div class="content-footer col-12 d-flex">
                        <div class="content-footer-col col-12">
                            <h2 class="txt-footer fw-600">
                                Giải pháp hạ tầng đa quốc gia
                                <p class="txt-footer fw-600  txt-bg bg-host">Chính là tương lai của Mona</p>
                            </h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="earth">
        <div class="container">
            <div class="main">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/news-earth.png" alt="" />
            </div>
        </div>
        <div class="header-cloud-2">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/session-news-cloud-2.png" alt="" />
        </div>
        <div class="earth-decor x1">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/news-star-2.png" alt="">
        </div>
        <div class="decor-sc">
            <div class="image">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/fix-home/sc.png" alt="">
            </div>
        </div>
    </div>
</section>