<?php

/**
 * Section name: Home Company
 * Description: 
 * Author: Monamedia
 * Order: 1
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> seccion-company">
    <div class="container">
        <div class="seccion-company-tt">
            <h2 class="host-tt title fw-600">
                An cư thì mới lập nghiệp

                <p class="sub-title fw-600 txt-bg bg-host">
                    Yên tâm kinh doanh với dịch vụ Hosting tại MONA.Host
                </p>
            </h2>
            <div class="desr fw-400">
                Khách hàng chỉ cần tập trung vào việc bán sản phẩm và dịch vụ,
                chăm sóc khách hàng và tăng doanh thu. MONA sẽ trở thành cánh tay
                phải đắc lực cho bạn trên thị trường Online
            </div>
            <a class="btn-clh">
                <span class="txt">
                    Hãy kết nối với Mona Host ngay hôm nay
                </span>
            </a>
        </div>
        <div class="content">
            <div class="items-tehnique item-res">
                <div class="items-wrap items-wrap-tehnique">
                    Không cần lo lắng về kỹ thuật
                </div>
            </div>
            <div class="items-time item-res">
                <div class="items-wrap items-wrap-time">
                    Không tốn quá nhiều thời gian
                </div>
            </div>
            <div class="items-code item-res">
                <div class="items-wrap items-wrap-code">
                    Không cần kiến thức về lập trình,...
                </div>
            </div>
            <div class="bg-session">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/session-1.png" alt="" />
            </div>
        </div>
    </div>
    <div class="bg-cloud-solution-cloud">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/solution-cloud.png" alt="" />
    </div>
</section>