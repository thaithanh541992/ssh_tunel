<?php

/**
 * Section name: Home Professor
 * Description: 
 * Author: Monamedia
 * Order: 5
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> hclhser style-pri">
    <div class="hclhser-decor x1">
        <div class="image">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/fix-home/hclhser-decor-01.png" alt="">
        </div>
    </div>
    <div class="hclhser-decor x2">
        <div class="image">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/fix-home/hclhser-decor-02.png" alt="">
        </div>
    </div>
    <div class="hclhser-decor x3">
        <div class="image">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/fix-home/hclhser-decor-03.png" alt="">
        </div>
    </div>
    <div class="host-big-content">
        <div class="container">
            <div class="content">
                <div class="bg-main">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/fix-home/hclhser-br.png" alt="" />
                </div>
                <div class="content-main">
                    <div class="content-tt">
                        <h2 class="host-tt tt">
                            Với Mona chúng tôi

                            <p class="sub tt-vio tt-df">
                                <span class="">Hosting là nền móng cho doanh nghiệp của bạn</span>
                            </p>
                        </h2>
                    </div>
                    <div class="main-img">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/fix-home/hclhser-img.png" alt="" />
                    </div>
                </div>
                <div class="content-footer">
                    <a href="#" class="btn-clh btn-mess content-footer-wrap">
                        <span class="txt">
                            Hãy cho chúng tôi biết những gì bạn đang phải đối mặt</br>
                            chuyên gia Mona Host sẽ giúp bạn tìm giải pháp tốt nhất!
                        </span>
                    </a>
                </div>
            </div>
        </div>
    </div>
</section>