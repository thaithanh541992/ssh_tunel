<?php

/**
 * Section name: Home Banner
 * Description: 
 * Author: Monamedia
 * Order: 0
 */
?>
<div id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> banner">
    <div class="container">
        <div class="banner-content">
            <h1 class="tt">
                Hệ thống Hosting
                <span class="txt-bg bg-host fw-700">#TOP1</span> Việt Nam
            </h1>
            <p class="sub">
                Bắt đầu xây dựng đế chế kinh doanh online với Hosting tại MONA
            </p>
            <div class="banner-search">
                <form action="<?php echo MONA_SITE_URL ?>/dang-ky-ten-mien" class="banner-search-form">
                    <div class="f-r">
                        <div class="f-c">
                            <input type="search" name="domain_name" id="" placeholder="Nhập tên miền của bạn tại đây" class="ipt-txt" required />
                            <button class="ipt-sub">
                                <img src="<?php echo MONA_SITE_URL  ?>/template/assets/images/icon-search-white.svg" alt="search" />
                                Kiểm tra tên miền
                            </button>
                            <input type="submit" value="" class="ipt-sub-mobile" />
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="banner-background">

        <div class="bg-circle">
            <div class="circle-one">
                <img src="<?php echo MONA_SITE_URL  ?>/template/assets/images/hosts/banner-circle-1.png" alt="" />
            </div>
            <div class="circle-two">
                <img src="<?php echo MONA_SITE_URL  ?>/template/assets/images/hosts/banner-circle-2.png" alt="" />
            </div>
            <div class="circle-three">
                <img src="<?php echo MONA_SITE_URL  ?>/template/assets/images/hosts/banner-circle-3.png" alt="" />
            </div>
        </div>
        <div class="bg-content">
            <div class="bg-cpu col col-11">
                <img src="<?php echo MONA_SITE_URL  ?>/template/assets/images/hosts/Group 40183.png" alt="" />
            </div>

            <div class="bg-star">
                <div class="circle-saturn cir">
                    <img src="<?php echo MONA_SITE_URL  ?>/template/assets/images/hosts/saturn.png" alt="" />
                </div>
                <div class="circle-uranus cir">
                    <img src="<?php echo MONA_SITE_URL  ?>/template/assets/images/hosts/uranus.png" alt="" />
                </div>
                <div class="circle-saturn-2 cir">
                    <img src="<?php echo MONA_SITE_URL  ?>/template/assets/images/hosts/saturn-2.png" alt="" />
                </div>
            </div>
            <div class="bg-member">
                <img src="<?php echo MONA_SITE_URL  ?>/template/assets/images/hosts/banner-key-member-2.png" alt="" />
            </div>
            <div class="bg-cloud">
                <img src="<?php echo MONA_SITE_URL  ?>/template/assets/images/hosts/bg-cloud.png" alt="" />
                <div class="bg-hand">
                    <div class="content">
                        <div class="hand">
                            <img src="<?php echo MONA_SITE_URL  ?>/template/assets/images/hosts/hand.png" alt="" />
                            <div class="earth">
                                <img src="<?php echo MONA_SITE_URL  ?>/template/assets/images/hosts/earth.png" alt="" class="cir" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="bg-rocket">
                <div class="img-decor img-rocket">
                    <img src="<?php echo MONA_SITE_URL  ?>/template/assets/images/hosts/home-rocket.png" alt="" class="ani-rocket">

                    <div class="ani-fire-render"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="dvs-hero-sky-star">
        <div class="star-sky">
            <div class="lms-ani-stars"></div>
        </div>
    </div>
</div>