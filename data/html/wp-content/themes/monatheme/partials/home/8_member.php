<?php

/**
 * Section name: Home Member
 * Description:  
 * Author: Monamedia
 * Order: 8
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> session-about-member">
    <div class="container">
        <div class="header">
            <h2 class="host-tt tt">
                Mona có NHỮNG CHUYÊN GIA Mạnh và Giỏi

                <p class="sub bg">Hoàn toàn “ăn đứt” mảng hosting</p>
            </h2>
            <div class="about">
                Xuất phát từ những Developer kỳ cựu. Kinh qua hàng ngàn dự án khó
                nhằn <br /><span class="txt-digital fw-600"><u>Đến những huyền thoại làm nên Mona Host</u></span>
            </div>
        </div>
        <div class="content">
            <div class="content-main-inner">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/about-main-member-1.png" alt="" />
            </div>
            <a href="https://mona.media/author/thoaivonguyen/" class="ab-three-cover thoai" target="_blank">
                <div class="ab-three-name-img">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/ab-three-name-thoai.png" alt="" />
                </div>
            </a>
            <div class="ab-three-cover thanh">
                <div class="ab-three-name-img">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/ab-three-name-thanh.png" alt="" />
                </div>
            </div>
            <div class="ab-three-cover phuc">
                <div class="ab-three-name-img">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/ab-three-name-phuc.png" alt="" />
                </div>
            </div>
        </div>
    </div>
</section>