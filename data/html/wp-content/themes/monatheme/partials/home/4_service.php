<?php
/**
 * Section name: Home Service
 * Description: 
 * Author: Monamedia
 * Order: 4
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> session-service">
    <div class="session-service-title-wrap">
        <?php
        $mona_available_hosting = get_field('mona_available_hosting');
        if ( !empty( $mona_available_hosting ) && is_array( $mona_available_hosting )) {
            ?>
            <div class="hosting-wrap-js">
                <div class="title">
                    <p class="tt">Dịch vụ Premium Cloud Hosting</p>
                    <p class="sub-tt txt-bg bg-host">
                        Tốt nhất thị trường đến từ Mona Host
                    </p>
                </div>
                <div class="session-pay-host-header">
                    <div class="tab-button-custom">
                        <div class="wrap">
                            <?php $first_option_qty = 60 ?>
                            <div class="item active hosting-tab-js" data-qty="60">60 tháng</div>
                            <div class="item hosting-tab-js" data-qty="36">36 tháng</div>
                            <div class="item hosting-tab-js" data-qty="24">24 tháng</div>
                            <div class="item hosting-tab-js" data-qty="12">12 tháng</div>
                        </div>
                    </div>
                </div>
                <div style="display: none">
                    <?php
                    $hosting_ui_data = [];
                    foreach ( $mona_available_hosting as $key => $hosting ) {
                        $mona_product_ui_info = get_field( 'mona_product_ui_info', $hosting->ID );
                        if ( !empty( $mona_product_ui_info ) ) {
                            $hosting_ui_data[] = $mona_product_ui_info;
                            ?>
                            <div id="tooltip-hosting-<?php echo $key ?>">
                                <div class="gift-content">
                                    <?php echo $mona_product_ui_info['info_offer'] ?>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
                <div class="service-hosting-inner">
                    <div class="service-hosting">
                        <div class="service-hosting-wrap">
                            <?php
                            foreach ( $mona_available_hosting as $key => $hosting ) {
                                $mona_product_ui_info = $hosting_ui_data[$key];
                                if ( !empty( $mona_product_ui_info ) ) {
                                    ?>
                                    <div class="service-hosting-item bg-liner-<?php echo $key + 1 ?>"
                                        data
                                    >
                                        <?php
                                        if ( $mona_product_ui_info['info_recommend'] ) {
                                            ?>
                                            <div class="icon-recomment">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/icon-recomment.png" alt="" />
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <div class="txt-hosting-name">
                                            <div class="txt-white"><?php echo $hosting->post_title ?></div>
                                            <?php
                                            if ( !empty( $mona_product_ui_info['info_offer'] ) ) {
                                                ?>
                                                <div class="icon-gift tooltip-show" data-template="tooltip-hosting-<?php echo $key ?>">
                                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/icon-gift.png" alt="" />
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <?php
                                        $mona_billing_cycle_list = get_field( 'mona_billing_cycle_list', $hosting->ID );
                                        $original_price = $mona_product_ui_info['info_original_price'];
                                        $price = 0;
                                        $sale_percent = 0;
                                        if ( !empty( $mona_billing_cycle_list ) && is_array( $mona_billing_cycle_list ) ) {
                                            foreach ( $mona_billing_cycle_list as $billing_item ) {
                                                if ( $billing_item['term_quantity'] == $first_option_qty ) {
                                                    $price = floor($billing_item['price']/$billing_item['term_quantity']);
                                                    if ( !empty( $original_price ) ) {
                                                        $sale_percent = 100 - ceil($price / $original_price * 100);
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                        <div class="txt-hosting-price hosting-price-js" 
                                            data-billing_list='<?php echo json_encode( $mona_billing_cycle_list ) ?>'
                                            data-original_price='<?php echo $original_price ?>'
                                        >
                                            <div class="txt-price">
                                                <?php echo number_format($price, 0, '', ',') ?> <span class="dash-month fw-400"> đ/tháng </span>
                                            </div>
                                            <?php
                                            if ( $sale_percent > 0 ) {
                                                ?>
                                                <div><span class="txt-sale"><?php echo $sale_percent; ?>%</span></div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <?php
                                        if ( !empty( $original_price ) ) {
                                            ?>
                                            <div class="txt-sale-off">
                                                Giảm từ: <span class="txt-line-through"><?php echo number_format($original_price, 0, '', ',') ?> đ</span>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="head-name">CẤU HÌNH</div>
                        <div class="service-hosting-wrap">
                            <?php
                            foreach ( $mona_available_hosting as $key => $hosting ) {
                                $mona_product_ui_info = $hosting_ui_data[$key];
                                if ( !empty( $mona_product_ui_info ) ) {
                                    ?>
                                    <div class="service-hosting-item">
                                        <?php echo $mona_product_ui_info['info_config'] ?>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="head-name">DỊCH VỤ ĐI KÈM</div>
                        <div class="service-hosting-wrap">
                            <?php
                            foreach ( $mona_available_hosting as $key => $hosting ) {
                                $mona_product_ui_info = $hosting_ui_data[$key];
                                if ( !empty( $mona_product_ui_info ) ) {
                                    ?>
                                    <div class="service-hosting-item service-add-cart-js">
                                        <?php echo $mona_product_ui_info['info_service'] ?>
                                        <span class="add-to-cart-js is-loading-btn-2" data-pid="<?php echo $hosting->ID ?>" data-billing_cycle=<?php echo $first_option_qty ?>>
                                            <a class="btn-clh btn-clh-blue">
                                            <span class="txt">
                                                Đăng ký ngay
                                            </span>
                                            </a>
                                        </span>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }

        $mona_available_vps = get_field('mona_available_vps');
        if ( !empty( $mona_available_vps ) && is_array( $mona_available_vps )) {
            ?>
            <div class="service-hosting-wrap-bg">
                <div class="container session-besides">
                    <div class="session-besides-header">
                        <p class="txt-header">
                            Ngoài ra,
                            <span class="txt-lms">chúng tôi còn giúp bạn...</span>
                        </p>
                    </div>
                    <div class="session-besides-line">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/line-beside.png" alt="" />
                    </div>
                    <div class="session-besides-footer">
                        <div class="session-besides-wrap">
                            <p class="txt-header">Với dịch vụ</p>
                            <p class="txt-header txt-bg bg-host">
                                Với Premium Cloud VPS tại MONA.Host
                            </p>
                            <p class="txt-about">Sỡ hữu
                                <span class="txt-digital fw-600">server riêng,</span>
                                <span class="txt-blue fw-600"> toàn quyền kiểm soát</span>
                                và
                                <span class="txt-lms fw-600">mở rộng linh hoạt</span>
                                với VPS cấu hình khủng của chúng tôi để phát triển doanh nghiệp nhanh nhất!
                            </p>
                        </div>
                    </div>
                    <div class="session-besides-decor">
                        <div class="decor-cloud">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/cloud-item.png" alt="" />
                        </div>
                        <div class="decor-cloud">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/cloud-item.png" alt="" />
                        </div>
                        <div class="decor-cloud">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/cloud-item.png" alt="" />
                        </div>
                        <div class="decor-cloud">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/cloud-item.png" alt="" />
                        </div>
                        <div class="decor-cloud">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/cloud-item.png" alt="" />
                        </div>
                    </div>
                </div>
                <div class="session-pay-host">
                    <div class="session-pay-host-header">
                        <div class="tab-button-custom">
                            <div class="wrap">
                                <div class="item tab active">Linux</div>
                                <div class="item tab">Windows</div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="swiper swiper-service-hosting tab-panel active">
                            <div class="swiper-wrapper">
                                <?php
                                foreach ( $mona_available_vps as $key => $vps ) {
                                    $mona_product_ui_info = get_field( 'mona_product_ui_info', $vps->ID );
                                    $mona_billing_cycle_list = get_field( 'mona_billing_cycle_list', $vps->ID );
                                    $original_price = $mona_product_ui_info['info_original_price'];
                                    $price = 0;
                                    $sale_percent = 0;
                                    $first_option_qty = '';
                                    $mona_os = get_field( 'mona_os', $vps->ID );
                                    if ( !empty( $mona_product_ui_info ) && $mona_os == 'linux' ) {
                                        ?>
                                        <div class="swiper-slide">
                                            <div class="card vps-item-js">
                                                <div class="card-header">
                                                    <div class="header-content">
                                                        <div class="name">
                                                            <div>
                                                                <?php echo $vps->post_title ?>
                                                            </div>
                                                            <div class="month-inner">
                                                                <?php
                                                                    if ( !empty( $mona_billing_cycle_list ) && is_array( $mona_billing_cycle_list ) ) {
                                                                        ?>
                                                                        <select class="month vps-billing-cycle-js" 
                                                                            data-billing_list='<?php echo json_encode( $mona_billing_cycle_list ) ?>'
                                                                            data-original_price="<?php echo $original_price ?>"
                                                                        >
                                                                            <?php
                                                                            foreach ( $mona_billing_cycle_list as $billing_item ) {
                                                                                if ( empty( $first_option_qty ) ) {
                                                                                    $first_option_qty = $billing_item['term_quantity'];
                                                                                }
                                                                                ?>
                                                                                <option value="<?php echo $billing_item['term_quantity'] ?>"><?php echo $billing_item['label'] ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    
                                                                <div class="icon">
                                                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/icon-chevron-down.png" alt="" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        if ( !empty( $mona_billing_cycle_list ) && is_array( $mona_billing_cycle_list ) ) {
                                                            foreach ( $mona_billing_cycle_list as $billing_item ) {
                                                                if ( $billing_item['term_quantity'] == $first_option_qty ) {
                                                                    $price = floor($billing_item['price']/$billing_item['term_quantity']);
                                                                    if ( !empty( $original_price ) ) {
                                                                        $sale_percent = 100 - ceil($price / $original_price * 100);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                        <div class="price vps-price-js">
                                                            <div>
                                                                <?php echo number_format($price, 0, '', ',') ?> đ<span class="dash-month">/tháng</span>
                                                            </div>
                                                            <?php
                                                            if ( $sale_percent > 0 ) {
                                                                ?>
                                                                <div class="sale">Tiết kiệm <?php echo $sale_percent; ?>%</div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="wrap-bg">
                                                        <?php
                                                        if ( !empty( $mona_product_ui_info['info_bg'] ) ) {
                                                            echo wp_get_attachment_image($mona_product_ui_info['info_bg'], 'large');
                                                        } else {
                                                            ?>
                                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/card-header-ware-1.png" alt="" />
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="card-content">
                                                    <?php echo $mona_product_ui_info['info_config'] ?>
                                                    <div class="btn-tag">
                                                        <div class="add-to-cart-js is-loading-btn-2" data-pid='<?php echo $vps->ID ?>' data-billing_cycle='<?php echo $first_option_qty; ?>'>
                                                            <a class="btn-clh btn-clh-blue">
                                                                <span class="txt">
                                                                    Mua gói hosting ngay
                                                                </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                        <div class="swiper swiper-service-hosting tab-panel">
                            <div class="swiper-wrapper">
                                <?php
                                foreach ( $mona_available_vps as $key => $vps ) {
                                    $mona_product_ui_info = get_field( 'mona_product_ui_info', $vps->ID );
                                    $mona_billing_cycle_list = get_field( 'mona_billing_cycle_list', $vps->ID );
                                    $original_price = $mona_product_ui_info['info_original_price'];
                                    $price = 0;
                                    $sale_percent = 0;
                                    $first_option_qty = '';
                                    $mona_os = get_field( 'mona_os', $vps->ID );
                                    if ( !empty( $mona_product_ui_info ) && $mona_os != 'linux' ) {

                                        ?>
                                        <div class="swiper-slide">
                                            <div class="card vps-item-js">
                                                <div class="card-header">
                                                    <div class="header-content">
                                                        <div class="name">
                                                            <div>
                                                                <?php echo $vps->post_title ?>
                                                            </div>
                                                            <div class="month-inner">
                                                                <?php
                                                                    if ( !empty( $mona_billing_cycle_list ) && is_array( $mona_billing_cycle_list ) ) {
                                                                        ?>
                                                                        <select class="month vps-billing-cycle-js" 
                                                                            data-billing_list='<?php echo json_encode( $mona_billing_cycle_list ) ?>'
                                                                            data-original_price="<?php echo $original_price ?>"
                                                                        >
                                                                            <?php
                                                                            foreach ( $mona_billing_cycle_list as $billing_item ) {
                                                                                if ( empty( $first_option_qty ) ) {
                                                                                    $first_option_qty = $billing_item['term_quantity'];
                                                                                }
                                                                                ?>
                                                                                <option value="<?php echo $billing_item['term_quantity'] ?>"><?php echo $billing_item['label'] ?></option>
                                                                                <?php
                                                                            }
                                                                            ?>
                                                                        </select>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                    
                                                                <div class="icon">
                                                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/icon-chevron-down.png" alt="" />
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <?php
                                                        if ( !empty( $mona_billing_cycle_list ) && is_array( $mona_billing_cycle_list ) ) {
                                                            foreach ( $mona_billing_cycle_list as $billing_item ) {
                                                                if ( $billing_item['term_quantity'] == $first_option_qty ) {
                                                                    $price = floor($billing_item['price']/$billing_item['term_quantity']);
                                                                    if ( !empty( $original_price ) ) {
                                                                        $sale_percent = 100 - ceil($price / $original_price * 100);
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        ?>
                                                        <div class="price vps-price-js">
                                                            <div>
                                                                <?php echo number_format($price, 0, '', ',') ?> đ<span class="dash-month">/tháng</span>
                                                            </div>
                                                            <?php
                                                            if ( $sale_percent > 0 ) {
                                                                ?>
                                                                <div class="sale">Tiết kiệm <?php echo $sale_percent; ?>%</div>
                                                                <?php
                                                            }
                                                            ?>
                                                        </div>
                                                    </div>
                                                    <div class="wrap-bg">
                                                        <?php
                                                        if ( !empty( $mona_product_ui_info['info_bg'] ) ) {
                                                            echo wp_get_attachment_image($mona_product_ui_info['info_bg'], 'large');
                                                        } else {
                                                            ?>
                                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/card-header-ware-1.png" alt="" />
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="card-content">
                                                    <?php echo $mona_product_ui_info['info_config'] ?>
                                                    <div class="btn-tag">
                                                        <div class="add-to-cart-js is-loading-btn-2" data-pid='<?php echo $vps->ID ?>' data-billing_cycle='<?php echo $first_option_qty; ?>'>
                                                            <a class="btn-clh btn-clh-blue">
                                                                <span class="txt">
                                                                    Mua gói hosting ngay
                                                                </span>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php
                                    }
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</section>