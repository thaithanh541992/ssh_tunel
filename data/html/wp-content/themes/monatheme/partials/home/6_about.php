<?php

/**
 * Section name: Home About
 * Description: 
 * Author: Monamedia
 * Order: 6
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> session-about-sec">
    <div class="host-about-sec">
        <div class="container">
            <div class="host-about-sec-head-ctn d-flex f-ctn">
                <div class="host-about-sec-head-col col col-6">
                    <div class="host-about-sec-head-content">
                        <div class="sec-desc">
                            <h2 class="host-tt sec-desc-tt fw-600">
                                Mona cung cấp một hệ sinh thái

                                <p class="sec-desc-sub txt-bg bg-host fw-600">
                                    Giúp bạn thành công trên Internet
                                </p>
                            </h2>
                            <p class="sec-desc-about">
                                Bạn thiết kế Website bên A, mua Hosting bên B, thuê
                                Marketing bên C Tại sao phải lằng nhằng như vậy trong khi
                                chỉ cần
                            </p>
                        </div>
                        <div class="the-mona-img">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts//gia-nhap-the-mona.png" alt="" />
                        </div>
                    </div>
                </div>
                <div class="host-about-sec-head-col col col-6">
                    <div class="host-about-sec-head-img">
                        <div class="img">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/host-about-head-1.png" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="head-bg">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/head-bg.png" alt="" />
        </div>
    </div>
</section>