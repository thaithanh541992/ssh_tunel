<?php

/**
 * Section name: Home Statistical
 * Description: 
 * Author: Monamedia
 * Order: 3
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> session-statistical">
    <div class="container">
        <div class="slogan">
            <a href="" class="fw-600 title-slogan btn-clh">
                Giải quyết mọi vấn đề về Hosting chỉ với dịch vụ của Mona Host
                Chúng tôi có giải pháp giúp bạn tự tin thống trị lĩnh vực của mình
                Liên hệ với Mona ngay!
            </a>
            <div class="panda bg-host">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/slogan-panda.png" alt="" />
            </div>
            <div class="line-decor-rederect">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/decor-line-digital.png" alt="" />
            </div>
        </div>
        <div class="content-statistical">
            <div class="content-statistical-left">
                <h2 class="host-tt content-statistical-left-tt">
                    Mona không tính bằng GB

                    <p class="content-statistical-left-sub-tt txt-bg bg-host">
                        Mà tính bằng khách hàng
                    </p>
                </h2>
                <p class="content-statistical-left-desc">
                    Hãy nói lời tạm biệt với các gói hosting phức tạp. Ở Mona Host,
                    <span class="txt-lms fw-600">
                        chúng tôi tính theo số lượng khách hàng
                    </span>
                    mà bạn muốn phục vụ. Chứ không phải dung lượng lưu trữ
                </p>

                <a href="#" class="content-statistical-left-view">
                    <div class="img">
                        <div class="img-detail">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/fix-home/statistical-img-1.png" alt="" />
                        </div>
                        <div class="quote-view">
                            <div class="wrap">
                                <div class="quote-view-img">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/line-quote-statictical.png" alt="" />
                                </div>
                                <p class="quote-view-txt">Xem ngay</p>
                            </div>
                        </div>
                    </div>
                    <div class="txt">
                        Các dự toán cho Website bán hàng có lượt truy cập từ 1.000 đến
                        1 triệu / tháng
                    </div>
                </a>
            </div>
            <div class="content-statistical-right">
                <div class="statistical-group">
                    <div class="statistical-traffic-1">
                        <div class="main-traffic">
                            <div class="">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-100.png" alt="" />
                            </div>
                            <div class="zoom-plus">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-zoom-100.png" alt="" />
                            </div>
                            <div class="line-increase">
                                <div class="wrap">
                                    <div class="img">
                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/line-increase-100.png" alt="" />
                                    </div>
                                    <p class="txt-increase">Tăng 100%</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="statistical-traffic-2">
                        <div class="main-traffic">
                            <div>
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistiacal-224.png" alt="" />
                            </div>
                            <div class="zoom-plus">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-zoom-224.png" alt="" />
                            </div>
                            <div class="line-increase">
                                <div class="wrap">
                                    <div class="img">
                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/line-increase-224.png" alt="" />
                                    </div>
                                    <p class="txt-increase">Tăng 224%</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="statistical-traffic">
        <div class="statistical-traffic-bg">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-building-1.png" alt="" />
        </div>
        <div class="panda-rocket">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-panda-rocket.png" alt="" />
        </div>

        <div class="traffic">
            <div class="container">
                <div class="traffic-content">
                    <h2 class="host-tt traffic-content-tt">
                        Hệ thống Hosting Mona
                        <p class="traffic-content-sub-tt txt-bg bg-lms">
                            Có khả năng chịu tải cao
                        </p>
                    </h2>
                    <p class="traffic-content-desc">
                        Chúng tôi hiểu bạn cần
                        <span class="fw-600"><u>“tự tin”</u></span> triển khai những
                        dự án có tải trọng cao, đó là lý do tại sao các máy chủ của
                        chúng tôi được thiết kế để
                        <span class="fw-600"><u>hoạt động với hiệu suất cao mà không phải lo lắng về độ
                                trễ hoặc thời gian ngừng hoạt động</u></span>
                    </p>
                </div>
            </div>
        </div>
        <div class="traffic-decor">
            <div class="traffic-decor-img">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-trafic-1.png" alt="" />
            </div>
            <div class="traffic-decor-img">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-trafic-3.png" alt="" />
            </div>
            <div class="traffic-decor-img">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-trafic-2.png" alt="" />
            </div>
        </div>
        <div class="traffic-decor">
            <div class="traffic-decor-img-line">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-trafic-line-1.png" alt="" />
            </div>
            <div class="traffic-decor-img-line">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-trafic-line-2.png" alt="" />
            </div>
            <div class="traffic-decor-img-line">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-trafic-line-3.png" alt="" />
            </div>
        </div>
        <div class="communication">
            <div class="communication-content">
                <h2 class="host-tt communication-content-tt">
                    Nền tảng vững chắc
                    <p class="communication-content-sub-tt txt-bg bg-host">
                        Mở rộng không giới hạn
                    </p>
                </h2>
                <p class="communication-content-desc">
                    Nền tảng của chúng tôi cho phép bạn
                    <span class="txt-lms fw-600">mở rộng quy mô khách hàng</span>
                    một cách
                    <span class="txt-digital fw-600">nhanh chóng và dễ dàng</span>,
                    cho phép bạn tiếp đón bao nhiêu khách hàng tuỳ thích - vì vậy
                    bạn có thể bắt đầu thấy
                    <span class="txt-nhtq fw-600">lợi nhuận gần như ngay lập tức!</span>
                </p>

                <div class="quote-wrap">
                    <div class="quote-uptime">
                        <div class="quote quote-bg-software quote-bottom-right">
                            Uptime 99,9%
                        </div>
                    </div>
                    <div class="quote-backup">
                        <div class="quote quote-bg-nhtq bg-digital quote-bottom-right">
                            CPU Siêu tốc
                        </div>
                    </div>

                    <div class="quote-banwidth">
                        <div class="quote quote-bg-host">Bandwidth 20Gbps</div>
                    </div>
                    <div class="quote-ssd">
                        <div class="quote quote-bg-lms">SSD NVME U.2</br>
                            x20 tốc độ đọc/ghi</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="statistical-img">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/panda-group-cpu.png" alt="" />
    </div>
    <div class="content-support">
        <div class="container">
            <div class="the-mona d-flex f-between">
                <div class="the-mona-left col-5">
                    <h2 class="host-tt tt">
                        Hosting tại MONA

                        <p class="sub-tt txt-bg bg-host">Hỗ trợ tạo nên khác biệt</p>
                    </h2>
                    <p class="desc">
                        <span class="txt-lms fw-600">Dịch vụ khách hàng tốt nhất</span> luôn sẵn sàng để giúp bạn <span class="txt-blue fw-600">giải quyết mọi vấn đề.</span> Dễ dàng nâng cấp hoặc hạ cấp theo nhu cầu. Nhận
                        hỗ trợ tận tình từ Mona bất cứ lúc nào mà bạn cần!
                    </p>
                </div>
                <div class="the-mona-right col-6">
                    <div class="the-mona-right-img-main">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/10x50.png" alt="" />
                    </div>
                </div>
                <div class="line">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/support-line.png" alt="" />
                </div>
            </div>

            <div class="support d-flex f-between">
                <div class="left col-7">
                    <div class="main-img">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/THEMON-1.png" alt="" />
                    </div>
                    <!-- <div class="quote-wrap">
                <div class="performance">
                  <div class="quote quote-bg-lms quote-bottom-right">
                    Hiệu suất cao
                  </div>
                </div>
                <div class="setting">
                  <div class="quote quote-bg-nhtq quote-bottom-right">
                    Cài đặt,quản lý máy chủ dễ dàng
                  </div>
                </div>
                <div class="config">
                  <div class="quote quote-bg-host quote-bottom-right">
                    Cấu hình theo</br> nhu cầu
                  </div>
                </div>
                <div class="support">
                  <div class="quote quote-bg-software">
                    Hỗ trợ kỹ thuật 24/7/365
                  </div>
                </div>
                <div class="price">
                  <div class="quote quote-bg-digital">Giá cả hợp lý</div>
                </div>
              </div> -->
                </div>
                <div class="right col-5">
                    <div class="content">
                        <h2 class="host-tt tt">
                            Sỡ hữu Hosting tốt nhất

                            <p class="sub-tt txt-bg bg-host">
                                Tận hưởng tối đa tài nguyên
                            </p>
                        </h2>
                        <p class="desc">
                            Với dịch vụ
                            <span class="txt-lms fw-700">Premium Cloud VPS tại MONA.Host</span>
                            bạn sẽ nhận được rất nhiều thứ. Bạn sẽ cảm nhận được sự khác
                            biệt và hài lòng với dịch vụ của chúng tôi
                        </p>
                    </div>
                </div>
                <div class="line">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/support-line-2.png" alt="" />
                </div>
                <div class="quote-consult">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/quote-consult.png" alt="" />
                </div>
            </div>
        </div>
    </div>
</section>