<?php
/**
 * Section name: Home Expert
 * Description:  
 * Author: Monamedia
 * Order: 13
 */
?>
<?php
$mona_feartured_posts = get_field('mona_feartured_posts');
if ( !empty( $mona_feartured_posts ) && !empty( $mona_feartured_posts['posts_list'] ) ) {
    ?>
    <section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> session-expert">
        <div class="container">
            <div class="header-exfert">
                <?php
                    if ( $mona_feartured_posts['posts_title'] ) {
                        ?><p class="txt-header fw-600"><?php echo $mona_feartured_posts['posts_title'] ?></p><?php
                    }
                    if ( $mona_feartured_posts['posts_subtitle'] ) {
                        ?><p class="txt-header fw-600 txt-bg bg-nhtq"><?php echo $mona_feartured_posts['posts_subtitle'] ?></p><?php
                    }
                ?>
            </div>
            <div class="content-expert">
                <div class="list-item d-flex f-between">
                    <?php
                    foreach( $mona_feartured_posts['posts_list'] as $key => $post_id ) {
                        ?>
                        <div class="item col-<?php echo $key == 0 || $key == 5 ? '6' : '3' ?>">
                            <a href="<?php echo get_the_permalink( $post_id ); ?>" class="main-img">
                                <?php echo get_the_post_thumbnail( $post_id, 'large' ); ?>
                            </a>
                        </div>
                        <?php
                    }
                    ?>
                </div>
            </div>
            <div class="footer">
                <a class="btn-clh">
                    <span class="txt">
                        Tất cả bài viết
                    </span>
                </a>
            </div>
        </div>
    </section>
    <?php
}
?>