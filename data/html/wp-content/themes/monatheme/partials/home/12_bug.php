<?php

/**
 * Section name: Home Bug
 * Description:  
 * Author: Monamedia
 * Order: 12
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> session-bug">
    <div class="session-bug-wrap">
        <div class="main-img">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/session-bug-bg-1.png" alt="" />
        </div>
        <div class="content">
            <div class="container">
                <h2 class="host-tt title">
                    Ở Mona còn có một thú vui khác

                    <p class="sub-title txt-bg bg-host">
                        Đó là combat qua hệ thống
                    </p>
                </h2>
                <div class="quote-content">
                    <div class="txt-quote txt-quote-1">
                        <div class="txt-quote-wrap">
                            <div class="txt-quote-img">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/session-bug-quote-1.png" alt="" />
                            </div>
                            <div class="txt fw-400">
                                <p class="">
                                    Bugs kìa anh em. <br />
                                    Giải quyết nó
                                </p>
                            </div>
                        </div>
                    </div>

                    <div class="txt-quote txt-quote-2">
                        <div class="txt-quote-wrap">
                            <div class="txt-quote-img">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/session-bug-quote-2.png" alt="" />
                            </div>
                            <div class="txt fw-400">
                                <p class="">
                                    Dăm ba con Bugs.
                                    <br />
                                    Để anh xịt chết nó
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>