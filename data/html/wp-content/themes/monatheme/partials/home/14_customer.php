<?php

/**
 * Section name: Home Customer
 * Description:  
 * Author: Monamedia
 * Order: 14
 */
?>

<section class="home-session-customer">
    <div class="session-make-money">
        <div class="container">
            <div class="make-money-wrap">
                <div class="content">
                    <div class="host-tt sec-tt txt-center fw-600">
                        <h2>
                            Dịch vụ hosting tối ưu nhất cho doanh nghiệp

                            <p class="txt-bg-wrap txt-bg bg-host">
                                Giải quyết mọi vấn đề đối với website của bạn
                            </p>
                        </h2>
                    </div>
                    <div class="txt-sec-desc">
                        <p>
                            Mona cung cấp cho khách hàng của mình những dịch vụ về
                            Hosting tốt nhất Việt Nam bởi vì
                        </p>
                    </div>
                    <div class="desc-24 fw-600">
                        Chúng tôi tập hợp những đội ngũ và chuyên gia
                    </div>
                    <div class="txt-x d-flex">
                        <p>
                            <span class="txt-bg bg-digital txt-bg-ani add-active-js active">
                                <span class="txt">7+</span>
                            </span>
                        </p>
                        <p class="txt-x-sub">
                            Năm kinh nghiệm<br />
                            Cân luôn System
                        </p>
                    </div>
                    <p class="txt-center txt-sec-desc">
                        Chúng tôi không chỉ cung cấp dịch vụ hosting chất lượng mà còn
                        giúp website của bạn
                    </p>

                    <p class="txt-decor fw-600 txt-center txt-upper txt-lms txt-round">
                        <span>Hái ra tiền</span>
                        <span class="quote-decor">
                            <svg class="" width="206" height="78" viewBox="0 0 206 78" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M67.2584 74.485C106.584 74.485 149.4 75.4618 185.208 56.6043C194.659 51.6275 209.459 42.6947 202.409 30.2538C197.959 22.4 188.156 18.0148 180.189 14.7258C166.107 8.91218 150.889 5.74872 135.801 4.00785C104.907 0.443072 69.2724 1.01959 39.6531 11.5366C28.6167 15.4553 -3.00443 26.0351 2.6369 43.5859C7.37062 58.313 24.263 65.4092 37.98 68.8385C64.1458 75.3799 92.2163 75.6227 119.018 76.3672" stroke="#F41E92" stroke-width="3" stroke-linecap="round" />
                            </svg>
                        </span>
                    </p>
                    <div class="mk-completed-btn">
                        <a href="#" class="btn-clh btn-mess">
                            Sở hữu năng lực từ những chuyện gia tại Mona<br />
                            Tiết kiệm 3 năm phát triển bằng cách tham gia với chúng tôi
                        </a>
                    </div>
                </div>

                <div class="bg-left">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/session-make-money-bg-left.png" alt="" />
                </div>
                <div class="bg-right">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/session-make-money-bg-right.png" alt="" />
                </div>
                <div class="bg-panda">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/session-make-money-panda.png" alt="" />
                </div>
                <div class="bg-Hy">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/THEMON-2.png" alt="" />
                </div>
                <div class="decor-zalo-left">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/session-make-money-zalo-left.png" alt="" />
                </div>
                <div class="decor-zalo-right">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/session-make-money-zalo-right.png" alt="" />
                </div>
            </div>
        </div>
    </div>
</section>