<?php

/**
 * Section name: Home Mr Thoai
 * Description:  
 * Author: Monamedia
 * Order: 16
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> home-session-mr-thoai">
    <div class="background-main">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/home-session-mr-thoai.png" alt="" />
    </div>
    <div class="container main-content d-flex f-between">
        <div class="col-7"></div>
        <div class="right-contenet col-4">
            <div class="header">
                <h2 class="host-tt header-title fw-600">
                    Cách vận hành của
                    <span class="txt-bg bg-host"> những "thợ máy lành nghề" </span>
                </h2>
            </div>
            <div class="about-content">
                <p>
                    “Ngay cả khi chúng tôi mở rộng và quy mô lớn hơn, ưu tiên của
                    chúng tôi vẫn là
                    <span class="fw-600">đảm bảo rằng khách hàng của chúng tôi có những gì họ muốn và
                        hài lòng nhất.</span>
                    Và tôi tin rằng với MONA Host, chúng tôi có tài nguyên tốt nhất
                    để
                    <span class="fw-600">phát triển mọi loại hình doanh nghiệp</span>”
                </p>
                <p class="txt-signature-i-ciel txt-signature">Mr. Thoại Võ</p>
            </div>
        </div>
    </div>
</section>