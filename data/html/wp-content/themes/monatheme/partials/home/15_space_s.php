<?php

/**
 * Section name: Home Space S
 * Description:  
 * Author: Monamedia
 * Order: 15
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> clhser space-s horizontal-section-p style-pri">
    <div class="clhser-w horizontal-section">
        <div class="clhser-sticky">
            <div class="clhser-list horizontal-scroll">
                <div class="clhser-i">
                    <div class="container">
                        <div class="clhser-i-inner">
                            <div class="clhser-l">
                                <div class="clhser-tt">
                                    <h2 class="host-tt">
                                        Dịch vụ Hosting của Mona

                                        <p class="host-tt hl">Khiến 100% khách hàng tin tưởng</p>
                                    </h2>
                                </div>
                                <div class="clhser-in">
                                    <div class="clhser-in-tt">
                                        Photocopy Thiên Phú
                                    </div>
                                    <div class="clhser-ad">
                                        <a href="" class="clhser-ad-i">
                                            <span class="icon">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/loca.svg" alt="">
                                            </span>
                                            <span class="txt">
                                                Hồ Chí Minh
                                            </span>
                                        </a>
                                        <a href="" class="clhser-ad-i">
                                            <span class="icon">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/dnl.svg" alt="">
                                            </span>
                                            <span class="txt">
                                                Doanh nghiệp lớn
                                            </span>
                                        </a>
                                    </div>
                                    <p class="clhser-in-des">
                                        Khách dính DDoS từ hosting cũ được Mona tư vấn hướng giải <span class="clhser-in-line">
                                            quyết trong vòng 36h
                                        </span>
                                    </p>
                                    <ul class="clhser-in-l">
                                        <li class="clhser-in-i">Cử System Engineer ngày đêm kiểm tra, xác định cấu hình DNS để chặn bots
                                        </li>
                                        <li class="clhser-in-i">Giới hạn số lượng yêu cầu/một khoảng thời gian, cân bằng tải và chuyển
                                            hướng lưu lượng</li>
                                        <li class="clhser-in-i">Xoá hơn 80.000 trang bị ảnh hưởng bới DDoS, đẩy mạnh tài nguyên để từ
                                            khoá trở lại Top đầu</li>
                                    </ul>
                                </div>
                                <div class="clhser-btn">
                                    <a class="btn-clh">
                                        <span class="txt">
                                            Chi tiết dự án
                                        </span>
                                        <span class="icon">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/right.svg" alt="">
                                        </span>
                                    </a>
                                    <a class="btn-clh btn-clh-transparent">
                                        <span class="txt">
                                            Xem trang web
                                        </span>
                                        <span class="icon">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/right-01.svg" alt="">
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="clhser-r">
                                <p class="clhser-r-des">
                                    <span class="line-01">Chúng tôi luôn tự hào</span> mỗi khi nhắc đến những dịch vụ của Mona.
                                    Vì đó là điều làm nên <span class="line">
                                        sự tin tưởng tuyệt đối của khách hàng
                                    </span> vào chúng tôi.
                                </p>
                                <div class="clhser-r-inner">
                                    <div class="image">
                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhser-01.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clhser-decor x1">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhser-decor-01.png" alt="">
                    </div>
                    <div class="clhser-decor x2">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhser-decor-02.png" alt="">
                    </div>
                </div>
                <div class="clhser-i">
                    <div class="container">
                        <div class="clhser-i-inner">
                            <div class="clhser-l">
                                <div class="clhser-tt">
                                    <h3 class="host-tt">Levents - Local brand Việt </h3>
                                </div>
                                <div class="clhser-in">
                                    <div class="clhser-ad">
                                        <a href="" class="clhser-ad-i">
                                            <span class="icon">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/loca.svg" alt="">
                                            </span>
                                            <span class="txt">
                                                Hồ Chí Minh
                                            </span>
                                        </a>
                                        <a href="" class="clhser-ad-i">
                                            <span class="icon">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/dnl.svg" alt="">
                                            </span>
                                            <span class="txt">
                                                Doanh nghiệp lớn
                                            </span>
                                        </a>
                                    </div>
                                    <p class="clhser-in-des">
                                        Local brand hàng đầu Việt Nam có <span class="line-pri">nguy cơ mất doanh thu</span> Online vì
                                        đối mặt với tình trạng <span class="line-second">
                                            quá tải máy chủ
                                        </span> do lượng khách áp đảo hạ tầng máy chủ
                                    </p>
                                    <ul class="clhser-in-l">
                                        <li class="clhser-in-i">Thời gian load chậm, web đứng, lag, bay màu khi doanh nghiệp chạy sales
                                        </li>
                                        <li class="clhser-in-i">Chuyển hạ tầng lên MONA Cloud và cân bằng tải để phân phối lưu lượngg
                                        </li>
                                        <li class="clhser-in-i">Cài đặt và cấu hình hệ thống và cảnh báo trước khi có sự cố quá tải xảy
                                            ra để kịp thời xử lý </li>
                                    </ul>
                                </div>
                                <div class="clhser-btn">
                                    <a class="btn-clh">
                                        <span class="txt">
                                            Chi tiết dự án
                                        </span>
                                        <span class="icon">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/right.svg" alt="">
                                        </span>
                                    </a>
                                    <a class="btn-clh btn-clh-transparent">
                                        <span class="txt">
                                            Xem trang web
                                        </span>
                                        <span class="icon">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/right-01.svg" alt="">
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="clhser-r">
                                <div class="clhser-r-inner">
                                    <div class="image">
                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhser-02.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clhser-i">
                    <div class="container">
                        <div class="clhser-i-inner">
                            <div class="clhser-l">
                                <div class="clhser-tt">
                                    <h3 class="host-tt">Giang huy logistic</h3>
                                </div>
                                <div class="clhser-in">
                                    <div class="clhser-ad">
                                        <a href="" class="clhser-ad-i">
                                            <span class="icon">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/loca.svg" alt="">
                                            </span>
                                            <span class="txt">
                                                Hồ Chí Minh
                                            </span>
                                        </a>
                                        <a href="" class="clhser-ad-i">
                                            <span class="icon">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/dnl.svg" alt="">
                                            </span>
                                            <span class="txt">
                                                Doanh nghiệp lớn
                                            </span>
                                        </a>
                                    </div>
                                    <p class="clhser-in-des">
                                        Doanh nghiệp nhập hàng với hơn <span class="clhser-in-line">3000 đơn hàng mỗi tuần
                                        </span> đối mặt với thách thức quản lý khối lượng data khổng lồ, hàng nghìn truy vấn mỗi ngày
                                    </p>
                                    <ul class="clhser-in-l">
                                        <li class="clhser-in-i">Khối lượng dữ liệu cực lớn liên quan đến chuỗi cung ứng</li>
                                        <li class="clhser-in-i">Thời gian phản hồi cực lâu lên đến vài phút với những truy vấn phức tạp
                                            và cần xử lý nhiều dữ liệu</li>
                                        <li class="clhser-in-i">Mona lược đồ hoá cơ sở dữ liệu và tối ưu các tuy vấn để đạt thời gian
                                            phản hồi nhanh hơn, <span class="violet">chỉ mất 0.3ms</span></li>
                                    </ul>
                                </div>
                                <div class="clhser-btn">
                                    <a class="btn-clh">
                                        <span class="txt">
                                            Chi tiết dự án
                                        </span>
                                        <span class="icon">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/right.svg" alt="">
                                        </span>
                                    </a>
                                    <a class="btn-clh btn-clh-transparent">
                                        <span class="txt">
                                            Xem trang web
                                        </span>
                                        <span class="icon">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/right-01.svg" alt="">
                                        </span>
                                    </a>
                                </div>
                            </div>
                            <div class="clhser-r">
                                <div class="clhser-r-inner">
                                    <div class="image">
                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhser-03.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</section>