<?php

/**
 * Section name: Home Solar System
 * Description:  
 * Author: Monamedia
 * Order: 11
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> session-solar-system">
    <div class="main-bg">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/solar-system-1.png" alt="" />
    </div>
    <div class="main-bg-cus">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/fix-home/cus-dolar.png" alt="">
    </div>

    <div class="header">
        <div class="bg-cloud">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/solution-cloud.png" alt="" />
        </div>
        <div class="content">
            <h2 class="host-tt content-tt">
                Để khắc phục vấn đề mà khách hàng gặp phải
                <p class="content-sub txt-bg bg-host">
                    Chúng tôi đưa ra nhiều giải pháp triệt để
                </p>
            </h2>
        </div>
        <div class="solar-cus">
            <div class="quote-wrap">
                <div class="quote-uptime">
                    <div class="quote quote-bg-software quote-bottom-right">
                        Uptime 99,9%
                    </div>
                </div>
                <div class="quote-backup">
                    <div class="quote quote-bg-nhtq quote-bottom-right">
                        Backup dữ liệu <br>mỗi ngày
                    </div>
                </div>
                <div class="quote-cpu">
                    <div class="quote quote-bg-digital">CPU Siêu tốc</div>
                </div>
                <div class="quote-banwidth">
                    <div class="quote quote-bg-host">Bandwidth 20Gbps</div>
                </div>
                <div class="quote-ssd">
                    <div class="quote quote-bg-lms">SSD NVME U.2<br>
                        x20 tốc độ đọc/ghi</div>
                </div>
                <div class="quote-ssd">
                    <div class="quote quote-bg-digital">SSD NVME U.2<br>
                        x20 tốc độ đọc/ghi</div>
                </div>
            </div>
        </div>
    </div>
    <div class="footer">
        <div class="footer-quote">
            <div class="img-quote">
                <div class="footer-quote-br">
                </div>
                <a href="" class="btn-clh btn-m quote-content">
                    <span class="about fw-400 txt-center txt-white">
                        Còn chần chừ gì nữa mà không sử dụng ngay dịch vụ bảo mật
                        Wordpress độc quyền từ MONA.Host. Bạn sẽ không cần lo lắng về
                        vấn đề an toàn website nữa
                    </span>
                    <span class="fw-600 txt-center txt-white">
                        VÌ TẤT CẢ CÓ MONA LO
                    </span>
                </a>
            </div>
        </div>
    </div>
</section>