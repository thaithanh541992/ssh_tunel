<?php

/**
 * Section name: Home Support
 * Description: 
 * Author: Monamedia
 * Order: 18
 */
?>
<section  id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> hclhpr">
    <div class="container">
        <div class="hclhpr-w">
            <div class="hclhpr-top">
                <div class="hclhpr-t">
                    <div class="hclhpr-tt hl-white">
                        <h2 class="host-tt clcenter txt-center">
                            Đừng để vấn đề về hosting
                            <span class="bg-nhtq txt-center tt-df txt-bg">
                                Làm sụp đổ cơ ngơi của mình
                            </span>
                        </h2>
                        <p class="hclhpr-des txt-center  hl-white fw-600">
                            Mona chúng tôi luôn tự hào về việc hỗ trợ khách hàng của mình hết sức tận tình.
                            Luôn có nhân sự phục vụ 24/24. Chỉ cần bạn có câu hỏi chúng tôi luôn sẵn lòng trả lời.
                        </p>
                    </div>
                    <div class="hclhpr-c">
                        <div class="hclhpr-img">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/fix-home/hclhpr-img-1.png" alt="">
                        </div>
                    </div>
                </div>

            </div>
            <div class="hclhpr-bot">
                <div class="hclhpr-b">
                    <div class="clhpar clhpar-cus">
                        <div class="clhpar-w">
                            <div class="clhpar-top">
                                <div class="clhpar-in panda">
                                    <div class="dvs-solve-top-mask" id="dvs-completed-js">
                                        <div id="dvs-solve-top-list-js" class="dvs-solve-top-list dvs-solve-top-list-js">
                                            <div class="dvs-solve-top-item">
                                                <div id="dvs-solve-js" class="dvs-solve-top-item-wrap dvs-solve-js">
                                                    <span class="txt-bg bg-digital">
                                                        TOP 1
                                                    </span>
                                                    <div class="logo-bg">
                                                        <a href="#" class="logo">
                                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/logo/software-less-logo.svg" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="dvs-solve-top-item-name">
                                                        Hệ thống
                                                        phần mềm quản lý
                                                    </div>
                                                    <div class="dvs-solve-top-item-img">
                                                        <div class="img">
                                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/dvs/panda-boss-1.png" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="dvs-solve-top-item-bg">
                                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/dvs/dvs-solve-top-bg-1.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dvs-solve-top-item">
                                                <div id="dvs-solve-js" class="dvs-solve-top-item-wrap dvs-solve-js">
                                                    <span class="txt-bg bg-host">
                                                        TOP 1
                                                    </span>
                                                    <div class="logo-bg">
                                                        <a href="#" class="logo">
                                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/logo/host-less-logo.svg" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="dvs-solve-top-item-name">
                                                        Web Hosting Giải pháp hạ tầng
                                                    </div>
                                                    <div class="dvs-solve-top-item-img">
                                                        <div class="img">
                                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/dvs/panda-boss-2.png" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="dvs-solve-top-item-bg">
                                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/dvs/dvs-solve-top-bg-2.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dvs-solve-top-item">
                                                <div id="dvs-solve-js" class="dvs-solve-top-item-wrap dvs-solve-js">
                                                    <span class="txt-bg bg-software">
                                                        TOP 1
                                                    </span>
                                                    <div class="logo-bg">
                                                        <a href="#" class="logo">
                                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/logo/media-less-logo.svg" alt="">
                                                        </a>
                                                    </div>
                                                    <div class="dvs-solve-top-item-name">
                                                        Dịch vụ
                                                        Thiết kế website
                                                    </div>
                                                    <div class="dvs-solve-top-item-img">
                                                        <div class="img">
                                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/dvs/panda-boss-3.png" alt="">
                                                        </div>
                                                    </div>
                                                    <div class="dvs-solve-top-item-bg">
                                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/dvs/dvs-solve-top-bg-3.png" alt="">
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="clhpar-con">
                                        <div class="clhpar-mess">
                                            Với Mona, bạn chỉ cần quan tâm chuyện kinh </br>doanh của bạn. Còn lại chúng tôi lo.
                                        </div>
                                        <p class="clhpar-in-txt">
                                            <span class="blold">Hợp tác với Mona bạn được nhiều hơn là mất.</span> Với sự thống
                                            trị của Mona hiện nay, chúng tôi cam kết sẽ luôn <span class="line">
                                                ưu tiên khách hàng của mình lên đầu.
                                            </span>
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
        <div class="hclhpr-bot-mess btn-clh">
            <div class="txt txt-center hl-white">
                Bạn đang bế tắc vì vấn đề Hosting? </br>Đến với Mona để chúng tôi có thể hỗ trợ cho</br> bạn kịp thời. Tránh
                lợi bất cập hại.
            </div>
        </div>
    </div>
</section>