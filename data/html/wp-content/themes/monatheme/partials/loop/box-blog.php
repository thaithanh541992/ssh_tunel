<?php 
global $post;
$mona_post_read = get_field('mona_post_read', $post);
?>
<div class="blogr-inner">
    <?php if( !is_author() ){ ?>
    <div class="blogr-top">
        <?php 
        $author_id = get_post_field( 'post_author', $post->ID );
        $author = get_userdata($author_id);
        if( !empty( $author ) ){
            $author_avatar = get_field('mona_user_avatar', $author);
            $author_display = get_field('mona_user_display_name', $author);
            ?>
        <div class="blogr-top-gr">
            <span class="icon">
                <?php if( empty( $author_avatar ) ){ ?>
                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-user.png" alt="">
                <?php }else{ ?>
                    <?php echo wp_get_attachment_image($author_avatar, 'full'); ?>
                <?php } ?>
            </span>
            <span class="blogr-author">
                <?php echo __('Bởi ', 'monamedia'); ?><a href="<?php echo get_author_posts_url($author_id); ?>" class="text"><?php echo ( !empty( $author_display ) ) ? $author_display : $author->display_name; ?></a>
            </span>
        </div>
        <?php } ?>
        <span class="text"><?php echo !empty( $mona_post_read ) ? $mona_post_read : __('10 phút đọc', 'monamedia'); ?></span>
    </div>
    <?php } ?>
    <div class="blogr-img">
        <div class="inner">
            <a href="<?php echo get_the_permalink( $post ); ?>" target="_blank">
                <?php 
                $thumbnail = get_the_post_thumbnail($post, 'full');
                if( !empty( $thumbnail ) ){
                    echo $thumbnail;
                }else{ ?>
                    <img src="<?php echo get_template_directory_uri() ?>/public/helpers/images/global-thumbnail-mona.png" alt="">
                <?php } ?>
            </a>
        </div>
    </div>
    <div class="blogr-content">
        <a href="<?php echo get_the_permalink( $post ); ?>" class="blogr-link" target="_blank">
            <?php echo get_the_title( $post ); ?>
        </a>
        <div class="blogt-link">
            <a href="<?php echo get_the_permalink( $post ); ?>" class="text" target="_blank">
                <?php echo __('Xem thêm', 'monamedia'); ?>
            </a>
            <i class="fas fa-chevron-right icon"></i>
        </div>
        <?php 
        $post_tags = get_the_terms($post, 'post_tag');
        if( !empty( $post_tags ) ){
        ?>
        <div class="blogr-tag">
            <div class="blogr-tag-list">
                <?php foreach ($post_tags as $key => $tag) { ?>
                <div class="blogr-tag-item">
                    <a class="blogr-tag-link" href="<?php echo get_term_link( $tag ); ?>">
                        <?php echo $tag->name; ?>
                    </a>
                </div>
                <?php } ?>
            </div>
        </div>
        <?php } ?>
    </div>
</div>