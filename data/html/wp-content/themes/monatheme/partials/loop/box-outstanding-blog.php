<?php global $post; ?>
<div class="blogtc2-slide">
    <div class="blogtc2-pos">
        <div class="blogtc2-img">
            <div class="blogtc2-img-ani">
                <div class="inner">
                    <a href="<?php echo get_the_permalink( $post ); ?>" target="_blank">
                        <?php 
                        $thumbnail = get_the_post_thumbnail($post, 'full');
                        if( !empty( $thumbnail ) ){
                            echo $thumbnail;
                        }else{ ?>
                            <img src="<?php echo get_template_directory_uri() ?>/public/helpers/images/global-thumbnail-mona.png" alt="">
                        <?php } ?>
                    </a>
                </div>
            </div>
        </div>
        <div class="blogtc2-box">
            <div class="blogtc2-box-ani">
                <div class="blogtc2-box-py">
                    <div class="blogtc2-box-content">
                        <?php
                        $primary = get_post_primary_taxonomy( $post->ID , 'category' );
                        if( !empty( $primary['primary_category'] ) ){
                            $parent = get_taxonomy_term_root( $primary['primary_category'] );
                            if( !empty( $parent ) ){ ?>
                        <div class="blogtc2-box-tag">
                            <a href="<?php echo get_term_link( $parent ); ?>">
                                <?php echo $parent->name; ?>
                            </a>
                        </div>
                        <?php } } ?>

                        <h3 class="title">
                            <a href="<?php echo get_the_permalink( $post ); ?>" target="_blank">
                                <?php echo get_the_title( $post ); ?>
                            </a>
                        </h3>

                        <?php 
                        $author_id = get_post_field( 'post_author', $post->ID );
                        $author = get_userdata($author_id);
                        if( !empty( $author ) ){
                            $author_avatar              = get_field('mona_user_avatar', $author);
                            $author_display             = get_field('mona_user_display_name', $author);
                            $author_display_description = get_field('mona_user_display_description', $author);
                            ?>
                        <div class="blogtc2-box-gr">
                            <div class="img-cir">
                                <?php if( empty( $author_avatar ) ){ ?>
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-user.png" alt="">
                                <?php }else{ ?>
                                    <?php echo wp_get_attachment_image($author_avatar, 'full'); ?>
                                <?php } ?>
                            </div>
                            <div class="gr-txt">
                                <p class="name"><?php echo ( !empty( $author_display ) ) ? $author_display : $author->display_name; ?></p>
                                <?php if( !empty( $author_display_description ) ){ ?>
                                <p class="level">
                                    <?php echo $author_display_description; ?>
                                </p>
                                <?php } ?>
                            </div>
                        </div>
                        <?php } ?>
                        <?php 
                        $mona_post_viewed = get_field('mona_post_viewed');
                        $mona_post_favorited = get_field('mona_post_favorited');
                        $mona_post_shared = get_field('mona_post_shared'); ?>
                        <?php if( !empty( $mona_post_viewed ) || !empty( $mona_post_favorited ) || !empty( $mona_post_shared ) ){ ?>
                        <div class="blogtc2-bot">
                            <?php if( !empty( $mona_post_viewed ) ){ ?>
                            <div class="blogtc2-bot-gr">
                                <span class="icon">
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-eye.png"
                                        alt="">
                                </span>
                                <span class="text"><?php echo $mona_post_viewed; ?></span>
                            </div>
                            <?php } ?>

                            <?php if( !empty( $mona_post_favorited ) ){ ?>
                            <div class="blogtc2-bot-gr">
                                <span class="icon">
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-heart.png"
                                        alt="">
                                </span>
                                <span class="text"><?php echo $mona_post_favorited; ?></span>
                            </div>
                            <?php } ?>

                            <?php if( !empty( $mona_post_shared ) ){ ?>
                            <div class="blogtc2-bot-gr">
                                <span class="icon">
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-share.png"
                                        alt="">
                                </span>
                                <span class="text"><?php echo $mona_post_shared; ?></span>
                            </div>
                            <?php } ?>
                        </div>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>