<?php global $post; ?>
<div class="blogpc3-slide">
    <div class="blogpc3-flex">
        <div class="blogpc3-col">
            <div class="blogpc3-content">
                <div class="blogpc3-content-tag">
                    <span class="icon">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-star.png" alt="">
                    </span>
                    <span class="text">Feature blog</span>
                </div>
                <a href="<?php echo get_the_permalink($post); ?>" class="link" target="_blank">
                    <?php echo get_the_title($post); ?>
                </a>
                <p class="des">
                    <?php echo get_the_excerpt($post); ?>
                </p>
                <div class="blogpc3-gr">
                    <div class="blogt-link">
                        <a href="<?php echo get_the_permalink($post); ?>" class="text" target="_blank"><?php echo __('Xem thêm', 'monamedia'); ?></a>
                        <i class="fas fa-chevron-right icon"></i>
                    </div>
                    <?php 
                    $author_id = get_post_field( 'post_author', $post->ID );
                    $author = get_userdata($author_id);
                    if( !empty( $author ) ){
                        $author_avatar              = get_field('mona_user_avatar', $author);
                        $author_display             = get_field('mona_user_display_name', $author);
                        $author_display_description = get_field('mona_user_display_description', $author);
                        ?>
                    <span class="text">
                        <?php echo __('Bởi ', 'monamedia'); ?> <span class="c-pri"><?php echo !empty( $author_display ) ? $author_display : $author->display_name; ?></span><?php echo __(' 5 phút đọc', 'monamedia'); ?>
                    </span>
                    <?php } ?>
                </div>
            </div>
        </div>
        <div class="blogpc3-col">
            <div class="blogpc3-img">
                <div class="inner">
                    <a href="<?php echo get_the_permalink( $post ); ?>" target="_blank">
                        <?php 
                        $thumbnail = get_the_post_thumbnail($post, 'full');
                        if( !empty( $thumbnail ) ){
                            echo $thumbnail;
                        }else{ ?>
                            <img src="<?php echo get_template_directory_uri() ?>/public/helpers/images/global-thumbnail-mona.png" alt="">
                        <?php } ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>