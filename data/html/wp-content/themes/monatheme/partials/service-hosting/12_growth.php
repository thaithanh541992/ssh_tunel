<?php

/**
 * Section name: Service Hosting Growth
 * Description: 
 * Author: Monamedia
 * Order: 12
 */
?>
<section class="clhdeve">
    <div class="container">
        <div class="clhdeve-w">
            <div class="clhdeve-t">
                <div class="clhdeve-tt">
                    <h3 class="host-tt">Từ thất vọng đến đưa ra giải pháp
                        <p class="host-tt hl">MONA phát triển gói Hosting cân mọi vấn đề</p>
                    </h3>
                </div>
                <p class="clhdeve-txt">
                    Với nhiều năm kinh nghiệm trong mảng Hosting, chúng tôi tự tin khẳng định
                    hệ thống <span class="line">
                        Hosting độc quyền của Mona là độc nhất, hiệu quả nhất cho doanh nghiệp
                    </span>
                </p>
            </div>
            <div class="clhdeve-b">
                <div class="clhdeve-inner">
                    <div class="clhdeve-img">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhdeve-img.png" alt="">
                    </div>
                    <div class="clhdeve-decor x1">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhdeve-decor-01.png" alt="">
                    </div>
                    <div class="clhdeve-decor x2">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhdeve-decor-02.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clhdeve-mess">
        <!-- <div class="image">
					<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhdeve-mess.png" alt="">
				</div> -->
        <a href="" class="btn-clh clhdeve-c">
            <p class="clhdeve-sub">Liên hệ với chúng tôi nếu bạn đang gặp phải những vấn đề về hosting hoặc chưa
                biết bắt đầu từ đâu. </p>
            <p class="clhdeve-des">Mona đã sẵn sàng để hỗ trợ bạn!</p>
        </a>
    </div>
</section>