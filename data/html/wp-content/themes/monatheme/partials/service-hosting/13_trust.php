<?php

/**
 * Section name: Service Hosting Trust
 * Description: 
 * Author: Monamedia
 * Order: 13
 */
?>
<section class="clhab">
    <div class="container">
        <div class="clhab-w">
            <div class="clhab-l">
                <div class="clhab-image">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhab-image.png" alt="">
                </div>
                <div class="clhab-con">
                    <div class="clhab-tt">
                        <h3 class="host-tt">Mona Host chúng tôi có
                            <p class="host-tt hl">Dịch vụ ngon - xịn - mịn</p>
                        </h3>
                    </div>
                    <p class="clhab-sub-tt">
                        Top 1 là chuyện bình thường
                    </p>
                    <p class="clhab-txt">
                        Khách hàng đến với MONA.Host bằng sự tin tưởng vào năng lực và uy tín của chúng tôi
                    </p>
                    <div class="clhab-cus-l">
                        <div class="clhab-cus-i">
                            Lên top1 không</br>
                            còn là điều xa lạ với MONA
                        </div>
                        <div class="clhab-cus-i">
                            #TOP1 Google Search</br>
                            đối với MONA là dễ!
                        </div>
                        <div class="clhab-cus-i">
                            Công ty mới của MONA </br>
                            cũng lên luôn TOP
                        </div>
                    </div>
                    <a class="btn-clh">
                        <span class="txt">
                            Xem thêm lịch sử hình thành Mona Host
                        </span>
                    </a>
                </div>
                <div class="clhab-decor x1">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/ab-decor-01.png" alt="">
                </div>
                <div class="clhab-decor x2">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/ab-decor-02.png" alt="">
                </div>
                <div class="clhab-decor x4">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/ab-decor-04.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>