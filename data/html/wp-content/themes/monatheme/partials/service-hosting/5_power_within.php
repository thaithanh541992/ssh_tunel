<?php
/**
 * Section name: Service Hosting Power Within
 * Description: 
 * Author: Monamedia
 * Order: 5
 */
?>
		<section class="clhtn">
			<div class="container">
				<div class="clhtn-w">
					<div class="clhtn-t">
						<div class="clhtn-tt">
							<h3 class="host-tt">Tối đa tiềm năng website của bạn
								<p class="host-tt hl">Với dịch vụ Wordpress Premium Cloud Hosting</p>
							</h3>
						</div>
						<p class="clhtn-txt">
							<span class="clhtn-txt-violet">
								Website đem đến nguồn tài nguyên vô hạn
							</span>
							nhưng để khai thác hết mọi tiềm năng mà website bạn có sẽ đi kèm là nhiều thứ
						</p>
						<div class="clhtn-des">
							<div class="clhtn-des-txt">
								<p class="clhtn-des-i">
									Bạn chưa bao giờ biết
								</p>
								<p class="clhtn-des-i organ">
									NẾU CHƯA SỬ DỤNG DỊCH VỤ CỦA MONA
								</p>
							</div>
							<div class="clhtn-des-img">
								<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/sig.png" alt="">
							</div>
							<div class="clhtn-des-bot">
								<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/down.png" alt="">
							</div>
						</div>
						<div class="clhtn-t-w">
							<div class="clhtn-t-decor x1">
								<div class="image">
									<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/tn-decor-01.png" alt="">
								</div>
							</div>
							<div class="clhtn-t-decor x2">
								<div class="image">
									<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/tn-decor-02.png" alt="">
								</div>
							</div>
						</div>
					</div>
					<div class="clhtn-b">
						<div class="clhtn-l">
							<div class="clhtn-i">
								<div class="clhtn-i-l">
									<div class="clhtn-i-img">
										<div class="image">
											<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhtn-img-01.png" alt="">
										</div>
									</div>
								</div>
								<div class="clhtn-i-r">
									<div class="clhtn-info">
										<div class="clhtn-tag">
											Sự thật #1
										</div>
										<h3 class="clhtn-b-t">
											Có <span class="clhtn-b-t-pink">1001</span> lỗ hổng để hacker tấn công
											website của bạn
										</h3>
										<p class="clhtn-b-txt">
											Hệ thống bảo mật của website của nhiều bên chưa được hoàn thiện như của Mona
											Host vì thế bạn sẽ dễ bị DDoS Attack, index link lạ, làm cạn tài nguyên,
											nghẽn mạng, sập web
										</p>
									</div>
									<div class="clhtn-b-l">
										<div class="clhtn-b-i">
											<span class="icon">
												<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/check.png" alt="">
											</span>
											<span class="txt">
												Băng thông lên đến 20Gbps để xử lý các cuộc tấn công nhỏ tự động
											</span>
										</div>
										<div class="clhtn-b-i">
											<span class="icon">
												<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/check.png" alt="">
											</span>
											<span class="txt">
												Đội ngũ Dev kinh nghiệm để xử lý các cuộc tấn công quy mô lớn hơn
											</span>
										</div>
										<div class="clhtn-b-i">
											<span class="icon">
												<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/check.png" alt="">
											</span>
											<span class="txt">
												Tích hợp công nghệ chặn bọ độc quyền
											</span>
										</div>
									</div>
								</div>
							</div>
							<div class="clhtn-i">
								<div class="clhtn-i-l">
									<div class="clhtn-info">
										<div class="clhtn-tag">
											Sự thật #2
										</div>
										<h3 class="clhtn-b-t">
											Nâng cấp website Wordpress chỉ bằng </br><span class="clhtn-b-t-organ">
												1 Click
											</span> với dịch vụ Hosting <span class="clhtn-b-t-blue">
												TOP 1
											</span>
										</h3>
										<p class="clhtn-b-txt">
											Không cần phải tải gói Wordpress xuống máy tính, không cần phải đưa lên tài khoản lưu trữ, không cần tự cấu hình và cài đặt. Với tính năng 1-Click của chúng tôi, bạn sẽ <span class="txt-digital fw-600">mất chưa đến 1 phút để làm mọi thứ</span>
										</p>
									</div>
									<div class="clhtn-b-l">
										<div class="clhtn-b-i">
											<span class="icon">
												<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/check.png" alt="">
											</span>
											<span class="txt">
												Di chuyển hoặc sao chép trang web bằng 1 cú nhấp chuột
											</span>
										</div>
										<div class="clhtn-b-i">
											<span class="icon">
												<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/check.png" alt="">
											</span>
											<span class="txt">
												Dữ liệu được mã hoá và chỉ bạn mới có quyền truy cập bằng mật khẩu
											</span>
										</div>
										<div class="clhtn-b-i">
											<span class="icon">
												<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/check.png" alt="">
											</span>
											<span class="txt">
												Lưu trữ tệp trực tuyến mà không cần tải lên/xuống
											</span>
										</div>
										<div class="clhtn-b-i">
											<span class="icon">
												<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/check.png" alt="">
											</span>
											<span class="txt">
												Các tệp sẽ nhanh chóng được phân phối và không giới hạn dung lượng dữ
												liệu
											</span>
										</div>
									</div>
								</div>
								<div class="clhtn-i-r">
									<div class="clhtn-i-img">
										<div class="image">
											<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhtn-img-02.png" alt="">
										</div>
									</div>

								</div>
								<div class="clhtn-line-07">
									<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/line-07.png" alt="">
								</div>
							</div>
							<div class="clhtn-i">
								<div class="clhtn-i-l">
									<div class="clhtn-i-img">
										<div class="image">
											<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhtn-img-03.png" alt="">
										</div>
									</div>
								</div>
								<div class="clhtn-i-r">
									<div class="clhtn-info">
										<div class="clhtn-tag">
											Sự thật #3
										</div>
										<h3 class="clhtn-b-t">
											Hosting tại MONA có thể giúp bạn SEO lên
											<span class="clhtn-b-t-purple">
												TOP 1, x10, x20 doanh thu
											</span>
										</h3>
										<p class="clhtn-b-txt">
											Chúng tôi tối ưu trang web với các công cụ tìm kiếm hiện có, tăng trải
											nghiệm khách hàng liền mạch với thời gian load chưa đến <span
												class="clhtn-b-violet"> 100ms</span>

										</p>
									</div>
									<div class="clhtn-b-l">
										<div class="clhtn-b-i">
											<span class="icon">
												<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/check.png" alt="">
											</span>
											<span class="txt">
												Cấu trúc website thân thiện với Google Bots
											</span>
										</div>
										<div class="clhtn-b-i">
											<span class="icon">
												<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/check.png" alt="">
											</span>
											<span class="txt">
												Đảm bảo SEO chỉ có lên TOP
											</span>
										</div>
										<div class="clhtn-b-i">
											<span class="icon">
												<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/check.png" alt="">
											</span>
											<span class="txt">
												Doanh thu từ website chỉ có tăng
											</span>
										</div>
									</div>
								</div>
								<div class="clhtn-line-08">
									<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/line-08.png" alt="">
								</div>
							</div>
							<div class="clhtn-i">
								<div class="clhtn-i-l">
									<div class="clhtn-info">
										<div class="clhtn-tag">
											Sự thật #4
										</div>

										<h3 class="clhtn-b-t">
											Chúng tôi chủ động trong mọi tình</br> huống có thể xảy ra với khách hàng
										</h3>
										<p class="clhtn-b-txt">
											Không cần phải tải gói Wordpress xuống máy tính, không cần phải đưa lên tài
											khoản lưu trữ, không cần tự cấu hình và cài đặt. Với tính năng 1-Click của
											chúng tôi, bạn sẽ <span class="violet">mất chưa đến 1 phút để làm mọi
												thứ</span>
										</p>
									</div>
									<div class="clhtn-b-l">
										<div class="clhtn-b-i">
											<span class="icon">
												<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/check.png" alt="">
											</span>
											<span class="txt">
												Hệ thống Monitoring theo thời gian thực
											</span>
										</div>
										<div class="clhtn-b-i">
											<span class="icon">
												<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/check.png" alt="">
											</span>
											<span class="txt">
												Hỗ trợ kịp thời để website luôn trong trạng thái hoạt động
											</span>
										</div>
										<div class="clhtn-b-i">
											<span class="icon">
												<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/check.png" alt="">
											</span>
											<span class="txt">
												Back up mỗi ngày đảm bảo an toàn
											</span>
										</div>

									</div>
								</div>
								<div class="clhtn-i-r">
									<div class="clhtn-i-img">
										<div class="image">
											<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhtn-img-04.png" alt="">
										</div>
									</div>

								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</section>