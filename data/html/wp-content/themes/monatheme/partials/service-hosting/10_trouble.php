<?php

/**
 * Section name: Service Hosting Trouble
 * Description: 
 * Author: Monamedia
 * Order: 10
 */
?>
<section class="clhbu">
    <div class="container">
        <div class="clhbu-w">
            <div class="clhbu-top">
                <div class="clhbu-top-img">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhbu-img.png" alt="">
                </div>
                <div class="clhbu-in">
                    <div class="clhbu-tt">
                        <h3 class="host-tt">Doanh nghiệp của bạn sẽ gặp rắc rối
                            <p class="host-tt hl">Bởi những nguy cơ từ việc dùng Hosting giá rẻ</p>
                        </h3>
                    </div>
                    <p class="clhbu-txt">
                        Bộ mặt trên Internet của bạn đang bị <span class="clhbu-txt-h"> đe doạ bởi những nguy cơ
                            từ hosting “dỏm”</span>
                        Nhưng bạn vẫn phải <span class="clhbu-txt-t">xếp hàng chờ xử lý</span> vì không bên nào
                        chịu trách nhiệm
                    </p>
                </div>
                <div class="clhbu-top-l">
                    <div class="clhbu-top-decor x1">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhbu-top-01.png" alt="">
                    </div>
                    <div class="clhbu-top-decor x2">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhbu-top-02.png" alt="">
                    </div>
                </div>
            </div>
            <div class="clhbu-bot">
                <div class="clhbu-bot-l">
                    <div class="clhbu-bot-decor x1">
                        <p class="txt">
                            Không ai đứng ra chịu trách nhiệm, xử lý
                        </p>
                    </div>
                    <div class="clhbu-bot-decor x2">
                        <p class="txt">
                            Đơn vị Hosting
                            không hiểu bạn cần gì
                        </p>
                    </div>
                    <div class="clhbu-bot-decor x3">
                        <p class="txt">
                            Chất lượng phục vụ kém, không quan tâm khách hàng
                        </p>
                    </div>
                    <div class="clhbu-bot-decor x4">
                        <p class="txt">
                            Website gặp lỗi liên tục,
                            load chậm, dính virus,...
                        </p>
                    </div>
                </div>
                <div class="clhbu-bot-img">
                    <div class="image">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhbu-bot-img.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clhbu-br">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhbu-br.png" alt="">
    </div>
</section>