<?php

/**
 * Section name: Service Hosting Feedback
 * Description: 
 * Author: Monamedia
 * Order: 8
 */
?>
<section class="clhva">
    <div class="clhva-wrap">
        <div class="container">
            <div class="clhva-w">
                <div class="clhva-t">
                    <div class="clhva-tt">
                        <h3 class="host-tt">Mona Host không ma trận giá
                            <p class="host-tt hl">Chúng tôi chỉ cung cấp cho khách hàng giá trị tốt nhất</p>
                        </h3>
                    </div>
                    <p class="clhva-txt">
                        Do thị trường hosting chứa đầy ma trận chỉ-để-bán-được-hàng, chúng tôi phải <span class="violet">
                            tự nghiên cứu và phát triển sản phẩm hosting của riêng mình,
                        </span> để đáp ứng tiêu chí website cao cấp
                    </p>
                </div>
                <div class="clhva-b">
                    <div class="clhva-l">
                        <div class="clhva-decor rela">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhva-decor.png" alt="">
                        </div>
                        <div class="clhva-decor x1">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhva-decor-01.png" alt="">
                        </div>
                        <div class="clhva-decor x2">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhva-decor-02.png" alt="">
                        </div>
                        <div class="clhva-decor x3">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhva-decor-03.png" alt="">
                        </div>
                        <div class="clhva-decor x4">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhva-decor-04.png" alt="">
                        </div>
                        <div class="clhva-decor x5">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhva-decor-05.png" alt="">
                        </div>
                    </div>
                    <div class="clhva-mess btn-clh">
                        Hãy cùng tìm hiểu xem lịch sử hình thành cũng như giá trị mà
                        Mona Host mang lại cho khách hàng của mình
                    </div>
                </div>
            </div>
        </div>
        <div class="clhva-cloud x1">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/cloud-01.png" alt="">
        </div>
        <div class="clhva-cloud x2">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/cloud-02.png" alt="">
        </div>
        <div class="clhva-cloud x3">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/cloud-03.png" alt="">
        </div>
        <div class="clhva-cloud x4">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/cloud-04.png" alt="">
        </div>
        <div class="clhva-cloud x5">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/cloud-05.png" alt="">
        </div>
        <div class="clhva-cloud x6">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/cloud-06.png" alt="">
        </div>
    </div>
</section>

<section class="section feedback fix">
    <div class="panda">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/fix-home/slpanda.png" alt="" />
    </div>
    <div class="feedback-ctn">
        <div id="feedback-splide" class="feedback-big-item splide">
            <div class="splide__track">
                <div class="splide__list feedback-list d-flex f-ctn">
                    <div class="splide__slide feedback-item col">
                        <div class="feedback-wrap">
                            <div class="feedback-content">
                                <div class="feedback-content-header">
                                    <div class="name">Anh Lộc</div>
                                    <div class="from">Giám đốc Navis</div>
                                </div>
                                <div class="feedback-content-body">“Hoá ra sở hữu một website bán hàng và điều hành kinh doanh trên mạng dễ hơn mình tưởng nhiều. Cảm ơn sự tư vấn từ Mona Media. Trong tương lai gần...”</div>
                            </div>
                            <div class="feedback-img">
                                <img width="1920" height="2560" src="https://mona.media/wp-content/uploads/2023/03/Navis-scaled.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://mona.media/wp-content/uploads/2023/03/Navis-scaled.jpg 1920w, https://mona.media/wp-content/uploads/2023/03/Navis-225x300.jpg 225w, https://mona.media/wp-content/uploads/2023/03/Navis-768x1024.jpg 768w, https://mona.media/wp-content/uploads/2023/03/Navis-1152x1536.jpg 1152w, https://mona.media/wp-content/uploads/2023/03/Navis-1536x2048.jpg 1536w, https://mona.media/wp-content/uploads/2023/03/Navis-593x790.jpg 593w, https://mona.media/wp-content/uploads/2023/03/Navis-400x533.jpg 400w, https://mona.media/wp-content/uploads/2023/03/Navis-75x100.jpg 75w, https://mona.media/wp-content/uploads/2023/03/Navis-450x600.jpg 450w, https://mona.media/wp-content/uploads/2023/03/Navis-525x700.jpg 525w" sizes="(max-width: 1920px) 100vw, 1920px">
                            </div>
                        </div>
                        <div class="feedback-wrap">
                            <div class="feedback-content">
                                <div class="feedback-content-header">
                                    <div class="name">Chú Trung</div>
                                    <div class="from">Giám đốc Dưỡng lão Bình Mỹ</div>
                                </div>
                                <div class="feedback-content-body">“Tôi đã làm việc với Mona trong dự án thiết kế website chính cho hệ thống dưỡng lão Bình Mỹ của chúng tôi. Hiện tại tiếp tục hợp tác ở dự án phần mềm quản trị.”</div>
                            </div>
                            <div class="feedback-img">
                                <img width="1280" height="960" src="https://mona.media/wp-content/uploads/2023/03/duong-lao-binh-my.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://mona.media/wp-content/uploads/2023/03/duong-lao-binh-my.jpg 1280w, https://mona.media/wp-content/uploads/2023/03/duong-lao-binh-my-300x225.jpg 300w, https://mona.media/wp-content/uploads/2023/03/duong-lao-binh-my-1024x768.jpg 1024w, https://mona.media/wp-content/uploads/2023/03/duong-lao-binh-my-768x576.jpg 768w, https://mona.media/wp-content/uploads/2023/03/duong-lao-binh-my-1053x790.jpg 1053w, https://mona.media/wp-content/uploads/2023/03/duong-lao-binh-my-400x300.jpg 400w, https://mona.media/wp-content/uploads/2023/03/duong-lao-binh-my-100x75.jpg 100w, https://mona.media/wp-content/uploads/2023/03/duong-lao-binh-my-600x450.jpg 600w, https://mona.media/wp-content/uploads/2023/03/duong-lao-binh-my-900x675.jpg 900w" sizes="(max-width: 1280px) 100vw, 1280px">
                            </div>
                        </div>
                        <div class="feedback-wrap">
                            <div class="feedback-content">
                                <div class="feedback-content-header">
                                    <div class="name">Chị Tân</div>
                                    <div class="from">Giám đốc BGS Global</div>
                                </div>
                                <div class="feedback-content-body">“Quyền và Mona đã hỗ trợ chúng tôi thiết kế và hoàn thiện trang web đúng yêu cầu. Tôi đánh giá cao về tính chuyên nghiệp và tận tâm của các bạn.”</div>
                            </div>
                            <div class="feedback-img">
                                <img width="1280" height="960" src="https://mona.media/wp-content/uploads/2023/03/bds-global.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://mona.media/wp-content/uploads/2023/03/bds-global.jpg 1280w, https://mona.media/wp-content/uploads/2023/03/bds-global-300x225.jpg 300w, https://mona.media/wp-content/uploads/2023/03/bds-global-1024x768.jpg 1024w, https://mona.media/wp-content/uploads/2023/03/bds-global-768x576.jpg 768w, https://mona.media/wp-content/uploads/2023/03/bds-global-1053x790.jpg 1053w, https://mona.media/wp-content/uploads/2023/03/bds-global-400x300.jpg 400w, https://mona.media/wp-content/uploads/2023/03/bds-global-100x75.jpg 100w, https://mona.media/wp-content/uploads/2023/03/bds-global-600x450.jpg 600w, https://mona.media/wp-content/uploads/2023/03/bds-global-900x675.jpg 900w" sizes="(max-width: 1280px) 100vw, 1280px">
                            </div>
                        </div>
                    </div>
                    <div class="splide__slide feedback-item col">
                        <div class="feedback-wrap">
                            <div class="feedback-content">
                                <div class="feedback-content-header">
                                    <div class="name">Anh Phú</div>
                                    <div class="from">Founder &amp; CEO hệ thống Lầy Vape</div>
                                </div>
                                <div class="feedback-content-body">"Đã từng làm việc với rất nhiều đơn vị thiết kế nhưng chỉ tới khi gặp Mona Media tôi mới thực sự cảm thấy yên tâm. Các bạn bên Mona Media làm việc rất chuyên nghiệp.”</div>
                            </div>
                            <div class="feedback-img">
                                <img width="960" height="1282" src="https://mona.media/wp-content/uploads/2023/03/anh-Phu-Lay-vape.jpeg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://mona.media/wp-content/uploads/2023/03/anh-Phu-Lay-vape.jpeg 960w, https://mona.media/wp-content/uploads/2023/03/anh-Phu-Lay-vape-225x300.jpeg 225w, https://mona.media/wp-content/uploads/2023/03/anh-Phu-Lay-vape-767x1024.jpeg 767w, https://mona.media/wp-content/uploads/2023/03/anh-Phu-Lay-vape-768x1026.jpeg 768w, https://mona.media/wp-content/uploads/2023/03/anh-Phu-Lay-vape-592x790.jpeg 592w, https://mona.media/wp-content/uploads/2023/03/anh-Phu-Lay-vape-400x534.jpeg 400w, https://mona.media/wp-content/uploads/2023/03/anh-Phu-Lay-vape-75x100.jpeg 75w, https://mona.media/wp-content/uploads/2023/03/anh-Phu-Lay-vape-449x600.jpeg 449w, https://mona.media/wp-content/uploads/2023/03/anh-Phu-Lay-vape-524x700.jpeg 524w" sizes="(max-width: 960px) 100vw, 960px">
                            </div>
                        </div>
                        <div class="feedback-wrap">
                            <div class="feedback-content">
                                <div class="feedback-content-header">
                                    <div class="name">Anh Tâm</div>
                                    <div class="from">CEO IGcons</div>
                                </div>
                                <div class="feedback-content-body">"Tôi đã lựa chọn Mona Media là đơn vị thiết kế website chính cho IGcons của chúng tôi. Các bạn làm việc rất chuyên nghiệp, tận tâm, hỗ trợ nhiệt tình. Tôi rất ưng ý..."</div>
                            </div>
                            <div class="feedback-img">
                                <img width="1280" height="971" src="https://mona.media/wp-content/uploads/2023/03/Nguyen-Thanh-Tam-CEO-IGcons.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://mona.media/wp-content/uploads/2023/03/Nguyen-Thanh-Tam-CEO-IGcons.jpg 1280w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Thanh-Tam-CEO-IGcons-300x228.jpg 300w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Thanh-Tam-CEO-IGcons-1024x777.jpg 1024w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Thanh-Tam-CEO-IGcons-768x583.jpg 768w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Thanh-Tam-CEO-IGcons-1041x790.jpg 1041w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Thanh-Tam-CEO-IGcons-400x303.jpg 400w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Thanh-Tam-CEO-IGcons-100x76.jpg 100w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Thanh-Tam-CEO-IGcons-600x455.jpg 600w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Thanh-Tam-CEO-IGcons-900x683.jpg 900w" sizes="(max-width: 1280px) 100vw, 1280px">
                            </div>
                        </div>
                        <div class="feedback-wrap">
                            <div class="feedback-content">
                                <div class="feedback-content-header">
                                    <div class="name">Anh Hưng</div>
                                    <div class="from">Marketing Manager Groove</div>
                                </div>
                                <div class="feedback-content-body">"Cảm ơn Tuấn và Mona đã hỗ trợ Groove nhiệt tình trong dự án vừa qua, trong tương lai chúng ta sẽ còn nhiều cơ hội hợp tác nữa. Chúc công ty làm ăn phát đạt."</div>
                            </div>
                            <div class="feedback-img">
                                <img width="673" height="877" src="https://mona.media/wp-content/uploads/2023/03/a-Hung-Marketing-Manager-Groove.png" class="attachment-full size-full" alt="" loading="lazy" srcset="https://mona.media/wp-content/uploads/2023/03/a-Hung-Marketing-Manager-Groove.png 673w, https://mona.media/wp-content/uploads/2023/03/a-Hung-Marketing-Manager-Groove-230x300.png 230w, https://mona.media/wp-content/uploads/2023/03/a-Hung-Marketing-Manager-Groove-606x790.png 606w, https://mona.media/wp-content/uploads/2023/03/a-Hung-Marketing-Manager-Groove-400x521.png 400w, https://mona.media/wp-content/uploads/2023/03/a-Hung-Marketing-Manager-Groove-77x100.png 77w, https://mona.media/wp-content/uploads/2023/03/a-Hung-Marketing-Manager-Groove-460x600.png 460w, https://mona.media/wp-content/uploads/2023/03/a-Hung-Marketing-Manager-Groove-537x700.png 537w" sizes="(max-width: 673px) 100vw, 673px">
                            </div>
                        </div>
                    </div>
                    <div class="splide__slide feedback-item col">
                        <div class="feedback-wrap">
                            <div class="feedback-content">
                                <div class="feedback-content-header">
                                    <div class="name">Anh Bùi Lâm</div>
                                    <div class="from">TGĐ NAMTRUNGCONS</div>
                                </div>
                                <div class="feedback-content-body">"Với sự ăn ý trong lần hợp tác đầu tiên về website giới thiệu, Nam Trung Cons đánh giá cao chất lượng dịch vụ của Mona và quyết định tiếp tục hợp tác về SEO".</div>
                            </div>
                            <div class="feedback-img">
                                <img width="1280" height="960" src="https://mona.media/wp-content/uploads/2023/03/Namtrungcons-Anh-Bùi-Lam.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://mona.media/wp-content/uploads/2023/03/Namtrungcons-Anh-Bùi-Lam.jpg 1280w, https://mona.media/wp-content/uploads/2023/03/Namtrungcons-Anh-Bùi-Lam-300x225.jpg 300w, https://mona.media/wp-content/uploads/2023/03/Namtrungcons-Anh-Bùi-Lam-1024x768.jpg 1024w, https://mona.media/wp-content/uploads/2023/03/Namtrungcons-Anh-Bùi-Lam-768x576.jpg 768w, https://mona.media/wp-content/uploads/2023/03/Namtrungcons-Anh-Bùi-Lam-1053x790.jpg 1053w, https://mona.media/wp-content/uploads/2023/03/Namtrungcons-Anh-Bùi-Lam-400x300.jpg 400w, https://mona.media/wp-content/uploads/2023/03/Namtrungcons-Anh-Bùi-Lam-100x75.jpg 100w, https://mona.media/wp-content/uploads/2023/03/Namtrungcons-Anh-Bùi-Lam-600x450.jpg 600w, https://mona.media/wp-content/uploads/2023/03/Namtrungcons-Anh-Bùi-Lam-900x675.jpg 900w" sizes="(max-width: 1280px) 100vw, 1280px">
                            </div>
                        </div>
                        <div class="feedback-wrap">
                            <div class="feedback-content">
                                <div class="feedback-content-header">
                                    <div class="name">Chú Thành Nhơn</div>
                                    <div class="from">Nhà sáng lập KDR Dạ Lan</div>
                                </div>
                                <div class="feedback-content-body">"Cảm ơn Nhân và Mona đã hết sức tận tình hỗ trợ chú cũng như công ty trong việc thiết kế website. Trong tương lai, chú sẽ có cơ hội tiếp tục sử dụng dịch vụ của bên cháu".</div>
                            </div>
                            <div class="feedback-img">
                                <img width="300" height="300" src="https://mona.media/wp-content/uploads/2023/04/chu-Thanh-Nhon.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://mona.media/wp-content/uploads/2023/04/chu-Thanh-Nhon.jpg 300w, https://mona.media/wp-content/uploads/2023/04/chu-Thanh-Nhon-150x150.jpg 150w, https://mona.media/wp-content/uploads/2023/04/chu-Thanh-Nhon-100x100.jpg 100w" sizes="(max-width: 300px) 100vw, 300px">
                            </div>
                        </div>
                        <div class="feedback-wrap">
                            <div class="feedback-content">
                                <div class="feedback-content-header">
                                    <div class="name">Anh Ngọc</div>
                                    <div class="from">Giám Đốc Homesheel</div>
                                </div>
                                <div class="feedback-content-body">"Sin và Mona rất tận tâm hỗ trợ chúng mình trong quá trình thiết kế website. Trong tương lai chúng ta còn nhiều cơ hội hợp tác."</div>
                            </div>
                            <div class="feedback-img">
                                <img width="300" height="300" src="https://mona.media/wp-content/uploads/2023/04/anh-Ngoc.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://mona.media/wp-content/uploads/2023/04/anh-Ngoc.jpg 300w, https://mona.media/wp-content/uploads/2023/04/anh-Ngoc-150x150.jpg 150w, https://mona.media/wp-content/uploads/2023/04/anh-Ngoc-100x100.jpg 100w" sizes="(max-width: 300px) 100vw, 300px">
                            </div>
                        </div>
                    </div>
                    <div class="splide__slide feedback-item col">
                        <div class="feedback-wrap">
                            <div class="feedback-content">
                                <div class="feedback-content-header">
                                    <div class="name">Anh Vũ</div>
                                    <div class="from">Head of Marketing - Levents</div>
                                </div>
                                <div class="feedback-content-body">"Levents lựa chọn Mona là đơn vị thiết kế website bán hàng cho chúng tôi. Không phụ sự tin tưởng, Tuấn và Mona đã hỗ trợ chúng tôi hết sức nhiệt tình."</div>
                            </div>
                            <div class="feedback-img">
                                <img width="1280" height="959" src="https://mona.media/wp-content/uploads/2023/03/Levents-hop-dong-seo.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://mona.media/wp-content/uploads/2023/03/Levents-hop-dong-seo.jpg 1280w, https://mona.media/wp-content/uploads/2023/03/Levents-hop-dong-seo-300x225.jpg 300w, https://mona.media/wp-content/uploads/2023/03/Levents-hop-dong-seo-1024x767.jpg 1024w, https://mona.media/wp-content/uploads/2023/03/Levents-hop-dong-seo-768x575.jpg 768w, https://mona.media/wp-content/uploads/2023/03/Levents-hop-dong-seo-1054x790.jpg 1054w, https://mona.media/wp-content/uploads/2023/03/Levents-hop-dong-seo-400x300.jpg 400w, https://mona.media/wp-content/uploads/2023/03/Levents-hop-dong-seo-100x75.jpg 100w, https://mona.media/wp-content/uploads/2023/03/Levents-hop-dong-seo-600x450.jpg 600w, https://mona.media/wp-content/uploads/2023/03/Levents-hop-dong-seo-900x674.jpg 900w" sizes="(max-width: 1280px) 100vw, 1280px">
                            </div>
                        </div>
                        <div class="feedback-wrap">
                            <div class="feedback-content">
                                <div class="feedback-content-header">
                                    <div class="name">Anh Xuân</div>
                                    <div class="from">CEO - Phụ tùng Kim Thành</div>
                                </div>
                                <div class="feedback-content-body">“Kim Thành cung cấp số lượng lớn sản phẩm phụ tùng cho hầu hết các dòng xe, hãng xe. Để thiết kế được trang web đáp ứng mong muốn của chúng tôi..."</div>
                            </div>
                            <div class="feedback-img">
                                <img width="828" height="1101" src="https://mona.media/wp-content/uploads/2023/03/Anh-Xuan-KT.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://mona.media/wp-content/uploads/2023/03/Anh-Xuan-KT.jpg 828w, https://mona.media/wp-content/uploads/2023/03/Anh-Xuan-KT-226x300.jpg 226w, https://mona.media/wp-content/uploads/2023/03/Anh-Xuan-KT-770x1024.jpg 770w, https://mona.media/wp-content/uploads/2023/03/Anh-Xuan-KT-768x1021.jpg 768w, https://mona.media/wp-content/uploads/2023/03/Anh-Xuan-KT-594x790.jpg 594w, https://mona.media/wp-content/uploads/2023/03/Anh-Xuan-KT-400x532.jpg 400w, https://mona.media/wp-content/uploads/2023/03/Anh-Xuan-KT-75x100.jpg 75w, https://mona.media/wp-content/uploads/2023/03/Anh-Xuan-KT-451x600.jpg 451w, https://mona.media/wp-content/uploads/2023/03/Anh-Xuan-KT-526x700.jpg 526w" sizes="(max-width: 828px) 100vw, 828px">
                            </div>
                        </div>
                        <div class="feedback-wrap">
                            <div class="feedback-content">
                                <div class="feedback-content-header">
                                    <div class="name">Anh Tùng</div>
                                    <div class="from">CEO Dalabd</div>
                                </div>
                                <div class="feedback-content-body">“Tôi khá ấn tượng với Mona, các bạn đều còn khá trẻ nhưng trong công việc lại rất cẩn thận và chuyên nghiệp. Website Mona thiết kế rất đẹp, mượt. Cảm ơn Mona..."</div>
                            </div>
                            <div class="feedback-img">
                                <img width="720" height="960" src="https://mona.media/wp-content/uploads/2023/03/Dalad.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://mona.media/wp-content/uploads/2023/03/Dalad.jpg 720w, https://mona.media/wp-content/uploads/2023/03/Dalad-225x300.jpg 225w, https://mona.media/wp-content/uploads/2023/03/Dalad-593x790.jpg 593w, https://mona.media/wp-content/uploads/2023/03/Dalad-400x533.jpg 400w, https://mona.media/wp-content/uploads/2023/03/Dalad-75x100.jpg 75w, https://mona.media/wp-content/uploads/2023/03/Dalad-450x600.jpg 450w, https://mona.media/wp-content/uploads/2023/03/Dalad-525x700.jpg 525w" sizes="(max-width: 720px) 100vw, 720px">
                            </div>
                        </div>
                    </div>
                    <div class="splide__slide feedback-item col">
                        <div class="feedback-wrap">
                            <div class="feedback-content">
                                <div class="feedback-content-header">
                                    <div class="name">Anh Công</div>
                                    <div class="from">Founder &amp; CEO - Saigon Vape</div>
                                </div>
                                <div class="feedback-content-body">“Tuấn và Mona đã đồng hành với Saigon Retro Vape một thời gian khá là dài rồi. Từ thiết kế website cho tới Marketing. Hiện tại chúng tôi vẫn đang tin tưởng..."</div>
                            </div>
                            <div class="feedback-img">
                                <img width="720" height="960" src="https://mona.media/wp-content/uploads/2023/03/Nguyen-Chi-Cong-SGV.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://mona.media/wp-content/uploads/2023/03/Nguyen-Chi-Cong-SGV.jpg 720w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Chi-Cong-SGV-225x300.jpg 225w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Chi-Cong-SGV-593x790.jpg 593w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Chi-Cong-SGV-400x533.jpg 400w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Chi-Cong-SGV-75x100.jpg 75w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Chi-Cong-SGV-450x600.jpg 450w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Chi-Cong-SGV-525x700.jpg 525w" sizes="(max-width: 720px) 100vw, 720px">
                            </div>
                        </div>
                        <div class="feedback-wrap">
                            <div class="feedback-content">
                                <div class="feedback-content-header">
                                    <div class="name">Anh Quang Huy</div>
                                    <div class="from">Founder - The Forum Center</div>
                                </div>
                                <div class="feedback-content-body">“The Forum đã làm việc với Mona từ năm 2021 với 2 dự án là LMS, thiết kế 2 website và hiện tại là SEO. Chúng tôi rất tin tưởng và thực sự thích cách làm việc của Mona.”</div>
                            </div>
                            <div class="feedback-img">
                                <img width="2560" height="1920" src="https://mona.media/wp-content/uploads/2023/03/Nguyen-Quang-Huy-founder-The-Forum-scaled.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://mona.media/wp-content/uploads/2023/03/Nguyen-Quang-Huy-founder-The-Forum-scaled.jpg 2560w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Quang-Huy-founder-The-Forum-300x225.jpg 300w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Quang-Huy-founder-The-Forum-1024x768.jpg 1024w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Quang-Huy-founder-The-Forum-768x576.jpg 768w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Quang-Huy-founder-The-Forum-1536x1152.jpg 1536w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Quang-Huy-founder-The-Forum-2048x1536.jpg 2048w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Quang-Huy-founder-The-Forum-1053x790.jpg 1053w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Quang-Huy-founder-The-Forum-400x300.jpg 400w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Quang-Huy-founder-The-Forum-100x75.jpg 100w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Quang-Huy-founder-The-Forum-600x450.jpg 600w, https://mona.media/wp-content/uploads/2023/03/Nguyen-Quang-Huy-founder-The-Forum-900x675.jpg 900w" sizes="(max-width: 2560px) 100vw, 2560px">
                            </div>
                        </div>
                        <div class="feedback-wrap">
                            <div class="feedback-content">
                                <div class="feedback-content-header">
                                    <div class="name">Anh Long</div>
                                    <div class="from">P.TGĐ LPC Travel</div>
                                </div>
                                <div class="feedback-content-body">"Đã tham khảo khá nhiều đơn vị thiết kế website nhưng chỉ khi tới Mona, tôi mới quyết định hợp tác. Các bạn khác biệt, tận tâm, tự làm mọi thứ và rất lắng nghe.”</div>
                            </div>
                            <div class="feedback-img">
                                <img width="1280" height="720" src="https://mona.media/wp-content/uploads/2023/03/LPC-Travel.jpg" class="attachment-full size-full" alt="" loading="lazy" srcset="https://mona.media/wp-content/uploads/2023/03/LPC-Travel.jpg 1280w, https://mona.media/wp-content/uploads/2023/03/LPC-Travel-300x169.jpg 300w, https://mona.media/wp-content/uploads/2023/03/LPC-Travel-1024x576.jpg 1024w, https://mona.media/wp-content/uploads/2023/03/LPC-Travel-768x432.jpg 768w, https://mona.media/wp-content/uploads/2023/03/LPC-Travel-400x225.jpg 400w, https://mona.media/wp-content/uploads/2023/03/LPC-Travel-100x56.jpg 100w, https://mona.media/wp-content/uploads/2023/03/LPC-Travel-600x338.jpg 600w, https://mona.media/wp-content/uploads/2023/03/LPC-Travel-900x506.jpg 900w" sizes="(max-width: 1280px) 100vw, 1280px">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>