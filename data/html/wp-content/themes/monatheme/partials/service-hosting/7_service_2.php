<?php
/**
 * Section name: Service Hosting Service 2
 * Description: 
 * Author: Monamedia
 * Order: 7
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> session-service">
    <div class="session-service-title-wrap">
    <?php
        $mona_available_hosting = get_field('mona_available_hosting', MONA_PAGE_HOME);
        if ( !empty( $mona_available_hosting ) && is_array( $mona_available_hosting )) {
            ?>
            <div class="hosting-wrap-js">
                <div class="title">
                    <p class="tt">Dịch vụ Premium Cloud Hosting</p>
                    <p class="sub-tt txt-bg bg-host">
                        Tốt nhất thị trường đến từ Mona Host
                    </p>
                </div>
                <div class="session-pay-host-header">
                    <div class="tab-button-custom">
                        <div class="wrap">
                            <?php $first_option_qty = 60 ?>
                            <div class="item active hosting-tab-js" data-qty="60">60 tháng</div>
                            <div class="item hosting-tab-js" data-qty="36">36 tháng</div>
                            <div class="item hosting-tab-js" data-qty="24">24 tháng</div>
                            <div class="item hosting-tab-js" data-qty="12">12 tháng</div>
                        </div>
                    </div>
                </div>
                <div style="display: none">
                    <?php
                    $hosting_ui_data = [];
                    foreach ( $mona_available_hosting as $key => $hosting ) {
                        $mona_product_ui_info = get_field( 'mona_product_ui_info', $hosting->ID );
                        if ( !empty( $mona_product_ui_info ) ) {
                            $hosting_ui_data[] = $mona_product_ui_info;
                            ?>
                            <div id="tooltip-hosting-<?php echo $key ?>">
                                <div class="gift-content">
                                    <?php echo $mona_product_ui_info['info_offer'] ?>
                                </div>
                            </div>
                            <?php
                        }
                    }
                    ?>
                </div>
                <div class="service-hosting-inner">
                    <div class="service-hosting">
                        <div class="service-hosting-wrap">
                            <?php
                            foreach ( $mona_available_hosting as $key => $hosting ) {
                                $mona_product_ui_info = $hosting_ui_data[$key];
                                if ( !empty( $mona_product_ui_info ) ) {
                                    ?>
                                    <div class="service-hosting-item bg-liner-<?php echo $key + 1 ?>"
                                        data
                                    >
                                        <?php
                                        if ( $mona_product_ui_info['info_recommend'] ) {
                                            ?>
                                            <div class="icon-recomment">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/icon-recomment.png" alt="" />
                                            </div>
                                            <?php
                                        }
                                        ?>
                                        <div class="txt-hosting-name">
                                            <div class="txt-white"><?php echo $hosting->post_title ?></div>
                                            <?php
                                            if ( !empty( $mona_product_ui_info['info_offer'] ) ) {
                                                ?>
                                                <div class="icon-gift tooltip-show" data-template="tooltip-hosting-<?php echo $key ?>">
                                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/icon-gift.png" alt="" />
                                                </div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <?php
                                        $mona_billing_cycle_list = get_field( 'mona_billing_cycle_list', $hosting->ID );
                                        $original_price = $mona_product_ui_info['info_original_price'];
                                        $price = 0;
                                        $sale_percent = 0;
                                        if ( !empty( $mona_billing_cycle_list ) && is_array( $mona_billing_cycle_list ) ) {
                                            foreach ( $mona_billing_cycle_list as $billing_item ) {
                                                if ( $billing_item['term_quantity'] == $first_option_qty ) {
                                                    $price = floor($billing_item['price']/$billing_item['term_quantity']);
                                                    if ( !empty( $original_price ) ) {
                                                        $sale_percent = 100 - ceil($price / $original_price * 100);
                                                    }
                                                }
                                            }
                                        }
                                        ?>
                                        <div class="txt-hosting-price hosting-price-js" 
                                            data-billing_list='<?php echo json_encode( $mona_billing_cycle_list ) ?>'
                                            data-original_price='<?php echo $original_price ?>'
                                        >
                                            <div class="txt-price">
                                                <?php echo number_format($price, 0, '', ',') ?> <span class="dash-month fw-400"> đ/tháng </span>
                                            </div>
                                            <?php
                                            if ( $sale_percent > 0 ) {
                                                ?>
                                                <div><span class="txt-sale"><?php echo $sale_percent; ?>%</span></div>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                        <?php
                                        if ( !empty( $original_price ) ) {
                                            ?>
                                            <div class="txt-sale-off">
                                                Giảm từ: <span class="txt-line-through"><?php echo number_format($original_price, 0, '', ',') ?> đ</span>
                                            </div>
                                            <?php
                                        }
                                        ?>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="head-name">CẤU HÌNH</div>
                        <div class="service-hosting-wrap">
                            <?php
                            foreach ( $mona_available_hosting as $key => $hosting ) {
                                $mona_product_ui_info = $hosting_ui_data[$key];
                                if ( !empty( $mona_product_ui_info ) ) {
                                    ?>
                                    <div class="service-hosting-item">
                                        <?php echo $mona_product_ui_info['info_config'] ?>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                        <div class="head-name">DỊCH VỤ ĐI KÈM</div>
                        <div class="service-hosting-wrap">
                            <?php
                            foreach ( $mona_available_hosting as $key => $hosting ) {
                                $mona_product_ui_info = $hosting_ui_data[$key];
                                if ( !empty( $mona_product_ui_info ) ) {
                                    ?>
                                    <div class="service-hosting-item service-add-cart-js">
                                        <?php echo $mona_product_ui_info['info_service'] ?>
                                        <span class="add-to-cart-js is-loading-btn-2" data-pid="<?php echo $hosting->ID ?>" data-billing_cycle=<?php echo $first_option_qty ?>>
                                            <a class="btn-clh" >
                                            <span class="txt">
                                                Đăng ký ngay
                                            </span>
                                            </a>
                                        </span>
                                    </div>
                                    <?php
                                }
                            }
                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php
        }
        ?>
    </div>
</section>