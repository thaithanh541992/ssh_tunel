<?php
/**
 * Section name: Service Hosting SEO Item
 * Description: 
 * Author: Monamedia
 * Order: 15
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> seo-item">
    <div class="container">
        <div class="seo-item-header d-flex f-ctn">
            <div class="seo-item-tt col">
                <div class="sec-tt host-tt">
                    <h2>
                        Chinh phục mảng kinh doanh trên Internet
                        <p class="txt-bg bg-host">
                            <span class="txt">
                                Với hệ sinh thái khổng lồ của The MONA
                            </span>
                        </p>
                    </h2>
                </div>
            </div>
            <div class="seo-item-hotline col">
                <div class="img">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hotline-img.png" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="seo-item-img">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/SEO-team.png" alt="">
    </div>
</section>