<?php

/**
 * Section name: Service Hosting SEO
 * Description: 
 * Author: Monamedia
 * Order: 6
 */
?>
<section class="clhtop">
    <div class="clhtop-cloud-top">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/cloud-top.png" alt="">
    </div>
    <div class="clhtop-star">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/star-sky-04.png" alt="">
    </div>
    <div class="container">
        <div class="clhtop-w">
            <div class="clhtop-top">
                <div class="clhtop-image">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhtop-img.png" alt="">
                </div>
                <div class="clhtop-tt">
                    <h3 class="host-tt hl"><span class="hl">Đem đến cho khách hàng vị thế TOP đầu ngành hàng</span>
                        <p class="host-tt">Với chiến lược SEO của Mona</p>
                    </h3>
                </div>
            </div>
            <div class="clhtop-bot">
                <div class="clhtop-c">
                    <div class="swiper swiper-clhtop">
                        <div class="swiper-wrapper">
                            <div class="swiper-slide">
                                <div class="clhtop-i">
                                    <div class="image">
                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhtop-sli.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="clhtop-i">
                                    <div class="image">
                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhtop-sli.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="swiper-slide">
                                <div class="clhtop-i">
                                    <div class="image">
                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhtop-sli.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <!-- <div class="swiper-slide">
										<div class="clhtop-i">
											<div class="image">
												<img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhtop-sli.png" alt="">
											</div>
										</div>
									</div> -->
                        </div>
                    </div>
                </div>
                <div class="clhtop-so">
                    <div class="the-mona-footer-cus">
                        <div class="seopj-info">
                            <div class="seopj-info-py">
                                <div class="seopj-info-wrap">
                                    <div class="seopj-info-star">
                                        <div class="seopj-info-star-img">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/icon-star2.png" alt="">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/icon-star2.png" alt="">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/icon-star2.png" alt="">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/icon-star2.png" alt="">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/icon-star2.png" alt="">
                                        </div>
                                        <p class="text">5.0</p>
                                    </div>
                                    <span class="text">Đánh từ 6,000+ khách hàng</span>
                                </div>
                            </div>
                            <div class="seopj-info-py">
                                <div class="seopj-info-list">
                                    <div class="seopj-info-item">
                                        <div class="seopj-info-img" style="background: #052D00;">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/seoif-info-img.png" alt="">
                                        </div>
                                    </div>
                                    <div class="seopj-info-item">
                                        <div class="seopj-info-img" style="background: #2263AF;">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/seoif-info-img2.png" alt="">
                                        </div>
                                    </div>
                                    <div class="seopj-info-item">
                                        <div class="seopj-info-img" style="background: #ED1489;">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/seoif-info-img3.png" alt="">
                                        </div>
                                    </div>
                                    <div class="seopj-info-item">
                                        <div class="seopj-info-img" style="background:  #2686EC;">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/seoif-info-img4.png" alt="">
                                        </div>
                                    </div>
                                    <div class="seopj-info-item">
                                        <div class="seopj-info-img" style="background: #FFFFFF;">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/seoif-info-img5.png" alt="">
                                        </div>
                                    </div>
                                    <div class="seopj-info-item">
                                        <div class="seopj-info-img" style="background: #FFC670;">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/seoif-info-img6.png" alt="">
                                        </div>
                                    </div>
                                    <div class="seopj-info-item">
                                        <div class="seopj-info-img" style="background: #00A85A;">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/seoif-info-img7.png" alt="">
                                        </div>
                                    </div>
                                    <div class="seopj-info-item">
                                        <div class="seopj-info-img" style="background:  #FFFFFF;">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/seoif-info-img8.png" alt="">
                                        </div>
                                    </div>
                                    <div class="seopj-info-item">
                                        <div class="seopj-info-img" style="background:  #001D61;">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/seoif-info-img9.png" alt="">
                                        </div>
                                    </div>
                                    <div class="seopj-info-item">
                                        <div class="seopj-info-img" style="background:  #FFFFFF;">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/seoif-info-img10.png" alt="">
                                        </div>
                                    </div>
                                    <div class="seopj-info-item">
                                        <div class="seopj-info-img" style="background:  #FFFFFF;">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/seoif-info-img11.png" alt="">
                                        </div>
                                    </div>
                                    <div class="seopj-info-item">
                                        <p class="text">
                                            2.500+
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clhtop-mess">
                    Liên hệ ngay</br>
                    Xem cách chúng tôi thực hiện chiến lược SEO cho khách hàng
                </div>
            </div>
        </div>
    </div>
    <div class="clhtop-cloud-bot">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/cloud-bot.png" alt="">
    </div>
    <div class="clhtop-decor x1">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhtop-decor-01.png" alt="">
    </div>
    <div class="clhtop-decor x2">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhtop-decor-02.png" alt="">
    </div>
    <div class="clhtop-decor x3">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhtop-decor-03.png" alt="">
    </div>
</section>