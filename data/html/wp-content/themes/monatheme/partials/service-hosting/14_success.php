<?php

/**
 * Section name: Service Hosting Success
 * Description: 
 * Author: Monamedia
 * Order: 14
 */
?>
<section class="clhdt horizontal-section-p">
    <div class="clhdt-cm">
        <div class="clhdt-w horizontal-section">
            <div class="clhdt-sticky">
                <div class="container">
                    <div class="clhdt-l horizontal-scroll">
                        <div class="clhdt-i">
                            <div class="clhdt-img">
                                <div class="image">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhdt-img-01.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="clhdt-i">
                            <div class="clhdt-c">
                                <div class="clhdt-n">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/number.png" alt="">
                                </div>
                                <div class="clhdt-tt">
                                    <h3 class="host-tt">khách hàng đang gặt hái thành công
                                        <p class="host-tt hl">Khi đầu tư vào dịch vụ Hosting</p>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="clhdt-i">
                            <div class="clhdt-img">
                                <div class="image">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhdt-img-02.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="clhdt-i">
                            <div class="clhdt-img">
                                <div class="image">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhdt-img-03.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="clhdt-i">
                            <div class="clhdt-c">
                                <div class="clhdt-n">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/number-01.png" alt="">
                                </div>
                                <div class="clhdt-tt">
                                    <h3 class="host-tt">Từ khoá được triển khai
                                        <p class="host-tt hl violet">Đạt TOP 1 trong nhiều lĩnh vực </p>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="clhdt-i">
                            <div class="clhdt-img">
                                <div class="image">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhdt-img-04.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="clhdt-i">
                            <div class="clhdt-img">
                                <div class="image">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhdt-img-05.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="clhdt-i">
                            <div class="clhdt-c">
                                <div class="clhdt-n">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/number-02.png" alt="">
                                </div>
                                <div class="clhdt-tt">
                                    <h3 class="host-tt">Trong nhiều lĩnh vực
                                        <p class="host-tt hl blue">Làm gì cũng ra kết quả </p>
                                    </h3>
                                </div>
                            </div>
                        </div>
                        <div class="clhdt-i">
                            <div class="clhdt-img">
                                <div class="image">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhdt-img-06.png" alt="">
                                </div>
                            </div>
                        </div>
                        <div class="clhdt-i">
                            <div class="clhdt-img">
                                <div class="image">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhdt-img-07.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>