<?php

/**
 * Section name: Service Hosting Conquer
 * Description: 
 * Author: Monamedia
 * Order: 3
 */
?>
<section class="clhpar">
    <div class="clhpar-star">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/star-sky-02.png" alt="">
    </div>
    <div class="clhpar-br">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/cloud-top.png" alt="">
    </div>
    <div class="container">
        <div class="clhpar-w">
            <div class="clhpar-top">
                <div class="clhpar-common">
                    <div class="clhpar-tt">
                        <h3 class="host-tt">Không đơn vị nào có thể
                            <p class="host-tt hl">Nắm rõ Hosting trong lòng bàn tay như Mona</p>
                        </h3>
                    </div>
                    <p class="clhpar-txt">
                        Bạn muốn website <span class="line">ổn định, nhanh chóng và an toàn</span> nhưng không
                        biết cách quản lý và bảo trì website một cách chuyên nghiệp
                    </p>
                </div>
                <div class="clhpar-in panda">
                    <div class="dvs-solve-top-mask" id="dvs-completed-js">
                        <div id="dvs-solve-top-list-js" class="dvs-solve-top-list dvs-solve-top-list-js">
                            <div class="dvs-solve-top-item">
                                <div id="dvs-solve-js" class="dvs-solve-top-item-wrap dvs-solve-js">
                                    <span class="txt-bg bg-digital">
                                        TOP 1
                                    </span>
                                    <div class="logo-bg">
                                        <a href="#" class="logo">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/logo/software-less-logo.svg" alt="">
                                        </a>
                                    </div>
                                    <div class="dvs-solve-top-item-name">
                                        Hệ thống
                                        phần mềm quản lý
                                    </div>
                                    <div class="dvs-solve-top-item-img">
                                        <div class="img">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/dvs/panda-boss-1.png" alt="">
                                        </div>
                                    </div>
                                    <div class="dvs-solve-top-item-bg">
                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/dvs/dvs-solve-top-bg-1.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="dvs-solve-top-item">
                                <div id="dvs-solve-js" class="dvs-solve-top-item-wrap dvs-solve-js">
                                    <span class="txt-bg bg-host">
                                        TOP 1
                                    </span>
                                    <div class="logo-bg">
                                        <a href="#" class="logo">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/logo/host-less-logo.svg" alt="">
                                        </a>
                                    </div>
                                    <div class="dvs-solve-top-item-name">
                                        Web Hosting Giải pháp hạ tầng
                                    </div>
                                    <div class="dvs-solve-top-item-img">
                                        <div class="img">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/dvs/panda-boss-2.png" alt="">
                                        </div>
                                    </div>
                                    <div class="dvs-solve-top-item-bg">
                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/dvs/dvs-solve-top-bg-2.png" alt="">
                                    </div>
                                </div>
                            </div>
                            <div class="dvs-solve-top-item">
                                <div id="dvs-solve-js" class="dvs-solve-top-item-wrap dvs-solve-js">
                                    <span class="txt-bg bg-software">
                                        TOP 1
                                    </span>
                                    <div class="logo-bg">
                                        <a href="#" class="logo">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/logo/media-less-logo.svg" alt="">
                                        </a>
                                    </div>
                                    <div class="dvs-solve-top-item-name">
                                        Dịch vụ
                                        Thiết kế website
                                    </div>
                                    <div class="dvs-solve-top-item-img">
                                        <div class="img">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/dvs/panda-boss-3.png" alt="">
                                        </div>
                                    </div>
                                    <div class="dvs-solve-top-item-bg">
                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/dvs/dvs-solve-top-bg-3.png" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="clhpar-con">
                        <div class="clhpar-mess">
                            Chinh phục và làm chủ thị trường Internet
                            Với dịch vụ Premium Cloud Hosting tại MONA.Host ngay
                        </div>
                        <p class="clhpar-in-txt">
                            <span class="blold">Hợp tác với Mona bạn được nhiều hơn là mất.</span> Với sự thống
                            trị của Mona hiện nay, chúng tôi cam kết sẽ luôn <span class="line">
                                ưu tiên khách hàng của mình lên đầu.
                            </span>
                        </p>
                    </div>
                </div>
            </div>
            <div class="clhpar-bot">
                <div class="clhpar-bot-tt">
                    <h3 class="host-tt">Làm việc với Mona Host
                        <p class="host-tt hl">Đem lại nhiều lợi ích cho chặng đường phát triển của bạn</p>
                    </h3>
                </div>
                <div class="clhpar-bot-inner">
                    <div class="clhpar-inner-mes">
                        <div class="clhpar-mess-line-01">
                            <div class="mess">
                                Sử dụng phần cứng</br>
                                <span class="bold">MẠNH NHẤT THỊ TRƯỜNG</span></br>
                                Lưu trữ trên hệ thống máy chủ hiện đại giúp tiết kiệm chi phí</br> và vận hành
                            </div>
                        </div>
                        <div class="clhpar-mess-line-02">
                            <div class="mess">
                                Không lo vấn đề sập web, lag, hacker với</br>
                                <span class="bold">
                                    Đội ngũ túc trực 24/7/365</br>
                                </span>
                                Đồng hành cùng doanh nghiệp trên con đường phát triển đế</br> chế kinh doanh
                                Online
                            </div>
                        </div>
                    </div>
                    <div class="clhpar-bot-l">
                        <div class="clhpar-bot-l-inner">
                            <div class="image">
                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhpar-01.png" alt="">
                            </div>
                        </div>
                    </div>
                    <div class="clhpar-bot-r">
                        <div class="image">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhpar-02.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clhpar-cloud-bot">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/cloud-bot.png" alt="">
    </div>
</section>