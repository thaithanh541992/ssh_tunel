<?php

/**
 * Section name: Service Hosting Solve
 * Description: 
 * Author: Monamedia
 * Order: 1
 */
?>
<section class="clhso">
    <div class="container">
        <div class="clhso-w">
            <div class="clhso-t">
                <div class="clhso-tt">
                    <h3 class="host-tt">Mọi trường hợp mà khách gặp phải
                        <p class="host-tt hl">Mona Host đều có thể giải quyết</p>
                    </h3>
                </div>
                <p class="clhso-txt">
                    Tất cả cấu hình trên thị trường đều có tại MONA, bạn có thể <span class="line">theo dõi,
                        định hình và quản
                        lý dịch vụ một cách dễ dàng</span> trên giao diện thân thiện với người dùng.
                </p>
            </div>
            <div class="clhso-in">
                <div class="clhso-b">
                    <div class="clhso-lt">
                        <div class="clhso-decor-01 abs ">
                            <div class="clhso-decor-com">
                                <p class="txt">
                                    Bạn không có người</br>
                                    am hiểu về công nghệ?
                                </p>
                            </div>
                        </div>
                        <div class="clhso-decor-02 abs ">
                            <div class="clhso-decor-com">
                                <p class="txt">
                                    Bạn không biết chọn </br>
                                    hosting nào cho website?
                                </p>
                            </div>
                        </div>
                        <div class="clhso-decor-03 abs ">
                            <div class=" clhso-decor-com">
                                <p class="txt btn-clh">
                                    Xem cách Mona Host giải quyết các vấn đề
                                </p>
                            </div>
                        </div>
                        <div class="clhso-decor-04 abs ">
                            <div class="clhso-decor-com">
                                <p class="txt">
                                    Website bạn bị chết</br>
                                    lên chết xuống ?
                                </p>
                            </div>
                        </div>
                        <div class="clhso-decor-05 abs">
                            <div class="clhso-decor-com">
                                <p class="txt">
                                    Bạn muốn xây dựng đế </br>
                                    chế online của mình?
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="clhso-decor">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhso-img.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>