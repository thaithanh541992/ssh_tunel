<?php

/**
 * Section name: Service Hosting Confuse
 * Description: 
 * Author: Monamedia
 * Order: 2
 */
?>
<section class="clhph">
    <div class="container">
        <div class="clhph-w">
            <div class="clhph-i">
                <div class="clhph-l">
                    <div class="clhph-tt">
                        <h3 class="host-tt">Bạn đang không biết chọn </br> hosting nào
                            <p class="host-tt hl">Giữa ma trận giá và </br> cấu hình hosting</p>
                        </h3>
                    </div>
                    <p class="clhph-txt">
                        Ở Mona, chúng tôi đề ra những gói <span class="line">hosting chất lượng cho khách
                            hàng</span> của mình
                        bất kể cấu hình và nhu cầu sử dụng
                    </p>
                    <div class="clhph-in">
                        <div class="clhph-in-t">Vì Mona chỉ bán Hosting</div>
                        <div class="clhph-in-m">PHÙ HỢP</div>
                        <div class="clhph-in-b">
                            với nhu cầu khách hàng
                        </div>
                        <div class="clhph-lg">
                            <div class="clhph-lg-l">KHÔNG</div>
                            <div class="clhph-lg-r">
                                <span class="txt">thừa</span>
                                <span class="txt">Thiếu</span>
                                <span class="txt">Ma Trận</span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clhph-r">
                    <div class="clhph-r-inner">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhp-img-01.png" alt="">
                    </div>
                </div>
                <div class="clhph-line">
                    <div class="image">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhph-line.png" alt="">
                    </div>
                </div>
                <div class="clhph-gif">
                    <div class="image">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/gif.png" alt="">
                    </div>
                </div>
            </div>
            <div class="clhph-i style-pri">
                <div class="clhph-l">
                    <div class="clhph-l-inner">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhp-img-02.png" alt="">
                    </div>
                </div>
                <div class="clhph-r">
                    <div class="clhph-r-inner">
                        <div class="clhph-tt">
                            <h3 class="host-tt">Sử dụng hosting kém chất lượng
                                <p class="host-tt hl">Khi xảy ra vấn đề không ai hỗ trợ</p>
                            </h3>
                        </div>
                        <p class="clhph-txt">
                            Khách hàng của chúng tôi đã <span class="line">phải gánh chịu hậu quả khi sử dụng
                                hosting kém chất
                                lượng </span> Rất nhiều khách hàng phải vừa phải kiểm tra lỗi gì, ở đâu, vừa
                            phải xác định lỗi do
                            bên nào (hosting hay website) và liên hệ thì không ai hỗ trợ.
                        </p>
                        <div class="clhph-mess">
                            KHÔNG AI CHỊU TRÁCH NHIỆM,
                            ĐỔ LỖI LẪN NHAU
                        </div>
                    </div>
                </div>
                <div class="clhph-line-02">
                    <div class="image">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/line-02.png" alt="">
                    </div>
                </div>
                <div class="clhph-ellip-02">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/ellip-02.png" alt="">
                </div>
                <div class="clhph-gif-03">
                    <div class="image">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/gif-03.png" alt="">
                    </div>
                </div>
            </div>
            <div class="clhph-i style-second">
                <div class="clhph-l">
                    <div class="clhph-tt">
                        <h3 class="host-tt style-pri">Bạn đang không biết chọn</br>hosting nào
                            <p class="host-tt hl">Giữa ma trận giá và cấu hình hosting</p>
                        </h3>
                    </div>
                    <div class="clhph-txt-list">
                        <p class="clhph-txt x1">
                            Không chỉ là sập web vài phút, vài ngày <span class="line">khách hàng sẽ rời bỏ,
                                thậm
                                chí không quay lại vì web tệ,</span> mỗi việc truy cập thôi cũng không được,
                            chết
                            lên chết xuống
                        </p>
                        <p class="clhph-txt x2">
                            Lỗi gì cũng khiến web bạn bị đánh giá là không an toàn: <span class="line">Khách
                                hàng bỏ
                                chạy vì sợ virus, hacker, mất thông tin, web đen
                            </span>
                        </p>
                        <p class="clhph-txt x3">
                            Website load chậm, giật lag khiến việc SEO lên top gần như không thể, muốn <span class="line">khách hàng tìm thấy bạn khó như mò kim đáy bể</span>
                        </p>
                    </div>
                </div>
                <div class="clhph-r">
                    <div class="clhph-r-inner">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhp-img-03.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>