<?php
/**
 * Section name: Service Hosting SEO FAQ
 * Description: 
 * Author: Monamedia
 * Order: 16
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> sec-seofaq">
    <div class="container">
        <div class="seofaq">
            <div class="seofaq-top">
                <h2 class="title text-ani bg-blue">FAQ
                </h2>
                <div class="seofaq-line">
                    <svg class="ani" width="317" height="165" viewBox="0 0 317 165" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M239.914 54.4646C97.9425 -6.18229 6.55242 27.0745 2.17067 68.9596C-2.21107 110.845 64.1018 152.108 150.285 161.124C236.467 170.14 309.884 143.494 314.266 101.609C318.518 60.9678 222.966 13.0692 140.531 2.48972" stroke="#7C0FD1" stroke-width="3.2557" class="svg-elem-1"></path>
                    </svg>

                </div>
            </div>
            <div class="seofaq-flex">
                <div class="seofaq-left">
                    <div class="seofaq-title">
                        <h2 class="title">
                            Chúng tôi <br>
                            có thể giúp gì <br>
                            cho bạn?
                        </h2>
                    </div>
                    <div class="seofaq-bg-img">
                        <div class="img">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/faq-panda.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="seofaq-right">
                    <div class="seofaq-wrap collapse-block">
                        <h3 class="title">Câu hỏi về dịch vụ SEO</h3>
                        <div class="seofaq-list">
                            <div class="seofaq-item collapse-item">
                                <div class="seofaq-head collapse-head">
                                    <h4 class="title">Dịch vụ SEO tổng thể là gì?</h4>
                                    <i class="far fa-chevron-circle-down icon"></i>
                                </div>
                                <div class="seofaq-tbody collapse-body">
                                    <p class="text">
                                        “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet
                                        sint. Velit officia consequat duis enim velit mollit. Exercitation
                                        veniam.”
                                    </p>
                                    <ul class="seoif-des-list">
                                        <li class="seoif-des-item">
                                            <p class="text">Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet </p>
                                        </li>
                                        <li class="seoif-des-item">
                                            <p class="text">Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet sint. Velit officia consequat duis enim
                                                velit mollit.</p>
                                        </li>
                                        <li class="seoif-des-item">
                                            <p class="text">Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet sint. Velit officia consequat duis enim
                                                velit mollit. Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet sint. Velit officia consequat duis enim
                                                velit mollit.</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="seofaq-item collapse-item">
                                <div class="seofaq-head collapse-head">
                                    <h4 class="title">Tại sao bạn cần đến dịch vụ SEO website?</h4>
                                    <i class="far fa-chevron-circle-down icon"></i>
                                </div>
                                <div class="seofaq-tbody collapse-body">
                                    <p class="text">
                                        “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet
                                        sint. Velit officia consequat duis enim velit mollit. Exercitation
                                        veniam.”
                                    </p>
                                    <ul class="seoif-des-list">
                                        <li class="seoif-des-item">
                                            <p class="text">Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet </p>
                                        </li>
                                        <li class="seoif-des-item">
                                            <p class="text">Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet sint. Velit officia consequat duis enim
                                                velit mollit.</p>
                                        </li>
                                        <li class="seoif-des-item">
                                            <p class="text">Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet sint. Velit officia consequat duis enim
                                                velit mollit. Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet sint. Velit officia consequat duis enim
                                                velit mollit.</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="seofaq-item collapse-item">
                                <div class="seofaq-head collapse-head">
                                    <h4 class="title">Ưu điểm của SEO tổng thể website so với SEO từ khóa
                                    </h4>
                                    <i class="far fa-chevron-circle-down icon"></i>
                                </div>
                                <div class="seofaq-tbody collapse-body">
                                    <p class="text">
                                        “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet
                                        sint. Velit officia consequat duis enim velit mollit. Exercitation
                                        veniam.”
                                    </p>
                                    <ul class="seoif-des-list">
                                        <li class="seoif-des-item">
                                            <p class="text">Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet </p>
                                        </li>
                                        <li class="seoif-des-item">
                                            <p class="text">Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet sint. Velit officia consequat duis enim
                                                velit mollit.</p>
                                        </li>
                                        <li class="seoif-des-item">
                                            <p class="text">Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet sint. Velit officia consequat duis enim
                                                velit mollit. Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet sint. Velit officia consequat duis enim
                                                velit mollit.</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="seofaq-item collapse-item">
                                <div class="seofaq-head collapse-head">
                                    <h4 class="title">Đối tượng nên sử dụng dịch vụ SEO tổng thể website
                                    </h4>
                                    <i class="far fa-chevron-circle-down icon"></i>
                                </div>
                                <div class="seofaq-tbody collapse-body">
                                    <p class="text">
                                        “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet
                                        sint. Velit officia consequat duis enim velit mollit. Exercitation
                                        veniam.”
                                    </p>
                                    <ul class="seoif-des-list">
                                        <li class="seoif-des-item">
                                            <p class="text">Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet </p>
                                        </li>
                                        <li class="seoif-des-item">
                                            <p class="text">Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet sint. Velit officia consequat duis enim
                                                velit mollit.</p>
                                        </li>
                                        <li class="seoif-des-item">
                                            <p class="text">Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet sint. Velit officia consequat duis enim
                                                velit mollit. Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet sint. Velit officia consequat duis enim
                                                velit mollit.</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="seofaq-item collapse-item">
                                <div class="seofaq-head collapse-head">
                                    <h4 class="title">Một số điều cần lưu ý khi bạn chọn SEO tổng thể cho
                                        website</h4>
                                    <i class="far fa-chevron-circle-down icon"></i>
                                </div>
                                <div class="seofaq-tbody collapse-body">
                                    <p class="text">
                                        “Amet minim mollit non deserunt ullamco est sit aliqua dolor do amet
                                        sint. Velit officia consequat duis enim velit mollit. Exercitation
                                        veniam.”
                                    </p>
                                    <ul class="seoif-des-list">
                                        <li class="seoif-des-item">
                                            <p class="text">Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet </p>
                                        </li>
                                        <li class="seoif-des-item">
                                            <p class="text">Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet sint. Velit officia consequat duis enim
                                                velit mollit.</p>
                                        </li>
                                        <li class="seoif-des-item">
                                            <p class="text">Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet sint. Velit officia consequat duis enim
                                                velit mollit. Amet minim mollit non deserunt ullamco est sit
                                                aliqua dolor do amet sint. Velit officia consequat duis enim
                                                velit mollit.</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>