<?php

/**
 * Section name: Service Hosting Tech
 * Description: 
 * Author: Monamedia
 * Order: 9
 */
?>
<section class="clhtech horizontal-section-p">
    <div class="clhtech-w horizontal-section">
        <div class="clhtech-sticky">
            <div class="container">
                <div class="clhtech-top">
                    <div class="clhtech-tt">
                        <h3 class="host-tt">
                            Ngoài công nghệ ra Mona còn có
                            <p class="host-tt hl">
                                Hệ thống với cấu hình xịn xò bậc nhất hiện nay
                            </p>
                        </h3>
                    </div>
                </div>
                <div class="clhtech-l horizontal-scroll">
                    <div class="clhtech-i">
                        <div class="clhtech-in">
                            <p class="clhtech-in-txt">Dung lượng RAM lên đến</p>
                            <h3 class="clhtech-in-tt">
                                512 GB
                            </h3>
                            <p class="clhtech-in-des">
                                Gần bằng 128 laptop 4GB</br> RAM cộng lại
                            </p>
                        </div>
                        <div class="clhtech-img">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhtech-img-01.png" alt="">
                        </div>
                    </div>
                    <div class="clhtech-i">
                        <div class="clhtech-in">
                            <p class="clhtech-in-txt">Ổ đĩa nhanh gấp</p>
                            <h3 class="clhtech-in-tt style-pri">
                                12 lần
                            </h3>
                            <p class="clhtech-in-des style-pri">
                                Laptop bạn đang sử dụng hằng ngày
                            </p>
                        </div>
                        <div class="clhtech-img">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhtech-img-02.png" alt="">
                        </div>
                    </div>
                    <div class="clhtech-i">
                        <div class="clhtech-in">
                            <p class="clhtech-in-txt">Băng thông gấp</p>
                            <h3 class="clhtech-in-tt style-second">
                                200 lần
                            </h3>
                            <p class="clhtech-in-des style-second">
                                Tổng băng thông mạng ở nhà bạn
                            </p>
                        </div>
                        <div class="clhtech-img">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhtech-img-03.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>