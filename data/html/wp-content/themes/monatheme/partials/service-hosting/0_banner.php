<?php

/**
 * Section name: Service Hosting Banner
 * Description: 
 * Author: Monamedia
 * Order: 0
 */
?>
<section class="clhbn">
    <div class="clhbn-star">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/star-sky.png" alt="">
    </div>
    <div class="clhbn-w">
        <div class="clhbn-top">
            <div class="container">
                <div class="clhbn-c">
                    <h1 class="clhbn-hl host-tt">
                        Premium Cloud Hosting
                    </h1>
                    <p class="clhbn-txt">
                        Hệ thống ổn định, <span class="bold">x20</span> lần tốc độ xử lý
                    </p>
                </div>
            </div>
        </div>
        <div class="clhbn-bot">
            <div class="clhbn-br">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhbn-br.png" alt="">
            </div>
            <div class="clhbn-img">
                <div class="image">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/clhbn-img.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>