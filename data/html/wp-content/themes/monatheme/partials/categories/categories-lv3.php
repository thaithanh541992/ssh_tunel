<?php 
$current_taxonomy = get_queried_object();
?>
<main class="main">
    <section class="sec-blogt sec-blogpc2 sec-blogpc3">
        <div class="blogt blogpc2">
            <div class="blogt-wrap">
                <div class="blogt-dot"></div>
                <div class="container">
                    <div class="blogpc2-wrap">
                        <div class="blogpc2-py">
                            <div class="blogpc3-top">
                                <h2 class="title"><?php echo $current_taxonomy->name; ?></h2>
                                <div class="blogpc3-logo">
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/logo/host-less-logo.png" alt="">
                                </div>
                                <div class="des">
                                    <?php echo term_description( $current_taxonomy->term_id , $current_taxonomy->taxonomy ); ?>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php
                $sticky = get_option('sticky_posts');
                $post_ids = get_posts( array(
                    'post_type' => 'post',
                    'fields'          => 'ids',
                    'posts_per_page'  => -1,
                    'tax_query' => array(
                        array(
                            'taxonomy' => $current_taxonomy->taxonomy,
                            'field' => 'slug',
                            'terms' => $current_taxonomy->slug,
                        )
                    )
                ) );
                $result = array_intersect($post_ids, $sticky);

                $argsPost = [
                    'post_type'      => 'post',
                    'post_status'    => 'publish',
                    'order'          => 'DESC',
                    'meta_query'     => [
                        'relation' => 'AND',
                    ],
                    'tax_query'      => [
                        'relation' => 'AND',
                        array(
                            'taxonomy' => $current_taxonomy->taxonomy,
                            'field' => 'slug',
                            'terms' => $current_taxonomy->slug
                        )
                    ],
                ];
        
                if ( !empty( $sticky ) && !empty( $result ) ) {
                    $argsPost['post__in']       =   $sticky;
                }else{
                    $argsPost['posts_per_page'] = 6;
                    $argsPost['orderby']  = 'meta_value_num';
                    $argsPost['meta_key'] = '_mona_post_view';
                    $argsPost['order']    = 'DESC';
                }
        
                $outstanding_posts = new WP_Query( $argsPost );
                if( $outstanding_posts->have_posts() ){
                ?>
                <div class="blogpc3-sw">
                    <div class="swiper">
                        <div class="swiper-wrapper">
                            <?php 
                            while( $outstanding_posts->have_posts() ){
                                $outstanding_posts->the_post(); ?>
                            <div class="swiper-slide">
                                <?php 
                                /**
                                    * GET TEMPLATE PART
                                    * partials
                                    * box outstanding blog
                                    */
                                    $slug = '/partials/loop/box-outstanding';
                                    $name = 'blog-3';
                                    echo get_template_part($slug, $name);
                                ?>
                            </div>
                            <?php } wp_reset_query(); ?>
                        </div>
                    </div>
                </div>
                <?php } ?>
            </div>
        </div>
    </section>

    <?php 
	$post_type = 'post';
	$posts_per_page = 12;
	$paged = max( 1, get_query_var('paged') );
	$offset = ( $paged - 1 ) * $posts_per_page;
	$argsPost = [
		'post_type' => $post_type,
		'post_status' => 'publish',
		'posts_per_page' => $posts_per_page,
		'paged' => $paged,
		'offset' => $offset,
		'meta_query' => [
			'relation' => 'AND',
		],
		'tax_query' => [
			'relation'=>'AND',
            array(
                'taxonomy' => $current_taxonomy->taxonomy,
                'field' => 'slug',
                'terms' => $current_taxonomy->slug,
            )
		]
	];

	$postsMONA = new WP_Query( $argsPost );  
	?>
    <section class="sec-blogr">
        <div class="container">
            <div class="blogr">
                <h2 class="title">
                    <?php echo $current_taxonomy->name; ?>
                </h2>
                <div class="blogr-wrap">
                    <div class="blogr-flex">
                        <div class="blogr-left">
                            <?php if( $postsMONA->have_posts() ){ ?>
                            <form id="frmPostAjax">
								<div class="blogr-list is-loading-btn" id="monaPostList">
									<?php while( $postsMONA->have_posts() ){
										$postsMONA->the_post();
										?>
									<div class="blogr-item col-4">
										<?php 
										/**
										 * GET TEMPLATE PART
										 * partials
										 * blog box
										 */
										$slug = '/partials/loop/box';
										$name = 'blog';
										echo get_template_part($slug, $name);
										?>
									</div>
									<?php } wp_reset_query(); ?>
									<div class="pagination-products-ajax col-12 override">
										<?php mona_pagination_links_ajax( $postsMONA ); ?>
									</div>
								</div>
								<input type="hidden" name="post_type" value="<?php echo $post_type ?>" />
								<input type="hidden" name="posts_per_page" value="<?php echo $posts_per_page ?>" />
								<input type="hidden" name="paged" value="<?php echo $paged ?>" />
                                <?php if( is_category($current_taxonomy) ){ ?>
                                <input type="hidden" name="taxonomies[<?php echo $current_taxonomy->taxonomy ?>]" value="<?php echo $current_taxonomy->slug; ?>" />
                                <?php } ?>
							</form>
							<?php }else{ ?>
								
							<div class="mona-mess-empty">
								<p><?php echo __( 'Nội dung đang được cập nhật', 'monamedia' ) ?></p>
							</div>

							<?php } ?>
                        </div>
                        <div class="blogr-right">
                            <div class="blogr-pos">
                                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar_blog_detail_right')) : ?>
								<?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    
    
    <?php
    /**
     * GET TEMPLATE PART
     * contact section
     *  */ 
    $slug = '/partials/global/contact';
    $name = '';
    echo get_template_part($slug, $name);
    ?>

    <?php
	/**
	 * GET TEMPLATE PART
	 * categories menu section
	 *  */ 
	$slug = '/partials/global/categories';
	$name = 'menu';
	echo get_template_part($slug, $name);
	?>
	
	<?php
	/**
	 * GET TEMPLATE PART
	 * outstanding information section
	 *  */ 
	$slug = '/partials/global/outstanding';
	$name = 'information';
	echo get_template_part($slug, $name);
	?>
</main>