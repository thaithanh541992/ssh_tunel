<?php 
$current_taxonomy = get_queried_object();
$argsChildCurrentTaxonomy  = [
    'taxonomy' 		=> $current_taxonomy->taxonomy,
    'hide_empty'	=>	true,
    'parent'		=>	$current_taxonomy->term_id,
];
$child_of_current_taxonomy = get_terms(
	$argsChildCurrentTaxonomy
);
$current_paged = max( 1, get_query_var('paged') );
?>
<main class="main">
    <?php if( $current_paged == 1 && !isset( $_GET['keyword'] ) && !isset( $_GET['post_tags'] )  ){ ?>
        <!-- Giới thiệu chuyên mục & bài viết nổi bật -->
        <section class="sec-blogt sec-blogtc2">
            <div class="blogt blogtc2">
                <div class="blogt-wrap">
                    <div class="blogt-dot"></div>
                    <div class="container">
                        <div class="blogtc2-py">
                            <div class="blogtc2-top">
                                <h2 class="title"><?php echo $current_taxonomy->name; ?></h2>
                                <div class="des">
                                    <?php echo term_description( $current_taxonomy->term_id , $current_taxonomy->taxonomy ); ?>
                                </div>
                            </div>
                            <?php
                            $sticky = get_option('sticky_posts');
                            $post_ids = get_posts( array(
                                'post_type' => 'post',
                                'fields'          => 'ids',
                                'posts_per_page'  => -1,
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => $current_taxonomy->taxonomy,
                                        'field' => 'slug',
                                        'terms' => $current_taxonomy->slug,
                                    )
                                )
                            ) );
                            $result = array_intersect($post_ids, $sticky);

                            $argsPost = [
                                'post_type'      => 'post',
                                'post_status'    => 'publish',
                                'order'          => 'DESC',
                                'meta_query'     => [
                                    'relation' => 'AND',
                                ],
                                'tax_query'      => [
                                    'relation' => 'AND',
                                    array(
                                        'taxonomy' => $current_taxonomy->taxonomy,
                                        'field' => 'slug',
                                        'terms' => $current_taxonomy->slug
                                    )
                                ],
                            ];
                    
                            if ( !empty( $sticky ) && !empty( $result ) ) {

                                $argsPost['post__in']       =   $sticky;
                    
                            }else{

                                $argsPost['posts_per_page'] = 6;

                            }
                    
                            $outstanding_posts = new WP_Query( $argsPost );
                            if( $outstanding_posts->have_posts() ){
                            ?>
                            <div class="blogtc2-sw">
                                <div class="swiper">
                                    <div class="swiper-wrapper">
                                        <?php while( $outstanding_posts->have_posts() ){
                                            $outstanding_posts->the_post(); ?>
                                        <div class="swiper-slide">
                                        <?php 
                                        /**
                                            * GET TEMPLATE PART
                                            * partials
                                            * box outstanding blog
                                            */
                                            $slug = '/partials/loop/box-outstanding';
                                            $name = 'blog';
                                            echo get_template_part($slug, $name);
                                        ?>
                                        </div>
                                        <?php } wp_reset_query(); ?>
                                    </div>
                                </div>
                                <div class="blogtc2-control">
                                    <div class="blogtc2-prev"><i class="fas fa-chevron-left icon"></i></div>
                                    <div class="blogtc2-next"><i class="fas fa-chevron-right icon"></i></div>
                                </div>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!-- end -->

        <!-- Chuyên mục con -->
        <?php if( !empty( $child_of_current_taxonomy ) ){ ?>
        <section class="blog-post web-design fix">
            <div class="container">
                <div class="sec-header">
                    <div class="sec-tt">
                        <h2 data-aos="fade-up"><?php echo __('Tổng hợp bài viết', 'monamedia'); ?></h2>
                    </div>
                </div>
                <div class="blog-post-list d-flex f-ctn">
                    <?php 
                    $countTax = 1;
                    foreach ($child_of_current_taxonomy as $keyChild => $child) { ?>
                    <div class="blog-post-item col col-4" data-aos="fade-up" data-aos-delay="<?php echo $countTax*100; ?>">
                        <div class="blog-post-wrap">
                            <div class="blog-post-img">
                                <?php $mona_taxonomy_thumbnail = get_field('mona_taxonomy_thumbnail', $child);
                                if( empty( $mona_taxonomy_thumbnail ) ){ ?>
                                    <img src="<?php echo get_template_directory_uri() ?>/public/helpers/images/global-thumbnail-mona.png" alt="">
                                <?php }else{ ?> 
                                    <?php echo wp_get_attachment_image($mona_taxonomy_thumbnail, 'thumbnail-taxonomy'); ?>	
                                <?php } ?>
                            </div>
                            <div class="blog-post-content">
                                <div class="blog-post-header">
                                    <h3 class="name">
                                        <a href="<?php echo get_term_link($child) ?>"><?php echo $child->name; ?></a>
                                    </h3>
                                </div>
                                <div class="blog-post-body">
                                    <?php 
                                    $post_type = 'post';
                                    $posts_per_page = 3;
                                    $paged = max( 1, get_query_var('paged') );
                                    $offset = ( $paged - 1 ) * $posts_per_page;
                                    $argsPost = [
                                        'post_type' => $post_type,
                                        'post_status' => 'publish',
                                        'posts_per_page' => $posts_per_page,
                                        'paged' => $paged,
                                        'offset' => $offset,
                                        'meta_query' => [
                                            'relation' => 'AND',
                                        ],
                                        'tax_query' => [
                                            'relation'=>'AND',
                                            array(
                                                'taxonomy' => $child->taxonomy,
                                                'field' => 'slug',
                                                'terms' => $child->slug
                                            )
                                        ]
                                    ];
        
                                    $postsMONA = new WP_Query( $argsPost );
                                    if( $postsMONA->have_posts() ){  ?>
                                    <div class="blog-post-body-list">
                                        <?php 
                                        $countPost = 1;
                                        while( $postsMONA->have_posts() ){
                                            $postsMONA->the_post();
                                            global $post; ?>
                                        <div class="blog-post-body-item">
                                            <a href="<?php echo get_the_permalink( $post ); ?>">
                                                <span class="num"><?php echo $countPost; ?>.</span>
                                                <span class="name"><?php echo get_the_title( $post ); ?></span>
                                            </a>
                                        </div>
                                        <?php 
                                            $countPost++;
                                        } wp_reset_query(); ?>
                                    </div>
                                    <?php } ?>

                                </div>

                                <?php if( $postsMONA->found_posts > $posts_per_page ){ ?>
                                <div class="blog-post-footer">
                                    <a href="<?php echo get_term_link( $child ); ?>" class="read-all read-more ani-arrow">
                                        <?php echo __('Đọc tất cả', 'monamedia'); ?>
                                    </a>
                                </div>
                                <?php } ?>

                                <div class="blog-post-footer-time d-flex">
                                    <div class="sticky-cate"><?php echo $current_taxonomy->name; ?></div>
                                    <div class="item-wrap d-flex">
                                        <div class="item d-flex">
                                            <div class="img">
                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-docs.png" alt="">
                                            </div>
                                            <div class="txt"><?php echo $child->count < 10 ? '0'.$child->count : $child->count; ?><?php echo __('  bài đọc' , 'monamedia'); ?></div>
                                        </div>
                                        <div class="item d-flex">
                                            <div class="img">
                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-clock.png" alt="">
                                            </div>
                                            <div class="txt"><?php echo $child->count*5 ?><?php echo __('  phút đọc','monamedia'); ?></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php 
                        if( $countTax < 3 ){
                            $countTax++;
                        }else{
                            $countTax = 1;
                        }
                    } ?>
                </div>
            </div>
        </section>
        <?php } ?>
        <!-- end -->

        <!-- Đăng ký nhận bản tin -->
        <?php $section_register_2_shortcode = mona_get_option( 'section_register_2_shortcode' );
        if( !empty( $section_register_2_shortcode ) ){ ?>
        <div class="sec-blogrc2">
            <div class="container">
                <div class="blogrc2">
                    <div class="blogrc2-wrap">
                        <div class="blogrc2-py">
                            <div class="blogrc2-content">
                                <?php $title    = mona_get_option('section_register_2_title');
                                $subtitle       =   mona_get_option('section_register_2_subtitle'); ?>
                                <p class="t-small"><?php echo $subtitle; ?></p>
                                <h2 class="t-lager"><?php echo $title; ?></h2>
                                <div class="blogrc2-form">
                                    <?php echo do_shortcode($section_register_2_shortcode); ?>
                                </div>
                                <div class="blogrc2-decor">
                                    <img srcset="<?php echo get_site_url() ?>/template/assets/images/blog/blogrc2-icon.png 2x" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <?php } ?>
        <!-- end -->

    <?php }else{ ?>
    
        <!-- Cấp 2 trang 2 -->
        <section class="sec-blogt sec-blogpc2">
            <div class="blogt blogpc2">
                <div class="blogt-wrap">
                    <div class="blogt-dot"></div>
                    <div class="container">
                        <div class="blogpc2-wrap">
                            <div class="blogpc2-py">
                                <?php if( !isset( $_GET['keyword'] ) && !isset( $_GET['post_tags'] ) ){ ?>
                                <div class="blogpc2-top">
                                    <h2 class="title">
                                        <?php echo $current_taxonomy->name; ?>
                                    </h2>
                                    <div class="des">
                                        <?php echo term_description( $current_taxonomy->term_id , $current_taxonomy->taxonomy ); ?>
                                    </div>
                                </div>   
                                <?php } ?> 
                                <form id="frmSearching" action="<?php echo get_term_link( $current_taxonomy ); ?>">
                                    <div class="blogpc2-form">
                                        <div class="blogpc2-control">
                                            <input placeholder="<?php echo __('Tìm kiếm', 'monamedia'); ?>" name="keyword" value="<?php echo esc_attr( @$_GET['keyword'] ); ?>" type="text" class="blogpc2-input">
                                            <span class="icon">
                                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-search-sw.png"
                                                    alt="">
                                            </span>
                                        </div>
                                    </div>
                                    <?php 
                                    $mona_taxonomy_outstanding_post_tags = get_field('mona_taxonomy_outstanding_post_tags', $current_taxonomy);
                                    if( !empty( $mona_taxonomy_outstanding_post_tags ) ){
                                        $getPostTags = !empty( $_GET['post_tags'] ) ? $_GET['post_tags'] : array();
                                    ?>
                                    <div class="blogpc2-tag">
                                        <div class="blogpc2-tag-list">
                                            <?php foreach ($mona_taxonomy_outstanding_post_tags as $key => $tag) { ?>
                                            <label class="postTagJS">
                                                <input type="checkbox" name="post_tags[]" value="<?php echo $tag->slug; ?>" <?php checked( in_array( $tag->slug , $getPostTags ), 1, 'checked' ); ?>/>
                                                <span class="blogpc2-tag-item postTagName"><?php echo $tag->name; ?></span>
                                            </label>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>
                                </form>
                                <?php if( isset( $_GET['keyword'] ) || isset( $_GET['post_tags'] ) ){ ?>
                                <div class="blogpc2-result">
                                    <p class="t-lager">
                                        <?php echo __('Kết quả tìm kiếm: ', 'monamedia'); ?><?php echo esc_attr(@$_GET['keyword']); ?>
                                    </p>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

        <!-- Cấp 2 trang 2
        Bài viết nổi bật
        -->
        <?php 
        $sticky = get_option('sticky_posts');
        $post_ids = get_posts( array(
            'post_type' => 'post',
            'fields'          => 'ids',
            'posts_per_page'  => -1,
            'tax_query' => array(
                array(
                    'taxonomy' => $current_taxonomy->taxonomy,
                    'field' => 'slug',
                    'terms' => $current_taxonomy->slug,
                )
            )
        ) );
        $result = array_intersect($post_ids, $sticky);
        
        $argsPost = [
            'post_type'      => 'post',
            'post_status'    => 'publish',
            'order'          => 'DESC',
            'meta_query'     => [
                'relation' => 'AND',
            ],
            'tax_query'      => [
                'relation' => 'AND',
                array(
                    'taxonomy' => $current_taxonomy->taxonomy,
                    'field' => 'slug',
                    'terms' => $current_taxonomy->slug
                )
            ],
        ];

        if ( !empty( $result ) ) {
            $argsPost['post__in']       =   $result;
        }else{
            $argsPost['posts_per_page'] = 6;
            $argsPost['orderby']  = 'meta_value_num';
            $argsPost['meta_key'] = '_mona_post_view';
            $argsPost['order']    = 'DESC';
        }

        $outstanding_posts = new WP_Query( $argsPost );
        if( $outstanding_posts->have_posts() && !isset( $_GET['keyword'] ) && !isset( $_GET['post_tags'] ) && $current_paged == 1 ){
            while( $outstanding_posts->have_posts() ){
                $outstanding_posts->the_post();
                global $post;
            ?>
            <section class="sec-blogpc2-mid">
                <div class="container">
                    <div class="blogpc2-mid">
                        <div class="blogpc2-mid-wrap">
                            <div class="blogpc2-mid-flex">
                                <div class="blogpc2-mid-left">
                                    <div class="blogpc2-mid-img">
                                        <div class="inner">
                                            <a href="<?php echo get_the_permalink( $post ); ?>">
                                                <?php 
                                                if( !empty( get_the_post_thumbnail($post, 'thumbnail-post') ) ){
                                                    echo get_the_post_thumbnail($post, 'thumbnail-post');
                                                }else{ ?>
                                                    <img src="<?php echo get_template_directory_uri() ?>/public/helpers/images/global-thumbnail-mona.png" alt="">
                                                <?php } ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="blogpc2-mid-right">
                                    <div class="blogpc2-mid-content">
                                        <div class="blogpc2-mid-top">
                                            <?php 
                                            $author_id = get_post_field( 'post_author', $post->ID );
                                            $author = get_userdata($author_id);
                                            if( !empty( $author ) ){
                                                $author_avatar = get_field('mona_user_avatar', $author);
                                                $author_display = get_field('mona_user_display_name', $author);
                                                ?>
                                            <div class="blogpc2-mid-gr">
                                                <span class="blogpc2-mid-avt">
                                                    <?php if( empty( $author_avatar ) ){ ?>
                                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-user.png" alt="">
                                                    <?php }else{ ?>
                                                        <?php echo wp_get_attachment_image($author_avatar, 'full'); ?>
                                                    <?php } ?>
                                                </span>
                                                <span class="text">
                                                    <?php echo ( !empty( $author_display ) ) ? $author_display : $author->display_name; ?>
                                                </span>
                                            </div>
                                            <?php } ?>
                                            <div class="blogpc2-mid-dflex">
                                                <div class="blogpc2-mid-box">
                                                    <span class="text">
                                                        <?php echo get_the_date( 'd F, Y' , $post ); ?>
                                                    </span>
                                                </div>
                                                <div class="blogpc2-mid-box">
                                                    <span class="icon">
                                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/alarm-clock.png" alt="">
                                                    </span>
                                                    <span class="text">
                                                        <?php echo __('5 phút đọc', 'monamedia'); ?>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="blogpc2-mid-inner">
                                            <a href="<?php echo get_the_permalink( $post ); ?>" class="link">
                                                <?php echo get_the_title( $post ); ?>
                                            </a>
                                            <div class="des">
                                                <?php echo get_the_excerpt( $post ); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php } wp_reset_query(); 
        } ?>
        <!-- end sticky post c2 -->

        <?php
        $post_type = 'post';
        $posts_per_page = 8;
        $paged = max( 1, get_query_var('paged') );
        $offset = ( $paged - 1 ) * $posts_per_page;
        $argsPost = [
            'post_type' => $post_type,
            'post_status' => 'publish',
            'posts_per_page' => $posts_per_page,
            'paged' => $paged,
            'offset' => $offset,
            'meta_query' => [
                'relation' => 'AND',
            ],
            'tax_query' => [
                'relation'=>'AND',
                array(
                    'taxonomy' => $current_taxonomy->taxonomy,
                    'field' => 'slug',
                    'terms' => $current_taxonomy->slug
                )
            ]
        ];

        if( isset( $_GET['keyword'] ) && !empty( $_GET['keyword'] )  ){
            $argsPost['s'] = esc_attr( $_GET['keyword'] );
        }
        if( isset( $_GET['post_tags'] ) && !empty( $_GET['post_tags'] ) ){
            $argsPost['tax_query'][] = [
                'taxonomy' => 'post_tag',
                'field' => 'slug',
                'terms' => (array)@$_GET['post_tags']
            ];
        }
        
        $postsMONA = new WP_Query( $argsPost );
        if( !$outstanding_posts->have_posts() ){
            $class_blog = 'fix';
        }elseif( !isset( $_GET['keyword'] ) && !isset( $_GET['post_tags'] ) ){
            $class_blog = 'fix';
        }else{
            $class_blog = '';
        }
        if( $postsMONA->have_posts() ){ ?>
        <section class="sec-blogr sec-bloghc2 sec-blogpc2 <?php echo $class_blog; ?>">
            <div class="container">
                <div class="blogr">
                    <div class="blogr-wrap">
                        <div class="blogr-flex">
                            <div class="blogr-left override">
                                <div class="blogr-list">
                                    <?php 
                                    while( $postsMONA->have_posts() ){
                                        $postsMONA->the_post();
                                        ?>
                                    <div class="blogr-item col-3">
                                        <?php 
                                        /**
                                         * GET TEMPLATE PART
                                         * partials
                                         * blog box
                                         */
                                        $slug = '/partials/loop/box';
                                        $name = 'blog';
                                        echo get_template_part($slug, $name);
                                        ?>
                                    </div>
                                    <?php } wp_reset_query(); ?>
                                </div>
                                <?php mona_pagination_links( $postsMONA ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php }else{ ?>
        <section class="sec-blogr sec-bloghc2 sec-blogpc2 <?php echo $class_blog; ?>">
            <div class="container">
                <div class="blogr">
                    <div class="blogr-wrap">
                        <div class="blogr-flex">
                            <div class="blogr-left">

                                <div class="mona-mess-empty">
                                    <p><?php echo __( 'Không tìm thấy bài viết phù hợp với từ khoá tìm kiếm của bạn', 'monamedia' ) ?></p>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php } ?>
        
    <?php } ?>

    <!-- Video nổi bật thuộc chuyên mục mục -->
    <?php 
    $post_type = 'mona_video';
    $posts_per_page = -1;
    $argsPost = [
        'post_type' => $post_type,
        'post_status' => 'publish',
        'posts_per_page' => $posts_per_page,
        'meta_query' => [
            'relation' => 'AND',
            [
                'key' => 'mona_video_attachment_link',
                'value' => '',
                'compare' => '!=',
            ]
        ],
        'tax_query' => [
            'relation'=>'AND',
        ]
    ];

    $parent = get_taxonomy_term_root( $current_taxonomy );
    if( !empty( $parent ) ){
        $argsPost['tax_query'][] =   array(
            'taxonomy' => $parent->taxonomy,
            'field' => 'slug',
            'terms' => $parent->slug
        );
    }else{
        $argsPost['tax_query'][] =   array(
            'taxonomy' => $current_taxonomy->taxonomy,
            'field' => 'slug',
            'terms' => $current_taxonomy->slug
        );
    }

    $postsMONA = new WP_Query( $argsPost );
    if( $postsMONA->have_posts() ){
        $mona_taxonomy_video_title = get_field('mona_taxonomy_video_title', $current_taxonomy);
        $mona_taxonomy_video_description = get_field('mona_taxonomy_video_description', $current_taxonomy);  ?>
        <section class="sec-blogvc2 <?php echo ( !isset( $_GET['keyword'] ) && !isset( $_GET['post_tags'] ) ) ? '' : 'fix'; ?>">
            <div class="container">
                <div class="blogvc2">
                    <div class="blogvc2-top">
                        <h2 class="title">
                            <?php echo !empty( $mona_taxonomy_video_title ) ? $mona_taxonomy_video_title : __('Video nổi bật', 'monamedia'); ?>
                        </h2>
                        <?php if( !empty( $mona_taxonomy_video_description ) ){ ?>
                        <p class="des">
                            <?php echo $mona_taxonomy_video_description; ?>
                        </p>
                        <?php } ?>
                    </div>
                    <div class="blogvc2-wrap gallery">
                        <div class="blogvc2-list">
                            <?php while( $postsMONA->have_posts() ){
                                $postsMONA->the_post(); ?>
                            <div class="blogvc2-item col-4">
                                <?php 
                                /**
                                 * GET TEMPLATE PART
                                 * partials
                                 * video box
                                 */
                                $slug = '/partials/loop/box';
                                $name = 'video';
                                echo get_template_part($slug, $name);
                                ?>
                            </div>
                            <?php } wp_reset_query(); ?>
                        </div>
                        <!-- <a href="#" class="btn-second">
                            <span class="txt">
                                Xem thêm
                            </span>
                            <span class="icon">
                                <img src="<?php //echo get_site_url() ?>/template/assets/images/icon-arrow-right2.png" alt="">
                            </span>
                        </a> -->
                    </div>
                </div>
            </div>
        </section>
    <?php } ?>
    <!-- end -->

    
    <?php if( $current_paged == 1 && !isset( $_GET['keyword'] ) && !isset( $_GET['post_tags'] )  ){ ?>
        <?php 
        $post_type = 'post';
        $posts_per_page = 8;
        $paged = max( 1, get_query_var('paged') );
        $offset = ( $paged - 1 ) * $posts_per_page;
        $argsPost = [
            'post_type' => $post_type,
            'post_status' => 'publish',
            'posts_per_page' => $posts_per_page,
            'paged' => $paged,
            'offset' => $offset,
            'meta_query' => [
                'relation' => 'AND',
            ],
            'tax_query' => [
                'relation'=>'AND',
                array(
                    'taxonomy' => $current_taxonomy->taxonomy,
                    'field' => 'slug',
                    'terms' => $current_taxonomy->slug
                )
            ]
        ];

        $postsMONA = new WP_Query( $argsPost );
        if( $postsMONA->have_posts() ){  ?>
        <section class="sec-blogr sec-bloghc2">
            <div class="container">
                <div class="blogr">
                    <h2 class="title"><?php echo __('Bài viết nổi bật', 'monamedia'); ?></h2>
                    <div class="blogr-wrap">
                        <div class="blogr-flex">
                            <div class="blogr-left override">
                                <div class="blogr-list">
                                    <?php 
                                    while( $postsMONA->have_posts() ){
                                        $postsMONA->the_post();
                                        ?>
                                    <div class="blogr-item col-3">
                                        <?php 
                                        /**
                                         * GET TEMPLATE PART
                                         * partials
                                         * blog box
                                         */
                                        $slug = '/partials/loop/box';
                                        $name = 'blog';
                                        echo get_template_part($slug, $name);
                                        ?>
                                    </div>
                                    <?php } wp_reset_query(); ?>
                                </div>
                                <?php mona_pagination_links( $postsMONA ); ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <?php } ?>
    <?php } ?>

    <?php
    /**
     * GET TEMPLATE PART
     * contact section
     *  */ 
    $slug = '/partials/global/contact';
    $name = '';
    echo get_template_part($slug, $name);
    ?>

    <?php
	/**
	 * GET TEMPLATE PART
	 * categories menu section
	 *  */ 
	$slug = '/partials/global/categories';
	$name = 'menu';
	echo get_template_part($slug, $name);
	?>
	
	<?php
	/**
	 * GET TEMPLATE PART
	 * outstanding information section
	 *  */ 
	$slug = '/partials/global/outstanding';
	$name = 'information';
	echo get_template_part($slug, $name);
	?>
</main>