<?php

/**
 * Section name: Service VPS Image
 * Description: 
 * Author: Monamedia
 * Order: 5
 */
?>
<section class="pv-team">
    <div class="container">
        <div class="pv-team-w">
            <div class="pv-team-t">
                <div class="pv-team-tt">
                    <h2 class="host-tt">Với đội ngũ hỗ trợ 24/7 của Mona</h2>
                    <p class="host-tt hl">Mọi vấn đề đều được giải quyết triệt để</p>
                </div>
                <p class="txt-center pv-team-txt">
                    Mona chúng tôi luôn <span class="line-team">tự hào về việc hỗ trợ khách hàng</span> của mình hết sức tận
                    tình. <br>
                    <span class="line-team-2">Luôn có nhân sự phục vụ 24/24.</span> Giải quyết nhanh gọn mọi vấn đề của bạn
                </p>
            </div>
            <div class="pv-team-in">
                <div class="pv-team-b">
                    <div class="pv-team-decor">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvteamimg.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>