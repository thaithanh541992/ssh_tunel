<?php

/**
 * Section name: Service VPS Price
 * Description: 
 * Author: Monamedia
 * Order: 7
 */
?>
<section class="pv-price">
    <div class="container">
        <div class="pv-price-w">
            <div class="pv-price-t">
                <div class="pv-price-tt">
                    <h2 class="host-tt">Mona Host không ma trận giá</h2>
                    <p class="host-tt hl">Chúng tôi chỉ cung cấp cho khách hàng giá trị tốt nhất</p>
                </div>
                <p class="pv-price-txt">
                    Do thị trường hosting chứa đầy ma trận chỉ-để-bán-được-hàng, chúng tôi phải <span class="line">tự nghiên
                        <br> cứu và phát triển sản phẩm hosting của riêng mình,</span> trên giao diện thân thiện với người dùng.
                </p>
            </div>
            <div class="pv-price-in">
                <div class="pv-price-b">
                    <div class="pv-price-lt">
                        <div class="pv-price-decor-01 abs ">
                            <div class="pv-price-decor-com">
                                <p class="txt">
                                    Tối ưu hoá tỷ lệ <br> chuyển đổi CRO
                                </p>
                            </div>
                        </div>
                        <div class="pv-price-decor-02 abs ">
                            <div class="pv-price-decor-com">
                                <p class="txt">
                                    Đồ hoạ <br> chất lượng cao
                                </p>
                            </div>
                        </div>
                        <div class="pv-price-decor-03 abs ">
                            <div class="pv-price-decor-com">
                                <p class="txt">
                                    Tối ưu trên <br> mọi thiết bị
                                </p>
                            </div>
                        </div>
                        <div class="pv-price-decor-04 abs ">
                            <div class="pv-price-decor-com">
                                <p class="txt">
                                    Tối ưu hoá công <br> cụ tìm kiếm SEO
                                </p>
                            </div>
                        </div>
                        <div class="pv-price-decor-05 abs">
                            <div class="pv-price-decor-com">
                                <p class="txt">
                                    Tối ưu hoá các <br> trang xã hội SMO
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="pv-price-decor">
                        <a href="" class="pv-price-btn btn-clh">
                            <p class="txt txt-center">
                                Xem quá trình phát triển sản phẩm và những giá trị mà <br> MONA.Host mang lại cho khách hàng của
                                mình
                            </p>
                        </a>
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvpriceBg.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pv-price-element-1">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pv-10plus-element.png" alt="">
    </div>
    <div class="pv-price-element-2">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pv-10plus-element1.png" alt="">
    </div>
</section>