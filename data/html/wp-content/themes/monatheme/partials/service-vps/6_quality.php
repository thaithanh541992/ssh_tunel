<?php

/**
 * Section name: Service VPS Quality
 * Description: 
 * Author: Monamedia
 * Order: 6
 */
?>
<section class="pv-ps">
    <div class="pv-ps-br">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/cloud-top.png" alt="">
    </div>
    <div class="container">
        <div class="pv-ps-header">
            <div class="pv-ps-top">
                <div class="pv-ps-common">
                    <div class="pv-ps-tt">
                        <h2 class="host-tt">Sở hữu VPS chất lượng cao, ổn định và an toàn</h2>
                        <p class="host-tt hl">Với dịch vụ Premium Cloud VPS tại MONA.Host</p>
                    </div>
                    <p class="pv-ps-txt">
                        Hợp tác với Mona bạn được nhiều hơn là mất. Với sự thống trị của Mona hiện nay, chúng tôi <br>
                        <span class="line">cam kết sẽ luôn ưu tiên sự thành công của khách hàng lên đầu.</span>
                    </p>
                </div>
            </div>
            <div class="pv-ps-bottom">
                <div class="pv-ps-b">
                    <div class="pv-ps-lt">
                        <div class="pv-ps-decor-01 abs ">
                            <div class="pv-ps-decor-com">
                                <p class="txt">
                                    Dễ dàng quản lý </br> qua web và SSH
                                </p>
                            </div>
                        </div>
                        <div class="pv-ps-decor-02 abs ">
                            <div class="pv-ps-decor-com">
                                <p class="txt">
                                    Máy chủ ảo riêng </br> biệt, bảo mật cao
                                </p>
                            </div>
                        </div>
                        <div class="pv-ps-decor-03 abs ">
                            <div class="pv-ps-decor-com">
                                <p class="txt">
                                    Cấu hình theo nhu cầu, </br> tuỳ ý nâng cấp và thu hồi
                                </p>
                            </div>
                        </div>
                        <div class="pv-ps-decor-04 abs ">
                            <div class="pv-ps-decor-com">
                                <p class="txt">
                                    Hỗ trợ kỹ thuật</br>
                                    24/7/365
                                </p>
                            </div>
                        </div>
                        <div class="pv-ps-decor-05 abs">
                            <div class="pv-ps-decor-com">
                                <p class="txt">
                                    Giá cả hợp lý</br>
                                    không phát sinh chi phí
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="pv-ps-decor">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvPeople.png" alt="">
                        <a href="" class="pv-ps-btn btn-clh">
                            <p class="txt">
                                Đăng ký sỡ hữu VPS chất lượng tại MONA.Host ngay để cảm nhận <br> được sự khác biệt và hài lòng với
                                dịch vụ của chúng tôi
                            </p>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pv-ps-cloud-bot">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/cloud-bot.png" alt="">
    </div>
</section>