<?php

/**
 * Section name: Service VPS Statistic
 * Description: 
 * Author: Monamedia
 * Order: 4
 */
?>
<section class="session-statistical">
    <div class="statistical-traffic">
        <div class="statistical-traffic-bg">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-building-1.png" alt="" />
        </div>
        <div class="panda-rocket">
            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-panda-rocket.png" alt="" />
        </div>

        <div class="traffic">
            <div class="container">
                <div class="traffic-content">
                    <h2 class="host-tt traffic-content-tt">
                        Hệ thống Hosting Mona
                        <p class="traffic-content-sub-tt txt-bg bg-lms">
                            Có khả năng chịu tải cao
                        </p>
                    </h2>
                    <p class="traffic-content-desc">
                        Chúng tôi hiểu bạn cần
                        <span class="fw-600"><u>“tự tin”</u></span> triển khai những
                        dự án có tải trọng cao, đó là lý do tại sao các máy chủ của
                        chúng tôi được thiết kế để
                        <span class="fw-600"><u>hoạt động với hiệu suất cao mà không phải lo lắng về độ
                                trễ hoặc thời gian ngừng hoạt động</u></span>
                    </p>
                </div>
            </div>
        </div>
        <div class="traffic-decor">
            <div class="traffic-decor-img">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-trafic-1.png" alt="" />
            </div>
            <div class="traffic-decor-img">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-trafic-3.png" alt="" />
            </div>
            <div class="traffic-decor-img">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-trafic-2.png" alt="" />
            </div>
        </div>
        <div class="traffic-decor">
            <div class="traffic-decor-img-line">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-trafic-line-1.png" alt="" />
            </div>
            <div class="traffic-decor-img-line">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-trafic-line-2.png" alt="" />
            </div>
            <div class="traffic-decor-img-line">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/statistical-trafic-line-3.png" alt="" />
            </div>
        </div>
        <div class="communication">
            <div class="communication-content">
                <h2 class="host-tt communication-content-tt">
                    Nền tảng vững chắc
                    <p class="communication-content-sub-tt txt-bg bg-host">
                        Mở rộng không giới hạn
                    </p>
                </h2>
                <p class="communication-content-desc">
                    Nền tảng của chúng tôi cho phép bạn
                    <span class="txt-lms fw-600">mở rộng quy mô khách hàng</span>
                    một cách
                    <span class="txt-digital fw-600">nhanh chóng và dễ dàng</span>,
                    cho phép bạn tiếp đón bao nhiêu khách hàng tuỳ thích - vì vậy
                    bạn có thể bắt đầu thấy
                    <span class="txt-nhtq fw-600">lợi nhuận gần như ngay lập tức!</span>
                </p>

                <div class="quote-wrap">
                    <div class="quote-uptime">
                        <div class="quote quote-bg-software quote-bottom-right">
                            Uptime 99,9%
                        </div>
                    </div>
                    <div class="quote-backup">
                        <div class="quote quote-bg-nhtq bg-digital quote-bottom-right">
                            CPU Siêu tốc
                        </div>
                    </div>

                    <div class="quote-banwidth">
                        <div class="quote quote-bg-host">Bandwidth 20Gbps</div>
                    </div>
                    <div class="quote-ssd">
                        <div class="quote quote-bg-lms">SSD NVME U.2</br>
                            x20 tốc độ đọc/ghi</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="statistical-img">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/panda-group-cpu.png" alt="" />
    </div>
</section>