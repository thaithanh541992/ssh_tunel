<?php

/**
 * Section name: Service VPS Tech
 * Description: 
 * Author: Monamedia
 * Order: 9
 */
?>
<section class="pv-tech horizontal-section-p">
    <div class="pv-tech-w horizontal-section">
        <div class="pv-tech-sticky">
            <div class="container">
                <div class="pv-tech-top">
                    <div class="pv-tech-tt">
                        <h2 class="host-tt">
                            Ngoài công nghệ ra Mona còn có
                        </h2>
                        <p class="host-tt hl">
                            Hệ thống với cấu hình xịn xò bậc nhất hiện nay
                        </p>
                    </div>
                </div>
                <div class="pv-tech-l horizontal-scroll">
                    <div class="pv-tech-i">
                        <div class="pv-tech-in">
                            <p class="pv-tech-in-txt">Dung lượng RAM lên đến</p>
                            <h3 class="pv-tech-in-tt">
                                512 GB
                            </h3>
                            <p class="pv-tech-in-des">
                                Gần bằng 128 laptop 4GB</br> RAM cộng lại
                            </p>
                        </div>
                        <div class="pv-tech-img">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvtech-img-01.png" alt="">
                        </div>
                    </div>
                    <div class="pv-tech-i">
                        <div class="pv-tech-in">
                            <p class="pv-tech-in-txt">Ổ đĩa nhanh gấp</p>
                            <h3 class="pv-tech-in-tt style-pri">
                                12 lần
                            </h3>
                            <p class="pv-tech-in-des style-pri">
                                Laptop bạn đang sử dụng <br> hằng ngày
                            </p>
                        </div>
                        <div class="pv-tech-img">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvtech-img-02.png" alt="">
                        </div>
                    </div>
                    <div class="pv-tech-i">
                        <div class="pv-tech-in">
                            <p class="pv-tech-in-txt">Băng thông gấp</p>
                            <h3 class="pv-tech-in-tt style-second">
                                200 lần
                            </h3>
                            <p class="pv-tech-in-des style-second">
                                Tổng băng thông mạng ở <br> nhà bạn
                            </p>
                        </div>
                        <div class="pv-tech-img">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvtech-img-03.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>