<?php

/**
 * Section name: Service VPS Customer
 * Description: 
 * Author: Monamedia
 * Order: 11
 */
?>
<section class="pv-customer horizontal-section-p">
    <div class="pv-customer-w horizontal-section">
        <div class="pv-customer-sticky">
            <div class="container">
                <div class="d-flex">
                    <div class="pv-customer-tt">
                        <div class="pv-customer-tt-col">
                            <h2 class="host-tt">
                                Chúng tôi đã giải quyết nhiều trường hợp
                            </h2>
                            <p class="host-tt sub-tt bg-color bg-color-blue">Đặt niềm tin sai chỗ khi đăng ký VPS ngoài</p>
                        </div>
                        <div class="pv-customer-desc-col">
                            <p class="pv-customer-r-des">
                                <span class="line-01">Chúng tôi luôn tự hào</span> mỗi khi nhắc đến những dịch vụ của Mona.
                                Vì đó là điều làm nên <span class="line">
                                    sự tin tưởng tuyệt đối của khách hàng
                                </span> vào chúng tôi.
                            </p>
                        </div>
                    </div>
                </div>
                <div class="pv-customer-list horizontal-scroll">
                    <div class="pv-customer-i">
                        <div class="container">
                            <div class="pv-customer-i-inner">
                                <div class="pv-customer-i-l">
                                    <div class="pv-customer-in">
                                        <h3 class="pv-customer-tx">
                                            Photocopy Thiên Phú
                                        </h3>
                                        <div class="pv-customer-ad">
                                            <a href="" class="pv-customer-ad-i">
                                                <span class="icon">
                                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/loca.svg" alt="">
                                                </span>
                                                <span class="txt">
                                                    Hồ Chí Minh
                                                </span>
                                            </a>
                                            <a href="" class="pv-customer-ad-i">
                                                <span class="icon">
                                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/dnl.svg" alt="">
                                                </span>
                                                <span class="txt">
                                                    Doanh nghiệp lớn
                                                </span>
                                            </a>
                                        </div>
                                        <p class="pv-customer-in-des">
                                            Khách dính DDoS từ hosting cũ được Mona tư vấn hướng giải <span class="pv-customer-in-line">
                                                quyết trong vòng 36h
                                            </span>
                                        </p>
                                        <ul class="pv-customer-in-l">
                                            <li class="pv-customer-in-i">Cử System Engineer ngày đêm kiểm tra, xác định cấu hình DNS để
                                                chặn bots</li>
                                            <li class="pv-customer-in-i">Giới hạn số lượng yêu cầu/một khoảng thời gian, cân bằng tải và
                                                chuyển hướng lưu lượng</li>
                                            <li class="pv-customer-in-i">Xoá hơn 80.000 trang bị ảnh hưởng bới DDoS, đẩy mạnh tài nguyên
                                                để từ khoá trở lại Top đầu</li>
                                        </ul>
                                    </div>
                                    <div class="pv-customer-btn">
                                        <a class="btn-clh">
                                            <span class="txt">
                                                Chi tiết dự án
                                            </span>
                                            <span class="icon">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/right.svg" alt="">
                                            </span>
                                        </a>
                                        <a class="btn-clh btn-clh-transparent">
                                            <span class="txt">
                                                Xem trang web
                                            </span>
                                            <span class="icon">
                                                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/right-01.svg" alt="">
                                            </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="pv-customer-i-r">
                                    <div class="pv-customer-r-inner">
                                        <div class="image">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvcustomer-01.png" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="pv-customer-decor x1">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/pv-customer-decor-01.png" alt="">
                  </div>
                  <div class="pv-customer-decor x2">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/pv-customer-decor-02.png" alt="">
                  </div> -->
                    </div>
                    <div class="pv-customer-i">
                        <div class="container">
                            <div class="pv-customer-i-inner">
                                <div class="pv-customer-i-l">
                                    <div class="pv-customer-tt">
                                        <h3 class="pv-customer-tx">Levents - </br>Local brand Việt </h3>
                                    </div>
                                    <div class="pv-customer-in">
                                        <div class="pv-customer-ad">
                                            <a href="" class="pv-customer-ad-i">
                                                <span class="icon">
                                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/loca.svg" alt="">
                                                </span>
                                                <span class="txt">
                                                    Hồ Chí Minh
                                                </span>
                                            </a>
                                            <a href="" class="pv-customer-ad-i">
                                                <span class="icon">
                                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/dnl.svg" alt="">
                                                </span>
                                                <span class="txt">
                                                    Doanh nghiệp lớn
                                                </span>
                                            </a>
                                        </div>
                                        <p class="pv-customer-in-des">
                                            Local brand hàng đầu Việt Nam có <span class="line-pri">nguy cơ mất doanh thu</span> Online vì
                                            đối mặt với tình trạng <span class="line-second">
                                                quá tải máy chủ
                                            </span> do lượng khách áp đảo hạ tầng máy chủ
                                        </p>
                                        <ul class="pv-customer-in-l">
                                            <li class="pv-customer-in-i">Thời gian load chậm, web đứng, lag, bay màu khi doanh nghiệp chạy
                                                sales</li>
                                            <li class="pv-customer-in-i">Chuyển hạ tầng lên MONA Cloud và cân bằng tải để phân phối lưu
                                                lượngg</li>
                                            <li class="pv-customer-in-i">Cài đặt và cấu hình hệ thống và cảnh báo trước khi có sự cố quá
                                                tải xảy ra để kịp thời xử lý </li>
                                        </ul>
                                    </div>
                                    <div class="pv-customer-btn">
                                        <a class="btn-clh">
                                            <span class="txt">
                                                Chi tiết dự án
                                            </span>
                                            <span class="icon">
                                                <img src="/assets/images/cloudhosting/right.svg" alt="">
                                            </span>
                                        </a>
                                        <a class="btn-clh btn-clh-transparent">
                                            <span class="txt">
                                                Xem trang web
                                            </span>
                                            <span class="icon">
                                                <img src="/assets/images/cloudhosting/right-01.svg" alt="">
                                            </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="pv-customer-i-r">
                                    <div class="pv-customer-r-inner">
                                        <div class="image">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvcustomer-02.png" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pv-customer-i">
                        <div class="container">
                            <div class="pv-customer-i-inner">
                                <div class="pv-customer-i-l">
                                    <div class="pv-customer-tt">
                                        <h3 class="pv-customer-tx">Giang huy logistic</h3>
                                    </div>
                                    <div class="pv-customer-in">
                                        <div class="pv-customer-ad">
                                            <a href="" class="pv-customer-ad-i">
                                                <span class="icon">
                                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/loca.svg" alt="">
                                                </span>
                                                <span class="txt">
                                                    Hồ Chí Minh
                                                </span>
                                            </a>
                                            <a href="" class="pv-customer-ad-i">
                                                <span class="icon">
                                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/dnl.svg" alt="">
                                                </span>
                                                <span class="txt">
                                                    Doanh nghiệp lớn
                                                </span>
                                            </a>
                                        </div>
                                        <p class="pv-customer-in-des">
                                            Doanh nghiệp nhập hàng với hơn <span class="pv-customer-in-line">3000 đơn hàng mỗi tuần
                                            </span> đối mặt với thách thức quản lý khối lượng data khổng lồ, hàng nghìn truy vấn mỗi ngày
                                        </p>
                                        <ul class="pv-customer-in-l">
                                            <li class="pv-customer-in-i">Khối lượng dữ liệu cực lớn liên quan đến chuỗi cung ứng</li>
                                            <li class="pv-customer-in-i">Thời gian phản hồi cực lâu lên đến vài phút với những truy vấn
                                                phức tạp và cần xử lý nhiều dữ liệu</li>
                                            <li class="pv-customer-in-i">Mona lược đồ hoá cơ sở dữ liệu và tối ưu các tuy vấn để đạt thời
                                                gian phản hồi nhanh hơn, <span class="violet">chỉ mất 0.3ms</span></li>
                                        </ul>
                                    </div>
                                    <div class="pv-customer-btn">
                                        <a class="btn-clh">
                                            <span class="txt">
                                                Chi tiết dự án
                                            </span>
                                            <span class="icon">
                                                <img src="/assets/images/cloudhosting/right.svg" alt="">
                                            </span>
                                        </a>
                                        <a class="btn-clh btn-clh-transparent">
                                            <span class="txt">
                                                Xem trang web
                                            </span>
                                            <span class="icon">
                                                <img src="/assets/images/cloudhosting/right-01.svg" alt="">
                                            </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="pv-customer-i-r">
                                    <div class="pv-customer-r-inner">
                                        <div class="image">
                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvcustomer-03.png" alt="">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>