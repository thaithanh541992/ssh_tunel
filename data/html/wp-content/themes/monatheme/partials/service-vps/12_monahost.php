<?php

/**
 * Section name: Service VPS Mona Host
 * Description: 
 * Author: Monamedia
 * Order: 12
 */
?>
<section class="pv-monahost">
    <div class="pv-monahost-br">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/cloud-top.png" alt="">
    </div>
    <div class="container">
        <div class="pv-monahost-header">
            <div class="pv-monahost-top">
                <div class="pv-monahost-common">
                    <div class="pv-monahost-tt">
                        <h2 class="host-tt">Mang đến giải pháp toàn diện</h2>
                        <p class="host-tt hl">Hosting tại MONA giải quyết mọi vấn đề</p>
                    </div>
                    <p class="pv-monahost-txt">
                        Với nhiều năm kinh nghiệm trong mảng Hosting, chúng tôi tự tin khẳng định <br>
                        <span class="line">hệ thống Hosting độc quyền của Mona là độc nhất, hiệu quả nhất cho doanh
                            nghiệp</span>
                    </p>
                </div>
            </div>
            <div class="pv-monahost-bottom">
                <div class="pv-monahost-b">
                    <div class="pv-monahost-lt">
                        <div class="pv-monahost-decor-01 abs ">
                            <div class="pv-monahost-decor-com">
                                <p class="txt">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvCer.png" alt="">
                                </p>
                            </div>
                        </div>
                        <div class="pv-monahost-decor-02 abs ">
                            <div class="pv-monahost-decor-com">
                                <p class="txt">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvHost_Insight.png" alt="">
                                </p>
                            </div>
                        </div>
                        <div class="pv-monahost-decor-03 abs ">
                            <div class="pv-monahost-decor-com">
                                <p class="txt">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvHost_Insight2.png" alt="">
                                </p>
                            </div>
                        </div>
                        <div class="pv-monahost-decor-04 abs ">
                            <div class="pv-monahost-decor-com">
                                <p class="txt">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvHost_Mor.png" alt="">
                                </p>
                            </div>
                        </div>
                        <div class="pv-monahost-decor-05 abs">
                            <div class="pv-monahost-decor-com">
                                <p class="txt">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvHost_Speed.png" alt="">
                                </p>
                            </div>
                        </div>
                        <div class="pv-monahost-decor-06 abs">
                            <div class="pv-monahost-decor-com">
                                <p class="txt">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvSystem.png" alt="">
                                </p>
                            </div>
                        </div>
                        <div class="pv-monahost-decor-07 abs">
                            <div class="pv-monahost-decor-com">
                                <p class="txt">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvSystem_2.png" alt="">
                                </p>
                            </div>
                        </div>
                        <a href="" class="footer">
                            <div class="footer-quote">
                                <div class="img-quote">
                                    <div>
                                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvHost_btn_bg.png" alt="" />
                                    </div>
                                    <div class="quote-content">
                                        <div class="about fw-400 txt-center txt-white">
                                            Liên hệ với chúng tôi nếu bạn đang gặp phải những <br> vấn đề về hosting hoặc chưa biết bắt
                                            đầu từ đâu.
                                        </div>
                                        <div class="fw-600 txt-center txt-white">
                                            Mona đã sẵn sàng để hỗ trợ bạn!
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </a>
                        <div class="panda">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvHost_PvPanda.png" alt="">
                        </div>
                    </div>
                    <div class="pv-monahost-decor">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvHost-br.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="pv-monahost-cloud-bot">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/cloudhosting/cloud-bot.png" alt="">
    </div>
</section>