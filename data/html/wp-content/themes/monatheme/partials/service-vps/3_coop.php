<?php

/**
 * Section name: Service VPS Coop
 * Description: 
 * Author: Monamedia
 * Order: 3
 */
?>
<section class="pv-coop">
    <div class="container">
        <div class="pv-coop-w">
            <div class="pv-coop-panda">
                <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvcoop-pandabalo.png" alt="">
            </div>
            <div class="pv-coop-t">
                <div class="pv-coop-tt">
                    <h2 class="host-tt">Hợp tác với Mona</h2>
                    <p class="host-tt hl">Lợi ích luỹ tiến theo thời gian</p>
                </div>
                <p class="pv-coop-txt">
                    Khách hàng chỉ cần tập trung vào việc bán sản phẩm/dịch vụ, chăm sóc khách hàng và <br> tăng doanh thu.
                    MONA sẽ trở thành cánh tay phải đắc lực cho bạn trên thị trường Online
                </p>
            </div>
            <div class="pv-coop-in">
                <div class="pv-coop-b">
                    <div class="pv-coop-lt">
                        <div class="pv-coop-decor-01 abs ">
                            <div class="pv-coop-decor-com">
                                <p class="txt">
                                    Không tốn quá <br> nhiều thời gian
                                </p>
                            </div>
                        </div>
                        <div class="pv-coop-decor-02 abs ">
                            <div class="pv-coop-decor-com">
                                <p class="txt">
                                    Không cần lo lắng <br> về kỹ thuật
                                </p>
                            </div>
                        </div>
                        <div class="pv-coop-decor-03 abs ">
                            <div class="pv-coop-decor-com">
                                <p class="txt">
                                    Không cần kiến thức <br> về lập trình
                                </p>
                            </div>
                        </div>
                        <div class="pv-coop-decor-04 abs ">
                            <div class="pv-coop-decor-com">
                                <p class="txt">
                                    Đội ngũ <br> giàu kinh nghiệm
                                </p>
                            </div>
                        </div>
                        <a href="" class="pv-coop-decor-05 abs btn-clh">
                            Hãy kết nối với Mona Host ngay hôm nay
                        </a>
                    </div>
                    <div class="pv-coop-decor">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvcoop-img.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>