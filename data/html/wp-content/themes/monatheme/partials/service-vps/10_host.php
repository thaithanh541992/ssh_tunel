<?php

/**
 * Section name: Service VPS Host
 * Description: 
 * Author: Monamedia
 * Order: 10
 */
?>
<section class="pv-host">
    <div class="container">
        <div class="pv-host-w">
            <div class="pv-host-top">
                <div class="pv-host-common">
                    <div class="pv-host-tt">
                        <h2 class="host-tt">Mona Host không như các nhà cung cấp khác</h2>
                        <p class="host-tt hl">Không sử dụng thủ thuật giảm chi phí</p>
                    </div>
                    <p class="pv-host-txt">
                        Trong khi các nhà cung cấp khác đặt thông số phần cứng ban đầu ở mức thấp đến <span class="line">“phi
                            <br> thực tế”</span>. Các tính năng quan trọng trở thành <span class="line-2">tính năng bổ sung “đắt
                            đỏ”</span>. <span class="line-3">Chi phí bỏ ra có <br> thể lên đến cả chục triệu/tháng</span>
                    </p>
                </div>
            </div>
            <div class="pv-host-bottom">
                <div class="pv-host d-flex f-between">
                    <div class="pv-host-left col-6">
                        <div class="pv-host-in">
                            <div class="pv-host-b" data-aos="">
                                <div class="pv-host-lt">
                                    <div class="pv-host-decor-01 abs ">
                                        <div class="pv-host-decor-com">
                                            <p class="txt">
                                                64GB RAM
                                            </p>
                                        </div>
                                    </div>
                                    <div class="pv-host-decor-02 abs ">
                                        <div class="pv-host-decor-com">
                                            <p class="txt">
                                                30 Cores CPU
                                            </p>
                                        </div>
                                    </div>
                                    <div class="pv-host-decor-03 abs ">
                                        <div class="pv-host-decor-com">
                                            <p class="txt">
                                                Không giới hạn dữ <br> liệu truyền tải
                                            </p>
                                        </div>
                                    </div>
                                    <div class="pv-host-decor-04 abs ">
                                        <div class="pv-host-decor-com">
                                            <p class="txt">
                                                Băng thông <br> 20Gbps
                                            </p>
                                        </div>
                                    </div>
                                    <div class="pv-host-decor-05 abs">
                                        <div class="pv-host-decor-com">
                                            <p class="txt">
                                                Giá cả hợp lý
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                <div class="pv-host-decor">
                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvhost.png" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pv-host-right col-6">
                        <div class="pv-host-tc">
                            Với Mona Host
                        </div>
                        <div class="pv-host-10plus50">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pv10x50.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>