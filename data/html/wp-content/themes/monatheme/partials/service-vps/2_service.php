<?php
/**
 * Section name: Service VPS Service
 * Description: 
 * Author: Monamedia
 * Order: 2
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> pv-pay session-service">
<?php
$mona_available_vps = get_field('mona_available_vps', MONA_PAGE_HOME);
if ( !empty( $mona_available_vps ) && is_array( $mona_available_vps )) {
    ?>
    <div class="service-hosting-wrap-bg">
        <div class="container">
            <div class="pv-pay-w">
                <div class="pv-pay-t">
                    <div class="pv-pay-tt">
                        <h3 class="host-tt">Dịch vụ Premium Cloud VPS</h3>
                        <p class="host-tt bg-color bg-color-purpule">Tốt nhất thị trường đến từ Mona Host</p>
                    </div>
                </div>
            </div>
        </div>
        <div class="session-pay-host">
            <div class="session-pay-host-header">
                <div class="tab-button-custom">
                    <div class="wrap">
                        <div class="item tab active">Linux</div>
                        <div class="item tab">Windows</div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="swiper swiper-service-hosting tab-panel active">
                    <div class="swiper-wrapper">
                        <?php
                        foreach ( $mona_available_vps as $key => $vps ) {
                            $mona_product_ui_info = get_field( 'mona_product_ui_info', $vps->ID );
                            $mona_billing_cycle_list = get_field( 'mona_billing_cycle_list', $vps->ID );
                            $original_price = $mona_product_ui_info['info_original_price'];
                            $price = 0;
                            $sale_percent = 0;
                            $first_option_qty = '';
                            $mona_os = get_field( 'mona_os', $vps->ID );
                            if ( !empty( $mona_product_ui_info ) && $mona_os == 'linux' ) {
                                ?>
                                <div class="swiper-slide">
                                    <div class="card vps-item-js">
                                        <div class="card-header">
                                            <div class="header-content">
                                                <div class="name">
                                                    <div>
                                                        <?php echo $vps->post_title ?>
                                                    </div>
                                                    <div class="month-inner">
                                                        <?php
                                                            if ( !empty( $mona_billing_cycle_list ) && is_array( $mona_billing_cycle_list ) ) {
                                                                ?>
                                                                <select class="month vps-billing-cycle-js" 
                                                                    data-billing_list='<?php echo json_encode( $mona_billing_cycle_list ) ?>'
                                                                    data-original_price="<?php echo $original_price ?>"
                                                                >
                                                                    <?php
                                                                    foreach ( $mona_billing_cycle_list as $billing_item ) {
                                                                        if ( empty( $first_option_qty ) ) {
                                                                            $first_option_qty = $billing_item['term_quantity'];
                                                                        }
                                                                        ?>
                                                                        <option value="<?php echo $billing_item['term_quantity'] ?>"><?php echo $billing_item['label'] ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <?php
                                                            }
                                                            ?>
                                                            
                                                        <div class="icon">
                                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/icon-chevron-down.png" alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                if ( !empty( $mona_billing_cycle_list ) && is_array( $mona_billing_cycle_list ) ) {
                                                    foreach ( $mona_billing_cycle_list as $billing_item ) {
                                                        if ( $billing_item['term_quantity'] == $first_option_qty ) {
                                                            $price = floor($billing_item['price']/$billing_item['term_quantity']);
                                                            if ( !empty( $original_price ) ) {
                                                                $sale_percent = 100 - ceil($price / $original_price * 100);
                                                            }
                                                        }
                                                    }
                                                }
                                                ?>
                                                <div class="price vps-price-js">
                                                    <div>
                                                        <?php echo number_format($price, 0, '', ',') ?> đ<span class="dash-month">/tháng</span>
                                                    </div>
                                                    <?php
                                                    if ( $sale_percent > 0 ) {
                                                        ?>
                                                        <div class="sale">Tiết kiệm <?php echo $sale_percent; ?>%</div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="wrap-bg">
                                                <?php
                                                if ( !empty( $mona_product_ui_info['info_bg'] ) ) {
                                                    echo wp_get_attachment_image($mona_product_ui_info['info_bg'], 'large');
                                                } else {
                                                    ?>
                                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/card-header-ware-1.png" alt="" />
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="card-content">
                                            <?php echo $mona_product_ui_info['info_config'] ?>
                                            <div class="btn-tag">
                                                <div class="add-to-cart-js is-loading-btn-2" data-pid='<?php echo $vps->ID ?>' data-billing_cycle='<?php echo $first_option_qty; ?>'>
                                                    <a class="btn-clh btn-clh-blue">
                                                        <span class="txt">
                                                            Mua gói hosting ngay
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
                <div class="swiper swiper-service-hosting tab-panel">
                    <div class="swiper-wrapper">
                        <?php
                        foreach ( $mona_available_vps as $key => $vps ) {
                            $mona_product_ui_info = get_field( 'mona_product_ui_info', $vps->ID );
                            $mona_billing_cycle_list = get_field( 'mona_billing_cycle_list', $vps->ID );
                            $original_price = $mona_product_ui_info['info_original_price'];
                            $price = 0;
                            $sale_percent = 0;
                            $first_option_qty = '';
                            $mona_os = get_field( 'mona_os', $vps->ID );
                            if ( !empty( $mona_product_ui_info ) && $mona_os != 'linux' ) {

                                ?>
                                <div class="swiper-slide">
                                    <div class="card vps-item-js">
                                        <div class="card-header">
                                            <div class="header-content">
                                                <div class="name">
                                                    <div>
                                                        <?php echo $vps->post_title ?>
                                                    </div>
                                                    <div class="month-inner">
                                                        <?php
                                                            if ( !empty( $mona_billing_cycle_list ) && is_array( $mona_billing_cycle_list ) ) {
                                                                ?>
                                                                <select class="month vps-billing-cycle-js" 
                                                                    data-billing_list='<?php echo json_encode( $mona_billing_cycle_list ) ?>'
                                                                    data-original_price="<?php echo $original_price ?>"
                                                                >
                                                                    <?php
                                                                    foreach ( $mona_billing_cycle_list as $billing_item ) {
                                                                        if ( empty( $first_option_qty ) ) {
                                                                            $first_option_qty = $billing_item['term_quantity'];
                                                                        }
                                                                        ?>
                                                                        <option value="<?php echo $billing_item['term_quantity'] ?>"><?php echo $billing_item['label'] ?></option>
                                                                        <?php
                                                                    }
                                                                    ?>
                                                                </select>
                                                                <?php
                                                            }
                                                            ?>
                                                            
                                                        <div class="icon">
                                                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/icon-chevron-down.png" alt="" />
                                                        </div>
                                                    </div>
                                                </div>
                                                <?php
                                                if ( !empty( $mona_billing_cycle_list ) && is_array( $mona_billing_cycle_list ) ) {
                                                    foreach ( $mona_billing_cycle_list as $billing_item ) {
                                                        if ( $billing_item['term_quantity'] == $first_option_qty ) {
                                                            $price = floor($billing_item['price']/$billing_item['term_quantity']);
                                                            if ( !empty( $original_price ) ) {
                                                                $sale_percent = 100 - ceil($price / $original_price * 100);
                                                            }
                                                        }
                                                    }
                                                }
                                                ?>
                                                <div class="price vps-price-js">
                                                    <div>
                                                        <?php echo number_format($price, 0, '', ',') ?> đ<span class="dash-month">/tháng</span>
                                                    </div>
                                                    <?php
                                                    if ( $sale_percent > 0 ) {
                                                        ?>
                                                        <div class="sale">Tiết kiệm <?php echo $sale_percent; ?>%</div>
                                                        <?php
                                                    }
                                                    ?>
                                                </div>
                                            </div>
                                            <div class="wrap-bg">
                                                <?php
                                                if ( !empty( $mona_product_ui_info['info_bg'] ) ) {
                                                    echo wp_get_attachment_image($mona_product_ui_info['info_bg'], 'large');
                                                } else {
                                                    ?>
                                                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hosts/card-header-ware-1.png" alt="" />
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                        <div class="card-content">
                                            <?php echo $mona_product_ui_info['info_config'] ?>
                                            <div class="btn-tag">
                                                <div class="add-to-cart-js is-loading-btn-2" data-pid='<?php echo $vps->ID ?>' data-billing_cycle='<?php echo $first_option_qty; ?>'>
                                                    <a class="btn-clh btn-clh-blue">
                                                        <span class="txt">
                                                            Mua gói hosting ngay
                                                        </span>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <?php
                            }
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php
}
?>
</section>