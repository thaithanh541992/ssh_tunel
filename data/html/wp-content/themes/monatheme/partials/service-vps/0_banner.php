<?php

/**
 * Section name: Service VPS Banner
 * Description: 
 * Author: Monamedia
 * Order: 0
 */
?>
<div class="pv-bn">
    <div class="pv-bn-header">
        <div class="pv-bn-top">
            <div class="pv-bn-c">
                <h1 class="pv-bn-hl host-tt">
                    Premium Cloud VPS
                </h1>
                <p class="pv-bn-txt">Cá nhân hóa hệ thống server riêng của bạn</p>
            </div>
            <div class="pv-bn-bot">
                <div class="pv-bn-br">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvMoutain.png" alt="">
                </div>
                <div class="pv-bn-img">
                    <div class="image">
                        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/PCVPS/pvPanda.png" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>