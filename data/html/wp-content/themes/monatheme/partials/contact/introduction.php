<?php
/**
 * Section name: Introduction Contact
 * Description: 
 * Author: Monamedia
 * Order: 0
 */
?>
<?php $mona_contact_introduction = get_field('mona_contact_introduction'); ?>
<section id="<?php echo !empty( $args['id'] ) ? $args['id'] : 'monaIntroductionContact'; ?>" 
class="<?php echo !empty( $args['classes'] ) ? $args['classes'] : 'class_monaIntroductionContact';?> sec-contacts">
    <div class="decor2">
        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/pcontact/icon-to.png" alt="">
    </div>
    <div class="decor3">
        <img class="cir" src="<?php echo get_site_url() ?>/template/assets/images/pcontact/heart2.png" alt="">
    </div>
    <div class="decor4">
        <img class="cir" src="<?php echo get_site_url() ?>/template/assets/images/pcontact/heart3.png" alt="">
    </div>
    <div class="container">
        <div class="contacts">
            <div class="contacts-pos">
                <div class="contacts-tdecor">
                    <div class="decor-gr">
                        <span class="icon">
                            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/pcontact/icon-bu.png" alt="">
                        </span>
                        <span class="text">Đồng hành và trải nghiệm <br>
                            dịch vụ tại MONA.Media</span>
                        <span class="icon icon-2">
                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/pcontact/decor-sig.png" alt="">
                            </span>
                    </div>
                </div>
                <div class="contacts-top">
                    <h1 class="sec-tt" data-aos="fade-down">
                        <?php echo $mona_contact_introduction['title']; ?>
                    </h1>
                    <span class="text ani"><?php echo $mona_contact_introduction['subtitle']; ?></span>
                </div>
                <div class="contacts-line">
                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/pcontact/v-line.png" alt="">
                </div>
                <div class="decor">
                    <img class="cir" src="<?php echo get_site_url() ?>/template/assets/images/pcontact/heart.png" alt="">
                </div>
            </div>
            <div class="contacts-wrap">

                <div class="dsmall sample-ten-list">
                    <?php $introduction_1 = $mona_contact_introduction['introduction_1'];
                    if( !empty( $introduction_1['title'] ) && !empty( $introduction_1['link']['url'] ) ){ ?>
                    <div class="dsmall-item sample-ten-item col-6">
                        <div class="sample-ten-wrap four">
                            <div class="sample-ten-row">
                                <div class="sample-ten-col">
                                    <div class="sample-ten-content">
                                        <div class="sample-ten-block">
                                            <h2 class="sample-ten-name c-title sub2 mb-16">
                                                <?php echo $introduction_1['title']; ?>
                                            </h2>
                                            <div class="sample-ten-des mb-24">
                                                <?php echo $introduction_1['description']; ?>
                                            </div>
                                        </div>
                                        <a href="<?php echo $introduction_1['link']['url']; ?>" 
                                        target="<?php echo $introduction_1['link']['target']; ?>" 
                                        class="sample-ten-btn">
                                            <i class="fas fa-arrow-right"></i>
                                            <span class="text">
                                                <?php echo $introduction_1['link']['title']; ?>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="sample-ten-col">
                                    <div class="sample-ten-img">
                                        <div class="sample-ten-rectangle">
                                            <div class="inner"></div>
                                        </div>
                                        <div class="sample-ten-img-inner">
                                            <?php echo wp_get_attachment_image($introduction_1['thumbnail'], 'full'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php $introduction_2 = $mona_contact_introduction['introduction_2'];
                    if( !empty( $introduction_2['title'] ) && !empty( $introduction_2['link']['url'] ) ){ ?>
                    <div class="dsmall-item sample-ten-item col-6">
                        <div class="sample-ten-wrap one">
                            <div class="sample-ten-row">
                                <div class="sample-ten-col">
                                    <div class="sample-ten-content">
                                        <div class="sample-ten-block">
                                            <h2 class="sample-ten-name c-title sub2 mb-16">
                                                <?php echo $introduction_2['title']; ?>
                                            </h2>
                                            <div class="sample-ten-des mb-24">
                                                <?php echo $introduction_2['description']; ?>
                                            </div>
                                        </div>
                                        <a href="<?php echo $introduction_2['link']['url']; ?>" 
                                        target="<?php echo $introduction_2['link']['target']; ?>" 
                                        class="sample-ten-btn">
                                            <i class="fas fa-arrow-right"></i>
                                            <span class="text">
                                                <?php echo $introduction_2['link']['title']; ?>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="sample-ten-col">
                                    <div class="sample-ten-img">
                                        <div class="sample-ten-img-wrapper relative">
                                            <div class="sample-ten-vector">
                                                <svg class="vector" viewBox="0 0 116 97" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <path
                                                        d="M113.943 93.0278C101.749 99.125 90.1967 92.3185 80.1208 85.202C68.1817 76.7694 56.202 67.6579 46.191 56.9539C40.1745 50.5209 32.1881 41.2038 31.7186 31.8148C30.8976 15.3939 70.7825 17.9914 75.6183 29.1347C80.6597 40.752 58.4576 49.692 51.2296 52.1298C43.3981 54.7712 32.7583 56.8255 24.8576 53.0946C21.3155 51.422 19.1593 48.1769 19.7119 44.1432C20.898 35.4846 29.6199 31.8669 37.0788 30.2068C40.9304 29.3495 59.6449 24.9601 61.9499 31.386C64.0235 37.167 54.2784 42.158 50.6936 43.6072C33.4952 50.5597 3.96202 48.557 2.23779 24.4178C0.783284 4.05467 37.075 3.90419 50.2648 2.33398"
                                                        stroke="#F5851E" stroke-width="3" stroke-linecap="round"
                                                        class="line"></path>
                                                </svg>
                                            </div>
                                            <div class="sample-ten-pacman">
                                                <div class="sample-ten-pacman-wrapper">
                                                    <div class="sample-ten-pacman-top">
                                                        <div class="inner"></div>
                                                    </div>
                                                    <div class="sample-ten-pacman-bot">
                                                        <div class="inner"></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="sample-ten-img-inner">
                                                <?php echo wp_get_attachment_image($introduction_2['thumbnail'], 'full'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php $introduction_3 = $mona_contact_introduction['introduction_3'];
                    if( !empty( $introduction_3['title'] ) && !empty( $introduction_3['link']['url'] ) ){ ?>
                    <div class="dsmall-item sample-ten-item col-6">
                        <div class="sample-ten-wrap three">
                            <div class="sample-ten-row">
                                <div class="sample-ten-col">
                                    <div class="sample-ten-content">
                                        <div class="sample-ten-block">
                                            <h2 class="sample-ten-name c-title sub2 mb-16">
                                                <?php echo $introduction_3['title']; ?>
                                            </h2>
                                            <div class="sample-ten-des mb-24">
                                                <?php echo $introduction_3['description']; ?>
                                            </div>
                                        </div>
                                        <a href="<?php echo $introduction_3['link']['url']; ?>" 
                                        target="<?php echo $introduction_3['link']['target']; ?>" 
                                        class="sample-ten-btn">
                                            <i class="fas fa-arrow-right"></i>
                                            <span class="text">
                                                <?php echo $introduction_3['link']['title']; ?>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="sample-ten-col">
                                    <div class="sample-ten-img">
                                        <div class="sample-ten-triangle">
                                            <div class="inner"></div>
                                        </div>
                                        <div class="sample-ten-img-inner">
                                            <?php echo wp_get_attachment_image($introduction_3['thumbnail'], 'full'); ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>

                    <?php $introduction_4 = $mona_contact_introduction['introduction_4'];
                    if( !empty( $introduction_4['title'] ) && !empty( $introduction_4['link']['url'] ) ){ ?>
                    <div class="dsmall-item sample-ten-item col-6">
                        <div class="sample-ten-wrap two">
                            <div class="sample-ten-row">
                                <div class="sample-ten-col">
                                    <div class="sample-ten-content">
                                        <div class="sample-ten-block">
                                            <h2 class="sample-ten-name c-title sub2 mb-16">
                                                <?php echo $introduction_4['title']; ?>
                                            </h2>
                                            <div class="sample-ten-des mb-24">
                                                <?php echo $introduction_4['description']; ?>
                                            </div>
                                        </div>
                                        <a href="<?php echo $introduction_4['link']['url']; ?>" 
                                        target="<?php echo $introduction_4['link']['target']; ?>" 
                                        class="sample-ten-btn">
                                            <i class="fas fa-arrow-right"></i>
                                            <span class="text">
                                                <?php echo $introduction_4['link']['title']; ?>
                                            </span>
                                        </a>
                                    </div>
                                </div>
                                <div class="sample-ten-col">
                                    <div class="sample-ten-img">
                                        <div class="sample-ten-img-wrapper relative">
                                            <div class="sample-ten-circle">
                                            </div>
                                            <div class="sample-ten-img-inner">
                                                <?php echo wp_get_attachment_image($introduction_4['thumbnail'], 'full'); ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php } ?>
                </div>

                <div class="decor">
                    <img class="cir" src="<?php echo get_site_url() ?>/template/assets/images/pcontact/icon-decor.png" alt="">
                </div>
            </div>
        </div>
    </div>
</section>