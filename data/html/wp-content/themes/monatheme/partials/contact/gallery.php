<?php
/**
 * Section name: Gallery Contact
 * Description: 
 * Author: Monamedia
 * Order: 2
 */
?>
<section id="<?php echo !empty( $args['id'] ) ? $args['id'] : 'monaContactInformationContact'; ?>" 
class="<?php echo !empty( $args['classes'] ) ? $args['classes'] : 'class_monaContactInformationContact';?> sec-cwe">
    <div class="cwe ss-pd-t">
        <div class="cwe-panda">
            <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/seoadv-img.png" alt="">
        </div>

        <div class="container">
            <div class="head txt-center mb-32">
                <h2 class="c-title mb-32 txt-spl ani" data-spl>
                    <span class="pri">HÃY GỌI CHÚNG TÔI LÀ ACCOUNT</span> <br> không phải Sales
                </h2>
                <p class="des txt-spl ani" data-spl>
                    Chúng tôi là <span class="des-pri fw-7">người đồng hành & đứng cùng phía với bạn</span>, thay mặt bạn làm việc với team kỹ thuật của chúng tôi Đảm bảo tiến độ và sự thành công của dự án
                </p>
            </div>
        </div>
        <div class="cwe-persons">
            <div class="cwe-persons-inner" data-aos="fade-up">
                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-persons-img.png" alt="">
            </div>
        </div>
        <div class="cwe-wrapper gallery">
            <div class="cwe-list">
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-1.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-1.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-2.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-2.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-3.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-3.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-4.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-4.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-5.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-5.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-6.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-6.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-7.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-7.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-8.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-8.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item cwe-content pink" data-aos="fade">
                    <div class="cwe-wrap">
                        <div class="cwe-content-inner">
                            Gọi điện thoại <br> Online
                        </div>
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-9.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-9.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item cwe-content purple" data-aos="fade">
                    <div class="cwe-wrap">
                        <div class="cwe-content-inner">
                            Tư vấn Online, <br> Meeting Online <br> với khách hàng
                        </div>
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-10.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-10.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-11.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-11.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/monaContactAnhTuan.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/monaContactAnhTuan.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-14.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-14.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-15.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-15.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-16.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-16.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-17.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-17.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-18.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-18.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-19.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-19.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-20.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-20.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item cwe-content orange" data-aos="fade">
                    <div class="cwe-wrap">
                        <div class="cwe-content-inner">
                            Thậm chí ngay cả <br> ngoài không gian <br> thoải mái như quán cf <br> nếu khách cần
                        </div>
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-21.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-21.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-22.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-22.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-wrap">
                        <div class="cwe-content-inner">
                            Tư vấn tận nơi cho <br> khách hàng
                        </div>
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-23.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-23.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-24.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-24.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-26.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-26.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-27.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/cwe-img-27.jpg" alt="">
                    </div>
                </div>
                <div class="cwe-item" data-aos="fade">
                    <div class="cwe-img gallery__img" data-src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/ulagift-new.jpg">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/chuan-image/ulagift-new.jpg" alt="">
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>