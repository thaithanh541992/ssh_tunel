<?php
/**
 * Section name: Map Contact
 * Description: 
 * Author: Monamedia
 * Order: 3
 */
?>
<?php $mona_contact_iframe = get_field('mona_contact_iframe');
if( !empty( $mona_contact_iframe ) ){ ?>
<section id="<?php echo !empty( $args['id'] ) ? $args['id'] : 'monaMapContact'; ?>" 
class="<?php echo !empty( $args['classes'] ) ? $args['classes'] : 'class_monaMapContact';?> section mona-contact second fix">
    <div class="mona-map">
        <?php echo $mona_contact_iframe; ?>
    </div>
</section>
<?php } ?>