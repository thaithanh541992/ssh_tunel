<?php
/**
 * Section name: Contact Information Contact
 * Description: 
 * Author: Monamedia
 * Order: 1
 */
?>
<?php 
$mona_global_contact_information_2 = get_field('mona_global_contact_information_2', MONA_PAGE_BLOG);
if( !empty( $mona_global_contact_information_2 ) ){
?>
<section id="<?php echo !empty( $args['id'] ) ? $args['id'] : 'monaContactInformationContact'; ?>" 
class="<?php echo !empty( $args['classes'] ) ? $args['classes'] : 'class_monaContactInformationContact';?> sec-fcts">
    <div class="decor">
        <img class="cir" src="<?php echo get_site_url() ?>/template/assets/images/pcontact/icon-panda.png" alt="">
    </div>

    <div class="container">
        <div class="fcts">
            <div class="fcts-wrap">
                <div class="fcts-flex">
                    <div class="fcts-left">
                        <div class="fcts-content">
                            <div class="fcts-top">
                                <h2 class="title">Liên hệ ngay với </br>chúng tôi</h2>
                                <div class="fcts-top-fl">
                                    <div class="fcts-top-fl-item">
                                        <div class="des">
                                            <p>
                                                Bạn có thể liên hệ với Mona bằng bất cứ Kênh nào 
                                                mà bạn có thể trao đổi với chung tôi một cách thoải mái nhất
                                            </p>
                                        </div>
                                    </div>
                                            
                                    <?php $information = $mona_global_contact_information_2['information'];
                                    if( !empty( $information ) ){ ?>
                                    <div class="fcts-top-fl-item">
                                        <div class="fcts-top-add">
                                            <?php foreach ($information as $key => $item) { ?>
                                            <div class="fcts-top-add-item">
                                                <?php echo wp_get_attachment_image($item['icon'], 'full'); ?>
                                                <div class="text"><?php echo $item['content']; ?></div>
                                            </div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <?php } ?>

                                </div>
                            </div>
                            <div class="fcts-mid">
                                <?php $section_global_zalo = mona_get_option('section_global_zalo');
                                if( !empty( $section_global_zalo ) ){ ?>
                                <div class="fcts-mid-gr fcts-zalo">
                                    <div class="fcts-mid-right">
                                        <div class="fcts-mid-txt"><?php echo __('Phong cách', 'monamedia'); ?><br><?php echo __(' người Việt', 'monamedia'); ?></div>
                                    </div>
                                    <div class="fcts-mid-left">
                                        <div class="fcts-mid-control">
                                            <a href="<?php echo $section_global_zalo; ?>" class="fcts-mid-link" target="_blank">
                                                <span class="text">
                                                    <?php echo sprintf('Nhắn tin qua Zalo Official <br> của Mona'); ?>
                                                </span>
                                                <span class="icon">
                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/pcontact/icon-zalo.png" alt="">
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>

                                <?php $section_global_hotline = mona_get_option('section_global_hotline');
                                if( !empty( $section_global_hotline ) ){ ?>
                                <div class="fcts-mid-gr fcts-hotline">
                                    <div class="fcts-mid-right">
                                        <div class="fcts-mid-txt"><?php echo __('Phong cách', 'monamedia'); ?><br><?php echo __(' truyền thống', 'monamedia'); ?></div>
                                    </div>
                                    <div class="fcts-mid-left">
                                        <div class="fcts-mid-control">
                                            <a href="<?php echo mona_replace_tel( $section_global_hotline ); ?>" class="fcts-mid-link">
                                                <span class="text"><?php echo __('Gọi ngay Hotline', 'monamedia'); ?><br>
                                                    <?php echo $section_global_hotline; ?>    
                                                </span>
                                                <span class="icon">
                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/pcontact/icon-call-2.png" alt="">
                                                </span>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>

                                <div class="fcts-mid-gr fcts-live">
                                    <div class="fcts-mid-right">
                                        <div class="fcts-mid-txt"><?php echo __('Phong cách', 'monamedia'); ?><br><?php echo __(' công nghệ', 'monamedia'); ?></div>
                                    </div>
                                    <div class="fcts-mid-left">
                                        <div class="fcts-mid-control">
                                            <a href="javascript:Tawk_API.maximize();" class="fcts-mid-link">
                                                <span class="text">Livechat trực tuyến với <br>
                                                    Tawkto</span>
                                                <span class="icon">
                                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/pcontact/icon-live.png" alt="">
                                                </span>
                                            </a>
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="fcts-right">
                        <div class="fcts-form mona-contact fix">
                            <div class="mona-contact-form">
                                <div class="mona-contact-form-wrap">
                                    <div class="form-tab d-flex">
                                        <div class="tab active">Khách hàng</div>
                                        <div class="tab">Đối tác</div>
                                        <div class="tab">Ứng viên</div>
                                    </div>
                                    <div class="form-cus">
                                        <div class="form-panel tab-panel active">
                                            <?php $shortcode_customer = $mona_global_contact_information_2['shortcode_customer'];
                                            if( !empty( $shortcode_customer ) ){
                                                echo do_shortcode( $shortcode_customer );
                                            } ?>
                                        </div>
                                        <div class="form-panel tab-panel">
                                            <?php $shortcode_partner = $mona_global_contact_information_2['shortcode_partner'];
                                            if( !empty( $shortcode_partner ) ){
                                                echo do_shortcode( $shortcode_partner );
                                            } ?>
                                        </div>
                                        <div class="form-panel tab-panel">
                                            <?php $shortcode_candicate = $mona_global_contact_information_2['shortcode_candicate'];
                                            if( !empty( $shortcode_candicate ) ){
                                                echo do_shortcode( $shortcode_candicate );
                                            } ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>