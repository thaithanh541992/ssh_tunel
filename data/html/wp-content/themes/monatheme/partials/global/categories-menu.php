<?php 
$mona_blog_categories_menu = get_field('mona_blog_categories_menu', MONA_PAGE_BLOG);
if( !empty( $mona_blog_categories_menu ) ){ ?>
<!-- Danh mục tin -->
<div class="blogf">
    <?php
    $categories_menu_logo 	= $mona_blog_categories_menu['categories_menu_logo'];
    $categories_menu_title 	= !empty( $mona_blog_categories_menu['categories_menu_title'] ) ? $mona_blog_categories_menu['categories_menu_title'] : '';
    $categories_menu_option = $mona_blog_categories_menu['categories_menu_option'];
    if( !$categories_menu_option ){
        $categories = get_terms(
            array(
                'taxonomy' 		=> 'category',
                'hide_empty' 	=> true,
                'parent'		=> 0
            )
        );
        if( !empty( $categories ) ){ ?>
        <div class="blogf-menu blogfMenu">
            <div class="blogf-menu-inner blogfInner">
                <div class="blogf-menu-flex blogfMenuFlex">
                    <?php 
                    foreach ($categories as $keyCategory => $category) {
                        $children =  get_terms(
                            array(
                                'taxonomy' 		=> $category->taxonomy,
                                'hide_empty' 	=> true,
                                'parent'		=> $category->term_id
                            )
                        ); ?>

                        <?php if( !empty( $children ) ) {  ?>
                        <div class="blogf-menu-col blogfItem">
                            <div class="blogf-menu-wrap blogfCol">
                                <a href="<?php echo get_term_link( $category ); ?>" class="text"><?php echo $category->name; ?></a>
                                <div class="blogf-menu-list">
                                    <?php foreach ($children as $keyChild => $child) {
                                        if( is_category($child) ){
                                            $current_taxonomy = get_queried_object();
                                            if( $current_taxonomy->slug == $child->slug ){
                                                $classChild = 'active';
                                            }else{
                                                $classChild = 'non-active';
                                            }
                                        }else{
                                            $classChild = 'non-active';
                                        } ?>
                                    <div class="blogf-menu-item <?php echo $classChild; ?>">
                                        <a href="<?php echo get_term_link( $child ); ?>" class="blogf-menu-link"><?php echo $child->name; ?></a>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    
                    <?php } ?>
                </div>
            </div>
        </div>
        <?php } ?>
    <?php }else{ ?> 
        <?php $categories_menu = $mona_blog_categories_menu['categories_menu'];
        if( !empty( $categories_menu ) && is_array( $categories_menu ) ){ ?>
        <div class="blogf-menu blogfMenu">
            <div class="blogf-menu-inner blogfInner">
                <div class="blogf-menu-flex blogfMenuFlex">
                    <?php 
                    foreach ($categories_menu as $keyMenuItem => $menuItem) {
                        $category = $menuItem['parent'];
                        if( !empty( $category ) ){

                            $children_option = $menuItem['children_option'];
                            if( !$children_option ){
                                $children =  get_terms(
                                    array(
                                        'taxonomy' 		=> $category->taxonomy,
                                        'hide_empty' 	=> true,
                                        'parent'		=> $category->term_id
                                    )
                                );
                            }else{
                                $children =  $menuItem['children'];
                            }
                        ?>

                        <?php if( !empty( $children ) ) {  ?>
                        <div class="blogf-menu-col blogfItem">
                            <div class="blogf-menu-wrap blogfCol">
                                <a href="<?php echo get_term_link( $category ); ?>" class="text"><?php echo $category->name; ?></a>
                                <div class="blogf-menu-list">
                                    <?php 
                                    foreach ($children as $keyChild => $child) {
                                        if( is_category($child) ){
                                            $current_taxonomy = get_queried_object();
                                            if( $current_taxonomy->slug == $child->slug ){
                                                $classChild = 'active';
                                            }else{
                                                $classChild = 'non-active';
                                            }
                                        }else{
                                            $classChild = 'non-active';
                                        } ?>
                                    <div class="blogf-menu-item <?php echo $classChild; ?>">
                                        <a href="<?php echo get_term_link( $child ); ?>" class="blogf-menu-link"><?php echo $child->name; ?></a>
                                    </div>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                        <?php } ?>
                    
                    <?php } } ?>
                </div>
            </div>
        </div>
        <?php } ?>
    <?php } ?>
    <div class="blogf-wrap">
        <div class="blogf-py">
            <div class="blogf-py-inner">
                <div class="blogf-gr">
                    <?php if( !empty( $categories_menu_logo ) ){ ?>
                    <div class="blogf-logo">
                        <?php echo wp_get_attachment_image($categories_menu_logo , 'full'); ?>
                    </div>
                    <?php } ?>
                    <div class="blogf-box">
                        <div class="blogf-control">
                            <span class="text"><?php echo !empty( $categories_menu_title ) ? $categories_menu_title : __('Danh mục tin', 'monamedia'); ?></span>
                            <span class="icon">
                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-menu-f.png" alt="">
                            </span>
                        </div>
                    </div>
                </div>
            </div>
            <?php $categories_menu_icon = $mona_blog_categories_menu['categories_menu_icon'];
            $categories_menu_link = $mona_blog_categories_menu['categories_menu_link'];
            if( !empty( $categories_menu_link ) ){ ?>
            <div class="blogf-sale">
                <div class="blogf-sale-inner">
                    <?php if( !empty( $categories_menu_icon ) ){ ?>
                    <div class="icon">
                        <?php echo wp_get_attachment_image($categories_menu_icon, 'full'); ?>
                    </div>
                    <?php } ?>
                    <a href="<?php echo $categories_menu_link['url']; ?>" target="<?php echo $categories_menu_link['target']; ?>" class="text">
                        <?php echo $categories_menu_link['title']; ?>
                    </a>
                </div>
                <div class="blogf-sale-bg">
                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/blogf-bg.png" alt="">
                </div>
            </div>
            <?php } ?>
        </div>
    </div>
</div>
<?php } ?>