<?php
/**
 * Section name: Eco System GLOBAL
 * Description: 
 * Author: Monamedia
 * Order: 1
 */
?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> seo-item">
    <div class="container">
        <div class="seo-item-header d-flex f-ctn">
            <div class="seo-item-tt col">
                <div class="sec-tt host-tt">
                    <h2>
                        Phát triển doanh nghiệp, không phải chi phí
                        <p class="txt-bg bg-host">
                            <span class="txt">
                                Cùng MONA tạo đế chế riêng của bạn
                            </span>
                        </p>
                    </h2>
                </div>
                <div class="sec-desc">
                    <p>
                        Thực hiện bước tiếp theo <br>
                        Liên hệ với chúng tôi để nhận được những tư vấn giá trị giúp doanh nghiệp bạn đứng trên đỉnh thành công
                </p>
                </div>
            </div>
            <div class="seo-item-hotline col">
                <div class="img">
                    <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/hotline-img.png" alt="">
                </div>
            </div>
        </div>
    </div>

    <div class="seo-item-img">
        <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/SEO-team.png" alt="">
    </div>
</section>