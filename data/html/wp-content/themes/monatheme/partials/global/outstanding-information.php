<!-- Thông tin nổi bật -->
<?php $mona_blog_outstanding_information = get_field('mona_blog_outstanding_information', MONA_PAGE_BLOG);
$flagDisabled = true;
if( !empty( $mona_blog_outstanding_information ) && !$flagDisabled ){ ?>
<div class="blogg">
    <?php foreach ($mona_blog_outstanding_information as $key => $item) { ?>
    <div class="blogg-py">
        <div class="blogg-flex">
            <?php if( !empty( $item['outstanding_information_icon'] ) ){ ?>
            <div class="icon">
                <?php echo wp_get_attachment_image($item['outstanding_information_icon'], 'full'); ?>
            </div>
            <?php } ?>
            <div class="text">
                <?php echo $item['outstanding_information']; ?>
            </div>
            <div class="icon">
                <i class="fas fa-times"></i>
            </div>
        </div>
    </div>
    <?php } ?>
</div>
<?php } ?>