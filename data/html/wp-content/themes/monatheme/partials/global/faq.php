<?php
/**
 * Section name: FAQ GLOBAL
 * Description: 
 * Author: Monamedia
 * Order: 1
 */
?>
<?php $mona_global_faqs = get_field('mona_global_faqs');
if( empty( $mona_global_faqs['faqs'] ) ){
    $mona_global_faqs = get_field('mona_global_faqs', MONA_PAGE_HOME);
}
if( !empty( $mona_global_faqs['faqs'] ) ){ ?>
<section id="<?php echo $args['id'] ?>" class="<?php echo $args['classes'] ?> sec-seofaq">
    <div class="container">
        <div class="seofaq">
            <div class="seofaq-top">
                <h2 class="title text-ani bg-blue">FAQ
                </h2>
                <div class="seofaq-line">
                    <svg class="ani" width="317" height="165" viewBox="0 0 317 165" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path d="M239.914 54.4646C97.9425 -6.18229 6.55242 27.0745 2.17067 68.9596C-2.21107 110.845 64.1018 152.108 150.285 161.124C236.467 170.14 309.884 143.494 314.266 101.609C318.518 60.9678 222.966 13.0692 140.531 2.48972" stroke="#3A3A3A" stroke-width="3.2557" class="svg-elem-1"></path>
                    </svg>

                </div>
            </div>
            <div class="seofaq-flex">
                <div class="seofaq-left">
                    <?php $title = $mona_global_faqs['title'];
                    if( !empty( $title ) ){ ?>
                    <div class="seofaq-title">
                        <h2 class="title">
                            <?php echo str_replace(
                                ['[', ']'],
                                ['<span class="hl-pri">', '</span>'],
                                $title
                            ); ?>
                        </h2>
                    </div>
                    <?php } ?>
                    <div class="seofaq-bg-img">
                        <div class="img">
                            <img src="<?php echo MONA_SITE_URL ?>/template/assets/images/faq-panda.png" alt="">
                        </div>
                    </div>
                </div>
                <div class="seofaq-right">
                    <div class="seofaq-wrap collapse-block">
                        <h3 class="title">
                            <?php echo str_replace( 
                                ['[', ']'],
                                ['<span class="fw-700">','</span>'],
                                $mona_global_faqs['subtitle']
                            ); ?>
                        </h3>
                        <div class="seofaq-list">
                        <?php foreach ($mona_global_faqs['faqs'] as $key => $item) { ?>
                        <div class="seofaq-item collapse-item">
                            <div class="seofaq-head collapse-head monaQuestion">
                                <h4 class="title"><?php echo $item['question']; ?></h4>
                                <i class="far fa-chevron-circle-down icon"></i>
                            </div>
                            <div class="seofaq-tbody collapse-body monaAnswer">
                                <?php echo $item['answer']; ?>
                            </div>
                        </div>
                        <?php } ?>
                    </div>

                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>