<?php
/**
 * Section name: Contact 2 GLOBAL
 * Description: 
 * Author: Monamedia
 * Order: 2
 */
?>
<?php 
$mona_global_contact_information_2 = get_field('mona_global_contact_information_2', MONA_PAGE_BLOG);
if( !empty( $mona_global_contact_information_2 ) ){
?>
<div id="<?php echo !empty( $args['id'] ) ? $args['id'] : 'monaGlobalContact_2';  ?>" class="<?php echo !empty( $args['classes'] ) ? $args['classes'] : 'class_monaGlobalContact_2';  ?> section mona-contact fix">
    <div class="mona-map">
        <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3919.2660681275!2d106.65394845057953!3d10.790922361838433!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31752ed189fa855d%3A0xf63e15bfce46baef!2sC%C3%B4ng%20ty%20TNHH%20-%20MONA%20MEDIA!5e0!3m2!1svi!2s!4v1676457646716!5m2!1svi!2s"
            width="100%" height="100%" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
    </div>
    <div class="container">
        <div class="mona-contact-ctn d-flex">
            <div class="mona-contact-content col col-6">
                <div class="content">
                    <div class="mona-contact-tt">
                        <span class="hello" data-aos="fade-left" data-aos-delay="300">
                            Hello! 👋
                        </span>
                        <div class="name" data-aos="fade-up">Mona</div>
                    </div>
                    <div class="mona-contact-desc" data-aos="fade-up">
                    Bạn có thể liên hệ với Mona bằng bất cứ
                    </br>
                    Kênh nào mà bạn có thể trao đổi với chúng tôi
                    </div>

                    <?php $information = $mona_global_contact_information_2['information'];
                    if( !empty( $information ) ){ ?>
                    <div class="mona-contact-address">
                        <?php foreach ($information as $key => $item) { ?>
                        <div class="mona-contact-address-item d-flex aniBop" data-aos="fade-up">
                            <?php echo wp_get_attachment_image($item['icon'], 'full'); ?>
                            <div><?php echo $item['content']; ?></div>
                        </div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                    
                   <?php 
                   /**
                    * GET TEMPLATE PART
                    * method contact
                    */
                    $slug = '/partials/global/contact';
                    $name = 'methods';
                    echo get_template_part($slug, $name);
                   ?>

                    <?php 
                    $flagShowDepartment = false;
                    $note = $mona_global_contact_information_2['note'];
                    if( !empty( $note ) && $flagShowDepartment){ ?>
                    <div class="mona-contact-note">
                        <?php foreach ($note as $key => $item) { ?>
                        <div class="mona-contact-note-item <?php echo $item['class']; ?> d-flex" data-aos="fade-left">
                            <div class="mona-contact-note-btn">
                                <?php echo $item['title']; ?>
                                <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/arrow-up-right.png" alt="">
                            </div>
                            <div class="txt"><?php echo $item['description']; ?></div>
                        </div>
                        <?php } ?>
                    </div>
                    <?php } ?>
                </div>
            </div>
            <div class="mona-contact-form col col-6" id="monaContactForm">
                <div class="mona-contact-form-wrap tabP-js">
                    <div class="form-tab d-flex">
                        <div class="tab active">Khách hàng</div>
                        <div class="tab">Đối tác</div>
                        <div class="tab">Ứng viên</div>
                    </div>

                    <div class="form-desc">
                        *Điền form phía dưới để Mona có thể trao đổi cụ thể với bạn
                    </div>

                    <div class="form-cus">
                        <div class="form-panel tab-panel active">
                            <?php $shortcode_customer = $mona_global_contact_information_2['shortcode_customer'];
                            if( !empty( $shortcode_customer ) ){
                                echo do_shortcode( $shortcode_customer );
                            } ?>
                        </div>
                        <div class="form-panel tab-panel">
                            <?php $shortcode_partner = $mona_global_contact_information_2['shortcode_partner'];
                            if( !empty( $shortcode_partner ) ){
                                echo do_shortcode( $shortcode_partner );
                            } ?>
                        </div>
                        <div class="form-panel tab-panel">
                            <?php $shortcode_candicate = $mona_global_contact_information_2['shortcode_candicate'];
                            if( !empty( $shortcode_candicate ) ){
                                echo do_shortcode( $shortcode_candicate );
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mona-contact-decor">
                <div class="mona-contact-decor-aos" data-aos="zoom-in">
                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/contact-decor.png" alt="">
                </div>
            </div>
        </div>
    </div>
</div>
<?php } ?>