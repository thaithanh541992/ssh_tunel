<?php
/**
 * Section name: Appointment GLOBAL
 * Description: 
 * Author: Monamedia
 * Order: 2
 */
?>
<!-- BOOKING -->
<?php 
$mona_global_booking_appointment_updated = get_field('mona_global_booking_appointment_updated');
if( empty( $mona_global_booking_appointment_updated ) ){
    $mona_global_booking_appointment_updated = get_field('mona_global_booking_appointment_updated', MONA_PAGE_BLOG);
}
if( !empty( $mona_global_booking_appointment_updated ) ){ ?>
<section id="<?php echo !empty( $args['id'] ) ? $args['id'] : 'monaGlobalContact_2'; ?>" 
class="<?php echo !empty( $args['classes'] ) ? $args['classes'] : 'class_monaGlobalContact_2';?> sec-booking ss-mg">
    <div class="container">
        <div class="booking">
            <div class="booking-wrap">
                <div class="booking-decor">
                    <div class="booking-aos" data-aos="zoom-in" data-aos-delay="800">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/booking-img.png" alt="">
                    </div>
                </div>
                <div class="booking-flex">
                    <div class="booking-left">
                        <div class="booking-content">
                            <div class="booking-title">
                                <div class="booking-title-decor">
                                    <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/M-vector.png" alt="">
                                </div>
                                <?php $title = $mona_global_booking_appointment_updated['title'];
                                        if( !empty( $title ) ){ ?>
                                <h2 class="title" data-aos="fade-right">
                                    <?php echo str_replace(
                                                ['[', ']'],
                                                ['<span class="text">', '</span>'],
                                                $title
                                            ); ?>
                                </h2>
                                <?php } ?>
                            </div>
                            
                            <?php $description = $mona_global_booking_appointment_updated['description'];
                            if( !empty( $description ) ){ ?>
                            <div class="des">
                                <?php echo $description; ?>
                            </div>
                            <?php } ?>

                            <?php $link = $mona_global_booking_appointment_updated['link'];
                            if( !empty( $link['url'] ) ){ ?>
                            <div class="booking-link" data-aos="fade-up">
                                <a href="<?php echo $link['url']; ?>" class="btn-second" target="<?php echo $link['target']; ?>">
                                    <span class="txt">
                                        <?php echo $link['title']; ?>
                                    </span>
                                    <span class="icon">
                                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/icon-arrow-right2.png"
                                            alt="">
                                    </span>
                                </a>
                            </div>
                            <?php } ?>
                        </div>
                    </div>
                    <div class="booking-right">
                        <div class="booking-pos" data-aos="zoom-in">
                            <a href="javascript:;" class="booking-btn openPop">
                                <span class="text"><?php echo __('Đặt lịch hẹn.', 'monamedia'); ?></span>
                            </a>
                            <div class="booking-svg ani">
                                <svg width="502" height="435" viewBox="0 0 502 435" fill="none"
                                    xmlns="http://www.w3.org/2000/svg">
                                    <path
                                        d="M141.738 531.582C111.419 534.312 82.7821 529.939 57.7293 509.42C0.256418 462.349 -4.06281 388.198 6.38055 319.806C18.4157 240.989 64.8593 161.75 127.142 111.68C139.479 101.762 165.489 81.3763 176.174 103.544C186.621 125.219 189.314 151.542 190.399 175.085C191.989 209.6 189.482 245.201 179.88 278.553C169.781 313.632 145.353 352.588 104.23 352.265C63.4512 351.946 52.4825 311.606 57.3698 277.222C63.2789 235.65 84.906 198.309 110.638 165.975C151.212 114.991 202.845 73.5298 258.154 39.5126C307.053 9.43752 360.593 -15.4233 416.6 -28.7244C428.592 -31.5724 440.794 -33.6742 453.059 -34.899C498.716 -39.4584 460.515 -32.4516 499.716 -32.5631"
                                        stroke="#7C0FD1" stroke-width="2.81452" stroke-linecap="round"
                                        class="svg-elem-1"></path>
                                </svg>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>