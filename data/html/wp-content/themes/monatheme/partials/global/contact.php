<?php 
$mona_global_extension_information = get_field('mona_global_extension_information', MONA_PAGE_BLOG);
if( !empty( $mona_global_extension_information ) ){
?>
<section class="sec-blogb <?php echo ( !is_singular( 'post' ) ) ? 'fix' : ''; ?>">
    <div class="blogb-bg">
        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/blogb-img-host.png" alt="">
    </div>
    <div class="blogb-decor">
        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/blogb-decor-host.png" alt="">
    </div>
    <div class="blogb-decor2">
        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/iconM2.png" alt="">
    </div>
    <div class="container">
        <div class="blogb">
            <div class="blogb-flex">
                <div class="blogb-col">
                    <div class="blogb-wrap">
                        <p class="title">
                            <?php echo $mona_global_extension_information['title']; ?>
                        </p>
                        <p class="des">
                            <?php echo $mona_global_extension_information['description']; ?>
                        </p>
                        <?php if( !$mona_global_extension_information['option'] ){ ?>
                        <a href="<?php echo $mona_global_extension_information['link']['url']; ?>" target="<?php echo $mona_global_extension_information['link']['target']; ?>" class="btn btn-orange">
                            <span class="txt">
                                <?php echo $mona_global_extension_information['link']['title']; ?>
                            </span>
                        </a>
                        <?php }else{ ?>
                        <a href="<?php echo MONA_SITE_URL ?>/lien-he" class="btn-clh openPop">
                            <span class="txt">
                                <?php echo __('Liên hệ Mona'); ?>
                            </span>
                        </a>
                        <?php } ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php } ?>