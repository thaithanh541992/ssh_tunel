<div class="fcts-mid">
    <?php $section_global_zalo = mona_get_option('section_global_zalo');
    if( !empty( $section_global_zalo ) ){ ?>
    <div class="fcts-mid-gr fcts-zalo">
        <div class="fcts-mid-left">
            <div class="fcts-mid-control">
                <a href="<?php echo $section_global_zalo; ?>" class="fcts-mid-link" target="_blank">
                    <p class="text-wp">
                        <?php echo sprintf('Nhắn tin qua </br><span class="text">Zalo Official của Mona</span>'); ?>
                    </p>   
                    <span class="icon">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/pcontact/icon-zalo.png" alt="">
                    </span>
                </a>
            </div>
        </div>
    </div>
    <?php } ?>

    <?php $section_global_hotline = mona_get_option('section_global_hotline');
    if( !empty( $section_global_hotline ) ){ ?>
    <div class="fcts-mid-gr fcts-hotline">
        <div class="fcts-mid-left">
            <div class="fcts-mid-control">
                <a href="<?php echo mona_replace_tel( $section_global_hotline ); ?>" class="fcts-mid-link">
                    <p class="text-wp">
                        <?php echo sprintf('Gọi ngay Hotline</br><span class="text">') . $section_global_hotline . '</span>'; ?>
                    </p>     
                    <span class="icon">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/pcontact/icon-call-2.png" alt="">
                    </span>
                </a>
            </div>
        </div>
    </div>
    <?php } ?>

    <div class="fcts-mid-gr fcts-live">
        <div class="fcts-mid-left">
            <div class="fcts-mid-control">
                <a href="javascript:void(Tawk_API.toggle());" class="fcts-mid-link">
                    <p class="text-wp">
                        <?php echo sprintf('Livechat trực tuyến </br><span class="text">với Tawkto</span>'); ?>
                    </p>  
                    <span class="icon">
                        <img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/pcontact/icon-live.png" alt="">
                    </span>
                </a>
            </div>
        </div>

    </div>
    </div>