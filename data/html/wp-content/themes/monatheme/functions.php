<?php
/**
 * The template for displaying index.
 *
 * @package MONA.Media / Website
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

// efine acf
if ( get_current_user_id() == 1 ) {
    define( 'ACF_LITE', false );
} else {
    define( 'ACF_LITE', true );
}

// define theme page
define( 'MONA_PAGE_HOME', get_option( 'page_on_front', true ) );
define( 'MONA_PAGE_BLOG', get_option( 'page_for_posts', true ) );

define( 'MONA_SITE_URL', get_site_url() );


// Woocommerce
define('MONA_WC_PRODUCTS', get_option('woocommerce_shop_page_id'));
define('MONA_WC_CART', get_option('woocommerce_cart_page_id'));
define('MONA_WC_CHECKOUT', get_option('woocommerce_checkout_page_id'));
define('MONA_WC_MYACCOUNT', get_option('woocommerce_myaccount_page_id'));
define('MONA_WC_THANKYOU', get_option('woocommerce_thanks_page_id'));
require_once( get_template_directory() . '/__autoload.php' );

function return_megamenu_active( $url , $class_active = 'current-menu-item' ){
    global $wp;
    $current_url = home_url( $wp->request );
    if( empty( $url ) ){
        return 'mona-non-active';
    }

    if( strcmp( $current_url, $url ) == 0 ){
        return $class_active;
    }else{
        return 'mona-non-active';
    }
}

define( 'MEDIA_BASE_URL', 'https://mona.media/wp-json/api/' );

function get_media_newsfeed() {
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => MEDIA_BASE_URL . 'getNewsfeed/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}


function get_media_customer_gallery() {
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => MEDIA_BASE_URL . 'getCustomerGallery/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

function get_media_customer_gallery_2() {
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => MEDIA_BASE_URL . 'getCustomerGallery2/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

function get_media_customer_collab() {
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => MEDIA_BASE_URL . 'getCustomerCollab/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}


function get_media_customer_feedback() {
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => MEDIA_BASE_URL . 'getCustomerFeedback/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}

function get_media_customer_project() {
    $curl = curl_init();

    curl_setopt_array($curl, array(
        CURLOPT_URL => MEDIA_BASE_URL . 'getCustomerProject/',
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => '',
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 0,
        CURLOPT_FOLLOWLOCATION => true,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => 'GET',
    ));

    $response = curl_exec($curl);

    curl_close($curl);
    return $response;
}