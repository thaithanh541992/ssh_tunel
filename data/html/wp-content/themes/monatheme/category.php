<?php
/**
 * The template for displaying category.
 *
 * @package Monamedia
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
?>

<?php 
$current_taxonomy = get_queried_object();
$child_of_current_taxonomy = get_terms(
	[
		'taxonomy' 		=> $current_taxonomy->taxonomy,
		'hide_empty'	=>	false,
		'parent'		=>	$current_taxonomy->term_id,
	]
);
if( $current_taxonomy->parent == 0 && !empty( $child_of_current_taxonomy ) ){
	$slug = '/partials/categories/categories';
	$name = 'lv2';
	echo get_template_part($slug, $name);
}else{
	$slug = '/partials/categories/categories';
	$name = 'lv3';
	echo get_template_part($slug, $name);
}
?>

<?php get_footer();
