<?php
/**
 * Order Item Details
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/order/order-details-item.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 5.2.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! apply_filters( 'woocommerce_order_item_visible', true, $item ) ) {
	return;
}
?>
<div class="info-item-flex d-flex f-between">
	<div class="info-item-flex-col">
		<div class="name"><?php echo $item->get_name() ?></div>
		<?php
			$custom_data = $item['custom_data'];
			if ( !empty( $custom_data ) && !empty( $custom_data['billingcycle'] ) ) {
				?>
				<div class="desc">Thời gian sử dụng: <span class="fw-600"><?php echo $custom_data['billingcycle'] ?> tháng</span></div>
				<?php
			}
		?>
	</div>
	<div class="info-item-flex-col">
		<div class="price fw-600"><?php echo $order->get_formatted_line_subtotal( $item ); ?></div>
	</div>
</div>
