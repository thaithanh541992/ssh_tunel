<?php
/**
 * Checkout billing information form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-billing.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see     https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.6.0
 * @global WC_Checkout $checkout
 */

defined( 'ABSPATH' ) || exit;
?>
<div class="woocommerce-billing-fields">

	<?php do_action( 'woocommerce_before_checkout_billing_form', $checkout ); ?>
	<?php
	$coutnry_list = json_decode(@inet_get_list_country());
	?>

	<div class="host-checkout-info-form host-form">
		<div class="form-checkout-wrap-js">
			<div class="f-r">
				<div class="f-c f-gender">
					<select class="select2choose" name="billing_gender">
						<option value="male">
							Ông
						</option>
						<option value="female">
							Bà
						</option>
					</select>
				</div>
				<div class="f-c">
					<input type="text" name="billing_first_name" placeholder="Tên của bạn" class="txt-ipt" required>
				</div>
			</div>
			<div class="f-r">
				<div class="f-c">
					<input type="date" name="billing_birthday" placeholder="Ngày sinh" class="txt-ipt">
					<div class="label label-birthday label-birthday-js">Ngày sinh</div>
				</div>
			</div>
			<div class="f-r">
				<div class="f-c">
					<input type="number" name="billing_id_number" placeholder="CMND/CCCD" class="txt-ipt" required>
				</div>
			</div>
			<div class="f-r">
				<div class="f-c">
					<input type="number" name="billing_phone" placeholder="Số điện thoại" class="txt-ipt" required>
				</div>
			</div>
			<div class="f-r">
				<div class="f-c">
					<input type="email" name="billing_email" placeholder="Email" class="txt-ipt" required>
				</div>
			</div>
			<div class="f-r form-half">
				<div class="f-c">
					<select class="select2choose country-js" name="billing_country" data-placeholder="Quốc gia / khu vực">
						<option value=""></option>
						<?php
						if ( !empty( $coutnry_list ) && is_array( $coutnry_list ) ) {
							foreach( $coutnry_list as $country ) {
								?>
								<option value="<?php echo $country->name ?>" <?php selected( $country->name, 'VN' ) ?>><?php echo $country->value ?></option>
								<?php
							}
						}
						?>
					</select>
					<input type="text" hidden name="billing_country_label" class="country-label-js">
				</div>
			</div>
			<div class="f-r form-half state-wrap-js">
				<div class="f-c">
					<select class="select2choose state-js" name="billing_state" data-post_name="billing_state" data-placeholder="Tỉnh / thành phố">
					</select>
					<input type="text" placeholder="<?php echo __('Bang / thành phố', 'monamedia') ?>" hidden class="txt-ipt state-default-js">
					<input type="text" hidden name="billing_state_label" class="state-label-js">
				</div>
			</div>
			<div class="f-r form-half city-wrap-js">
				<div class="f-c">
					<select class="select2choose city-js city-input-js" name="billing_city" data-placeholder="Quận / huyện">
					</select>
					<input type="text" hidden class="txt-ipt city-default-js" value="">
				</div>
			</div>
			<div class="f-r form-half ward-wrap-js">
				<div class="f-c">
					<select class="select2choose ward-js ward-input-js" name="billing_ward" data-placeholder="Phường / xã">
					</select>
					<input type="text" hidden class="txt-ipt ward-default-js" value="">
				</div>
			</div>
			<div class="f-r">
				<div class="f-c">
					<input type="text" name="billing_address_1" placeholder="Địa chỉ" class="txt-ipt" required>
				</div>
			</div>
		</div>
		<?php
		// $fields = $checkout->get_checkout_fields( 'billing' );

		// foreach ( $fields as $key => $field ) {
		// 	woocommerce_form_field( $key, $field, $checkout->get_value( $key ) );
		// }
		?>
		<p class="txt-single fw-600">Thông tin doanh nghiệp (dành cho khách hàng là tổ chức)</p>
		<div class="form-checkout-wrap-js">
			<div class="f-r">
				<div class="f-c">
					<input type="text" name="organization_first_name" placeholder="Tên tổ chức" class="txt-ipt">
				</div>
			</div>
			<div class="f-r">
				<div class="f-c">
					<input type="number" name="organization_id_number" placeholder="Mã số thuế" class="txt-ipt">
				</div>
			</div>
			<div class="f-r">
				<div class="f-c">
					<input type="number" name="organization_phone" placeholder="Số điện thoại" class="txt-ipt">
				</div>
			</div>
			<div class="f-r">
				<div class="f-c">
					<input type="email" name="organization_email" placeholder="Email" class="txt-ipt">
				</div>
			</div>
			<div class="f-r form-half">
				<div class="f-c">
					<select class="select2choose country-js" name="organization_country" data-placeholder="Quốc gia / khu vực">
						<option value=""></option>
						<?php
						if ( !empty( $coutnry_list ) && is_array( $coutnry_list ) ) {
							foreach( $coutnry_list as $country ) {
								?>
								<option value="<?php echo $country->name ?>"><?php echo $country->value ?></option>
								<?php
							}
						}
						?>
					</select>
					<input type="text" hidden name="organization_country_label" class="country-label-js">
				</div>
			</div>
			<div class="f-r form-half state-wrap-js">
				<div class="f-c">
					<select class="select2choose state-js" name="organization_state" data-post_name="organization_state" data-placeholder="Tỉnh / thành phố">
					</select>
					<input type="text" placeholder="<?php echo __('Bang / thành phố', 'monamedia') ?>" hidden class="txt-ipt state-default-js">
					<input type="text" hidden name="organization_state_label" class="state-label-js">
				</div>
			</div>
			<div class="f-r form-half city-wrap-js">
				<div class="f-c">
					<select class="select2choose city-js" name="organization_city" data-placeholder="Quận / huyện">
					</select>
				</div>
			</div>
			<div class="f-r form-half ward-wrap-js">
				<div class="f-c">
					<select class="select2choose ward-js" name="organization_ward" data-placeholder="Phường / xã">
					</select>
				</div>
			</div>
			<div class="f-r">
				<div class="f-c">
					<input type="text" name="organization_address_1" placeholder="Địa chỉ" class="txt-ipt">
				</div>
			</div>
		</div>
	</div>

	<?php do_action( 'woocommerce_after_checkout_billing_form', $checkout ); ?>
</div>
