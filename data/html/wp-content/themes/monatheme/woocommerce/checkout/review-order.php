<?php
/**
 * Review order table
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/review-order.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 5.2.0
 */

defined( 'ABSPATH' ) || exit;
?>
<div class="woocommerce-checkout-review-order-table">
	<div class="host-service-cart checkout-wrap-js is-loading-btn-2">
	<?php
	do_action( 'woocommerce_review_order_before_cart_contents' );
	foreach ( WC()->cart->get_cart() as $cart_item_key => $cart_item ) {
		$_product = apply_filters( 'woocommerce_cart_item_product', $cart_item['data'], $cart_item, $cart_item_key );
		$custom_data = $cart_item['custom_data'];
	
		if ( $_product && $_product->exists() && $cart_item['quantity'] > 0 && apply_filters( 'woocommerce_checkout_cart_item_visible', true, $cart_item, $cart_item_key ) ) {
			
			$product = $cart_item['data'];
			$product_name = $product->get_name();

			$ptype = get_field('mona_product_type', $product->get_id());

            if ( $ptype == 'domain' && !empty( $cart_item['custom_data']['domain_name'] ) ) {
                $product_name = $cart_item['custom_data']['domain_name'];
            }
            ?>
			<div class="host-service-cart-item">
				<div class="host-service-cart-left">
					<div class="name"><?php echo $product_name; ?></div>
					<?php
					$mona_billing_cycle_list = get_field('mona_billing_cycle_list', $product->get_id());
					if( !empty( $mona_billing_cycle_list ) && is_array( $mona_billing_cycle_list ) ) {
						?>
						<div class="desc">
							<div class="label"><?php echo __('Thời gian sử dụng', 'monamedia') ?></div>
							<select class="select2choose billingcycle-cart-js" 
								data-pid=<?php echo $product->get_id() ?>
								data-cart_item_key="<?php echo $cart_item_key; ?>"
							>
								<?php
								foreach ( $mona_billing_cycle_list as $billing_cycle ) {
									?>
									<option value="<?php echo $billing_cycle['term_quantity'] ?>"
										data-price=<?php echo $billing_cycle['price'] ?>
										<?php selected($cart_item['custom_data']['billingcycle'], $billing_cycle['term_quantity']) ?>
									>
										<?php echo $billing_cycle['label'] ?>
									</option>
									<?php
								}
								?>
							</select>
						</div>
						<?php
					}
					?>
				</div>
				<div class="host-service-cart-right">
					<div class="price">
						<?php echo apply_filters( 'woocommerce_cart_item_subtotal', WC()->cart->get_product_subtotal( $_product, $cart_item['quantity'] ), $cart_item, $cart_item_key ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
					</div>
				</div>
			</div>
			<?php
		}
	}
	do_action( 'woocommerce_review_order_after_cart_contents' );
	?>
	</div>
	<div class="host-service-aside-item">
		<div class="host-service-aside-item-header">
			<div class="label-small">
				<?php echo __('Tổng tiền dịch vụ', 'monamedia'); ?>
			</div>
			<div class="price price-big"><?php wc_cart_totals_subtotal_html(); ?></div>
		</div>
	</div>
	<div class="host-service-aside-item">
		<div class="host-service-aside-item-body">
			<?php foreach ( WC()->cart->get_coupons() as $code => $coupon ) : ?>
				<div class="item cart-discount cart-discount coupon-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
					<p><?php wc_cart_totals_coupon_label( $coupon ); ?></p>
					<p class="price"><?php wc_cart_totals_coupon_html( $coupon ); ?></p>
				</div>
			<?php endforeach; ?>
			
			<?php foreach ( WC()->cart->get_fees() as $fee ) : ?>
				<div class="item">
					<p><?php echo esc_html( $fee->name ); ?></p>
					<p class="price"><?php wc_cart_totals_fee_html( $fee ); ?></p>
				</div>
			<?php endforeach; ?>
	
			<?php if ( wc_tax_enabled() && ! WC()->cart->display_prices_including_tax() ) : ?>
				<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
					<?php foreach ( WC()->cart->get_tax_totals() as $code => $tax ) : // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited ?>
						<div class="item tax-rate tax-rate-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
							<p><?php echo esc_html( $tax->label ); ?></p>
							<p class="price"><?php echo wp_kses_post( $tax->formatted_amount ); ?></p>
						</div>
					<?php endforeach; ?>
				<?php else : ?>
					<div class="item tax-total tax-rate tax-rate-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
						<p><?php echo esc_html( WC()->countries->tax_or_vat() ); ?></p>
						<p class="price"><?php wc_cart_totals_taxes_total_html(); ?></p>
					</div>
				<?php endif; ?>
			<?php endif; ?>
		</div>
	
		<?php do_action( 'woocommerce_review_order_before_order_total' ); ?>
	
		<div class="host-service-aside-item-header">
			<div class="label-small">
				<?php echo __('Tổng tiền thanh toán', 'monamedia'); ?>
			</div>
			<div class="price price-biggest"><?php wc_cart_totals_order_total_html(); ?></div>
		</div>
	
		<?php do_action( 'woocommerce_review_order_after_order_total' ); ?>
	
	</div>
	
	<script src="<?php echo MONA_SITE_URL ?>/template/js/libs/select2/select2.min.js"></script> 
	<script src="<?php echo get_template_directory_uri() . '/public/helpers/scripts/modules/checkout.js' ?>"></script>  
</div>

