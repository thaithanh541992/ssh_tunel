<?php
/**
 * Thankyou page
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/thankyou.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.7.0
 */

defined( 'ABSPATH' ) || exit;
?>
<div class="host-checkout-process">
	<div class="host-checkout-process-timeline">
		<p class="process-percent" style="width: 100%;"></p>
	</div>

	<div class="host-checkout-process-list d-flex f-center">
		<div class="host-checkout-process-item process-done">
			<span class="process-check"></span>
			<span class="process-txt">Chọn tên miền</span>
		</div>
		<div class="host-checkout-process-item process-done">
			<span class="process-check"></span>
			<span class="process-txt">Nhập thông tin <br> thanh toán</span>
		</div>
		<div class="host-checkout-process-item process-done">
			<span class="process-check"></span>
			<span class="process-txt">Hoàn thành</span>
		</div>
	</div>
</div>
<div class="host-checkout-completed">
	<div class="woocommerce-order">

		<?php
		if ( $order ) :

			do_action( 'woocommerce_before_thankyou', $order->get_id() );
			$order_data = $order->get_data();
			$order_id = $order->get_id();
			?>

			<?php if ( $order->has_status( 'failed' ) ) : ?>

				<div class="host-checkout-completed-header">
					<div class="host-checkout-completed-tt txt-center failed">
					<?php esc_html_e( 'Unfortunately your order cannot be processed as the originating bank/merchant has declined your transaction. Please attempt your purchase again.', 'woocommerce' ); ?>
					</div>
				</div>

			<?php else : ?>
				<div class="host-checkout-completed-header">
					<div class="host-checkout-completed-tt txt-center">
						<?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), $order ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?>
					</div>
					<div class="host-checkout-completed-desc fw-600 txt-center">Thông tin hóa đơn</div>
				</div>
				<div class="host-checkout-completed-info">
					<div class="info-left">
						<div class="info-item">
							<div class="info-item-tt">Thông tin thanh toán</div>
							
							<div class="info-item-col">
								<div class="info-item-detail d-flex">
									<div class="info-item-detail-col">
										<div class="item d-flex f-between">
											<div class="info-item-txt-nor">Mã đơn hàng</div>
											<div class="dt fw-600 hl-pri"><?php echo $order->get_order_number(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></div>
										</div>
										<div class="item d-flex f-between">
											<div class="info-item-txt-nor">Ngày tạo đơn</div>
											<div class="dt "><?php echo wc_format_datetime( $order->get_date_created(), 'd/m/Y' ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></div>
										</div>
										<div class="item d-flex f-between">
											<div class="info-item-txt-nor">Hình thức thanh toán</div>
											<div class="dt "><?php echo wp_kses_post( $order->get_payment_method_title() ); ?></div>
										</div>
									</div>
									<div class="info-item-detail-col">
										<div class="item d-flex f-between">
											<div class="info-item-txt-nor">Tổng tiền dịch vụ</div>
											<div class="dt price"><?php echo wc_price($order->get_subtotal()); ?></div>
										</div>
										<?php if ( wc_tax_enabled() ) : ?>
											<?php if ( 'itemized' === get_option( 'woocommerce_tax_total_display' ) ) : ?>
												<?php foreach ( $order->get_items('tax') as $code => $tax ) : // phpcs:ignore WordPress.WP.GlobalVariablesOverride.Prohibited ?>
													<div class="item d-flex f-between tax-rate tax-rate-<?php echo esc_attr( sanitize_title( $code ) ); ?>">
														<p class="info-item-txt-nor"><?php echo esc_html( $tax->get_label() ); ?></p>
														<p class="dt price"><?php echo wc_price( $tax->get_tax_total() ); ?></p>
													</div>
												<?php endforeach; ?>
											<?php endif; ?>
										<?php endif; ?>
										<div class="item d-flex f-between">
											<div class="info-item-txt-nor price-total">Tổng tiền thanh toán</div>
											<div class="dt price fw-600 hl-sec"><?php echo $order->get_formatted_order_total(); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php
						if ( $order->get_payment_method() == 'bacs' ) {
							$bacs_accounts = get_option( 'woocommerce_bacs_accounts');
							if ( !empty( $bacs_accounts ) && is_array( $bacs_accounts ) ) {
								foreach ( $bacs_accounts as $item ) {
									?>
									<div class="info-item">
										<div class="info-right-item-w">
											<div class="info-item-tt">Thông tin chuyển khoản</div>

											<div class="info-right-list d-flex f-start">
												<div class="info-right-item">
													<div class="info-item-txt-nor">Tên ngân hàng</div>
													<p><?php echo $item['bank_name']; ?></p>
												</div>
												<div class="info-right-item">
													<div class="info-item-txt-nor">Tên tài khoản</div>
													<p><?php echo $item['account_name']; ?></p>
												</div>
												<div class="info-right-item">
													<div class="info-item-txt-nor">Số tài khoản</div>
													<p><?php echo $item['account_number']; ?></p>
												</div>
												<div class="info-right-item">
													<div class="info-item-txt-nor">Nội dung chuyển khoản</div>
													<p><?php echo $order->get_id() . '-' . $order_data['billing']['phone'] . '-' . $order_data['billing']['email']; ?></p>
												</div>
											</div>
										</div>
									</div>
									<?php
								}
							}
						}
						?>
						<?php do_action( 'woocommerce_thankyou', $order->get_id() ); ?>
						
					</div>
					<div class="info-right">
						<div class="info-item">
							<div class="info-right-item-w">
								<div class="info-item-tt">Thông tin khách hàng</div>

								<div class="info-right-list d-flex f-start">
									<div class="info-right-item">
										<div class="info-item-txt-nor">Tên khách hàng</div>
										<p>
											<?php 
												$gender = get_post_meta( $order_id, '_billing_gender', true );
												echo $gender == 'male' ? __('(Ông) ', 'monamedia') : __('(Bà) ', 'monamedia');
												echo $order_data['billing']['first_name']; 
											?>
										</p>
									</div>
									<div class="info-right-item">
										<div class="info-item-txt-nor">Ngày sinh</div>
										<p><?php echo get_post_meta( $order_id, '_billing_birthday', true ); ?></p>
									</div>
									<div class="info-right-item">
										<div class="info-item-txt-nor">Số điện thoại</div>
										<p><?php echo $order_data['billing']['phone']; ?></p>
									</div>
									<div class="info-right-item">
										<div class="info-item-txt-nor">Email</div>
										<p><?php echo $order_data['billing']['email']; ?></p>
									</div>
									<div class="info-right-item">
										<div class="info-item-txt-nor">CMND/CCCD</div>
										<p><?php echo get_post_meta( $order_id, '_billing_id_number', true ); ?></p>
									</div>
									<div class="info-right-item">
										<div class="info-item-txt-nor">Địa chỉ</div>
										<p>
											<?php 
												$address = $order_data['billing']['address_1'];
												$ward = get_post_meta( $order_id, '_billing_ward', true );
												$city = $order_data['billing']['city'];
												$state = $order_data['billing']['state'];
												$state_label = get_post_meta( $order_id, '_billing_state_label', true );
												$country = $order_data['billing']['country'];
												$country_label = get_post_meta( $order_id, '_billing_country_label', true );

												echo $address ? $address : ''; 
												echo $ward && $ward != 'default' ? ', ' . $ward : ''; 
												echo $city && $city != 'default' ?  ', ' . $city : ''; 
												echo $state ? ', ' . ( strlen($state) <=3 ? $state_label : $state )  : ''; 
												echo $country ? ', ' . ( strlen($country) <=3 ? $country_label : $country ) : ''; 
											?>
										</p>
									</div>
								</div>
							</div>
							<?php
								$is_organization =  get_post_meta( $order_id, '_is_organization', true );
								if ( $is_organization ) {
									?>
									<div class="info-right-item-w">
										<div class="info-item-tt">Thông tin doanh nghiệp</div>
		
										<div class="info-right-list d-flex f-start">
											<?php
											$organization_first_name =  get_post_meta( $order_id, '_organization_first_name', true );
											if ( !empty( $organization_first_name ) ) {
												?>
												<div class="info-right-item">
													<div class="info-item-txt-nor">Tên tổ chức</div>
													<p><?php echo $organization_first_name; ?></p>
												</div>
												<?php
											}
		
											$organization_id_number =  get_post_meta( $order_id, '_organization_id_number', true );
											if ( !empty( $organization_id_number ) ) {
												?>
												<div class="info-right-item">
													<div class="info-item-txt-nor">Mã số thuế</div>
													<p><?php echo $organization_id_number; ?></p>
												</div>
												<?php
											}
		
											$organization_phone =  get_post_meta( $order_id, '_organization_phone', true );
											if ( !empty( $organization_phone ) ) {
												?>
												<div class="info-right-item">
													<div class="info-item-txt-nor">Số điện thoại</div>
													<p><?php echo $organization_phone; ?></p>
												</div>
												<?php
											}
		
											$organization_email =  get_post_meta( $order_id, '_organization_email', true );
											if ( !empty( $organization_email ) ) {
												?>
												<div class="info-right-item">
													<div class="info-item-txt-nor">Email</div>
													<p><?php echo $organization_email; ?></p>
												</div>
												<?php
											} 
											?>
											<div class="info-right-item">
												<div class="info-item-txt-nor">Địa chỉ</div>
												<p>
												<?php
													$address = get_post_meta( $order_id, '_organization_address_1', true );
													$ward = get_post_meta( $order_id, '_organization_ward', true );
													$city = get_post_meta( $order_id, '_organization_city', true );
													$state = get_post_meta( $order_id, '_organization_state', true );
													$state_label = get_post_meta( $order_id, '_organization_state_label', true );
													$country = get_post_meta( $order_id, '_organization_country', true );
													$country_label = get_post_meta( $order_id, '_organization_country_label', true );
				
													echo $address ? $address : ''; 
													echo $ward && $ward != 'default' ? ', ' . $ward : ''; 
													echo $city && $city != 'default' ?  ', ' . $city : ''; 
													echo $state ? ', ' . ( strlen($state) <=3 ? $state_label : $state )  : ''; 
													echo $country ? ', ' . ( strlen($country) <=3 ? $country_label : $country ) : '';
												?>
												</p>
											</div>
										</div>
									</div>
									<?php
								}
							?>
						</div>
					</div>
				</div>

			<?php endif; ?>

		<?php else : ?>

			<p class="woocommerce-notice woocommerce-notice--success woocommerce-thankyou-order-received"><?php echo apply_filters( 'woocommerce_thankyou_order_received_text', esc_html__( 'Thank you. Your order has been received.', 'woocommerce' ), null ); // phpcs:ignore WordPress.Security.EscapeOutput.OutputNotEscaped ?></p>

		<?php endif; ?>

	</div>
</div>
