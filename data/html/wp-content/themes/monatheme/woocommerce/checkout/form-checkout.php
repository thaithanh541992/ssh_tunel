<?php
/**
 * Checkout Form
 *
 * This template can be overridden by copying it to yourtheme/woocommerce/checkout/form-checkout.php.
 *
 * HOWEVER, on occasion WooCommerce will need to update template files and you
 * (the theme developer) will need to copy the new files to your theme to
 * maintain compatibility. We try to do this as little as possible, but it does
 * happen. When this occurs the version of the template file will be bumped and
 * the readme will list any important changes.
 *
 * @see https://docs.woocommerce.com/document/template-structure/
 * @package WooCommerce\Templates
 * @version 3.5.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

do_action( 'woocommerce_before_checkout_form', $checkout );
?>
<div class="host-checkout-process">
	<div class="host-checkout-process-timeline">
		<p class="process-percent" style="width: 50%;"></p>
	</div>

	<div class="host-checkout-process-list d-flex f-center">
		<div class="host-checkout-process-item process-done">
			<span class="process-check"></span>
			<span class="process-txt">Chọn tên miền</span>
		</div>
		<div class="host-checkout-process-item process-current">
			<span class="process-check"></span>
			<span class="process-txt">Nhập thông tin <br> thanh toán</span>
		</div>
		<div class="host-checkout-process-item">
			<span class="process-check"></span>
			<span class="process-txt">Hoàn thành</span>
		</div>
	</div>
</div>

<div class="host-checkout-info">
	<?php
	// If checkout registration is disabled and not logged in, the user cannot checkout.
	if ( ! $checkout->is_registration_enabled() && $checkout->is_registration_required() && ! is_user_logged_in() ) {
		echo esc_html( apply_filters( 'woocommerce_checkout_must_be_logged_in_message', __( 'You must be logged in to checkout.', 'woocommerce' ) ) );
		return;
	}
	?>

	<form name="checkout" method="post" class="checkout woocommerce-checkout" action="<?php echo esc_url( wc_get_checkout_url() ); ?>" enctype="multipart/form-data">
		<div class="host-checkout-info-ctn host-service-ctn d-flex f-ctn">
			<div class="host-checkout-info-col host-service-col col-7 col">
				<div class="host-checkout-info-content">
					<div class="host-tt"><?php echo __('Nhập thông tin của bạn', 'monamedia'); ?></div>
					<?php if ( $checkout->get_checkout_fields() ) : ?>
			
						<?php do_action( 'woocommerce_checkout_before_customer_details' ); ?>
		
						<?php do_action( 'woocommerce_checkout_billing' ); ?>
			
						<?php do_action( 'woocommerce_checkout_after_customer_details' ); ?>
			
					<?php endif; ?>
				</div>
			</div>
			<div class="host-checkout-info-col host-service-col col-5 col">
				<?php do_action( 'woocommerce_checkout_before_order_review_heading' ); ?>
				<div class="host-service-aside">
					<div class="host-service-aside-wrap">
						<div class="host-service-aside-tt host-tt"><?php echo __('Đơn hàng của bạn', 'monamedia'); ?></div>
						<?php do_action( 'woocommerce_checkout_before_order_review' ); ?>
					
						<?php do_action( 'woocommerce_checkout_order_review' ); ?>
					
						<?php do_action( 'woocommerce_checkout_after_order_review' ); ?>
					</div>
				</div>
				
			</div>
		</div>
	</form>
</div>
<?php do_action( 'woocommerce_after_checkout_form', $checkout ); ?>
