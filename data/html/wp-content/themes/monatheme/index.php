<?php
/**
 * The template for displaying index.
 *
 * @package Monamedia
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}

get_header();
?>

<main class="main">
	<!-- MONA BLOG: introduction -->
	<?php $mona_blog_introduction = get_field('mona_blog_introduction', MONA_PAGE_BLOG); ?>
	<section class="sec-blogt">
		<div class="sec-blogt-bg">
            <img src="<?php echo get_site_url(); ?>/template/assets/images/blog/sec-blogt-bg-big-host.png" alt="">
        </div>
		<div class="blogt">
			<div class="blogt-wrap">
				<div class="blogt-dot"></div>
				<div class="container">
					<div class="blogt-py">
						<div class="blogt-top">
							<h2 class="title" data-aos="fade-left">
								<?php echo str_replace(
									['[', ']'],
									['<span class="text">' , '</span>'],
									$mona_blog_introduction['introduction_title']
								); ?>
							</h2>
							<p class="des" data-aos="fade-right">
								<?php echo $mona_blog_introduction['introduction_description']; ?>
							</p>
						</div>
						<?php $introduction_outstanding_category = $mona_blog_introduction['introduction_outstanding_category'];
						if( !empty( $introduction_outstanding_category ) ){
						?>
						<div class="blogt-mid" data-aos="fade-up">
							<div class="blogt-mid-py">
								<div class="blogt-flex">
									<div class="blogt-col">
										<div class="blogt-content">
											<?php 
											$parent = get_taxonomy_term_root( $introduction_outstanding_category );
											if( !empty( $parent ) ){ ?>
											<div class="tsale">
												<a href="<?php echo get_term_link( $parent ); ?>">
													<?php echo $parent->name; ?>
												</a>
											</div>
											<?php } ?>
											<h3 class="title">
												<?php echo $introduction_outstanding_category->name; ?>
											</h3>
											<?php $overview_content = get_field('mona_taxonomy_overview_content', $introduction_outstanding_category);
											if( !empty( $overview_content ) ){ ?>
											<div class="description mona-content">
												<?php echo $overview_content ?>
											</div>
											<?php } ?>
											
											<div class="blogt-link">
												<a href="<?php echo get_term_link( $introduction_outstanding_category ); ?>" class="text">
													<?php echo __('Xem thêm', 'monamedia'); ?>
												</a>
												<i class="fas fa-chevron-right icon"></i>
											</div>
										</div>
									</div>
									<div class="blogt-col">
										<div class="blogt-img">
											<div class="inner">
												<?php $mona_taxonomy_thumbnail = get_field('mona_taxonomy_thumbnail', $introduction_outstanding_category);
												if( empty( $mona_taxonomy_thumbnail ) ){ ?>
													<img src="<?php echo get_template_directory_uri() ?>/public/helpers/images/global-thumbnail-mona.png" alt="">
												<?php }else{ ?> 
													<?php echo wp_get_attachment_image($mona_taxonomy_thumbnail, 'thumbnail-taxonomy'); ?>	
												<?php } ?>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>
			</div>
		</div>
	</section>

	<?php if( !empty( $mona_blog_introduction['introduction_video'] ) && !empty( $mona_blog_introduction['introduction_video_thumbnail'] ) ){ ?>
	<section class="sec-blogv">
		<div class="container">
			<div class="blogv">
				<div class="blogv-wrap">
					<div class="blogv-img">
						<div class="inner">
							<div id="blogv-video-youtube" class="">
								<?php echo $mona_blog_introduction['introduction_video']; ?>
							</div>
							<div class="blogv-bg">
								<img srcset="<?php echo wp_get_attachment_image_url($mona_blog_introduction['introduction_video_thumbnail'], 'full'); ?> 2x" alt="">
							</div>
							<div class="play-btn">
								<img srcset="<?php echo get_site_url() ?>/template/assets/images/blog/blogt-play.png 2x" alt="">
							</div>
						</div>
						<div class="decor">
							<div class="decor-aos" data-aos="fade-right">
								<img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/blogv-decor.png" alt="">
							</div>
						</div>
						<div class="decor2">
							<div class="decor-aos" data-aos="zoom-in">
								<img srcset="<?php echo get_site_url() ?>/template/assets/images/blog/blogv-decor2.png 2x" alt="">
							</div>
						</div>
						<div class="decor3">
							<img srcset="<?php echo get_site_url() ?>/template/assets/images/blog/blogv-decor3.png 2x" alt="">
						</div>
						<div class="decor4">
							<img class="cir" srcset="<?php echo get_site_url() ?>/template/assets/images/blog/blogv-decor4.png 2x" alt="">
						</div>
						<div class="decor5">
							<img class="cir" srcset="<?php echo get_site_url() ?>/template/assets/images/blog/blogv-decor5.png 2x" alt="">
						</div>
						<div class="decor6">
							<span class="text"><?php echo __('Kiến thức tại Mona Blog <br>
								không phải để kiếm tiền,','monamedia'); ?></span>
							<span class="line">
								<img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/blogv-line.png" alt="">
							</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php } ?>
	<!-- END -->

	<?php $taxonomy_others = get_field('mona_blog_taxonomy_others', MONA_PAGE_BLOG);
	if( !empty( $taxonomy_others ) && is_array( $taxonomy_others ) ){ ?>
	<!-- MONA BLOG: categories -->
	<section class="sec-blogl-block">
		<div class="container">
			<div class="sec-blogl-block-flex">
				<div class="sec-blogl-block-wrap">
					<?php foreach ($taxonomy_others as $key => $other) {
						$detail = $other['taxonomy_detail'];
						$title 	= $other['taxonomy_title'];
						$color 	= $other['taxonomy_color'];
						if( !empty( $detail ) && $detail->count > 0 ){
							$post_type = 'post';
							$posts_per_page = 5;
							// $paged = max( 1, get_query_var('paged') );
							$paged = 1;
							$offset = ( $paged - 1 ) * $posts_per_page;
							$argsPost = [
								'post_type' => $post_type,
								'post_status' => 'publish',
								'posts_per_page' => $posts_per_page,
								'paged' => $paged,
								'offset' => $offset,
								'meta_query' => [
									'relation' => 'AND',
								],
								'tax_query' => [
									'relation'=>'AND',
									array(
										'taxonomy' => $detail->taxonomy,
										'field' => 'slug',
										'terms' => $detail->slug
									)
								]
							];

							$postsMONA = new WP_Query( $argsPost );  
						?>
						<section class="sec-blogl panel" data-color="<?php echo $color; ?>">
							<div class="blogl">
								<div class="blogl-wrap">
									<div class="blogl-flex">
										<div class="blogl-left">
											<div class="blogl-top" data-aos="fade-right">
												<?php 
												$parent = get_taxonomy_term_root( $detail );
												if( !empty( $parent ) ){ ?>
												<div class="blogl-tag">
													<a href="<?php echo get_term_link( $parent ); ?>">
														<?php echo $parent->name; ?>
													</a>
												</div>
												<?php } ?>
												<div class="blogl-box">
													<div class="blogl-gr">
														<span class="icon">
															<img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-word.png" alt="">
														</span>
														<span class="text">
															<?php echo ( ( $detail->count < 10 ) ? '0' . $detail->count : $detail->count ) . __(' bài đọc', 'monamedia'); ?>
														</span>
													</div>
													<?php $overview_readtime = $detail->count * 5; ?>
													<div class="blogl-gr">
														<span class="icon">
															<img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-arm.png" alt="">
														</span>
														<span class="text">
															<?php echo !empty( $overview_readtime ) ? $overview_readtime . __(' phút đọc','monamedia') : ''; ?>
														</span>
													</div>
												</div>
											</div>
											<div class="blogl-content">
												<div class="blogl-mid">
													<h2 class="title" data-aos="fade-left" data-aos-delay="400">
														<?php echo ( !empty( $title ) ) ? $title : $detail->name; ?>
													</h2>
													<?php if( $postsMONA->have_posts() ){ ?>
													<div class="blogl-py">
														<ul class="blogl-mid-list">
															<?php while( $postsMONA->have_posts() ){
																$postsMONA->the_post();
																global $post; ?>
															<li class="blogl-mid-item">
																<a href="<?php echo get_the_permalink( $post ); ?>" class="text">
																	<?php echo get_the_title( $post ); ?>
																</a>
															</li>
															<?php } wp_reset_query(); ?>
														</ul>
													</div>
													<?php } ?>
												</div>
												<?php if( $postsMONA->have_posts() && $postsMONA->post_count < $detail->count  ){ ?>
												<div class="blogl-bot">
													<a href="<?php echo get_term_link( $detail ); ?>" class="btn-three">
														<span class="text"><?php echo __('Xem thêm', 'monamedia'); ?></span>
														<span class="icon">
															<img loading="lazy" src="<?php echo get_site_url() ?>/template/assets/images/blog/icon-btnthree.png" alt="">
														</span>
													</a>
												</div>
												<?php } ?>
											</div>
										</div>
										<div class="blogl-right">
											<div class="blogl-img">
												<div class="inner">
													<?php $mona_taxonomy_thumbnail = get_field('mona_taxonomy_thumbnail', $detail);
													if( empty( $mona_taxonomy_thumbnail ) ){ ?>
														<img src="<?php echo get_template_directory_uri() ?>/public/helpers/images/global-thumbnail-mona.png" alt="">
													<?php }else{ ?> 
														<?php echo wp_get_attachment_image($mona_taxonomy_thumbnail, 'thumbnail-taxonomy'); ?>	
													<?php } ?>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

						</section>
					<?php } } ?>
				</div>
				<div class="sec-blogl-block-img">
					<div class="blogl-img">
						<div class="inner">
							<?php 
							foreach ($taxonomy_others as $key => $other) {
								$detail = $other['taxonomy_detail'];
								if( !empty( $detail ) ){

								$mona_taxonomy_thumbnail = get_field('mona_taxonomy_thumbnail', $detail);
								if( empty( $mona_taxonomy_thumbnail ) ){ ?>
									<img src="<?php echo get_template_directory_uri() ?>/public/helpers/images/global-thumbnail-mona.png" alt="">
								<?php }else{ ?> 
									<?php echo wp_get_attachment_image($mona_taxonomy_thumbnail, 'thumbnail-taxonomy'); ?>	
								<?php } ?>

							<?php } } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<?php } ?>

	<?php 
	$post_type = 'post';
	$posts_per_page = 12;
	$paged = max( 1, get_query_var('paged') );
	$offset = ( $paged - 1 ) * $posts_per_page;
	$argsPost = [
		'post_type' => $post_type,
		'post_status' => 'publish',
		'posts_per_page' => $posts_per_page,
		'paged' => $paged,
		'offset' => $offset,
		'meta_query' => [
			'relation' => 'AND',
		],
		'tax_query' => [
			'relation'=>'AND',
		]
	];

	$postsMONA = new WP_Query( $argsPost );  
	?>
	<section class="sec-blogr">
		<div class="container">
			<div class="blogr">
				<h2 class="title"><?php echo __('Bài viết gần đây', 'monamedia'); ?></h2>
				<div class="blogr-wrap">
					<div class="blogr-flex">
						<div class="blogr-left">
							<?php if( $postsMONA->have_posts() ){ ?>
							<form id="frmPostAjax">
								<div class="blogr-list is-loading-btn" id="monaPostList">
									<?php while( $postsMONA->have_posts() ){
										$postsMONA->the_post();
										?>
									<div class="blogr-item col-4">
										<?php 
										/**
										 * GET TEMPLATE PART
										 * partials
										 * blog box
										 */
										$slug = '/partials/loop/box';
										$name = 'blog';
										echo get_template_part($slug, $name);
										?>
									</div>
									<?php } wp_reset_query(); ?>
									<div class="pagination-products-ajax col-12 override">
										<?php mona_pagination_links_ajax( $postsMONA ); ?>
									</div>
								</div>
								<input type="hidden" name="post_type" value="<?php echo $post_type ?>" />
								<input type="hidden" name="posts_per_page" value="<?php echo $posts_per_page ?>" />
								<input type="hidden" name="paged" value="<?php echo $paged ?>" />
							</form>
							<?php }else{ ?>
								
							<div class="mona-mess-empty">
								<p><?php echo __( 'Nội dung đang được cập nhật', 'monamedia' ) ?></p>
							</div>

							<?php } ?>
						</div>
						<div class="blogr-right">
							<div class="blogr-pos">
								
								<?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar_blog')) : ?>
								<?php endif; ?>
								
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	
	<?php
	/**
	 * GET TEMPLATE PART
	 * contact section
	 *  */ 
	$slug = '/partials/global/contact';
	$name = '';
	echo get_template_part($slug, $name);
	?>

	<?php
	/**
	 * GET TEMPLATE PART
	 * categories menu section
	 *  */ 
	$slug = '/partials/global/categories';
	$name = 'menu';
	echo get_template_part($slug, $name);
	?>
	
	<?php
	/**
	 * GET TEMPLATE PART
	 * outstanding information section
	 *  */ 
	$slug = '/partials/global/outstanding';
	$name = 'information';
	echo get_template_part($slug, $name);
	?>
</main>

<?php
get_footer();